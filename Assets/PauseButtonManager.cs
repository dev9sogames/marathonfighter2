﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseButtonManager : MonoBehaviour {
	public Button RetryButton;
	public Button GiveupButton;
	public Button CancelButton;
	public Text RetryText;
	public Text GiveupText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
