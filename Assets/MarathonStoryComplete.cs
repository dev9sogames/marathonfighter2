﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MarathonStoryComplete : MonoBehaviour {
	public Button CancelButton;
	public Button NextButton;

	// Use this for initialization
	void Start () {
		if (AllGameDefinition.gameMode != 1 || AllGameDefinition.StoryStageType == 5) {
			CancelButton.gameObject.SetActive (false);
			NextButton.gameObject.SetActive (false);
			StartCoroutine (GoBackDifficultyCanvas ());
		} else {
			CancelButton.gameObject.SetActive (true);
			NextButton.gameObject.SetActive (true);
			MoveButtonList ();
		}
		MoveTextList ();
		MoveImageList ();
	}

	IEnumerator GoBackDifficultyCanvas () {
		yield return new WaitForSeconds (2);
		SceneManager.LoadScene ("Difficulty");
	}

	public void GoNextStage () {
		int currentStageType = AllGameDefinition.StoryStageType;
		int currentCourtType = AllGameDefinition.StoryStageCourt;

		if (currentCourtType == 3) {
			AllGameDefinition.StoryStageType = (currentStageType + 1);
			AllGameDefinition.StoryStageCourt = 1;
		} else {
			AllGameDefinition.StoryStageCourt = (currentCourtType + 1);
		}

		AllGameDefinition.CharacterType = 1; // hero

		AllGameDefinition.gameMode = 1;
		SceneManager.LoadScene("Loading");
	}

	public void GoDifficulty () {
		SceneManager.LoadScene ("Difficulty");
	}

	public Button[] buttonList;
	public Text[] textList;
	public Image[] imageList;

	public void MoveButtonList () {
		foreach (Button button in buttonList) {
			button.GetComponent<GuiAnimation> ().MoveStart ();
		}
	}

	public void MoveTextList () {
		foreach (Text text in textList) {
			text.GetComponent<GuiAnimation> ().MoveStart ();
		}
	}

	public void MoveImageList () {
		foreach (Image image in imageList) {
			image.GetComponent<GuiAnimation> ().MoveStart ();
		}
	}

	public void SetDefaultPosForButton () {
		foreach (Button button in buttonList) {
			button.GetComponent<GuiAnimation> ().SetDefalutPosition ();
		}
	}

	public void SetDefaultPosForText () {
		foreach (Text text in textList) {
			text.GetComponent<GuiAnimation> ().SetDefalutPosition ();
		}
	}

	public void SetDefaultPosForImage () {
		foreach (Image image in imageList) {
			image.GetComponent<GuiAnimation> ().SetDefalutPosition ();
		}
	}
}
