﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialFieldController : MonoBehaviour {

	public int fieldType; //1:1jump, 2:1jump success, 3:2jump, 4:2jump success, 

	GameObject gameController;
	public MarathonPlayer player;

	bool isTutorialStart;

	// Use this for initialization
	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (isTutorialStart) {
			return;
		}

		if (other.tag == "Player") {
			isTutorialStart = true;
			switch (fieldType) {
			case 1:
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("FlashJumpButton", 1);
				break;
			case 2:
				gameController.SendMessage ("JumpSuccess");
				break;
			case 3:
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("FlashJumpButton", 2);
				break;
			case 4:
				gameController.SendMessage ("JumpSuccess");
				break;
			case 5:
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("FlashJumpButton", 3);
				break;
			case 6:
				gameController.SendMessage ("JumpSuccess");
				break;
			case 7: // normalAttack enemy start
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("PlayerAttackButton", fieldType);
				StartCoroutine (CreateEnemy (other, fieldType));
				break;
			case 8: // hadouken enemy start
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("PlayerAttackButton", fieldType);
				StartCoroutine (CreateEnemy (other, fieldType));
				break;
			case 9: // hadouken jump enemy start
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("PlayerAttackButton", fieldType);
				StartCoroutine (CreateEnemy (other, fieldType));
				break;
			case 10: // yogaFire nurikabe start
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("PlayerAttackButton", fieldType);
				StartCoroutine (CreateEnemy (other, fieldType));
				break;
			case 11: // upper enemy start
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("PlayerAttackButton", fieldType);
				StartCoroutine (CreateEnemy (other, fieldType));
				break;
			case 12: // kick enemy start
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("PlayerAttackButton", fieldType);
				StartCoroutine (CreateEnemy (other, fieldType));
				break;
			case 13: // rolling enemy start
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("PlayerAttackButton", fieldType);
				StartCoroutine (CreateEnemy (other, fieldType));
				break;
			case 14: // fall out hammer
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("PlayerAttackButton", fieldType);
				StartCoroutine (CreateEnemy (other, fieldType));
				break;
			case 15: // boss battle
				gameController.SendMessage ("_stopField");
				gameController.SendMessage ("PlayerAttackButton", fieldType);
				StartCoroutine (CreateEnemy (other, fieldType));
				break;
			}
		}
	}

	IEnumerator CreateEnemy (Collider2D other, int fieldType) {
		yield return new WaitForSeconds (2);
		other.GetComponent<MarathonPlayer> ().CreateTutorialEnemy (fieldType);
		if (fieldType == 15) {
			other.GetComponent<MarathonPlayer> ().bossBattle ();
			other.GetComponent<MarathonPlayer> ().SetCameraStatus (true);
		}
	}
}
