﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GachaButton : MonoBehaviour {
	public GameObject gachaNurikabe;

	public void GachaCanvasBack () {
		Destroy (gachaNurikabe.gameObject);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load ("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);
	}

	public void GachaRetry () {
		GameObject gameController = GameObject.FindGameObjectWithTag ("GachaGameController");
		gameController.SendMessage ("InitializeGacha");

		GameObject egg = GameObject.FindGameObjectWithTag ("GachaEgg");
		if (egg != null) {
			egg.GetComponent<GachaEgg> ().StopEvent ();
		}

		GameObject explosion = GameObject.FindGameObjectWithTag ("Explosion");
		if (explosion != null) {
			Destroy (explosion.gameObject);
		}

		Destroy (gachaNurikabe.gameObject);

		GameObject Gacha = ResourceCache.Load ("gachaNurikabe");
		Instantiate (Gacha, new Vector3 (0, 0, 0), Quaternion.identity);
	}
}
