﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class CharacterMultiSelectController : MonoBehaviour {
	public GameObject StageSelectCanvas;
//	public GameObject[] buttonList;

	public int stageType;
	public int courtType;

	public int beforeStageType;
	public int beforeCourtType;

	public Text totalPointText;

	public bool isHardMode;
	public bool isButton;

	public Button rightButton;
	public Button leftButton;

	public GameObject easyModeCanvas;
	public GameObject hardModeCanvas;

	public Text modeText;

	// Use this for initialization
	void Start () {
		if (isButton) {
			easyModeCanvas.SetActive (true);
			hardModeCanvas.SetActive (false);
			rightButton.GetComponent<Button>().interactable = true;
			leftButton.GetComponent<Button>().interactable = false;
			modeText.text = "HARD MODE";
			return;
		}
		if (totalPointText != null) {
			totalPointText.text = PlayerPrefs.GetInt ("totalPoint", 0).ToString ();
			return;
		}

//		if (isHardMode) {
//			AllGameDefinition.isHardMode = true;
//		} else {
//			AllGameDefinition.isHardMode = false;
//		}

		_setStartImage ();
		_setButtonText ();
		_setKeyImage ();
//		totalPointText.text = PlayerPrefs.GetInt ("totalPoint", 0).ToString ();

	}

	public void GoRightCanvas () {
		easyModeCanvas.SetActive (false);
		hardModeCanvas.SetActive (true);
		rightButton.GetComponent<Button>().interactable = false;
		leftButton.GetComponent<Button>().interactable = true;
		modeText.text = "HELL MODE";
	}
	public void GoLeftCanvas () {
		easyModeCanvas.SetActive (true);
		hardModeCanvas.SetActive (false);
		rightButton.GetComponent<Button>().interactable = true;
		leftButton.GetComponent<Button>().interactable = false;
		modeText.text = "HARD MODE";
	}
	public void BackDifficultyCanvas () {
		Destroy (StageSelectCanvas.gameObject);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);
	}

	public void GoToCharacterSelect () {
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Select/CharaSelectCanvas");
		Instantiate (prefab, position, transform.rotation);

		Destroy (this.gameObject);
	}

	void _setButtonText () {
		string stageName = stageType.ToString () + " - " + courtType.ToString ();

		string key = "";

		if (isHardMode) {
			key = stageType.ToString () + "-" + courtType.ToString () + "-highestPoint-hard";
		} else {
			key = stageType.ToString () + "-" + courtType.ToString () + "-highestPoint";
		}
		int highestPoint = PlayerPrefs.GetInt (key, 0);

		transform.Find ("text").gameObject.GetComponent<Text> ().text = stageName;
		transform.Find ("textPoint").gameObject.GetComponent<Text> ().text = "BEST : " + highestPoint;

		/*
		int stageCount = 1;
		int courtCount = 1;

		foreach (GameObject button in buttonList) {
			string stageName = stageCount.ToString () + " - " + courtCount.ToString ();

			string key = stageCount.ToString() + "-" + courtCount.ToString() + "-highestPoint";
			int highestPoint = PlayerPrefs.GetInt (key, 0);

			button.transform.FindChild ("text").gameObject.GetComponent<Text> ().text = stageName;
			button.transform.FindChild ("textPoint").gameObject.GetComponent<Text> ().text = "BEST : " + highestPoint;

			courtCount ++;
			if (courtCount > 3) {
				stageCount++;
				courtCount = 1;
			}
		}
		*/
	}
	/**
	 * create star point image
	 * 
	 **/
	void _setStartImage () {
		string keyName = "";
		if (isHardMode) {
			keyName = stageType.ToString () + "-" + courtType.ToString () + "-starCount-hard";
		} else {
			keyName = stageType.ToString () + "-" + courtType.ToString () + "-starCount";
		}
		int bonusPoint = PlayerPrefs.GetInt (keyName, 0);

		switch (bonusPoint) {
		case 0:
			transform.Find ("noStar").gameObject.SetActive (true);
			break;
		case 1:
			transform.Find ("oneStar").gameObject.SetActive (true);
			break;
		case 2:
			transform.Find ("twoStar").gameObject.SetActive (true);
			break;
		case 3:
			transform.Find ("threeStar").gameObject.SetActive (true);
			break;
		default:
			transform.Find ("noStar").gameObject.SetActive (true);
			break;
		}

		/*
		for (int i = 1; i <= 3; i++) {
			if (i == 1 || i == 4) {
				posX = -281;
			} else if (i == 2) {
				posX = 0;
			} else if (i == 3) {
				posX = 281;
			}

			for (int m = 1; m <= 4; m++) { // vertical
				string keyName = m.ToString() + "-" + i.ToString() + "-starCount";
				if (i == 1) {
					keyNameBefore = (m - 1).ToString () + "-3-starCount";
				} else {
					keyNameBefore = m.ToString () + "-" + (i - 1).ToString () + "-starCount";
				}
				if (i == 1 && m == 1) { // horizontal
					keyNameBefore = "1-1-starCount";
				}
				//				Debug.Log (keyName);
				//				Debug.Log (keyNameBefore);
				int bonusPointBefore = PlayerPrefs.GetInt (keyNameBefore, 0);
				int bonusPoint = PlayerPrefs.GetInt (keyName, 0);

				pos = new Vector3 (posX, 71 - (91 * (m -1)), 0);

				Vector3 keyPos;
				keyPos = new Vector3 (posX, 71 - (91 * (m -1) -20), 0);
				GameObject image;
				GameObject keyImage;

				if (bonusPointBefore == 0) {
					keyImage = Instantiate (key, pos, Quaternion.identity);
					keyImage.transform.SetParent (StageSelectCanvas.transform, false);
					keyImage.transform.localPosition = pos;
					keyImage.transform.localScale = new Vector3 (1, 1, 1);
				}
				switch (bonusPointBefore) {
				case 1:
					image = Instantiate (star1, pos, Quaternion.identity);
					break;
				case 2:
					image = Instantiate (star2, pos, Quaternion.identity);
					break;
				case 3:
					image = Instantiate (star3, pos, Quaternion.identity);
					break;
				default:
					image = null;
					break;
				}
				if (image != null) {
					image.transform.SetParent (StageSelectCanvas.transform, false);
					image.transform.localPosition = pos;
					image.transform.localScale = new Vector3 (1, 1, 1);
				}
			}
		}
		*/
	}

	void _setKeyImage () {
		string keyName = "";
		int bonusPoint = 0;

		if (stageType == 1 && courtType == 1) {
			if (isHardMode) {
				keyName = "5-1-starCount";
				bonusPoint = PlayerPrefs.GetInt (keyName, 0);
				if (bonusPoint > 0) {
					transform.Find ("key").gameObject.SetActive (false);
				} else {
					this.GetComponent<Button> ().interactable = false;
					transform.Find ("key").gameObject.SetActive (true);
				}
			} else {
				transform.Find ("key").gameObject.SetActive (false);
			}
			return;
		}

		if (isHardMode) {
			keyName = beforeStageType.ToString () + "-" + beforeCourtType.ToString () + "-starCount-hard";
		} else {
			keyName = beforeStageType.ToString () + "-" + beforeCourtType.ToString () + "-starCount";
		}
		bonusPoint = PlayerPrefs.GetInt (keyName, 0);

		if (bonusPoint > 0) {
			transform.Find ("key").gameObject.SetActive (false);
		} else {
			this.GetComponent<Button> ().interactable = false;
			transform.Find ("key").gameObject.SetActive (true);
		}
	}
}
