﻿// GUI Animator FREE
// Version: 1.1.5
// Compatilble: Unity 5.5.1 or higher, see more info in Readme.txt file.
//
// Developer:							Gold Experience Team (https://www.assetstore.unity3d.com/en/#!/search/page=1/sortby=popularity/query=publisher:4162)
//
// Unity Asset Store:					https://www.assetstore.unity3d.com/en/#!/content/58843
// See Full version:					https://www.assetstore.unity3d.com/en/#!/content/28709
//
// Please direct any bugs/comments/suggestions to geteamdev@gmail.com

//#region Namespaces

using UnityEngine;
using System.Collections;

//#endregion // Namespaces

// ######################################################################
// GA_FREE_Demo07 class
// - Animates all GUIAnimFREE elements in the scene.
// - Responds to user mouse click or tap on buttons.
//
// Note this class is attached with "-SceneController-" object in "GA FREE - Demo07 (960x600px)" scene.
// ######################################################################

public class GuiAnimController : MonoBehaviour
{

	// ########################################
	// Variables
	// ########################################

//	#region Variables

//	// Canvas
	public Canvas m_Canvas;
//
//	// GUIAnimFREE object of dialogs
//	public GUIAnimFREE m_Dialog;
//	public GUIAnimFREE m_DialogButtons;
//
//	// GUIAnimFREE objects of buttons
//	public GUIAnimFREE m_Button1;
	public GUIAnimFREE RetryButton;
	public GUIAnimFREE RetryText;
	public GUIAnimFREE GiveupButton;
	public GUIAnimFREE GiveupText;
	public GUIAnimFREE CancelButton;
//	public GUIAnimFREE m_Button3;
//	public GUIAnimFREE m_Button4;

//	#endregion // Variables

	// ########################################
	// MonoBehaviour Functions
	// http://docs.unity3d.com/ScriptReference/MonoBehaviour.html
	// ########################################

//	#region MonoBehaviour

	// Awake is called when the script instance is being loaded.
	// http://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html
	void Awake ()
	{
		if(enabled)
		{
			// Set GUIAnimSystemFREE.Instance.m_AutoAnimation to false in Awake() will let you control all GUI Animator elements in the scene via scripts.
			GUIAnimSystemFREE.Instance.m_AutoAnimation = false;
		}
	}

	// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	// http://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html
	void Start ()
	{
//		GUIAnimSystemFREE.Instance.EnableButton(this.transform, false);

		GUIAnimSystemFREE.Instance.SetGraphicRaycasterEnable(m_Canvas, false);
		StartCoroutine (ButtonAppear ());
	}

	public void ButtonAnimation () {
		Debug.Log ("ButtonAnimation");
		StartCoroutine (ButtonAppear ());
	}

	IEnumerator ButtonAppear () {
		Debug.Log ("ButtonAppear coroutine");
		yield return new WaitForSeconds(1);

		// Enable all buttons
		RetryButton.MoveIn(GUIAnimSystemFREE.eGUIMove.SelfAndChildren);
		RetryText.MoveIn(GUIAnimSystemFREE.eGUIMove.SelfAndChildren);
		GiveupButton.MoveIn(GUIAnimSystemFREE.eGUIMove.SelfAndChildren);
		GiveupText.MoveIn(GUIAnimSystemFREE.eGUIMove.SelfAndChildren);
		CancelButton.MoveIn(GUIAnimSystemFREE.eGUIMove.SelfAndChildren);
	}
}
