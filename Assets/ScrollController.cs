﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollController : MonoBehaviour {
	public Canvas RankingCanvas;
	public GameObject loadingView;
	public GameObject errorView;
	public RectTransform prefab;
	public int RankingType; // 1:story, 2:endless, 3:marathon, 4:marathonHard
	Dictionary<string, int> rankData;

	int rankDataStatus = 0; //0:not, 1:success, 2:error

	void Start () {
		rankDataStatus = 0;
		loadingView.SetActive (true);
		switch (RankingType) {
		case 1:
			rankData = RankingManager.Instance.getStoryScoreRanking ();
			break;
		case 2:
			rankData = RankingManager.Instance.getEndlessScoreRanking ();
			break;
		case 3:
			rankData = RankingManager.Instance.getMarathonScoreRanking (1); //easy
			break;
		case 4:
			rankData = RankingManager.Instance.getMarathonScoreRanking (2); //hard
			break;
		}
		StartCoroutine (waitRankingData ());
	}

	IEnumerator waitRankingData () {
		while (rankDataStatus == 0) {
			yield return new WaitForSeconds (1);
			if (RankingManager.Instance.HasRankData () == 1) {
				_setRankingData ();
				loadingView.SetActive (false);
				break;
			} else if (RankingManager.Instance.HasRankData () == 2) {
				loadingView.SetActive (false);
				errorView.SetActive (true);
				StartCoroutine (GoBackDifficultyCanvas ());
			}
		}
	}

	IEnumerator GoBackDifficultyCanvas () {
		yield return new WaitForSeconds (2);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);

		Destroy (RankingCanvas.gameObject);
	}
	void _setRankingData () {
		int count = 1;

		foreach (KeyValuePair<string, int> pair in rankData) {
			RectTransform item = GameObject.Instantiate(prefab) as RectTransform;
			item.SetParent(transform, false);

			Text RankText = item.transform.Find ("RankText").GetComponent<Text> ();
			RankText.text = count.ToString();

			Text NameText = item.transform.Find ("NameText").GetComponent<Text> ();
			NameText.text = pair.Key;

			Text ScoreText = item.transform.Find ("ScoreText").GetComponent<Text> ();
			ScoreText.text = pair.Value.ToString ();

			count++;
		}
	}
}
