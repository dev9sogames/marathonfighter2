﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using UnityEngine.UI;

public class MarathonTitleController : MonoBehaviour {
	public Image heroImage;
	public Image nurikabeImage;
	public Image princessImage;
	public Button StartButton;

	public static bool AwakeFlag;

	void Awake () {
		// InvokeRepeating("関数名",初回呼出までの遅延秒数,次回呼出までの遅延秒数)
//		if (!AwakeFlag) {
//			InvokeRepeating ("CreatePlayer", 1, 5);
//			AwakeFlag = true;
//		}
	}

	void Start () {
		StartCoroutine (HeroStart ());
		StartCoroutine (NurikabeStart ());

//		PlayerPrefs.DeleteAll ();
		string name = PlayerPrefs.GetString ("nameKey", null);
//		if (name.Length < 1) {
//			Vector3 position = new Vector3 (960, 540, 0);
//			GameObject prefab = (GameObject)Resources.Load("DialogCanvasForTitle");
//			Instantiate (prefab, position, transform.rotation);
//
//			return;
//		}

		string pw = PlayerPrefs.GetString ("passwordKey", null);
		if (pw == null || pw == "") {
			Debug.Log ("passWord is null.");
			Guid passWord = System.Guid.NewGuid ();
			pw = passWord.ToString ();
			Debug.Log ("guid " + passWord);
			PlayerPrefs.SetString ("passwordKey", pw);
		}

		if (name.Length >= 1) {
			UserAuth.Instance.login (name, pw);
		}
	}

	void CreatePlayer () {
		GameObject prefab = (GameObject)Resources.Load ("hero");
		Instantiate (prefab , new Vector2 (30, 9) , Quaternion.identity);
	}

	IEnumerator HeroStart () {
		yield return new WaitForSeconds (0.5f);
		heroImage.GetComponent<GuiAnimation> ().MoveStart ();
		StartButton.GetComponent<GuiAnimation> ().MoveStart ();
	}

	IEnumerator NurikabeStart () {
		yield return new WaitForSeconds (1);
		nurikabeImage.GetComponent<GuiAnimation> ().MoveStart ();
		princessImage.GetComponent<GuiAnimation> ().MoveStart ();
	}
}
