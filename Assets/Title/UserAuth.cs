﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NCMB;

public class UserAuth : SingletonMonoBehaviour<UserAuth>{
	UserAuth instance = null;

	int changePwStatus = 0; // 0:yet, 1:success, 2:duplicate error, 3:other error

	public void login (string name, string pw, bool relogin = false) {
//		Debug.Log ("Login");
		NCMBUser.LogInAsync (name, pw, (NCMBException e) => {
			if (e != null) {
				Debug.Log ("UserAuth login error : " + e.ToString());
				if (!relogin) {
					logOut (name, pw);
//					NCMBUser.LogOutAsync ();

					signUp (name, pw);
				}
//				if (e.ToString ().Contains("Authentication error")) {
//					signUp (name, pw);
//				}
			}
		});
	}

	public void logOut (string name, string pw) {
		NCMBUser.LogOutAsync ((NCMBException e) => {
			if (e == null) {
				login (name, pw, true);
			}
		});
	}

	public void signUp (string name, string pw) {
		Debug.Log ("signUp");
		NCMBUser user = new NCMBUser();
		user.UserName = name;
		user.Password = pw;
		user.SignUpAsync((NCMBException e) => {
			if (e != null) {
				Debug.Log ("UserAuth signUp error : " + e);
			}
		});
	}

	public void changeName (string name, string pw) {
		changePwStatus = 0;

//		NCMBUser user = new NCMBUser ();
		NCMBUser user = NCMBUser.CurrentUser;
		user.UserName = name;
//		user.Password = pw;
		user.SignUpAsync ((NCMBException e) => {
			if (e != null) {
				Debug.Log ("UserAuth signUp error : " + e);
				if (e.ErrorCode == "E409001") { // duplicate
					changePwStatus = 2;
				} else {
					changePwStatus = 3;
				}
			} else {
				changePwStatus = 1;
			}
		});

		changeScoreName ();
	}

	public void changeNameForTitle (string name, string pw) {
		changePwStatus = 0;

		NCMBUser user = new NCMBUser ();
//		NCMBUser user = NCMBUser.CurrentUser;
		user.UserName = name;
		user.Password = pw;
		user.SignUpAsync ((NCMBException e) => {
			if (e != null) {
				Debug.Log ("UserAuth signUp error : " + e);
				if (e.ErrorCode == "E409001") { // duplicate
					changePwStatus = 2;
				} else {
					changePwStatus = 3;
				}
			} else {
				changePwStatus = 1;
			}
		});
	}

	void changeScoreName () {
	}

	public int getChangePwStatus () {
		return changePwStatus;
	}
}
