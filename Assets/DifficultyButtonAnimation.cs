﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DifficultyButtonAnimation : MonoBehaviour {
	public Button[] buttonList1;
	public Button[] buttonList2;
	public Button[] buttonList3;

	// Use this for initialization
	void Start () {
		StartCoroutine (MoveButton (buttonList1, 0.1f));
		StartCoroutine (MoveButton (buttonList2, 0.3f));
		StartCoroutine (MoveButton (buttonList3, 0.5f));
	}

	IEnumerator MoveButton (Button[] buttonList, float delay) {
		yield return new WaitForSeconds (delay);

		foreach (Button button in buttonList) {
			button.GetComponent<GuiAnimation> ().MoveStart ();
		}
	}
}
