﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonSprintController : MonoBehaviour {
	Vector3 pos;

	// Use this for initialization
	void Start () {
		pos = transform.position;
	}
	
	void FixedUpdate () {
		if (transform.position.x > 5 || transform.position.x < -5) {
			transform.position = pos;
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player") {
			other.GetComponent<MarathonPlayer> ().ResetCloud ();
		}
	}
}
