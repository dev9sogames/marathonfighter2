﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonTutorialEnemyTrigger : MonoBehaviour {
	GameObject gameController;
	MarathonPlayer targetPlayer;

	Rigidbody2D rdbg;
	/** enmeyNo definition
	 1:enmey
	 2:nurikabe
	 3:enemy Jump
	 4:enemy come 1
	 **/
	public int enemyNo;

	float posX;
	float posY;

	bool isHit;

	// Use this for initialization
	void Start () {
		rdbg = GetComponent<Rigidbody2D> ();
		posX = transform.localPosition.x;
		posY = transform.localPosition.y;
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x > 50 || transform.position.y > 50) {
			Destroy (this.gameObject);
		}

		switch (enemyNo) {
		case 5:
		case 8:
			if (!isHit) {
				posY -= Time.deltaTime * 2;
				transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
			}
			break;
		case 6:
			if (!isHit) {
				posX -= Time.deltaTime * 2;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, transform.localPosition.z);
			}
			break;
		}		
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (!GetComponent<Renderer> ().isVisible) {
			return;
		}

		if (isHit) {
			return;
		}

		switch (enemyNo) {
		case 1: // enemy very small
			if (other.tag == "PlayerNormalAttack") {
				isHit = true;
				gameController.SendMessage ("AttackSuccess", enemyNo);
				rdbg.AddForce (Vector2.up * 3000);
				StartCoroutine (Restart (0.5f));
			}
			break;
		case 2: // enemy small
			if (other.tag == "MarathonHadouken") {
				isHit = true;
				gameController.SendMessage ("AttackSuccess", enemyNo);
				rdbg.AddForce (Vector2.up * 3000);
				StartCoroutine (Restart (0.5f));
			}
			break;
		case 3: //jump enemy small
			if (other.tag == "MarathonHadouken") {
				isHit = true;
				gameController.SendMessage ("AttackSuccess", enemyNo);
				rdbg.AddForce (Vector2.up * 3000);
				StartCoroutine (Restart (0.5f));
			}
			break;
		case 4: //nurikabe
			if (other.tag == "MarathonYogaFire") {
				isHit = true;
				gameController.SendMessage ("AttackSuccess", enemyNo);
				rdbg.AddForce (Vector2.up * 3000);
				StartCoroutine (Restart (0.5f));
			}
			break;
		case 5: //fall out enemy
			if (other.tag == "PlayerUpper") {
				isHit = true;
				gameController.SendMessage ("AttackSuccess", enemyNo);
				rdbg.AddForce (Vector2.up * 3000);
				StartCoroutine (Restart (0.5f));
			} else if (other.tag == "Player") {
				isHit = true;
				Destroy (this.gameObject);
				other.GetComponent<MarathonPlayer> ().HitTutorialEnemy (enemyNo);
			}
			break;
		case 6: // move left nurikabe
			if (other.tag == "PlayerKick") {
				isHit = true;
				gameController.SendMessage ("AttackSuccess", enemyNo);
				rdbg.AddForce (Vector2.right * 3000);
				StartCoroutine (Restart (0.5f));
			} else if (other.tag == "Player") {
				isHit = true;
				Destroy (this.gameObject);
				other.GetComponent<MarathonPlayer> ().HitTutorialEnemy (enemyNo);
			}
			break;
		case 7: // rolling enemy uchuujin
			if (other.tag == "RollingAttack") {
				isHit = true;
				gameController.SendMessage ("AttackSuccess", enemyNo);
				rdbg.AddForce (Vector2.right * 3000);
				StartCoroutine (Restart (0.5f));
			}
			break;
		case 8: // tornado defense hammer
			if (other.tag == "PlayerTornado") {
				isHit = true;
				gameController.SendMessage ("AttackSuccess", enemyNo);
				rdbg.AddForce (Vector2.up * 3000);
				StartCoroutine (Restart (0.5f));
			} else if (other.tag == "Player") {
				isHit = true;
				Destroy (this.gameObject);
				other.GetComponent<MarathonPlayer> ().HitTutorialEnemy (enemyNo);
			}
			break;
		}
	}
	IEnumerator Restart (float num) {
		yield return new WaitForSeconds (num);
		gameController.SendMessage ("_restartField");
	}

	public void StartTutorialEnemy () {
		switch (enemyNo) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 8:
			GetComponent<CircleCollider2D> ().isTrigger = true;
			break;
		case 6:
		case 7:
			GetComponent<BoxCollider2D> ().isTrigger = true;
			break;
		}
	}
}
