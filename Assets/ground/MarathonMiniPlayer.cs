﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonMiniPlayer : MonoBehaviour {
	bool isAlive;

	GameObject normalAttackOno;
	GameObject attackHadouken;
	GameObject attackYogaFire;

	// Use this for initialization
	void Start () {
//		isAlive = false;
		normalAttackOno = ResourceCache.Load ("Ground/ono");
		attackHadouken = ResourceCache.Load ("Ground/MarathonHadouken");
		attackYogaFire = ResourceCache.Load ("Ground/MarathonYogaFire");
//		StartCoroutine (_miniPlayerAttack ());
	}

	IEnumerator _miniPlayerAttack () {
		while (isAlive) {
			yield return new WaitForSeconds (2);
			Debug.Log ("Player mini attack");
			_attack ();
		}
	}

	void _attack () {
		switch (UnityEngine.Random.Range (1, 4)) {
		case 1:
			normalAttack ();
			break;
		case 2:
			hadouken ();
			break;
		case 3:
			yogaFire ();
			break;
		}
	}
	public void SetAliveFlag (bool flag) {
		Debug.Log ("set active flag");
		isAlive = flag;
		if (isAlive) {
			StartCoroutine (_miniPlayerAttack ());
		}
	}

	void normalAttack () {
		Instantiate (normalAttackOno, transform.position, Quaternion.identity);
	}
	void hadouken () {
		Instantiate (attackHadouken, transform.position, Quaternion.identity);
	}
	void yogaFire () {
		Instantiate (attackYogaFire, transform.position, Quaternion.identity);
	}
}
