﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonBossBodyAttack : MonoBehaviour {
	public int attackNo;
	/*
	 * 12:bossBodyAttack -> MarathonBossBodyAttack に移動
	 * 13:bossJumpKick -> MarathonBossBodyAttack に移動
	 * 14:bossSliding -> MarathonBossBodyAttack に移動
	 */

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player") {
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			if (attackNo == 12) { // boss body attack
				player.HitEnemy (95);
			} else if (attackNo == 13) { // boss jump kick
				player.HitEnemy (94);
			} else if (attackNo == 14) { // boss sliding
				player.HitEnemy (93);
			} else {
				player.HitEnemy (99);
			}
		}
	}
}
