﻿using UnityEngine;
using System.Collections;

public class MarathonLevelUpCharacter : MonoBehaviour {
	int num;

	enum Direction {
		up,
		down
	};
	Direction dire;

	float posY;
	float posX;

	float speed;

	// Use this for initialization
	void Start () {
		num = UnityEngine.Random.Range (1, 4);
		posY = transform.localPosition.y;
		posX = transform.localPosition.x;
		dire = Direction.up;
		switch (num) {
		case 1:
			speed = 150;
			break;
		case 2:
			speed = 210;
			break;
		case 3:
			speed = 300;
			break;
		default:
			speed = 90;
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.localPosition.y >= -10) {
			dire = Direction.down;
		}
		if (transform.localPosition.y <= -59) {
			dire = Direction.up;
		}
		if (dire == Direction.down) {
			posY -= Time.deltaTime * speed;
		} else {
			posY += Time.deltaTime * speed;
		}
		transform.localPosition = new Vector3 (posX, posY, transform.localPosition.z);

	}
}
