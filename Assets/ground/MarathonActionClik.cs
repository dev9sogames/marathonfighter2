﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MarathonActionClik : MonoBehaviour {
	public MarathonPlayer player;

	public Button TrainingStartButton;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void TrainingStart () {
		TrainingStartButton.gameObject.SetActive (false);
		player.TrainingStart ();
	}
	public void JumpPlayer () {
		player.JumpPlayer ();
	}
	public void ActionButtonClick () {
		player.Attack ();
//		player.ActionClick ();
	}

	public void RightDown() {
//		player.RightDown ();
		player.RightPushDown ();
	}
	public void RightUp() {
//		player.RightUp ();
		player.RightPushUp ();
	}
	public void LeftDown() {
//		player.LeftDown ();
		player.LeftPushDown();
	}
	public void LeftUp() {
//		player.LeftUp ();
		player.LeftPushUp ();
	}
	public void TopDown() {
//		player.TopDown ();
		player.UpPushDown ();
	}
	public void TopUp() {
//		player.TopUp ();
		player.UpPushUp();
	}
	public void BottomDown() {
//		player.BottomDown ();
		player.BottomPushDown ();
	}
	public void BottomUp() {
//		player.BottomUp ();
		player.BottomPushUp ();
	}
	/*
	public void TopRightDown() {
		player.TopRightDown ();
	}
	public void TopRightUp() {
		player.TopRightUp ();
	}
	public void TopLeftDown() {
		player.TopLeftDown ();
	}
	public void TopLeftUp() {
		player.TopLeftUp ();
	}
	public void BottomRightDown() {
		player.BottomRightDown ();
	}
	public void BottomRightUp() {
		player.BottomRightUp ();
	}
	public void BottomLeftDown() {
		player.BottomLeftDown ();
	}
	public void BottomLeftUp() {
		player.BottomLeftUp ();
	}
	*/

}
