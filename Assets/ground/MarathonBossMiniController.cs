﻿using UnityEngine;
using System.Collections;

public class MarathonBossMiniController : MonoBehaviour {
	const int BOSS_EASY_HP = 100;

	GameObject gameController;

	bool isAttack;

	bool isDead;
	bool isJumping;
	bool isJumpingKick;
	bool isFalling;
	bool isDisappear;
	bool isProtect;
	bool isBodyAttack;
	bool isKick;
	bool isSliding;
	bool isMutekiAttack;
	//	bool isMirror;
	//	bool mirrorEnabled;

	int hitPoint = 0;
	int bossHitPoint;

	bool isScrewDriver;

	int difficulty;

	float posX;
	float posY;

	GameObject fire;
	GameObject hammer;
	GameObject beef;
	GameObject beefDokuro;
	GameObject muteki;
	GameObject homing;
	GameObject UpAndHoming;
	GameObject Ice;
	GameObject Dragon;
	GameObject Thunder;
	GameObject Reppuuken;
	GameObject MiniNurikabe;

	MarathonPlayer player;

	enum Direction {
		up,
		down
	};
	Direction dire;

	Rigidbody2D rgbd;
	Animator animator;

	// Use this for initialization
	void Awake () {
		difficulty = 0;
		int currentDifficulty = AllGameDefinition.currentDifficulty;
		if (currentDifficulty > 3 && currentDifficulty <= 7) {
			difficulty = 1;
		} else if (currentDifficulty > 7 && currentDifficulty <= 12) {
			difficulty = 2;
		} else if (currentDifficulty > 12) {
			difficulty = 3;
		}
	}

	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
		rgbd = GetComponent<Rigidbody2D> ();
		setBossStatus ();

		posX = transform.localPosition.x;
		posY = transform.localPosition.y;

		fire = ResourceCache.Load ("Ground/EnemyHonooImage");
		hammer = ResourceCache.Load ("Ground/Enemyhammer");
		beef = ResourceCache.Load ("Ground/EnemyBeef");
		beefDokuro = ResourceCache.Load ("Ground/EnemyBeefBad");
		muteki = ResourceCache.Load ("Ground/EnemyMuteki");
		homing = ResourceCache.Load ("Ground/EnemyHomingImage");
		UpAndHoming = ResourceCache.Load ("Ground/EnemyUpAndHoming");
		Ice = ResourceCache.Load ("Ground/EnemyIceObj");
		Dragon = ResourceCache.Load ("Ground/EnemyDragon");
		Thunder = ResourceCache.Load ("Ground/EnemyThunder");
		Reppuuken = ResourceCache.Load ("Ground/EnemyReppuuken");

		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<MarathonPlayer> ();
	}

	void setBossStatus () {
		bossHitPoint = BOSS_EASY_HP;
		StartCoroutine (bossAttack ());
		StartCoroutine (normalAction ());
	}
	// Update is called once per frame
	void FixedUpdate () {
		if (isScrewDriver) {
			return;
		}

		float speed = _getSpeed ();
		// jump
		//		if (isJumping || !isAttack) {
		if (isJumping) {
			if (transform.localPosition.y >= 4.0f) {
				dire = Direction.down;
			}
			if (transform.localPosition.y <= -4.0f) {
				dire = Direction.up;
			}
			if (dire == Direction.down) {
				posY -= Time.deltaTime * speed;
			} else {
				posY += Time.deltaTime * speed;
			}
			transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
		}

		if (isJumpingKick) {
			if (transform.localPosition.y <= 5.0f) {
				posY += Time.deltaTime * speed * 1.5f;
				transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
			} else if (transform.localPosition.y > 5.0f) {
				isFalling = true;
			}
		}
		if (isFalling) {
			if (transform.localPosition.y >= -1.5f) {
				moveToPlayer ();
			} else if (transform.localPosition.y < -1.5f) {
				if (isFalling) {
					animator.SetBool ("kickDown", false);
				}
				isFalling = false;
				GetComponent<Rigidbody2D> ().velocity = new Vector2(0,0);
				transform.localPosition = new Vector3 (7.25f, -1.34f, 0);
			}
		}
		if (isMutekiAttack) {
			if (transform.localPosition.x < -3.0f) {
				return;
			}
			posX -= Time.deltaTime * speed * 4;
			transform.localPosition = new Vector3 (posX, transform.localPosition.y, transform.localPosition.z);
		}
		if (isDead) {
			posY -= Time.deltaTime * speed * 2;
			transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
		}
	}
	void moveToPlayer () {
		isJumpingKick = false;
		isFalling = true;
		Vector2 targetPos = player.transform.position;
		float x = targetPos.x;
		float y = targetPos.y;

		Vector2 direction = new Vector2 (x - transform.position.x, y - transform.position.y).normalized;
		GetComponent<Rigidbody2D> ().velocity = (direction * 7);
	}
	IEnumerator normalAction () {
		while (!isDead) {
			yield return new WaitForSeconds (1);
			_normalAttack ();
		}
	}
	IEnumerator bossAttack () {
		Debug.Log ("boss easy attack");
		while (!isDead) {
			//			yield return new WaitForSeconds (UnityEngine.Random.Range (1, 2));

			yield return new WaitForSeconds (UnityEngine.Random.Range (3, 10));
			_attack ();
		}
	}
	IEnumerator bossNormalAttack () {
		Debug.Log ("boss normal attack");
		while (!isDead) {
			int num = UnityEngine.Random.Range (2, 7);
			yield return new WaitForSeconds (num);
			for (int i = 1; i <= num; i++) {
				_attack ();
			}
		}
	}
	IEnumerator bossHardAttack () {
		Debug.Log ("boss hard attack");
		while (!isDead) {
			int num = UnityEngine.Random.Range (1, 5);
			yield return new WaitForSeconds (num);
			for (int i = 1; i <= num; i++) {
				_attack ();
			}
		}
	}
	IEnumerator bossVeryHardAttack () {
		Debug.Log ("boss very hard attack");
		while (!isDead) {
			int num = UnityEngine.Random.Range (1, 3);
			yield return new WaitForSeconds (num);
			for (int i = 1; i <= num; i++) {
				_attack ();
			}
		}
	}
	void _attack () {
		/*
		Instantiate (Ice, transform.position, Quaternion.identity);
		return;
		*/
		// TODO : 泥投げ、バナナ、地面から出てくる(蔵馬)、雷たくさん、ミニヌリカベ生成
		if (isDead || isScrewDriver) {
			return;
		}
		/*
		 * JumpAndKick ();
		 * JumpAndFire ();
		 * JumpAndHoming ();
		 * moveNurikabe ();
		 * Disappear ();
		 * Protect ();
		 * bodyAttack ();
		 * kick ()
		 * speedFire ()
		 * MultipleFire (3)
		 * Sliding ()
		 * MutekiAttack ()
		 * Instantiate (fire, transform.position, Quaternion.identity);
		 * Instantiate (beef, transform.position, Quaternion.identity);
		 * Instantiate (beefDokuro, transform.position, Quaternion.identity);
		 * Instantiate (muteki, transform.position, Quaternion.identity);
		 * Instantiate (hammer, transform.position, Quaternion.identity);
		 * Instantiate (UpAndHoming, transform.position, Quaternion.identity);
		 * Instantiate (Ice, transform.position, Quaternion.identity);
		 * Instantiate (Dragon, transform.position, Quaternion.identity);
		 * Instantiate (Thunder, new Vector3 (-1.2f, 14.6f, 0), Quaternion.identity);
		 * Instantiate (Reppuuken, new Vector3 (12.0f, -2.3f, 0), Quaternion.identity);
		 * 
		 */
		switch (UnityEngine.Random.Range (1, 17)) {
		case 1:
			Instantiate (fire, transform.position, Quaternion.identity);
			break;
		case 2:
			JumpAndFire ();
			isAttack = true;
			break;
		case 3:
			JumpAndHoming ();
			isAttack = true;
			break;
		case 4:
			Instantiate (fire, transform.position, Quaternion.identity);
			break;
		case 5:
			Instantiate (UpAndHoming, transform.position, Quaternion.identity);
			break;
		case 6:
			Instantiate (beef, transform.position, Quaternion.identity);
			break;
		case 7:
			Disappear ();
			isAttack = true;
			break;
		case 8:
			moveNurikabe ();
			Instantiate (fire, transform.position, Quaternion.identity);
			break;
		case 9:
			Instantiate (fire, transform.position, Quaternion.identity);
			break;
		case 10:
			Instantiate (beefDokuro, transform.position, Quaternion.identity);
			break;
		case 11:
			Instantiate (Dragon, transform.position, Quaternion.identity);
			break;
		case 12:
			Instantiate (Ice, transform.position, Quaternion.identity);
			break;
		case 13:
			Instantiate (muteki, transform.position, Quaternion.identity);
			break;
		case 14:
			Instantiate (Thunder, new Vector3 (-1.2f, 14.6f, 0), Quaternion.identity);
			break;
		case 15:
			Instantiate (hammer, transform.position, Quaternion.identity);
			break;
		case 16:
			Instantiate (Reppuuken, new Vector3 (12.0f, -2.3f, 0), Quaternion.identity);
			break;
		default:
			break;
		}
	}

	void MultipleFire (int num = 3) {
		StartCoroutine (MultipleFireStart (num));
	}
	IEnumerator MultipleFireStart (int num) {
		int count = 1;
		while (count <= num) {
			yield return new WaitForSeconds (0.2f);
			Instantiate (fire, transform.position, Quaternion.identity);
			count++;
		}
	}
	void speedFire () {
		GameObject speedFire = Instantiate (fire, transform.position, Quaternion.identity) as GameObject;
		speedFire.GetComponent<MarathonBossAttack> ().attackNo = 15;
	}

	void moveNurikabe () {
		transform.localPosition = new Vector3 (2.5f, transform.localPosition.y, transform.localPosition.z);
		StartCoroutine ("positionReverse");
	}

	void JumpAndFire () {
		isJumping = true;
		StartCoroutine (FinishJump ());
		StartCoroutine (FireAttack ());
	}
	void JumpAndHoming () {
		isJumping = true;
		StartCoroutine (FinishJump ());
		StartCoroutine (HomingAttack ());
	}

	public void FinishProtect () {
		isProtect = false;
	}
	void Disappear () {
		if (isDisappear) {
			return;
		}
		isDisappear = true;
		iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 5f, "onupdate", "SetValue"));
		GetComponent<CircleCollider2D> ().enabled = false;
		//		StartCoroutine (BossAppear ());
	}
	void SetValue(float alpha) {
		if (!isDisappear) {
			return;
		}
		//		Color32 color = new Color32 (64,43,43,alpha);
		if (alpha == 0.0f) {
			GetComponent<CircleCollider2D> ().enabled = true;
			gameObject.GetComponent<SpriteRenderer> ().color = new Color(64/255, 43/255, 43/255, 1);
			isDisappear = false;
			isAttack = false;

			return;
		}
		gameObject.GetComponent<SpriteRenderer> ().color = new Color(64/255, 43/255, 43/255, alpha);
	}

	void kick () {
		rgbd.AddForce (Vector2.up * 200);
		rgbd.AddForce (Vector2.left * 300);
		StartCoroutine (kickDown ());
	}

	void _normalAttack () {
		if (isMutekiAttack) {
			return;
		}

		if (transform.position.x < 1.0f) {
			MoveRight ();
			return;
		} else if (transform.position.x > 10.0f) {
			MoveLeft ();
			return;
		}

		switch (UnityEngine.Random.Range (1, 8)) {
		case 1:
			MoveLeft ();
			break;
		case 2:
			MoveRight ();
			break;
		case 3:
			MoveUp ();
			break;
		case 4:
			MoveLeft ();
			break;
		case 5:
			MoveRight ();
			break;
		case 6:
			MoveUp ();
			break;
		case 7:
			Instantiate (fire, transform.position, Quaternion.identity);
			break;
		default:
			break;
		}
	}
	void MoveLeft () {
		Vector2 velocity = rgbd.velocity;
		velocity.x = -5.0f;
		rgbd.velocity = velocity;
	}
	void MoveRight () {
		Vector2 velocity = rgbd.velocity;
		velocity.x = 5.0f;
		rgbd.velocity = velocity;
	}
	void MoveUp () {
		rgbd.AddForce (Vector2.up * 150);
	}
	IEnumerator kickDown () {
		yield return new WaitForSeconds (0.5f);
		if (transform.position.y > -3.2f) {
			isKick = true;
			GetComponent<Rigidbody2D>().velocity = Vector3.zero;
			GetComponent<Rigidbody2D>().AddForce (Vector2.down * 200);
		}
	}

	IEnumerator FireAttack () {
		while (isJumping) {
			yield return new WaitForSeconds (1);
			Instantiate (fire, transform.position, Quaternion.identity);
		}
	}
	IEnumerator HomingAttack () {
		while (isJumping) {
			yield return new WaitForSeconds (1);
			Instantiate (homing, transform.position, Quaternion.identity);
		}
	}
	IEnumerator FinishJump () {
		yield return new WaitForSeconds (5);
		isJumping = false;
		isAttack = false;
	}
	IEnumerator positionReverse () {
		yield return new WaitForSeconds (1);
		transform.localPosition = new Vector3 (posX, transform.localPosition.y, transform.localPosition.z);
		isAttack = false;
	}
	void OnTriggerEnter2D (Collider2D other) {
		bool isNoDamage = false;
		if (other.tag == "Player") {
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			player.hitBoss ();
			isNoDamage = true;
		} else if (other.tag == "MarathonHadouken") {
			other.GetComponent<MarathonHadouken> ().DestroyObj ();
			hitPoint += 3;
		} else if (other.tag == "MarathonYogaFire") {
			other.GetComponent<MarathonYogaFire> ().DestroyObj ();
			hitPoint += 10;
		} else if (other.tag == "PlayerNormalAttack") {
			hitPoint += 2;
		} else if (other.tag == "PlayerUpper") {
			hitPoint += 5;
		} else if (other.tag == "PlayerKick") {
			hitPoint += 5;
		}

		if (hitPoint >= bossHitPoint && !isDead) {
			hitPoint = bossHitPoint;
			isDead = true;
			//			AllGameDefinition.groundDifficulty = 2;
			//			Debug.Log("nextGame#########");
			StartCoroutine ("nextGame");
			//			_gameComplete ();
		}
	}
	void _gameComplete () {
		//		Debug.Log ("_gameComplete");
		//		rdbg.AddForce (Vector2.up * 1000);
		AllGameDefinition.groundDifficulty = 2;
		StartCoroutine ("nextGame");

	}
	IEnumerator nextGame () {
		yield return new WaitForSeconds (3);
		//		Debug.Log ("nextGame");
		int num = 0;
		gameController.SendMessage ("nextStage", num);
		StartCoroutine ("DestroyObj");
		//		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}
	IEnumerator DestroyObj () {
		yield return new WaitForSeconds (2);
		Destroy (this.gameObject);
	}
	float _getSpeed () {
		return UnityEngine.Random.Range (1, 20);
	}

	public void HitPlayerAttack (int actionPattern) {
		hitPoint = hitPoint + 5;
	}

	public void SetDifficulty (int num) {
		difficulty = AllGameDefinition.groundDifficulty;
		Debug.Log ("boss " + difficulty);
		bossHitPoint = BOSS_EASY_HP;
	}

	public void SetScrewDriverFlag (bool flag) {
		isScrewDriver = flag;
	}

	public void HitScrewDriver () {
		Debug.Log ("Boss hit screw");
		StartCoroutine ("nextGame");
	}
}
