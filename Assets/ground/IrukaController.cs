﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IrukaController : MonoBehaviour {
	GameObject irukaHadouken;

	// Use this for initialization
	void Start () {
		irukaHadouken = ResourceCache.Load ("Ground/MarathonHadouken");
		StartCoroutine (IrukaHadouken ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator IrukaHadouken () {
		while (true) {
			yield return new WaitForSeconds (2);
			Vector3 itemPos = transform.position;
			itemPos.x = itemPos.x + 1.0f;
			Instantiate (irukaHadouken, itemPos, Quaternion.identity);
		}
	}
}
