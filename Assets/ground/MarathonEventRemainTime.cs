﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarathonEventRemainTime : MonoBehaviour {
	public Sprite DoroImage; // 1
	public Sprite HaneImage; // 2
	public Sprite IceImage; // 3
	public Sprite ThunderImage; // 4
	public Sprite UsagiImage; // 5
	public Sprite KameImage; // 6
	public Sprite StormImage; // 7
	public Sprite SkeborImage; // 8
	public Sprite IrukaImage; // 9
	public Sprite RocketImage; // 10
	public Sprite RainImage; // 11
	public Sprite BubbleImage; // 12

	Image remainImage;
	Slider remainSlider;

	bool isRemaining;
	int count;
	float value;

	// Use this for initialization
	void Start () {
	}

	public void StartCountDown (int type, float time) {
		remainImage = transform.Find ("EventImage").GetComponent<Image> ();
		remainSlider = transform.Find ("EventSlider").GetComponent<Slider> ();
		isRemaining = true;
		count = 5; // slider value の反映が遅延するので5からスタート
		value = 0;

//		Debug.Log (type);
//		Debug.Log (time);
		remainSlider.maxValue = time;
//		Debug.Log ("StartRemainSlider start" + System.DateTime.Now);
		StartCoroutine (StartRemainSlider (time));

		switch (type) {
		case 1:
			remainImage.sprite = DoroImage;
			break;
		case 2:
			remainImage.sprite = HaneImage;
			break;
		case 3:
			remainImage.sprite = IceImage;
			break;
		case 4:
			remainImage.sprite = ThunderImage;
			break;
		case 5:
			remainImage.sprite = UsagiImage;
			break;
		case 6:
			remainImage.sprite = KameImage;
			break;
		case 7:
			remainImage.sprite = StormImage;
			break;
		case 8:
			remainImage.sprite = SkeborImage;
			remainImage.GetComponent<RectTransform> ().sizeDelta = new Vector2 (150, 100);
			break;
		case 9:
			remainImage.sprite = IrukaImage;
			break;
		case 10:
			remainImage.sprite = RocketImage;
			remainImage.GetComponent<RectTransform> ().sizeDelta = new Vector2 (150, 70);
			break;
		case 11:
			remainImage.sprite = RainImage;
			remainImage.GetComponent<RectTransform> ().sizeDelta = new Vector2 (80, 100);
			break;
		case 12:
			remainImage.sprite = BubbleImage;
			break;
		}
	}

	IEnumerator StartRemainSlider (float time) {
		while (isRemaining) {
			yield return new WaitForSeconds (0.1f);
			count++;
			value = 0.1f * count;
			remainSlider.value = value;

			if (remainSlider.value >= remainSlider.maxValue) {
				Debug.Log ("StartRemainSlider end" + System.DateTime.Now);
//				this.gameObject.SetActive (false);
//				int chuldCount = this.transform.parent.transform.childCount;
//				float posY = 390 - (chuldCount * 170);
//				Vector3 pos = new Vector3(2260, posY);

				int count = 0;
				this.gameObject.SetActive (false);
				foreach (Transform child in this.transform.parent.transform) {
					if (!child.gameObject.activeSelf) {
						continue;
					}
					float posY = (790 - (count * 150)) * (Screen.height / 1080f);
					Vector3 pos = new Vector3(1460 * (Screen.width / 1920f), posY);
					child.position = pos;
					count++;
				}
				Destroy (this.gameObject);
				break;
			}
		}
	}
}
