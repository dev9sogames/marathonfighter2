﻿using UnityEngine;
using System.Collections;

public class MarathonPlayerKickTrigger : MonoBehaviour {
	public int actionPattern;
	/*
	 * 1:kick
	 * 2:upper
	 * 3:normalAttack
	 *
	 */

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "MarathonBoss") {
			MarathonBossBlackController boss = other.GetComponent<MarathonBossBlackController> ();
			boss.HitPlayerAttack (actionPattern);
		}
		if (actionPattern == 1) {
			if (other.tag == "FieldObj") {
				MarathonFieldObj obj = other.GetComponent<MarathonFieldObj> ();
				obj.Hit ();
			}
		}
	}

}
