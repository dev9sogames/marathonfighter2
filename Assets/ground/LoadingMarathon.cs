﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingMarathon : MonoBehaviour {
	public Text loadingText;
	public Slider loadingSlider;
	private AsyncOperation async;

	public Text TipsText;
	SystemLanguage lang;

	string loadingstr;

	// Use this for initialization
	void Start () {
		loadingstr = "Now loading .";
		StartCoroutine ("LoadScene");
		lang = Application.systemLanguage;
		_showTips ();
	}

	IEnumerator LoadScene () {
		//loading
//		Debug.Log ("AsyncLoad Start");
		// 非同期で読み込む
		async = SceneManager.LoadSceneAsync("ground");
		async.allowSceneActivation= false;

		// なぜか90パーで止まるのでとりあえず・・・
		while(async.progress < 0.9f) {
			yield return new WaitForSeconds(1);
//			Debug.Log ("async.progress : " + async.progress);
			loadingstr += ".";
			loadingText.text = loadingstr;
		}
//		loadingText.text = "100 %";
//		loadingSlider.value = 1;

		yield return new WaitForSeconds(0.0f);
		async.allowSceneActivation = true;
	}

	void _showTips () {
		switch (UnityEngine.Random.Range(1, 15)) {
		case 1:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. 肉をとることで攻撃できるポイントが回復します。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 这点可以通过取肉收回攻击";
			} else {
				TipsText.text = "Tips. The point where you can attack by taking the meat will recover.";
			}
			break;
		case 2:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. ドクロをとると攻撃できるポイントが減ります。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 这将减少可以攻击并采取头骨点";
			} else {
				TipsText.text = "Tips. If you take skull, you can reduce points that can attack.";
			}
			break;
		case 3:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. はてなマークのボックスにジャンプキックすると 攻撃物体に変化します。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 在攻击对象的变化在框中Hatena的标志跳踢";
			} else {
				TipsText.text = "Tips. If you jump to a box with a hatena mark, it changes into an attack object.";
			}
			break;
		case 4:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. 攻撃して敵を倒すことで必殺技ゲージがたまります。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 它通过击败敌人来攻积聚致命的打击计";
			} else {
				TipsText.text = "Tips. It attacks and defeats the enemy, and the deathblow skill gauge gathers.";
			}
			break;
		case 5:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. 画面上部にある必殺技ゲージがたまったらタップすることで 必殺技が打てます。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 致命的打击将通过在屏幕上方敲击一旦积累了致命的打击计罢工";
			} else {
				TipsText.text = "Tips. You can hit a Special Move by tapping it when the Special Moves Gauge at the top of the screen gathers up.";
			}
			break;
		case 6:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. 穴に落ちても雲が救ってくれることがあります。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 此外，它掉进洞里，你可能会节省云";
			} else {
				TipsText.text = "Tips. Even if it falls into a hole, clouds may be saved.";
			}
			break;
		case 7:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. ミラーに攻撃すると 反射して自分に返ってきます。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 反映到攻击镜将回到自己的";
			} else {
				TipsText.text = "Tips. When it attacks the mirror it will reflect back to you.";
			}
			break;
		case 8:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. 黒い物体に触れると 画面が真っ黒になってしまいます。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 屏幕会全黑，触动黑色目标";
			} else {
				TipsText.text = "Tips. When you touch a black object, the screen turns black.";
			}
			break;
		case 9:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. 亀をとるとスピードが遅くなり、 うさぎをとるとスピードが早くなります。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 速度减慢，并采取了乌龟，速度会更快，走的兔子";
			} else {
				TipsText.text = "Tips. Taking a turtle slows the speed, and when you take a rabbit you get faster.";
			}
			break;
		case 10:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. スマホが熱くなったら少し休みましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 让我们休息一下当智能手机是热点";
			} else {
				TipsText.text = "Tips. Let's take a short rest if the smartphone gets hot.";
			}
			break;
		case 11:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. 連続プレイすると動きが悪くなることがあります。 少し休みましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 当连续播放可移动恶化。让我们休息一下";
			} else {
				TipsText.text = "Tips. Continuous play may cause the motion to worsen. Let's take a short rest.";
			}
			break;
		case 12:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. 技がわからなくなったら 「Technique」ボタンをタップして再確認しましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 当技术已经不知道让我们再确认通过点击\"Technique\"按钮";
			} else {
				TipsText.text = "Tips. When you do not understand the technique, tap the \"Technique\" button and reconfirm it.";
			}
			break;
		case 13:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. ボスを倒せないときはあきらめましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 让我们放弃，如果你不打败老板";
			} else {
				TipsText.text = "Tips. Let's give up when you can not defeat the boss.";
			}
			break;
		case 14:
			if (lang == SystemLanguage.Japanese) {
				TipsText.text = "Tips. レベルを上げるとステータスが上昇します。 どんどん上げましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				TipsText.text = "Tips. 提高水平状态将上升。让我给你更多";
			} else {
				TipsText.text = "Tips. Increasing the level raises the status. Let's get better.";
			}
			break;
		}
	}
}
