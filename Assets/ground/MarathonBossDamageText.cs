﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MarathonBossDamageText : MonoBehaviour {
	public Text damageText;

	float alpha;

	// Use this for initialization
	void Start () {
//		alpha = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
		alpha -= 1.0f * Time.deltaTime;
		damageText.color = new Color(1, 0, 0, alpha);
	}

	public void StartText (int damage) {
		damageText.text = damage.ToString ();
		alpha = 1.0f;
	}

	public void SetText (int damage) {
		Debug.Log ("SetText : " + damage);
		damageText.text = damage.ToString ();
	}
}
