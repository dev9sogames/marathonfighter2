﻿using UnityEngine;
using System.Collections;

public class MarathonBossMirror : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D (Collider2D other) {
//		Debug.Log ("BossMirror " + other.tag);
		if (other.tag == "PlayerNormalAttack") {
			MarathonPlayerNormalAttack MarathonPlayerNormalAttack = other.GetComponent<MarathonPlayerNormalAttack> ();
			MarathonPlayerNormalAttack.Reverse ();
		}
		if (other.tag == "MarathonHadouken") {
			MarathonHadouken hadouken = other.GetComponent<MarathonHadouken> ();
			hadouken.Reverse ();
		}
		if (other.tag == "MarathonYogaFire") {
			MarathonYogaFire yoga = other.GetComponent<MarathonYogaFire> ();
			yoga.Reverse ();
		}
	}
}
