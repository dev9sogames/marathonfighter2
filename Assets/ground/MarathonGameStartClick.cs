﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MarathonGameStartClick : MonoBehaviour {
	GameObject gameController;

	public Button StartButton;
	public Button BossStartButton;
	public Button PowerUpButton;
	public Text PowerUpText;
	public MarathonPlayer player;

	// Use this for initialization
	void Start () {
//		Debug.Log ("MarathonGameStartClick ");
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void MarathonStart () {
//		Debug.Log ("start :");
		gameController.SendMessage ("GameStart");
		Destroy (StartButton.gameObject);
		if (PowerUpButton != null) {
			Destroy (PowerUpButton.gameObject);
			Destroy (PowerUpText.gameObject);
		}
//		Destroy (BossStartButton.gameObject);
	}

	public void MarathonBossStart () {
		gameController.SendMessage ("GameBossStart");

		Destroy (StartButton.gameObject);
//		Destroy (BossStartButton.gameObject);
	}

	public void PowerUpButtonClick () {
		player.ShowRewardedAd ();
	}
}
