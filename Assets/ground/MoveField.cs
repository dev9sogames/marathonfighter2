﻿using UnityEngine;
using System.Collections;

public class MoveField : MonoBehaviour {
	MarathonPlayer _player;

	Vector3 pos;
	float posX;
	float playerPosX;
	GameObject gameController;

	float speed = AllGameDefinition.fieldSpeed;

	bool addNextField;
	bool isReady;
//	bool isFirst;
	// Use this for initialization
	void Start () {
		addNextField = false;
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
		pos = transform.position;
		posX = transform.position.x;
//		StartCoroutine ("GetPlayerPosition");
	}

	IEnumerator GetPlayerPosition () {
		while (true) {
			yield return new WaitForSeconds (1);
			_getPlayerPosition ();
		}
	}

	public void SetPlayer (MarathonPlayer player) {
		_player = player;
	}
	void _getPlayerPosition () {
//		playerPosX = _player.getPosition ();
	}

	// Update is called once per frame
//	protected override void ChildUpdate () {
	void Update () {
		if (isReady) {
			return;
		}
		posX -= Time.deltaTime * speed;
//		Debug.Log (posX + " :INSTANCE_ID " + this.GetInstanceID());
		transform.position = new Vector3 (posX, pos.y, pos.z);

//		if (posX > (5.81f + playerPosX) && posX <= (6.0f + playerPosX) && !addNextField) {
//		if (posX > (5.81f) && posX <= (6.0f) && !addNextField) {
//		if (posX > 5.8f && posX <= (6.2f) && !addNextField) {
		if (posX <= 8.1f && !addNextField) {
			addNextField = true;
			gameController.SendMessage ("CreateNextField");
		}
		if (posX <= (-25.0f)) {
			this.gameObject.SetActive (false);
//			Destroy (this.gameObject);
		}
	}

	public void Ready () {
		isReady = true;
	}
	public void GameStart () {
		isReady = false;
		speed = AllGameDefinition.fieldSpeed;
	}

	public void SetFirst () {
		addNextField = true;
	}

	public void SetMoveSpeed (int moveSpeed) {
		speed = moveSpeed;
	}
}
