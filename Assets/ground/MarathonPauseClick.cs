﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MarathonPauseClick : MonoBehaviour {
	GameObject gameController;
//	public GuiAnimController AnimController;

//	public Canvas RetryCanvas;
//	public Canvas GiveupCanvas;
	public Canvas PauseCanvas;
//	public Image backGroundImage;

	public Button RetryButton;
	public Button GiveupButton;
	public Button CancelButton;
	public Text RetryText;
	public Text GiveupText;

	public bool isTutorial;

	// Use this for initialization
	void Start () {
//		Debug.Log ("marathon pause click start");
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void MoveStart () {
		RetryButton.GetComponent<ButtonAnimationController> ().MoveStart ();
		GiveupButton.GetComponent<ButtonAnimationController> ().MoveStart ();
		CancelButton.GetComponent<ButtonAnimationController> ().MoveStart ();
		RetryText.GetComponent<ButtonAnimationController> ().MoveStart ();
		GiveupText.GetComponent<ButtonAnimationController> ().MoveStart ();
	}

	public void PauseClick () {
		gameController.SendMessage ("PauseClick", false);
//		backGroundImage.enabled = true;
		PauseCanvas.enabled = true;
//		MoveStart ();
//		AnimController.enabled = true;
	}

//	public void RetryClick () {
//		gameController.SendMessage ("PauseClick", true);
//		RetryCanvas.enabled = true;
//	}
//
//	public void GiveupClick () {
//		gameController.SendMessage ("PauseClick", true);
//		GiveupCanvas.enabled = true;
//	}

	public void RetryYesClick () {
		gameController.SendMessage ("PauseClick", false);
		if (isTutorial) {
			SceneManager.LoadScene ("Tutorial");
		} else {
			SceneManager.LoadScene ("Ground");
		}
	}

//	public void RetryNoClick () {
//		RetryCanvas.enabled = false;
//		gameController.SendMessage ("PauseClick", false);
//	}

	public void GiveupYesClick () {
		gameController.SendMessage ("PauseClick", false);
		SceneManager.LoadScene ("Difficulty");
	}

	public void CancelClick () {
		PauseCanvas.enabled = false;
		gameController.SendMessage ("PauseClick", false);

//		RetryButton.GetComponent<ButtonAnimationController> ().SetDefalutPosition ();
//		GiveupButton.GetComponent<ButtonAnimationController> ().SetDefalutPosition ();
//		CancelButton.GetComponent<ButtonAnimationController> ().SetDefalutPosition ();
//		RetryText.GetComponent<ButtonAnimationController> ().SetDefalutPosition ();
//		GiveupText.GetComponent<ButtonAnimationController> ().SetDefalutPosition ();
	}
}
