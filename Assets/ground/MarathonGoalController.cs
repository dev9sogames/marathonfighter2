﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonGoalController : MonoBehaviour {
	public int goalLevel;
	public MarathonGoalManager goalManager;
	public MarathonGoalController[] goalList;

//	GameObject gameController;
	bool isMove;

	bool isGoal;
	bool isHit;
	bool isEnd;

	enum Direction {
		up,
		down
	};
	Direction dire;

	float posY;
	float posX;

	// Use this for initialization
	void Start () {
		posY = transform.localPosition.y;
		posX = transform.localPosition.x;
		dire = Direction.up;
//		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
	}
	
	// Update is called once per frame
	void Update () {
		if (!isMove) {
			return;
		}

		if (transform.localPosition.y >= 12.0f) {
			dire = Direction.down;
		}
		if (transform.localPosition.y <= 0.2f) {
			dire = Direction.up;
		}
		if (dire == Direction.down) {
			posY -= Time.deltaTime * 5;
		} else {
			posY += Time.deltaTime * 5;
		}
		transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);

	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "PlayerNormalAttack" || other.tag == "MarathonHadouken" || other.tag == "MarathonYogaFire") {
			goalManager.getGoal (goalLevel);
		}
	}

	public void getGoal () {
		if (!isGoal) {
			isEnd = true;
		} else {
			this.gameObject.SetActive (false);
		}
	}

	public void setMoveFlag (bool flag) {
		isMove = flag;
	}

	public void getGoal (int goalType) {
		if (goalType == goalLevel) {
			this.gameObject.SetActive (false);
		}
	}

}
