﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanboBaseController : MonoBehaviour {
	Rigidbody2D rgbd;

	float posX;

	// Use this for initialization
	void Start () {
		rgbd = GetComponent<Rigidbody2D> ();

		posX = transform.localPosition.x;
		rgbd.AddForce (Vector2.up * 2000);
		StartCoroutine (moveSanbo());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator moveSanbo () {
		while (true) {
			yield return new WaitForSeconds (0.5f);

			/*
			posX -= Time.deltaTime * 3;
			transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			*/
			rgbd.AddForce (Vector2.left * (UnityEngine.Random.Range (10, 50)));

			if (UnityEngine.Random.Range (1, 4) == 1) {
				rgbd.AddForce (Vector2.up * (UnityEngine.Random.Range (1000, 3000)));
			} else {
				rgbd.AddForce (Vector2.down * (UnityEngine.Random.Range (1000, 3000)));
			}
		}
	}
}
