﻿using UnityEngine;
using System.Collections;

public class MarathonBossAttack : MonoBehaviour {
	/*
	 * 1:fire
	 * 2:hammer
	 * 3:beef
	 * 4:beef bad
	 * 5:muteki
	 * 6:homing
	 * 7:UP and homing
	 * 8:ice
	 * 9:doragon
	 * 10:thunder
	 * 11:reppuuken
	 * 12:bossBodyAttack -> MarathonBossBodyAttack に移動
	 * 13:bossJumpKick -> MarathonBossBodyAttack に移動
	 * 14:bossSliding -> MarathonBossBodyAttack に移動
	 * 15:speed Fire attack
	 */
	public int attackNo;

	Rigidbody2D rgbd;

	MarathonPlayer targetPlayer;

	bool isDead;
	bool isMovingToPlayer;

	GameObject bossObj;

	// Use this for initialization
//	public override void Init() {
	void Start () {
		isDead = false;
		isMovingToPlayer = false;

		if (attackNo == 6) {
			
		}

		if (attackNo == 2 || attackNo == 3 || attackNo == 4 || attackNo == 5 || attackNo == 8) {
			rgbd = GetComponent<Rigidbody2D> ();
			rgbd.AddForce(Vector2.up * 400);
			rgbd.AddForce(Vector2.left * 300);
			/*
			Vector2 v;
			v.x = Mathf.Cos (Mathf.Deg2Rad * -45) * 8;
			v.y = Mathf.Sin (Mathf.Deg2Rad * -45) * 8;
			rgbd.velocity = v;
			*/

//			rgbd.AddForce (Vector2.left * 300);
		}
		if (attackNo == 6 || attackNo == 7 || attackNo == 9) {
			rgbd = GetComponent<Rigidbody2D> ();
			targetPlayer = GameObject.FindGameObjectWithTag ("Player").GetComponent<MarathonPlayer> ();
			if (attackNo == 7) {
				StartCoroutine (MoveToPlayer ());
			}

			StartCoroutine (GetBossStatus ());
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x >= 17 || transform.position.x <= -15 ||
			transform.position.y > 20 || transform.position.y < -10) {
			DestroyObj ();
		}
		if (isDead) {
			return;
		}
		if (attackNo == 1) {
			Vector2 position = transform.position;
			position.x -= Time.deltaTime * 9;
			transform.position = position;
		}
		if ((attackNo == 6 || attackNo == 9) && !isDead) {
			moveTargetDirection ();
		}
		if (attackNo == 7 && !isDead && !isMovingToPlayer) {
			Vector2 position = transform.position;
			position.y += Time.deltaTime * 3;
			transform.position = position;
		} else if (attackNo == 7 && !isDead && isMovingToPlayer) {
			moveTargetDirection ();
		}
		if (attackNo == 10) {
			Vector2 position = transform.position;
			position.y -= Time.deltaTime * 9;
			transform.position = position;
		}
		if (attackNo == 11) {
			Vector2 position = transform.position;
			position.x -= Time.deltaTime * 11;
			transform.position = position;
		}
		if (attackNo == 15) {
			Vector2 position = transform.position;
			position.x -= Time.deltaTime * 13;
			transform.position = position;
		}
	}

	IEnumerator MoveToPlayer () {
		yield return new WaitForSeconds (2);
		isMovingToPlayer = true;
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "PlayerUpper" || other.tag == "PlayerTornado") {
			if (attackNo == 2 || attackNo == 4 || attackNo == 6 || attackNo == 7) {
				isDead = true;
				rgbd.AddForce (Vector2.up * 1000);
			}
		}
		if (other.tag == "Player") {
//			Debug.Log ("Attack hit player");
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			if (attackNo == 3) {
				player.HitEnemy (8);
				DestroyObj ();
			} else if (attackNo == 5) {
				player.HitEnemy (7);
				DestroyObj ();
			} else if (attackNo == 6 || attackNo == 7) {
				player.HitEnemy (99);
				DestroyObj ();
			} else if (attackNo == 8) { // ice
				player.HitEnemy (98);
				DestroyObj ();
			} else if (attackNo == 9) { // dragon
				player.HitEnemy (96);
				DestroyObj ();
			} else if (attackNo == 10) { // thunder
				player.HitEnemy (97);
			} else if (attackNo == 12) { // boss body attack
				player.HitEnemy (95);
			} else if (attackNo == 13) { // boss jump kick
				player.HitEnemy (94);
			} else if (attackNo == 14) { // boss sliding
				player.HitEnemy (93);
			} else {
				player.HitEnemy (99);
			}
		}
		if (other.tag == "MarathonHadouken" || other.tag == "MarathonYogaFire") {
			if (attackNo == 6 || attackNo == 7) {
				isDead = true;
				DestroyObj ();
//				rgbd.AddForce (Vector2.right * 500);
			}
		}
		if (other.tag == "MarathonYogaFire") {
			if (attackNo == 9) {
				isDead = true;
				DestroyObj ();
			}
		}
	}
	void moveTargetDirection () {
		Vector2 targetPos = targetPlayer.transform.position;
		float x = targetPos.x;
		float y = targetPos.y;

		Vector2 direction = new Vector2 (x - transform.position.x, y - transform.position.y).normalized;
		GetComponent<Rigidbody2D> ().velocity = (direction * 7);
	}

	public void setBossObj (GameObject boss) {
		bossObj = boss;
	}

	IEnumerator GetBossStatus () {
		while (!isDead) {
			yield return new WaitForSeconds (1);
//			Debug.Log ("boss dead ? " + bossObj.GetComponent<MarathonBossBlackController> ().getBossIsDead ());
			if (bossObj.GetComponent<MarathonBossBlackController> ().getBossIsDead ()) {
				isDead = true;
				DestroyObj ();
			}
		}
	}

	public void DestroyObj () {
//		ReturnToPool ();
		Destroy(this.gameObject);
	}
}
