﻿using UnityEngine;
using System.Collections;
using System;

public class MarathonHadouken : MonoBehaviour {
	bool isReverse;
	BoxCollider2D cldr;
	PolygonCollider2D pcldr;

	public GameObject hadoukenPool;
	bool isTarget;

	public bool isTornado;
	float speedX;
	float speedY;
	float posX;
	float posY;

//	public GameObject targetObj;
	GameObject targetObj = null;

	// Use this for initialization
//	public override void Init() {
	void Start () {
		isReverse = false;
		isTarget = false;

		if (isTornado) {
			transform.localScale = new Vector3 (0.4f, 0.4f, 0);
		} else {
			transform.localScale = new Vector3 (1.0f, 1.0f, 0);
		}

		if (isTornado) {
			pcldr = GetComponent<PolygonCollider2D> ();
			pcldr.isTrigger = false;
			posX = transform.position.x;
			posY = transform.position.y;
		} else {
			cldr = GetComponent<BoxCollider2D> ();
			cldr.isTrigger = false;
		}

		/*
		try {
			targetObj = GameObject.FindGameObjectWithTag ("BlackHole");
		} catch (Exception e) {
			return;
		}
		*/
	}
	/*
	void existTarget () {
		bool exist;
		try {
			exist = targetObj.GetComponent<MarathonBlackHoleController>().enabled;
		} catch (Exception) {
			return;
		}
		if (exist) {
			moveTargetDirection ();
			return;
		}
	}
	*/
	// Update is called once per frame

	public void setTargetFlag (bool flag) {
		Debug.Log ("Haduoken setFlag : " + flag);
		isTarget = flag;
	}
	void Update () {
		if (isTornado) {
			if (transform.position.x >= 17 || transform.position.x <= -15) {
//				Destroy (this.gameObject);
				DestroyObj ();
			}
			if (isReverse) {
				posX -= Time.deltaTime * 15;
			} else {
				speedX = UnityEngine.Random.Range (-5.0f, 10.0f);
				speedY = UnityEngine.Random.Range (-10.0f, 10.0f);

				posX += Time.deltaTime * speedX;
				posY += Time.deltaTime * speedY;
			}
			transform.position = new Vector3 (posX, posY, 1);
		} else {
			if (isTarget) {
				moveTargetDirection ();
				return;
			}

			//		existTarget ();
			Vector2 position = transform.position;
			if (!isReverse) {
				position.x += Time.deltaTime * 5;
			} else {
				position.x -= Time.deltaTime * 15;
			}
			if (position.x >= 17 || position.x <= -15) {
//				Destroy (this.gameObject);
				DestroyObj ();
			}
			transform.position = position;
		}
	}
	void moveTargetDirection () {
//		Vector2 targetPos = targetObj.transform.position;
//		float x = targetPos.x;
//		float y = targetPos.y;
		float x = 3.38f;
		float y = 2.66f;

		Vector2 direction = new Vector2 (x - transform.position.x, y - transform.position.y).normalized;
		GetComponent<Rigidbody2D> ().velocity = (direction * 7);
	}

	void OnTriggerEnter2D (Collider2D other) {
//		Debug.Log (other.tag);
//		if (other.tag == "MarathonBoss") {
//			if (!isReverse) {
////				Destroy (this.gameObject);
//				DestroyObj ();
//			}
//		}
		if (other.tag == "Player") {
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			player.HitEnemy (1);
		}
	}

	public void Reverse () {
//		Debug.Log ("hadouken reverse");
		isReverse = true;
		if (isTornado) {
			if (pcldr != null) {
				pcldr.isTrigger = true;
			}
			transform.localScale = new Vector3 (-0.4f, 0.4f, 0);
		} else {
			if (cldr != null) {
				cldr.isTrigger = true;
			}
			transform.localScale = new Vector3 (-1.0f, 1.0f, 0);
		}
	}

	public void setTarget (GameObject obj) {
		targetObj = obj;
	}
	public void unSetTarget () {
		targetObj = null;
	}

	public void DestroyObj () {
//		Debug.Log ("pool ");
		Destroy (this.gameObject);
//		ReturnToPool ();
	}
}
