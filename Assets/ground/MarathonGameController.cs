﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using NCMB;
using UnityEngine.Advertisements;
using DG.Tweening;

public class MarathonGameController : MonoBehaviour {
//	public GameObject GroundBack;
//	public GameObject PlanetBack;
//	public GameObject SeaBack;
//	public GameObject DesertBack;
//	public GameObject BossBack;
	SystemLanguage lang;

	public Image sliderImage;
	public Slider wazaSlider;
	public Button wazaButton;

	public Button jumpButton;
	Image jumpButtonImg;
	public Button attackButton;
	Image attackButtonImg;
	public Button topButton;
	Image topButtonImg;
	public Button bottomButton;
	Image bottomButtonImg;
	public Button rightButton;
	Image rightButtonImg;
	public Button leftButton;
	Image leftButtonImg;

	bool isButtonChange;
	public Text tutorialText;
	public Text tutorialTipsText;
	public Image tutorialImageSmall;
	public Image tutorialImage;

	public Sprite tutorialJumpImage;
	public Sprite tutorialDoubleJumpImage;
	public Sprite tutorialTripleJumpImage;
	public Sprite tutorialNormalAttackImage;
	public Sprite tutorialHadoukenImage;
	public Sprite tutorialYogaFireImage;
	public Sprite tutorialUpperImage;
	public Sprite tutorialKickImage;
	public Sprite tutorialRollingImage;
	public Sprite tutorialTornadoDefenseImage;

	public Canvas TutotrialSkipCanvas;

	public bool isTraining;

	int[] expTable = {0,11000,23100,36410,51051,67156,84872,104359,125795,149374,175312,203843,235227,269750,307725,349497,395447,445992,501591,562750,630025,704027,785430,874973,973471,1081818,1200999,1332099,1476309,1634940,1809434,2001378,2212515,2444767,2700244,2981268,3290395,3630434,4004478,4415926,4868518,5366370,5914007,6516408,7179048,7907953,8709749,9591723,10561896,11629085,12802994};

//	const int ENEMY_NUM = 102;
	const string HIGH_SCORE_KEY_EASY = "marathonBestScoreEasy";
	const string HIGH_SCORE_KEY_NORMAL = "marathonBestScoreNormal";
	const string HIGH_SCORE_KEY_HARD = "marathonBestScoreHard";
//	const string MIDDLE_ACHIEVE = "middleAchieveFlag";

	const string ONE_ONE_STAR_COUNT = "1-1-starCount";
	const string ONE_TWO_STAR_COUNT = "1-2-starCount";
	const string ONE_THREE_STAR_COUNT = "1-3-starCount";

	const string TWO_ONE_STAR_COUNT = "2-1-starCount";
	const string TWO_TWO_STAR_COUNT = "3-2-starCount";
	const string TWO_THREE_STAR_COUNT = "2-3-starCount";

	const string THREE_ONE_STAR_COUNT = "3-1-starCount";
	const string THREE_TWO_STAR_COUNT = "3-2-starCount";
	const string THREE_THREE_STAR_COUNT = "3-3-starCount";

	const string FOUR_ONE_STAR_COUNT = "4-1-starCount";
	const string FOUR_TWO_STAR_COUNT = "4-2-starCount";
	const string FOUR_THREE_STAR_COUNT = "4-3-starCount";

	float finishTime;
	int currentPoint = 0;
	int actionPoint;
	int totalPoint;
	int moveSpeed;

	int totalDistance;
	int totalBestPoint;

	int battlePoint;
	public Text battlePointText;
	public Text totalDistanceText;
	public Text ExpNextText;

	public Text levelText;
	public Text StageText;

	int bossCount;
	int basePoint = Constants.basePoint;

	public MarathonPlayer player;
	public Text PointText;
	public Text BestScoreText;
	long startTime;

	public Canvas GameOverCanvas;
	public Canvas LevelUpCanvas;
	public Canvas StageCompleteCanvas;
	public Canvas ActionCanvas;
	public Canvas YogaFireUnlockCanvas;
	public GameObject LevelUpController;
	public Text AchieveRatio;

	public Canvas EnemyWarningCanvas;
	public Image backImage;
	public Image enemyWarningImage;
	public Canvas BossWarningCanvas;

	public Button BossStartButton;

	GameObject[] prefabList;
	Dictionary<int, GameObject> FieldObjectList = new Dictionary<int, GameObject>();

	Dictionary<int, GameObject> EndlessEasyFieldObjectList = new Dictionary<int, GameObject>();
	Dictionary<int, GameObject> EndlessNormalFieldObjectList = new Dictionary<int, GameObject>();
	Dictionary<int, GameObject> EndlessHardFieldObjectList = new Dictionary<int, GameObject>();

	Dictionary<int, GameObject> MarathonEasyFieldObjectList = new Dictionary<int, GameObject>();
	Dictionary<int, GameObject> MarathonNormalFieldObjectList = new Dictionary<int, GameObject>();
	Dictionary<int, GameObject> MarathonHardFieldObjectList = new Dictionary<int, GameObject>();

	MoveFieldInit field1;
	MoveFieldInit field2;
	MoveField field3;

	bool isBoss;
	bool isMiddleBossEnd;

	int middleBossDistance;
	int mainBossDistance;

	// "30" :ワープは保留.
//	int[] EasyMode = {1, 2, 4, 7, 8, 13, 14, 17, 18, 19, 22, 23, 36, 37, 39, 40, 44};
//	int[] NormalMode = {1, 3, 6, 9, 10, 12, 15, 21, 27, 28, 31, 32, 33, 35, 36, 37, 38, 39, 40, 41, 44, 69, 71, 72, 76, 78, 79, 80, 90, 99};
//	int[] HardMode = {1, 5, 11, 16, 20, 25, 26, 34, 38, 39, 40, 41, 42, 43, 44, 49, 50, 66, 70, 73, 74, 75, 77, 81, 86, 93, 95, 96};

//	int[] RareMode = {24, 1, 5, 11, 16, 20, 25, 26, 34, 38, 39, 40, 41, 42, 43, 44};
//	int[] planetMode = {83, 84, 85, 87, 88, 89, 91, 92, 94, 97, 98};
//	int[] seaMode = {45, 46, 47, 48, 51, 52, 53, 54, 55, 45, 46, 47, 48, 51, 52, 53, 54, 55, 67, 68, 82};
//	int[] seaManyMode = {45, 46, 47, 48, 51, 52, 53, 54, 55, 45, 46, 47, 48, 51, 52, 53, 54, 55};
//	int[] desertMode = {56, 57, 58, 59, 60, 61, 62, 56, 57, 58, 59, 60, 61, 62, 64, 65};
	int[] desertManyMode = {56, 57, 58, 59, 60, 61, 62, 56, 57, 58, 59, 60, 61, 62};
	int[] HoleUp = {2, 3};

//	int[] hardMode = {1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128};
	int[] easyMode = {1, 2, 3, 5, 6, 11, 13, 14, 15, 16, 17, 18, 19, 25, 28, 31, 35, 36, 37, 38, 39, 40, 43, 44, 72, 80, 81, 99};
	int[] normalMode = {1, 2, 3, 5, 6, 11, 13, 14, 15, 17, 22, 25, 33, 35, 37, 38, 39, 40, 41, 44, 69, 72, 75, 76, 78, 81, 90, 95};
	int[] hardMode = {1, 2, 3, 5, 6, 11, 15, 16, 20, 21, 22, 25, 26, 31, 33, 34, 41, 42, 43, 50, 66, 69, 70, 71, 73, 74, 76, 77, 79, 90, 93, 96};

	int[] endlessEasyMode = {1, 2, 3, 6, 11, 13, 16, 17, 18, 19, 21, 22, 28, 31, 35};
	int[] endlessNormalMode = {1, 2, 3, 36, 37, 38, 39, 40, 41, 43, 44, 45, 46, 48, 49, 50, 53, 62, 69, 70, 111, 112, 113, 114, 125, 126, 127, 128};
	int[] endlessHardMode = {1, 2, 3, 5, 71, 72, 73, 77, 78, 79, 80, 84, 88, 89, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124};

	int[] groundMode = {4, 7, 8, 9, 10, 12, 23, 27, 32};
	int[] planetMode = {83, 84, 85, 87, 88, 89, 91, 92, 94, 97, 98};
	int[] seaMode = {45, 46, 47, 48, 51, 52, 53, 54, 55 ,67, 68, 82};
	int[] desertMode = { 56, 57, 58, 59, 60, 61, 62, 64, 65 };
//	int[] bossMode = {4};
	int[] bossMode = {4, 7, 8, 9, 10, 12, 23, 27, 83, 84, 85, 87, 88, 89, 91, 92, 94, 97, 98, 45, 46, 47, 48, 51, 52, 53, 54, 55 ,67, 68, 82, 56, 57, 58, 59, 60, 61, 62, 64, 65, 102, 103};

	int[] marathonEasyMode = {1, 2, 3, 4, 7, 8, 9, 12, 17, 18, 19, 23, 35, 36, 37, 38, 39, 40, 69, 79, 80};
	int[] marathonNormalMode = {1, 2, 3, 9, 12, 19, 23, 35, 37, 38, 44, 66, 70, 77, 100, 104, 105, 108, 109, 115, 116, 119, 120, 124, 125};
	int[] marathonHardMode = {1, 2, 3, 5, 21, 27, 28, 31, 32, 41, 43, 48, 50, 71, 72, 73, 95, 102, 103, 106, 107, 110, 111, 112, 113, 114, 117, 118, 121, 122, 123, 126, 127, 128};

	int[] marathon2EasyMode = {1, 2, 3, 4, 5, 10, 12, 16, 27, 31, 35, 37, 38, 39, 40, 41, 43, 47, 50, 69, 70, 71, 72, 77, 78, 79, 80, 84, 89, 100, 104, 105, 108, 109, 115, 116, 119, 120, 124, 125};
	int[] marathon2NormalMode = {1, 2, 3, 5, 6, 10, 11, 12, 16, 21, 23, 27, 31, 32, 37, 43, 45, 46, 47, 49, 50, 61, 66, 67, 70, 73, 77, 85, 86, 87, 88, 91, 94, 95, 96};
	int[] marathon2HardMode = {1, 2, 3, 5, 47, 49, 50, 61, 66, 67, 70, 73, 77, 85, 86, 87, 88, 91, 94, 95, 96, 103, 106, 107, 110, 111, 112, 113, 114, 117, 118, 121, 122, 123, 126, 127, 128};

	int[] tutorialMode = {1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1501, 1502, 1503,
		1010, 1011, 1020, 1021, 1030, 1031, 1040, 1050, 1051, 1060};
//	int[] tutorialNumList = {1000, 1000, 1503, 1000, 1000, 1000};
	int[] tutorialNumList = {1000, 
		1001, 1010, 1011, 1000, // jump
		1002, 1020, 1021, 1000, // double jump
		1003, 1030, 1031, 1000, // triple jump
		1004, 1040, 1000, 1000, // normal attack
		1005, 1050, 1000, 1000, // hadouken
		1006, 1051, 1000, 1000, // jump hadouken
		1007, 1060, 1000, 1000, // yogafire
		1008, 1000, 1000, 1000, // upper
		1009, 1000, 1000, 1000, // kick
		1501, 1000, 1000, 1000, // rolling
		1502, 1000, 1000, 1000, // over protection
		1503, 1000, 1000, 1000 // boss battle
	};

	int[] NormalStage;
	int[] HardStage;

	int[] stageList;
	int[] endlessEasyStageList;
	int[] endlessNormalStageList;
	int[] endlessHardStageList;

	int[] marathonEasyStageList;
	int[] marathonNormalStageList;
	int[] marathonHardStageList;

	int warningCount = 0;

	bool isPause;
	bool isPauseClick;
	MoveField[] fieldList = new MoveField[4];
	enum GameState {
		Ready,
		Start,
		End
	};
	GameState state;

	float posX;
//	GameObject bossVeryMini;
	GameObject bossFireMan;
	GameObject bossMini;
	GameObject bossTako;
	GameObject bossUchuujin;
	GameObject bossMoai;
	GameObject bossNurikabe;
	GameObject bossBlack;
	GameObject bossLast;

	public Sprite groundField1;
	public Sprite groundField2;
	public Sprite groundField3;
	public Sprite groundField4;
	public Sprite groundField5;
	public Sprite groundField6;
	public Sprite groundField7;
	public Sprite groundField8;
	public Sprite groundField20;
	public Sprite groundField21;
	public Sprite groundField22;
	public Sprite groundField23;
	public Sprite groundField24;

	public Sprite snowGroundField;

	public Sprite spaceField1;
	public Sprite spaceField2;
	public Sprite spaceField3;
	public Sprite spaceField4;
	public Sprite spaceField5;
	public Sprite spaceField6;
	public Sprite spaceField7;
	public Sprite spaceField8;
	public Sprite spaceField20;
	public Sprite spaceField21;
	public Sprite spaceField22;
	public Sprite spaceField23;
	public Sprite spaceField24;

	public Sprite seaField1;
	public Sprite seaField2;
	public Sprite seaField3;
	public Sprite seaField4;
	public Sprite seaField5;
	public Sprite seaField6;
	public Sprite seaField7;
	public Sprite seaField8;

	public Sprite desertField1;
	public Sprite desertField2;
	public Sprite desertField3;
	public Sprite desertField4;
	public Sprite desertField5;
	public Sprite desertField6;
	public Sprite desertField7;
	public Sprite desertField8;

	public Sprite bossField1;
	public Sprite bossField2;
	public Sprite bossField3;
	public Sprite bossField4;
	public Sprite bossField5;
	public Sprite bossField6;
	public Sprite bossField7;
	public Sprite bossField8;

	Sprite commonField1;
	Sprite commonField2;
	Sprite commonField3;
	Sprite commonField4;
	Sprite commonField5;
	Sprite commonField6;
	Sprite commonField7;
	Sprite commonField8;
	Sprite commonField20;
	Sprite commonField21;
	Sprite commonField22;
	Sprite commonField23;
	Sprite commonField24;

	GameObject bubble;
	GameObject rains;
	GameObject sandStorm;

	int bossAliveCount;

	public bool isTutorial;
	void OnApplicationPause (bool pauseStatus)
	{
		if (pauseStatus) {
			isPause = true;
		} else {
			if (isPause) {
				isPause = false;
			}
		}
	}

	void Awake () {
		if (isTraining) {
			return;
		}
//		PlayerPrefs.DeleteAll();
		// resource load

		if (AllGameDefinition.gameMode != 4) {
			bossFireMan = ResourceCache.Load ("Ground/BossFireMan");
			bossTako = ResourceCache.Load ("Ground/BossTako");
			bossUchuujin = ResourceCache.Load ("Ground/BossUchuujin");
			bossMoai = ResourceCache.Load ("Ground/BossMoai");
			bossNurikabe = ResourceCache.Load ("Ground/BossNurikabe");
//		bossVeryMini = ResourceCache.Load ("Ground/BossVerySmallNurikabe");
			bossMini = ResourceCache.Load ("Ground/BossSmallNurikabe");
			bossBlack = ResourceCache.Load ("Ground/BossBlackHero");
			bossLast = ResourceCache.Load ("Ground/BossLast");

			ResourceCache.Load ("Ground/EnemyHonooImage");
			ResourceCache.Load ("Ground/Enemyhammer");
			ResourceCache.Load ("Ground/EnemyBeef");
			ResourceCache.Load ("Ground/EnemyBeefBad");
			ResourceCache.Load ("Ground/EnemyMuteki");
			ResourceCache.Load ("Ground/EnemyHomingImage");
			ResourceCache.Load ("Ground/EnemyUpAndHoming");
			ResourceCache.Load ("Ground/EnemyIceObj");
			ResourceCache.Load ("Ground/BossBunshin");
			ResourceCache.Load ("Ground/EnemyDragon");
			ResourceCache.Load ("Ground/EnemyThunder");
			ResourceCache.Load ("Ground/EnemyReppuuken");
			ResourceCache.Load ("Ground/BossVerySmallNurikabe");

			bubble = ResourceCache.Load ("Ground/bubble");
			rains = ResourceCache.Load ("Ground/rains");
			sandStorm = ResourceCache.Load ("Ground/sandStorm");
		}

	}

	// Use this for initialization
	void Start () {
		DOTween.Init (false, true, LogBehaviour.Verbose);

		if (AllGameDefinition.StoryStageType == 0) {
			AllGameDefinition.StoryStageType = 2;
		}
		if (AllGameDefinition.StoryStageCourt == 0) {
			AllGameDefinition.StoryStageCourt = 3;
		}
		bossCount = 1;
		AllGameDefinition.bossCount = bossCount;

		int baseSpeed = Constants.moveSpeed;
		if (AllGameDefinition.StoryStageCourt == 2) {
			baseSpeed += 1;
		} else if (AllGameDefinition.StoryStageCourt == 3) {
			baseSpeed += 1;
		}

		if (isTutorial) {
			AllGameDefinition.StoryStageType = 99;
			AllGameDefinition.StoryStageCourt = 99;
			baseSpeed = Constants.moveSpeed;
			tutorialText.text = "Tutorial Start";
			tutorialTipsText.text = "";
			TutotrialSkipCanvas.gameObject.SetActive (false);
		}
		AllGameDefinition.fieldSpeed = baseSpeed;

		if (!AllGameDefinition.isHardMode) {
			middleBossDistance = 2500;
			mainBossDistance = 5000;
		} else {
			middleBossDistance = 4000;
			mainBossDistance = 8000;
		}

		_setStageArray ();
		moveSpeed = baseSpeed;
		/*
		switch (AllGameDefinition.groundDifficulty) {
		case 1:
			AllGameDefinition.fieldSpeed = baseSpeed;
			break;
		case 2:
			AllGameDefinition.fieldSpeed = baseSpeed;
			break;
		case 3:
			moveSpeed = baseSpeed;
			AllGameDefinition.fieldSpeed = baseSpeed;
			break;
		default:
			AllGameDefinition.fieldSpeed = baseSpeed;
			break;
		}
		*/
		state = GameState.Ready;
		AllGameDefinition.currentDifficulty = 1;
//		AllGameDefinition.groundDifficulty = 1;
		_createStartField ();

//		isAds = false;
//		if (isTutorial) {
//			return;
//		}

		if (!isTutorial) {
			_getPlayerStatus ();
			_getBestScore ();
			GameOverCanvas.enabled = false;
			LevelUpCanvas.enabled = false;
			StageCompleteCanvas.enabled = false;
			ActionCanvas.enabled = true;
//			YogaFireUnlockCanvas.enabled = false;
			LevelUpController.GetComponent<MarathonLevelUpController> ().enabled = false;
//		_loadStatus ();
			battlePointText.text = "0 P";
			totalDistance = LoadHighScore (Constants.TOTAL_DISTANCE);
			totalDistanceText.text = totalDistance + " m";

			if (AllGameDefinition.gameMode != 4) {
				sliderImage.gameObject.SetActive (true);
				wazaSlider.gameObject.SetActive (true);
			}
		} else {
			AllGameDefinition.gameMode = 0;
			lang = Application.systemLanguage;
		}

		if (AllGameDefinition.gameMode == 4 || isTutorial) { // marathon mode or tutorial mode
			middleBossDistance = 2000000000;
			mainBossDistance = 2000000000;
			battlePointText.enabled = false;
		}

		if (AllGameDefinition.gameMode == 1) {
			StageText.text = "STAGE : " + AllGameDefinition.StoryStageType + " - " + AllGameDefinition.StoryStageCourt;
		}
		_createPrefab ();
//		Debug.Log ("game mode : " + AllGameDefinition.gameMode);
//		Debug.Log ("isHellMode? : " + AllGameDefinition.isHardMode);
//		RankingManager.Instance.setRankingData ("test11", 10000);
	}

	void _setStageArray () {
		if (AllGameDefinition.gameMode == 3) { // endless mode
			endlessEasyStageList = new int[endlessEasyMode.Length + groundMode.Length];
			Array.Copy (endlessEasyMode, endlessEasyStageList, endlessEasyMode.Length);
			Array.Copy (groundMode, 0, endlessEasyStageList, endlessEasyMode.Length, groundMode.Length);

			endlessNormalStageList = new int[endlessNormalMode.Length + groundMode.Length];
			Array.Copy (endlessNormalMode, endlessNormalStageList, endlessNormalMode.Length);
			Array.Copy (groundMode, 0, endlessNormalStageList, endlessNormalMode.Length, groundMode.Length);

			endlessHardStageList = new int[endlessHardMode.Length + groundMode.Length];
			Array.Copy (endlessHardMode, endlessHardStageList, endlessHardMode.Length);
			Array.Copy (groundMode, 0, endlessHardStageList, endlessHardMode.Length, groundMode.Length);
		} else if (AllGameDefinition.gameMode == 4) { // marathon mode
//			marathonEasyStageList = new int[marathonEasyMode.Length];
			if (AllGameDefinition.groundDifficulty == 1) {
				marathonEasyStageList = marathonEasyMode;
				marathonNormalStageList = marathonNormalMode;
				marathonHardStageList = marathonHardMode;
			} else if (AllGameDefinition.groundDifficulty == 2) {
				marathonEasyStageList = marathon2EasyMode;
				marathonNormalStageList = marathon2NormalMode;
				marathonHardStageList = marathon2HardMode;
			}
		} else {
			if (AllGameDefinition.StoryStageType == 1) {
				switch (AllGameDefinition.StoryStageCourt) {
				case 1:
					stageList = new int[easyMode.Length + groundMode.Length];
					Array.Copy (easyMode, stageList, easyMode.Length);
					Array.Copy (groundMode, 0, stageList, easyMode.Length, groundMode.Length);
					break;
				case 2:
					if (!AllGameDefinition.isHardMode) {
						stageList = new int[normalMode.Length + groundMode.Length];
						Array.Copy (normalMode, stageList, normalMode.Length);
						Array.Copy (groundMode, 0, stageList, normalMode.Length, groundMode.Length);
					} else {
						stageList = new int[hardMode.Length + groundMode.Length];
						Array.Copy (hardMode, stageList, hardMode.Length);
						Array.Copy (groundMode, 0, stageList, hardMode.Length, groundMode.Length);
					}
					break;
				case 3:
					stageList = new int[hardMode.Length + groundMode.Length];
					Array.Copy (hardMode, stageList, hardMode.Length);
					Array.Copy (groundMode, 0, stageList, hardMode.Length, groundMode.Length);
					break;
				}
			} else if (AllGameDefinition.StoryStageType == 2) {
				switch (AllGameDefinition.StoryStageCourt) {
				case 1:
					stageList = new int[easyMode.Length + planetMode.Length];
					Array.Copy (easyMode, stageList, easyMode.Length);
					Array.Copy (planetMode, 0, stageList, easyMode.Length, planetMode.Length);
					break;
				case 2:
					if (!AllGameDefinition.isHardMode) {
						stageList = new int[normalMode.Length + planetMode.Length];
						Array.Copy (normalMode, stageList, normalMode.Length);
						Array.Copy (planetMode, 0, stageList, normalMode.Length, planetMode.Length);
					} else {
						stageList = new int[hardMode.Length + planetMode.Length];
						Array.Copy (hardMode, stageList, hardMode.Length);
						Array.Copy (planetMode, 0, stageList, hardMode.Length, planetMode.Length);
					}
					break;
				case 3:
					stageList = new int[hardMode.Length + planetMode.Length];
					Array.Copy (hardMode, stageList, hardMode.Length);
					Array.Copy (planetMode, 0, stageList, hardMode.Length, planetMode.Length);
					break;
				}
//			NormalStage = NormalMode;

//			HardStage = HardMode;
			} else if (AllGameDefinition.StoryStageType == 3) {
				switch (AllGameDefinition.StoryStageCourt) {
				case 1:
					stageList = new int[easyMode.Length + seaMode.Length];
					Array.Copy (easyMode, stageList, easyMode.Length);
					Array.Copy (seaMode, 0, stageList, easyMode.Length, seaMode.Length);
					break;
				case 2:
					if (!AllGameDefinition.isHardMode) {
						stageList = new int[normalMode.Length + seaMode.Length];
						Array.Copy (normalMode, stageList, normalMode.Length);
						Array.Copy (seaMode, 0, stageList, normalMode.Length, seaMode.Length);
					} else {
						stageList = new int[hardMode.Length + seaMode.Length];
						Array.Copy (hardMode, stageList, hardMode.Length);
						Array.Copy (seaMode, 0, stageList, hardMode.Length, seaMode.Length);
					}
					break;
				case 3:
					stageList = new int[hardMode.Length + seaMode.Length];
					Array.Copy (hardMode, stageList, hardMode.Length);
					Array.Copy (seaMode, 0, stageList, hardMode.Length, seaMode.Length);
					break;
				}
				/*
			NormalStage = new int[NormalMode.Length + seaMode.Length];
			Array.Copy (NormalMode, NormalStage, NormalMode.Length);
			Array.Copy (seaMode, 0, NormalStage, NormalMode.Length, seaMode.Length);

			HardStage = new int[HardMode.Length + seaMode.Length];
			Array.Copy (HardMode, HardStage, HardMode.Length);
			Array.Copy (seaMode, 0, HardStage, HardMode.Length, seaMode.Length);
			*/
			} else if (AllGameDefinition.StoryStageType == 4) {
				switch (AllGameDefinition.StoryStageCourt) {
				case 1:
					stageList = new int[easyMode.Length + desertMode.Length];
					Array.Copy (easyMode, stageList, easyMode.Length);
					Array.Copy (desertMode, 0, stageList, easyMode.Length, desertMode.Length);
					break;
				case 2:
					if (!AllGameDefinition.isHardMode) {
						stageList = new int[normalMode.Length + desertMode.Length];
						Array.Copy (normalMode, stageList, normalMode.Length);
						Array.Copy (desertMode, 0, stageList, normalMode.Length, desertMode.Length);
					} else {
						stageList = new int[hardMode.Length + desertMode.Length];
						Array.Copy (hardMode, stageList, hardMode.Length);
						Array.Copy (desertMode, 0, stageList, hardMode.Length, desertMode.Length);
					}
					break;
				case 3:
					stageList = new int[hardMode.Length + desertMode.Length];
					Array.Copy (hardMode, stageList, hardMode.Length);
					Array.Copy (desertMode, 0, stageList, hardMode.Length, desertMode.Length);
					break;
				}
				/*
			NormalStage = new int[NormalMode.Length + desertMode.Length];
			Array.Copy (NormalMode, NormalStage, NormalMode.Length);
			Array.Copy (desertMode, 0, NormalStage, NormalMode.Length, desertMode.Length);

			HardStage = new int[HardMode.Length + desertMode.Length];
			Array.Copy (HardMode, HardStage, HardMode.Length);
			Array.Copy (desertMode, 0, HardStage, HardMode.Length, desertMode.Length);
		} else {
			NormalStage = NormalMode;

			HardStage = HardMode;
			*/
			} else if (AllGameDefinition.StoryStageType == 5) {
				stageList = new int[hardMode.Length + bossMode.Length];
				Array.Copy (hardMode, stageList, hardMode.Length);
				Array.Copy (bossMode, 0, stageList, hardMode.Length, bossMode.Length);
			}
		}

		if (isTutorial) {
			stageList = new int[tutorialMode.Length];
			stageList = tutorialMode;
		}
	}
	void _getPlayerStatus () {
		AllGameDefinition.level = PlayerPrefs.GetInt (Constants.PLAYER_LEVEL, 1);
		/*
		AllGameDefinition.yogaFireLocked = PlayerPrefs.GetInt (Constants.YOGA_FIRE_LOCK_FLAG, 0);
		AllGameDefinition.kickLocked = PlayerPrefs.GetInt (Constants.KICK_LOCK_FLAG, 0);
		AllGameDefinition.upperLocked = PlayerPrefs.GetInt (Constants.UPPER_LOCK_FLAG, 0);
		AllGameDefinition.screwLocked = PlayerPrefs.GetInt (Constants.SCREW_LOCK_FLAG, 0);
		*/
		AllGameDefinition.yogaFireLocked = 1;
		AllGameDefinition.kickLocked = 1;
		AllGameDefinition.upperLocked = 1;
		AllGameDefinition.screwLocked = 1;

		AllGameDefinition.expNextTo = PlayerPrefs.GetInt (Constants.EXP_NEXT_TO, 0);

//		Debug.Log ("nextTo : " + AllGameDefinition.expNextTo);
//		levelText.text = AllGameDefinition.level.ToString ();
//		ExpNextText.text = AllGameDefinition.expNextTo.ToString ();
	}
	public void SetDifficulty (int num) {
		// 1:easy, 2:normal, 3:hard
		AllGameDefinition.groundDifficulty = num;
		/*
		if (num == 2) {
			AllGameDefinition.fieldSpeed = 7;
		}
		*/
	}
	public void GameStart () {
		if (AllGameDefinition.gameMode == 2) {
			GameBossStart (AllGameDefinition.groundDifficulty);
		}
		player.GameStart ();
		field1.GameStart ();
		field2.GameStart ();
		field3.GameStart ();
		state = GameState.Start;
		startTime = DateTime.Now.Ticks;
	}
	public void GameBossStart (int num) {
		switch (num) {
		case 1:
			currentPoint = 9900;
			break;
		case 2:
			currentPoint = 9900;
			break;
		case 3:
			currentPoint = 19900;
			isMiddleBossEnd = true;
			break;
		default:
			break;
		}
//		GameStart ();
	}

	void FixedUpdate () {
//	protected override void ChildFixedUpdate () {
		if (state == GameState.Start && !isBoss && !isPause && !isPauseClick) {
			currentPoint += (int)(Time.deltaTime * basePoint);
			PointText.text = currentPoint.ToString () + " m";
		}
//		Debug.Log ("BossCount : " + bossCount);
//		if (currentPoint > bossCount * 3000 && !isBoss) {
		// とりあえずボス戦は2回とする。中ボス1回
		if (bossCount == 1 && currentPoint >= middleBossDistance && !isBoss) {
			StartCoroutine (bossBattle ());
			isBoss = true;
		} else if (bossCount == 2 && currentPoint >= mainBossDistance && !isBoss) {
			StartCoroutine (bossBattleWarning ());
			StartCoroutine (bossBattle ());
			isBoss = true;
		} else if (bossCount >= 3) {
			if (!isBoss && currentPoint > (mainBossDistance + ((middleBossDistance + 1000) * (bossCount - 2)))) {
				StartCoroutine (bossBattleWarning ());
				StartCoroutine (bossBattle ());
				isBoss = true;
			}
		}

		/*
		if (currentPoint > bossCount * 100 && !isBoss) {
			StartCoroutine (bossBattleWarning ());
			StartCoroutine (bossBattle ());
			isBoss = true;
		}
		*/
	}

	IEnumerator bossBattleWarning () {
		yield return new WaitForSeconds (2);
		EnemyWarningCanvas.gameObject.SetActive (true);
		iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 1f, "onupdate", "SetValue"));
	}

	void SetValue(float alpha) {
		if (alpha == 0.0f) {
			if (warningCount == 4) {
				EnemyWarningCanvas.gameObject.SetActive (false);
				warningCount = 0;
				return;
			}
			alpha = 1.0f;
			iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 1f, "onupdate", "SetValue"));
			warningCount++;
		}
		backImage.color = new Color (0, 0, 0, alpha);
		enemyWarningImage.color = new Color (255, 255, 255, alpha);
	}
	void SetValueReverse (float alpha) {
		if (alpha == 1.0f) {
//			alpha = 0.0f;
			iTween.ValueTo(gameObject, iTween.Hash("from", 0f, "to", 1f, "time", 1f, "onupdate", "SetValue"));
		}
		backImage.color = new Color (0, 0, 0, alpha);
		enemyWarningImage.color = new Color (255, 255, 255, alpha);
	}

	IEnumerator bossBattle () {
		yield return new WaitForSeconds (6);
		player.bossBattle ();
		_stopField ();
	}

	void _setOneOneBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossFireMan, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMini, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 1;
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossFireMan, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 1;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossFireMan, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMini, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			}
		}
	}

	void _setOneTwoBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossFireMan, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 1;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMini, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 1;
			} else {
				if (AllGameDefinition.gameMode == 3) {
					switch (bossCount) {
					case 3:
						bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossAliveCount = 1;
						break;
					case 4:
						bossObj = Instantiate (bossMini, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
						bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossAliveCount = 2;
						break;
					case 5:
						bossObj = Instantiate (bossMini, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
						bossObj = Instantiate (bossTako, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossAliveCount = 2;
						break;
					case 6:
						bossObj = Instantiate (bossTako, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
						bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossAliveCount = 2;
						break;
					case 7:
						bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossAliveCount = 2;
						break;
					case 8:
						bossObj = Instantiate (bossLast, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossAliveCount = 1;
						break;
					case 9:
						bossObj = Instantiate (bossTako, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossObj = Instantiate (bossLast, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossAliveCount = 2;
						break;
					case 10:
						bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossObj = Instantiate (bossLast, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossAliveCount = 2;
						break;
					default:
						bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossObj = Instantiate (bossLast, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
						bossAliveCount = 2;
						break;
					}
				}
			}
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossFireMan, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMini, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMini, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			}
		}
	}

	void _setOneThreeBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossMini, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 1;
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			}
		}
	}
	void _setTwoOneBoss(GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossFireMan, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMini, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 1;
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossFireMan, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossFireMan, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMini, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMini, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 2;
		}
	}
	void _setTwoTwoBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossMini, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 1;
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossMini, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossUchuujin, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 2;
		}
	}
	void _setTwoThreeBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossMini, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 1;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMini, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			}
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossMini, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossTako, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			}
		}
	}
	void _setThreeOneBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossFireMan, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossTako, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 1;
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossFireMan, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossFireMan, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossTako, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossTako, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 2;
		}
	}
	void _setThreeTwoBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossTako, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMoai, new Vector3 (4.0f, 0.0f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 1;
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossTako, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossTako, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossTako, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (4.0f, 0.0f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 2;
		}
	}
	void _setThreeThreeBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 1;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (4.0f, 0.0f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			}
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossUchuujin, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (4.0f, 0.0f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMoai, new Vector3 (6.0f, 0.0f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (4.0f, 0.0f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			}
		}
	}
	void _setFourOneBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMoai, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 1;
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossMoai, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMoai, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 2;
		}
	}
	void _setFourTwoBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMoai, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 1;
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossMoai, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 2;
		}
	}
	void _setFourThreeBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossMini, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 2;
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossMoai, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			} else if (bossCount == 2) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossNurikabe, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
			}
			bossAliveCount = 2;
		}
	}
	void _setFiveBoss (GameObject bossObj) {
		if (!AllGameDefinition.isHardMode) {
			if (bossCount == 1) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossMoai, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			} else {
				bossObj = Instantiate (bossLast, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 1;
			}
		} else {
			if (bossCount == 1) {
				bossObj = Instantiate (bossNurikabe, new Vector3 (8.5f, 2.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossLast, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 2;
			} else {
				bossObj = Instantiate (bossLast, new Vector3 (6.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossLast, new Vector3 (10f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossObj = Instantiate (bossLast, new Vector3 (8.5f, 0.17f, 0), Quaternion.identity) as GameObject;
				bossAliveCount = 3;
			}
		}
	}
	public void _stopField () {
		int count = 0;
		foreach (MoveField field in fieldList) {
			if (field != null) {
				field.Ready ();
				count++;
			}
		}

		if (isTutorial || state == GameState.End) {
			return;
		}

		int difficulty = AllGameDefinition.currentDifficulty;
		GameObject bossObj = null;
//		Debug.Log ("bossCount" + bossCount);
		switch (AllGameDefinition.StoryStageType) {
		case 1:
			switch (AllGameDefinition.StoryStageCourt) {
			case 1:
				_setOneOneBoss (bossObj);
				break;
			case 2:
				_setOneTwoBoss (bossObj);
				break;
			case 3:
				_setOneThreeBoss (bossObj);
				break;
			}
			break;
		case 2:
			switch (AllGameDefinition.StoryStageCourt) {
			case 1:
				_setTwoOneBoss (bossObj);
				break;
			case 2:
				_setTwoTwoBoss (bossObj);
				break;
			case 3:
				_setTwoThreeBoss (bossObj);
				break;
			}
			break;
		case 3:
			switch (AllGameDefinition.StoryStageCourt) {
			case 1:
				_setThreeOneBoss (bossObj);
					break;
			case 2:
				_setThreeTwoBoss (bossObj);
					break;
			case 3:
				_setThreeThreeBoss (bossObj);
					break;
			}
			break;
		case 4:
			switch (AllGameDefinition.StoryStageCourt) {
			case 1:
				_setFourOneBoss (bossObj);
					break;
			case 2:
				_setFourTwoBoss (bossObj);
					break;
			case 3:
				_setFourThreeBoss (bossObj);
					break;
			}
			break;
		case 5:
			_setFiveBoss (bossObj);
			break;
		default:
			bossObj = Instantiate (bossLast, new Vector3 (10.5f, 0.17f, 0), Quaternion.identity) as GameObject;
			break;
		}
//		MarathonBossController bossController = bossObj.GetComponent<MarathonBossController> ();
		return;
	}

	void _getBestScore () {
		float highScore;
		switch (AllGameDefinition.groundDifficulty) {
		case 1:
			highScore = LoadHighScore (HIGH_SCORE_KEY_EASY);
			break;
		case 2:
			highScore = LoadHighScore (HIGH_SCORE_KEY_NORMAL);
			break;
		case 3:
			highScore = LoadHighScore (HIGH_SCORE_KEY_HARD);
			break;
		default:
			highScore = 0;
			break;
		}
//		BestScoreText.text = highScore.ToString () + " m";
	}

	void SaveHighScore(string key , int score) {
		PlayerPrefs.SetInt (key , score);
		PlayerPrefs.Save ();
	}

	int LoadHighScore (string key) {
		// default 0
		return PlayerPrefs.GetInt (key, 0);
	}
	void _createStartField () {
		GameObject fieldPrefab = ResourceCache.Load ("Ground/marathonField_0");
		GameObject obj1 = Instantiate (fieldPrefab, new Vector3(-4.1f, -2.5f, 0), Quaternion.identity) as GameObject;
		field1 = obj1.GetComponent<MoveFieldInit> ();
		field1.Ready ();
		GameObject obj2 = Instantiate (fieldPrefab, new Vector3 (5.8f, -2.5f, 0), Quaternion.identity) as GameObject;
		field2 = obj2.GetComponent<MoveFieldInit> ();
		field2.Ready ();

		fieldPrefab = ResourceCache.Load ("Ground/marathonField_1");
		GameObject obj3 = Instantiate (fieldPrefab, new Vector3 (15.7f, -2.5f, 0), Quaternion.identity) as GameObject;
		field3 = obj3.GetComponent<MoveField> ();
		field3.Ready ();

		SpriteRenderer render1 = obj1.GetComponent<SpriteRenderer> ();
		SpriteRenderer render2 = obj2.GetComponent<SpriteRenderer> ();
		SpriteRenderer render3 = obj3.GetComponent<SpriteRenderer> ();

		GameObject backGroundObj = null;

		switch(AllGameDefinition.StoryStageType) {
		case 2:
			backGroundObj = ResourceCache.Load ("Ground2/PlanetBackGround");
			Instantiate (backGroundObj, new Vector3(4.709632f, -2.347472f, 0), Quaternion.identity);
			render1.sprite = spaceField1;
			render2.sprite = spaceField1;
			render3.sprite = spaceField1;
			break;
		case 3:
			backGroundObj = ResourceCache.Load ("Ground2/SeaBackGround");
			Instantiate (backGroundObj, new Vector3(4.709632f, -2.347472f, 0), Quaternion.identity);
			render1.sprite = seaField1;
			render2.sprite = seaField1;
			render3.sprite = seaField1;
			break;
		case 4:
			backGroundObj = ResourceCache.Load ("Ground2/DesertBackGround");
			Instantiate (backGroundObj, new Vector3(4.709632f, -2.347472f, 0), Quaternion.identity);
			render1.sprite = desertField1;
			render2.sprite = desertField1;
			render3.sprite = desertField1;
			break;
		case 5:
			Camera.main.backgroundColor = new Color (0, 0, 0);
			backGroundObj = ResourceCache.Load ("Ground2/BossBackGround");
			Instantiate (backGroundObj, new Vector3(4.709632f, -2.347472f, 0), Quaternion.identity);
			render1.sprite = bossField1;
			render2.sprite = bossField1;
			render3.sprite = bossField1;
			break;
		default:
			backGroundObj = ResourceCache.Load ("Ground2/SkyBackGround");
			Instantiate (backGroundObj, new Vector3(4.709632f, -2.347472f, 0), Quaternion.identity);
			render1.sprite = groundField1;
			render2.sprite = groundField1;
			render3.sprite = groundField1;
			break;
		}
	}

	void _createPrefab () {
		if (AllGameDefinition.gameMode == 3) { // endless mode
			for (int a = 0; a < endlessEasyStageList.Length; a++) {
				string prefabName = "Ground/marathonField_" + endlessEasyStageList [a];
				GameObject fieldPrefab1 = ResourceCache.Load (prefabName);
				EndlessEasyFieldObjectList.Add (endlessEasyStageList [a], fieldPrefab1);
			}
			for (int b = 0; b < endlessNormalStageList.Length; b++) {
				string prefabName = "Ground/marathonField_" + endlessNormalStageList [b];
				GameObject fieldPrefab2 = ResourceCache.Load (prefabName);
				EndlessNormalFieldObjectList.Add (endlessNormalStageList [b], fieldPrefab2);
			}
			for (int c = 0; c < endlessHardStageList.Length; c++) {
				string prefabName = "Ground/marathonField_" + endlessHardStageList [c];
				GameObject fieldPrefab3 = ResourceCache.Load (prefabName);
				EndlessHardFieldObjectList.Add (endlessHardStageList [c], fieldPrefab3);
			}
		} else if (AllGameDefinition.gameMode == 4) { // marathon mode
			for (int d = 0; d < marathonEasyStageList.Length; d++) {
				string prefabName = "Ground/marathonField_" + marathonEasyStageList [d];
				GameObject fieldPrefab4 = ResourceCache.Load (prefabName);
				MarathonEasyFieldObjectList.Add (marathonEasyStageList [d], fieldPrefab4);
			}
			for (int e = 0; e < marathonNormalStageList.Length; e++) {
				string prefabName = "Ground/marathonField_" + marathonNormalStageList [e];
				GameObject fieldPrefab5 = ResourceCache.Load (prefabName);
				MarathonNormalFieldObjectList.Add (marathonNormalStageList [e], fieldPrefab5);
			}
			for (int f = 0; f < marathonHardStageList.Length; f++) {
				string prefabName = "Ground/marathonField_" + marathonHardStageList [f];
				GameObject fieldPrefab6 = ResourceCache.Load (prefabName);
				MarathonHardFieldObjectList.Add (marathonHardStageList [f], fieldPrefab6);
			}
		} else {
			if (isTutorial) { // prefab place diffarent
				for (int i = 0; i < stageList.Length; i++) {
					string prefabName = "Tutorial/Ground/marathonField_" + stageList [i];
					GameObject fieldPrefab = ResourceCache.Load (prefabName);
//					Debug.Log ("###############");
					FieldObjectList.Add (stageList [i], fieldPrefab);
				}
			} else {
				for (int i = 0; i < stageList.Length; i++) {
					string prefabName = "Ground/marathonField_" + stageList [i];
					GameObject fieldPrefab = ResourceCache.Load (prefabName);
					FieldObjectList.Add (stageList [i], fieldPrefab);
				}
			}
		}
		/*
		prefabList = new GameObject[ENEMY_NUM];
		for (int i = 0; i <= ENEMY_NUM - 1; i++) {
			int num = i + 1;
			string prefabName = "Ground/marathonField_" + num;
			GameObject fieldPrefab = ResourceCache.Load(prefabName);
//			prefabList [i] = fieldPrefab;
			FieldObjectList.Add (FieldObjectList[i], fieldPrefab);
		}
		*/
	}

	public void PauseClick (bool isPause) {
//		Debug.Log ("PauseClick");
		if (isPause) {
			isPauseClick = isPause;
			Time.timeScale = 0;
			return;
		}

		if (isPauseClick) {
			isPauseClick = false;
			Time.timeScale = 1;
//			_restartField ();
//			player.unPauseGame ();
		} else {
			isPauseClick = true;
			Time.timeScale = 0;
//			_pauseField ();
//			player.pauseGame ();
		}
//		base.SetPauseStatus (isPauseClick);
	}
	void _pauseField () {
		foreach (MoveField field in fieldList) {
			if (field != null) {
				field.Ready ();
				player.SetReadyState ();
			}
		}
	}
	public void _restartField () {
		foreach (MoveField field in fieldList) {
			if (field != null) {
				field.GameStart ();
				player.SetStartState ();
			}
		}
	}

	int fieldCount = 0;
	int tutorialFieldCount = 0;
	bool isAllHole;
//	int debugFieldCount = ENEMY_NUM;
//	int debugFieldCount = 1;
	public void CreateNextField () {
		
		if (state != GameState.Ready) {
			/*
			int num = _getFieldName ();	
			string prefabName = "marathonField_" + num;
			GameObject fieldPrefab = (GameObject)Resources.Load (prefabName);
			*/
			int num;
			float playerPos = player.getPositionY ();
			if (state == GameState.End) {
				num = 1;
			} else if (playerPos <= -5 && !isTutorial) {
				num = RandomUtils.Random (HoleUp);
			} else {
				if (AllGameDefinition.gameMode == 3) {
					if (bossCount == 1 || bossCount == 2) {
						num = RandomUtils.Random (endlessEasyStageList);
					} else if (bossCount == 3 || bossCount == 4) {
						num = RandomUtils.Random (endlessNormalStageList);
					} else {
						num = RandomUtils.Random (endlessHardStageList);
					}
				} else if (AllGameDefinition.gameMode == 4) {
					if (currentPoint < 5000) {
						num = RandomUtils.Random (marathonEasyStageList);
					} else if (currentPoint >= 5000 && currentPoint < 15000) {
						num = RandomUtils.Random (marathonNormalStageList);
					} else {
						num = RandomUtils.Random (marathonHardStageList);
					}
				} else {
					if (isTutorial) {
						num = tutorialNumList[tutorialFieldCount];
					} else {
						num = RandomUtils.Random (stageList);
					}
				}
				if (num == 21 && !isAllHole) {
//					Debug.Log ("no renzoku all hole");
					num = 2;
					isAllHole = true;
				} else {
					isAllHole = false;
				}

// ################## FOR DEBUG #######################
//				num = debugFieldCount;
//				num = 70;
//				int aaa = UnityEngine.Random.Range (1, 4);
//				if (aaa == 1) {
//					num = 12;
//				} else if (aaa == 2) {
//					num = 39;
//				} else {
//					num = 37;
//				}
//				debugFieldCount++;
// ################## FOR DEBUG #######################

				/*
				if (AllGameDefinition.groundDifficulty == 1) {
//			if (currentPoint < 10000) {
//				num = UnityEngine.Random.Range (1, ENEMY_NUM);
					num = 4;
					num = _getFieldName ();
				} else {
					num = _getFieldNameHard ();
				num = UnityEngine.Random.Range (1, ENEMY_NUM);
				}
			*/
			}

			try {
				/*
				if (!isBoss) {
					Instantiate (prefabList[num-1], new Vector3 (15.7f, -2.5f, 0), Quaternion.identity);
				} else {
				*/
				if (fieldCount > 3) {
					fieldCount = 0;
				}
				if (isBoss) {
					num = 1;
				}
				// ********************
				// for debug
//				num = debugFieldCount--;
				// ********************
//				GameObject obj = Instantiate (prefabList[num-1], new Vector3 (15.7f, -2.5f, 0), Quaternion.identity) as GameObject;

				GameObject obj;
				if (AllGameDefinition.gameMode == 3) {
					if (bossCount == 1 || bossCount == 2) {
						obj = Instantiate (EndlessEasyFieldObjectList[num], new Vector3 (17.7f, -2.5f, 0), Quaternion.identity) as GameObject;
					} else if (bossCount == 3 || bossCount == 4) {
						obj = Instantiate (EndlessNormalFieldObjectList[num], new Vector3 (17.7f, -2.5f, 0), Quaternion.identity) as GameObject;
					} else {
						obj = Instantiate (EndlessHardFieldObjectList[num], new Vector3 (17.7f, -2.5f, 0), Quaternion.identity) as GameObject;
					}
				} else if (AllGameDefinition.gameMode == 4) {
					if (currentPoint < 5000) {
						obj = Instantiate (MarathonEasyFieldObjectList[num], new Vector3 (17.7f, -2.5f, 0), Quaternion.identity) as GameObject;
					} else if (currentPoint >= 5000 && currentPoint < 15000) {
						obj = Instantiate (MarathonNormalFieldObjectList[num], new Vector3 (17.7f, -2.5f, 0), Quaternion.identity) as GameObject;
					} else {
						obj = Instantiate (MarathonHardFieldObjectList[num], new Vector3 (17.7f, -2.5f, 0), Quaternion.identity) as GameObject;
					}
				} else {
					obj = Instantiate (FieldObjectList[num], new Vector3 (17.7f, -2.5f, 0), Quaternion.identity) as GameObject;
				}

				if (num == 45) {
//					bubble = ResourceCache.Load("Ground/bubble");
					Instantiate (bubble, new Vector3(11, 0, 0), Quaternion.identity);
				}
				if (num == 48) {
//					rains = ResourceCache.Load("Ground/rains");
					Instantiate (rains, new Vector3(0, 9, 0), Quaternion.identity);
				}
				if (num == 62) {
//					sandStorm = ResourceCache.Load ("Ground/sandStorm");
					Instantiate (sandStorm, new Vector3 (53, 0.5f, 0), Quaternion.identity);
				}
				// change background sprite
				if (!isTutorial) {
					_setBackGround (obj, num);
				}

				MoveField moveField = obj.GetComponent<MoveField>();
				fieldList [fieldCount] = moveField;
				fieldCount++;
				tutorialFieldCount++;
//				Debug.Log("tutorialFieldCount : " + tutorialFieldCount);
			} catch (Exception e) {
				Debug.Log (e);
				Debug.Log ("num : " + num);
			}
//			MoveField field = obj.GetComponent<MoveField> ();
//			Debug.Log (prefabName);
//			field.SetPlayer (player);
		}
	}

	/**
	 * change back ground sprite
	 * 
	 * 
	 **/
	void _setBackGround (GameObject obj, int num) {
//		Debug.Log ("story type : " + AllGameDefinition.StoryStageType);
//		Debug.Log ("stage type : " + AllGameDefinition.StoryStageCourt);
		SpriteRenderer render = obj.GetComponent<SpriteRenderer> ();
		int stageType = AllGameDefinition.StoryStageType;
		switch (stageType) {
		case 2:
			commonField1 = spaceField1;
			commonField2 = spaceField2;
			commonField3 = spaceField3;
			commonField4 = spaceField4;
			commonField5 = spaceField5;
			commonField6 = spaceField6;
			commonField7 = spaceField7;
			commonField8 = spaceField8;
			commonField20 = spaceField20;
			commonField21 = spaceField21;
			commonField22 = spaceField22;
			commonField23 = spaceField23;
			commonField24 = spaceField24;
			break;
		case 3:
			commonField1 = seaField1;
			commonField2 = seaField2;
			commonField3 = seaField3;
			commonField4 = seaField4;
			commonField5 = seaField5;
			commonField6 = seaField6;
			commonField7 = seaField7;
			commonField8 = seaField8;
			break;
		case 4:
			commonField1 = desertField1;
			commonField2 = desertField2;
			commonField3 = desertField3;
			commonField4 = desertField4;
			commonField5 = desertField5;
			commonField6 = desertField6;
			commonField7 = desertField7;
			commonField8 = desertField8;
			break;
		case 5:
			commonField1 = bossField1;
			commonField2 = bossField2;
			commonField3 = bossField3;
			commonField4 = bossField4;
			commonField5 = bossField5;
			commonField6 = bossField6;
			commonField7 = bossField7;
			commonField8 = bossField8;
			break;
		default:
			commonField1 = groundField1;
			commonField2 = groundField2;
			commonField3 = groundField3;
			commonField4 = groundField4;
			commonField5 = groundField5;
			commonField6 = groundField6;
			commonField7 = groundField7;
			commonField8 = groundField8;
			commonField20 = groundField20;
			commonField21 = groundField21;
			commonField22 = groundField22;
			commonField23 = groundField23;
			commonField24 = groundField24;
			break;
		}

		if (commonField1 == null || render.sprite != null) {
			return;
		}

		switch(num) {
		case 2:
			render.sprite = commonField2; // center hole
			break;
		case 3:
		case 77:
			render.sprite = commonField4; // left and right hole
			break;
		case 8:
			render.sprite = commonField3; // right hole
			break;
		case 21:
		case 40:
		case 41:
		case 43:
		case 45:
		case 53:
		case 54:
		case 55:
		case 70:
		case 71:
		case 80:
		case 100:
		case 101:
			render.sprite = commonField5; // all hole
			break;
		case 6:
		case 29:
		case 30:
			render.sprite = commonField6; // almost hole
			break;
		case 32:
			render.sprite = snowGroundField; // snow ground
			break;
		case 49:
		case 69:
			render.sprite = commonField7; // hosoi
			break;
		case 50:
			render.sprite = commonField8; // hosoi and cloud
			break;
		case 104:
		case 105:
		case 106:
		case 107:
		case 108:
			render.sprite = commonField20;
			break;
		case 109:
		case 110:
		case 111:
		case 112:
		case 113:
			render.sprite = commonField21;
			break;
		case 114:
		case 115:
		case 116:
		case 117:
		case 118:
			render.sprite = commonField22;
			break;
		case 119:
		case 120:
		case 121:
		case 122:
		case 123:
			render.sprite = commonField23;
			break;
		case 124:
		case 125:
		case 126:
		case 127:
		case 128:
			render.sprite = commonField24;
			break;
		default:
			render.sprite = commonField1;
			break;
		}

		/* *************************************************
				 * 背景色の変更
				 * 
				 * 
				 * 
				 * 
				SpriteRenderer render = obj.GetComponent<SpriteRenderer> ();
				render.sprite = testField;
				*
				*
				*
				*
				*
				*
				***************************************************/
	}

	int _getFieldName () {
		switch(UnityEngine.Random.Range (0, 8)) {
		case 1: // easy
			return _getEasyMode();
		case 2: // easy
			return _getEasyMode();
		case 3: // easy
			return _getEasyMode();
		case 4: // easy
			return _getEasyMode();
		case 5: // normal
			return _getEasyMode();
		case 6: // normal
			return _getEasyMode();
		case 7: // hard
			return _getNormalMode();
		default:
			return 1;
		}
	}

	int _getFieldNameNormal () {
		switch(UnityEngine.Random.Range (0, 8)) {
		case 1: // easy
			return _getEasyMode();
		case 2: // easy
			return _getEasyMode();
		case 3: // easy
			return _getEasyMode();
		case 4: // easy
			return _getNormalMode();
		case 5: // normal
			return _getNormalMode();
		case 6: // normal
			return _getNormalMode();
		case 7: // hard
			return _getHardMode();
		default:
			return 1;
		}
	}

	int _getFieldNameHard () {
		switch(UnityEngine.Random.Range (0, 10)) {
		case 1: // easy
			return _getEasyMode();
		case 2: // easy
			return _getEasyMode();
		case 3: // normal
			return _getEasyMode();
		case 4: // normal
			return _getNormalMode();
		case 5: // normal
			return _getNormalMode ();
		case 6: // hard
			return _getNormalMode ();
		case 7: // hard
			return _getHardMode ();
		case 8:
			return _getHardMode ();
		case 9:
			return _getRareMode ();
		default:
			return 1;
		}
	}
	int _getFieldNameVeryHard () {
		switch(UnityEngine.Random.Range (0, 10)) {
		case 1: // easy
			return _getEasyMode();
		case 2: // normal
			return _getEasyMode();
		case 3: // normal
			return _getNormalMode();
		case 4: // normal
			return _getNormalMode ();
		case 5: // hard
			return _getHardMode ();
		case 6: // hard
			return _getHardMode ();
		case 7: // hard
			return _getHardMode ();
		case 8:
			return _getHardMode ();
		case 9:
			return _getRareMode ();
		default:
			return 1;
		}
	}

	int _getEasyMode () {
		return 1;
//		return RandomUtils.Random (EasyMode);
	}
	int _getNormalMode () {
		return 1;
//		return RandomUtils.Random (NormalStage);
	}
	int _getHardMode () {
		return 1;
//		return RandomUtils.Random (HardStage);
	}
	int _getRareMode () {
		return 1;
//		return RandomUtils.Random (RareMode);
	}

	public void sendPosition (float positionX) {
		posX = positionX - (-0.51f);
	}
	IEnumerator GameEndAndStopField () {
		yield return new WaitForSeconds (1.5f);
		_stopField ();
	}
	public void GameOver () {
		state = GameState.End;
		StartCoroutine (GameEndAndStopField ());
//		finishTime = (DateTime.Now.Ticks - startTime) / 100000;
		PointText.text = currentPoint.ToString() + " m";
		actionPoint = player.getActionPoint ();
		totalPoint = currentPoint + (actionPoint * 10);
		bool isRankUp = false;
		if (AllGameDefinition.gameMode != 2) {
//			_bestScoreUpdate ();
			totalDistance += currentPoint;
			SaveHighScore (Constants.TOTAL_DISTANCE, totalDistance);
			isRankUp = JudgeRankUp ();
//			Debug.Log ("level : " + AllGameDefinition.level);

			if (AllGameDefinition.gameMode == 3) {
				int endlessBestScore = PlayerPrefs.GetInt ("endlessBestScore", 0);
				if (endlessBestScore < currentPoint) {
					SaveHighScore ("endlessBestScore", currentPoint);
					string name = PlayerPrefs.GetString ("nameKey", null);

					if (name != null) {
						RankingManager.Instance.setEndlessRankingData (name, currentPoint);
					}
				}
			}
			if (AllGameDefinition.gameMode == 4) {
				int marathonBestScore = 10000;
				string highScoreKey = "";
				if (AllGameDefinition.groundDifficulty == 1) {
					highScoreKey = "marathonBestScore";
				} else if (AllGameDefinition.groundDifficulty == 2) {
					highScoreKey = "marathonHardBestScore";
				}
				marathonBestScore = PlayerPrefs.GetInt (highScoreKey, 0);
				if (marathonBestScore < currentPoint) {
					SaveHighScore (highScoreKey, currentPoint);
					string name = PlayerPrefs.GetString ("nameKey", null);

					if (name != null) {
						RankingManager.Instance.setMarathonRankingData (name, currentPoint, AllGameDefinition.groundDifficulty);
					}
				}
			}
//			RankingManager.Instance.SendRanking (RankingManager.RankingType.MARATHON_TOTAL_DISTANCE, totalDistance);
		}
		if (!isRankUp) {
			StartCoroutine (GameoverCanvas ());
		} else {
			StartCoroutine (RankUpCanvas (0));
		}
	}
	bool JudgeRankUp () {
		int currentLevel = AllGameDefinition.level;
		int level = currentLevel;
		int next;
//		Debug.Log ("current level : " + currentLevel);
		while (true) {
			next = expTable [level];
//			Debug.Log ("next : " + next);
//			Debug.Log ("totalDistance : " + totalDistance);
			if (totalDistance < next) {
				break;
			}
			level++;
		}

		int newestLevel = level;
//		Debug.Log ("newestLevel : " + newestLevel);
		int nextTo = next - totalDistance;
//		Debug.Log ("nextTo : " + nextTo);
		PlayerPrefs.SetInt (Constants.EXP_NEXT_TO, nextTo);
		AllGameDefinition.expNextTo = nextTo;
		if (newestLevel > currentLevel) {
			// level up
			PlayerPrefs.SetInt(Constants.PLAYER_LEVEL, newestLevel);
			AllGameDefinition.level = newestLevel;
			unLockAction (newestLevel);
			return true;
		}
		return false;
	}
	void unLockAction (int level) {
		if (level > 5 && level < 10) {
			if (AllGameDefinition.yogaFireLocked == 1) {
				return;
			}
			if (UnityEngine.Random.Range (1, 6) == 1) {
				PlayerPrefs.SetInt (Constants.YOGA_FIRE_LOCK_FLAG, 1);
				AllGameDefinition.yogaFireLocked = 1;
			}
		} else if (level == 10) {
			if (AllGameDefinition.yogaFireLocked == 1) {
				return;
			}
			AllGameDefinition.yogaFireLocked = 1;
		} else if (level > 10 && level <= 20) {
			if (AllGameDefinition.kickLocked == 1) {
				return;
			}
			switch (level) {
			case 15:
				if (UnityEngine.Random.Range (1, 6) == 1) {
					PlayerPrefs.SetInt(Constants.KICK_LOCK_FLAG, 1);
					AllGameDefinition.kickLocked = 1;
				}
				break;
			case 16:
				if (UnityEngine.Random.Range (1, 6) == 1) {
					PlayerPrefs.SetInt(Constants.KICK_LOCK_FLAG, 1);
					AllGameDefinition.kickLocked = 1;
				}
				break;
			case 17:
				if (UnityEngine.Random.Range (1, 5) == 1) {
					PlayerPrefs.SetInt(Constants.KICK_LOCK_FLAG, 1);
					AllGameDefinition.kickLocked = 1;
				}
				break;
			case 18:
				if (UnityEngine.Random.Range (1, 4) == 1) {
					PlayerPrefs.SetInt(Constants.KICK_LOCK_FLAG, 1);
					AllGameDefinition.kickLocked = 1;
				}
				break;
			case 19:
				if (UnityEngine.Random.Range (1, 3) == 1) {
					PlayerPrefs.SetInt(Constants.KICK_LOCK_FLAG, 1);
					AllGameDefinition.kickLocked = 1;
				}
				break;
			case 20:
				PlayerPrefs.SetInt(Constants.KICK_LOCK_FLAG, 1);
				AllGameDefinition.kickLocked = 1;
				break;
			default:
				if (UnityEngine.Random.Range (1, 11) == 1) {
					PlayerPrefs.SetInt(Constants.KICK_LOCK_FLAG, 1);
					AllGameDefinition.kickLocked = 1;
				}
				break;
			}
		} else if (level > 20 && level <= 30) {
		} else if (level > 40 && level <= 50) {
		}
	}
	IEnumerator GameoverCanvas () {
		yield return new WaitForSeconds (3);
//		AchieveRatio.text = "distance : " + currentPoint + " m  +  Point : " + actionPoint + " × 10 = " + totalPoint;
		AchieveRatio.text = "distance : " + currentPoint + " m  ";
//		AchieveRatio.text = (currentPoint * 100 / 20000).ToString () + "%";
		ActionCanvas.enabled = false;
		GameOverCanvas.enabled = true;
		GameOverCanvas.GetComponent<GuiAnimationManager> ().MoveTextList ();
		GameOverCanvas.GetComponent<GuiAnimationManager> ().MoveImageList ();
//		GameObject gameOverCanvas = ResourceCache.Load ("Ground/GameOverCanvas");
//		Instantiate (gameOverCanvas, new Vector3 (138.5f, 352.5f, 0), Quaternion.identity);
//		Debug.Log ("GameoverCanvas###");
		StartCoroutine(GoBackDifficultyCanvas (4));
	}

	IEnumerator RankUpCanvas (int type) { // 0:gameover, 1:complete
		yield return new WaitForSeconds (3);
		LevelUpController.GetComponent<MarathonLevelUpController>().enabled = true;
		ActionCanvas.enabled = false;
		LevelUpCanvas.enabled = true;
		LevelUpCanvas.GetComponent<GuiAnimationManager> ().MoveTextList ();
		LevelUpCanvas.GetComponent<GuiAnimationManager> ().MoveImageList ();

		if (type == 0) {
			StartCoroutine (GameoverCanvas ());
		} else if (type == 1) {
			StartCoroutine (GoStageCompleteCanvas ());
		}
//		StartCoroutine(GoBackDifficultyCanvas ());
	}

	IEnumerator GoStageCompleteCanvas () {
		yield return new WaitForSeconds (3);
		StageCompleteCanvas.enabled = true;
		StageCompleteCanvas.transform.Find ("StageCompleteGameObject").GetComponent<MarathonStoryComplete> ().enabled = true;
	}

	public void nextStage (int difficulty) { //0:easy, 1:hard
		bossAliveCount --;
//		Debug.Log ("bossAliveCount" + bossAliveCount);
		if (bossAliveCount != 0) {
			return;
		}

//		Debug.Log("gameMode : " + AllGameDefinition.gameMode);
		if (AllGameDefinition.gameMode == 2) {
			SceneManager.LoadScene ("Difficulty");
			return;
		}
		difficulty = AllGameDefinition.groundDifficulty;
//		Debug.Log("GameCtrl nextStage : " + difficulty);
		bossCount++;
//		isMiddleBossEnd = true;

		AllGameDefinition.bossCount = bossCount;

		switch (AllGameDefinition.groundDifficulty) {
		case 1:
//			Debug.Log ("easy");
			if (UnityEngine.Random.Range (1, 3) == 1) {
				AllGameDefinition.currentDifficulty++;
			}
			break;
		case 2:
//			Debug.Log ("normal");
			AllGameDefinition.currentDifficulty++;
			break;
		case 3:
//			Debug.Log ("hard");
			AllGameDefinition.currentDifficulty += 2;
			break;
		default:
			break;
		}
//		Debug.Log ("CurrentDifficulty : " + AllGameDefinition.currentDifficulty);
//		if (difficulty == 1) {
//		foreach (MoveField field in fieldList) {
//			if (field != null) {
//				field.GameStart ();
//			}
//		}
//		fieldList = new MoveField[4];
//		fieldCount = 0;
		if (bossCount >= 3) {
			if (AllGameDefinition.gameMode == 1) {
				totalPoint = currentPoint + (actionPoint * 10);

				// bonus star count
				_setBonusStar ();

				// point count
				_setTotalPoint ();

				// stage clear
				switch (AllGameDefinition.CharacterType) {
				case 1:
					PlayerPrefs.SetInt (Constants.MINI_NURIKABE_LOCK_FLAG, 1);
//				AllGameDefinition.miniNurikabeUnlocked = true;
					break;
				case 2:
					PlayerPrefs.SetInt (Constants.NURIKABE_LOCK_FLAG, 1);
//				AllGameDefinition.nurikabeUnLocked = true;
					break;
				case 3:
					PlayerPrefs.SetInt (Constants.BLACK_HERO_LOCK_FLAG, 1);
//				AllGameDefinition.blackHeroUnlocked = true;
					break;
				case 4: // GAME CLEAR!!!!!!!
//				AllGameDefinition.lastBossUnlocked = true;
					break;
				default:
					break;
				}
			} else { // endless mode
				_continueGame ();
//				Debug.Log ("game star 1");
//				foreach (MoveField field in fieldList) {
//					if (field != null) {
//						field.GameStart ();
//					}
//				}
//				fieldList = new MoveField[4];
//				fieldCount = 0;
//				state = GameState.Start;
//				player.addActionPoint (20);
//				isBoss = false;
			}
		} else { // middle boss clear
			_continueGame ();
//			foreach (MoveField field in fieldList) {
//				if (field != null) {
//					field.GameStart ();
//				}
//			}
//			fieldList = new MoveField[4];
//			fieldCount = 0;
//			Debug.Log ("game star 2");
//			state = GameState.Start;
//			player.addActionPoint (20);
//			isBoss = false;
		}

		if (bossCount == 3 && AllGameDefinition.gameMode == 1) {
			// clear!!
			bool isRankUp;
			totalDistance += currentPoint;
			SaveHighScore (Constants.TOTAL_DISTANCE, totalDistance);
			isRankUp = JudgeRankUp ();

			if (isRankUp) {
				StartCoroutine (RankUpCanvas (1));
			} else {
				StartCoroutine (GoStageCompleteCanvas ());
			}
		}

	}

	void _continueGame () {
		/*
		 * performance tuning
		 * 
		 */
		//GC
		System.GC.Collect ();
		// unuse asset unload
		Resources.UnloadUnusedAssets();

		StartCoroutine (ContinueGame ());
	}

	IEnumerator ContinueGame () {
		yield return new WaitForSeconds (3);
		foreach (MoveField field in fieldList) {
			if (field != null) {
				field.GameStart ();
			}
		}
		fieldList = new MoveField[4];
		fieldCount = 0;
//		Debug.Log ("game star 2");
		state = GameState.Start;
		player.addActionPoint (20);
		isBoss = false;
	}

	IEnumerator GoBackDifficultyCanvas (int num = 2) {
//		Debug.Log ("Difficulty#####1");
		yield return new WaitForSeconds (num);
//		Debug.Log ("Difficulty#####2");

		if (isTutorial) {
			SceneManager.LoadScene ("Difficulty");
		} else {
			if (UnityEngine.Random.Range (1, 6) == 1) {
				ShowAd ();
				SceneManager.LoadScene ("Difficulty");
			} else {
				SceneManager.LoadScene ("Difficulty");
			}
		}
	}

	void _setTotalPoint () {
		string key;
		if (!AllGameDefinition.isHardMode) {
			key = AllGameDefinition.StoryStageType.ToString() + "-" + AllGameDefinition.StoryStageCourt.ToString() + "-highestPoint";
		} else {
			key = AllGameDefinition.StoryStageType.ToString() + "-" + AllGameDefinition.StoryStageCourt.ToString() + "-highestPoint-hard";
		}
		int highestPoint = PlayerPrefs.GetInt (key, 0);
//		Debug.Log ("highestPoint : " + highestPoint);
//		Debug.Log ("battlePoint : " + battlePoint);
		if (battlePoint > highestPoint) {
			PlayerPrefs.SetInt (key, battlePoint);
			int totalPoint = PlayerPrefs.GetInt ("totalPoint", 0);
			totalBestPoint = totalPoint + battlePoint;
			PlayerPrefs.SetInt ("totalPoint", totalBestPoint);
			PlayerPrefs.Save ();
			string name = PlayerPrefs.GetString ("nameKey", null);

//			UserAuth.Instance.setRankingData (name, battlePoint);
//			Debug.Log("name : " + name);
//			Debug.Log ("point : " + totalBestPoint);
			RankingManager.Instance.setRankingData (name, totalBestPoint);
		}
	}

	void _setBonusStar () {
		string key;
		if (!AllGameDefinition.isHardMode) {
			key = AllGameDefinition.StoryStageType.ToString () + "-" + AllGameDefinition.StoryStageCourt.ToString () + "-starCount";
		} else {
			key = AllGameDefinition.StoryStageType.ToString () + "-" + AllGameDefinition.StoryStageCourt.ToString () + "-starCount-hard";
		}
//		Debug.Log ("bonus star key : " + key);

		int bonusPoint = PlayerPrefs.GetInt (key, 0);

		int twoStarPoint = 5000;
		int threeStarPoint = 100000;
		int stageType = AllGameDefinition.StoryStageType;
		int courtType = AllGameDefinition.StoryStageCourt;

		if (stageType == 1) {
			if (courtType == 1) {
				twoStarPoint = 3000;
				threeStarPoint = 5000;
			} else if (courtType == 2) {
				twoStarPoint = 3000;
				threeStarPoint = 5000;
			} else if (courtType == 3) {
				twoStarPoint = 5000;
				threeStarPoint = 10000;
			}
		} else if (stageType == 2) {
			if (courtType == 1) {
				twoStarPoint = 3000;
				threeStarPoint = 5000;
			} else if (courtType == 2) {
				twoStarPoint = 3000;
				threeStarPoint = 5000;
			} else if (courtType == 3) {
				twoStarPoint = 5000;
				threeStarPoint = 10000;
			}
		} else if (stageType == 3) {
			if (courtType == 1) {
				twoStarPoint = 5000;
				threeStarPoint = 10000;
			} else if (courtType == 2) {
				twoStarPoint = 5000;
				threeStarPoint = 10000;
			} else if (courtType == 3) {
				twoStarPoint = 10000;
				threeStarPoint = 15000;
			}
		} else if (stageType == 4) {
			if (courtType == 1) {
				twoStarPoint = 10000;
				threeStarPoint = 15000;
			} else if (courtType == 2) {
				twoStarPoint = 20000;
				threeStarPoint = 30000;
			} else if (courtType == 3) {
				twoStarPoint = 30000;
				threeStarPoint = 50000;
			}
		} else if (stageType == 5) {
			if (courtType == 1) {
				twoStarPoint = 50000;
				threeStarPoint = 70000;
			}
		}
		if (AllGameDefinition.isHardMode) {
			twoStarPoint = twoStarPoint * 2;
			threeStarPoint = threeStarPoint * 3;
		}
//		Debug.Log ("battlePoint : " + battlePoint);
//		Debug.Log ("twoStarPoint : " + twoStarPoint);
//		Debug.Log ("threeStarPoint : " + threeStarPoint);
		if (battlePoint > threeStarPoint) {
			PlayerPrefs.SetInt (key, 3);
		} else if (totalPoint > twoStarPoint) {
			PlayerPrefs.SetInt (key, 2);
		} else {
			PlayerPrefs.SetInt (key, 1);
		}
		PlayerPrefs.Save ();
	}
	bool canHissatuwaza;
	public void AddPoint (int point) {
		if (state != GameState.End) {
			battlePoint += point;
		}
		battlePointText.text = battlePoint.ToString () + " P";
		if (!isTutorial || AllGameDefinition.gameMode != 4) {
			wazaSlider.value++;
			if (!canHissatuwaza && wazaSlider.value == 10) {
				canHissatuwaza = true;
				wazaSlider.value = 0;
				sliderImage.gameObject.SetActive (false);
				wazaSlider.gameObject.SetActive (false);
				wazaButton.gameObject.SetActive (true);
//				iTween.ColorTo(wazaButton.gameObject, iTween.Hash("color", new Color(0,0,0,255), "time", 1.0f, "looptype", iTween.LoopType.pingPong));
				iTween.ValueTo (gameObject, iTween.Hash ("from", 1f, "to", 0f, "time", 0.5f, "onupdate", "SetWazaButtonColorValue"));
			}
		}
		/*
		if (battlePoint == 3) {
			player.setEffect ();
		}
		*/
	}

	void SetWazaButtonColorValue(float imgColor) {
//		Debug.Log (imgColor);
		if (imgColor == 0f) {
			imgColor = 255f;
			iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 0.5f, "onupdate", "SetWazaButtonColorValue"));
		}
		wazaButton.GetComponent<Image>().color = new Color (255, imgColor, imgColor);
	}

	public void FinishHissatuWaza () {
		sliderImage.gameObject.SetActive (true);
		wazaSlider.gameObject.SetActive (true);
		wazaButton.gameObject.SetActive (false);
		wazaSlider.value = 0;
		canHissatuwaza = false;
	}

	bool isJumpMissed;
	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player" && state != GameState.End) {
			if (isTutorial) {
				isJumpMissed = true;
				_stopField ();
				player.lifeMinus ();
				_reStartJump ();
				return;
			}
			if (player.getLifePoint() > 1) {
				player.lifeMinus ();
				player.fallout ();
				if (player.getLifePoint() < 1) {
					GameOver ();
				}
			} else {
				GameOver ();
			}
		}
	}

	public void GoPlanet () {
		AllGameDefinition.currentPoint = currentPoint;
		AllGameDefinition.totalDistance = totalDistance;
	}

	public void SetBlackHoleFlag (bool flag) {
		player.SetBlackHole (flag);
	}
	public void SetBlackHoleBigFlag (bool flag) {
		player.SetBlackHoleBig (flag);
	}

	public void KameTimeStart () {
		foreach (MoveField field in fieldList) {
			if (field != null) {
				field.SetMoveSpeed (Constants.moveSpeed - 2);
			}
		}
		basePoint = basePoint / 3;
		AllGameDefinition.fieldSpeed = Constants.moveSpeed - 2;
		StartCoroutine (NormalTime(false));
	}
	public void UsagiTimeStart () {
		foreach (MoveField field in fieldList) {
			if (field != null) {
				field.SetMoveSpeed (Constants.moveSpeed + 5);
			}
		}
		basePoint = basePoint * 3;
		AllGameDefinition.fieldSpeed = Constants.moveSpeed + 5;
		StartCoroutine (NormalTime(true));
	}

	IEnumerator NormalTime (bool isFast) {
		yield return new WaitForSeconds (5);
		basePoint = Constants.basePoint;
		if (isFast) {
			foreach (MoveField field in fieldList) {
				if (field != null) {
					field.SetMoveSpeed (Constants.moveSpeed);
				}
			}
		}
		AllGameDefinition.fieldSpeed = moveSpeed;
	}

	public int getBattlePoint () {
		return battlePoint;
	}

	Color originalColor;
	int tutorialJumpCount;
	int tutorialButtonActionCount;
	Color attackOriginalColor;
	int tutorialAttackCount;
	int tutorialAttackButtonActionCount;
	public void FlashJumpButton (int count) {
		tutorialJumpCount = count;
		tutorialButtonActionCount = 0;

		jumpButtonImg = jumpButton.GetComponent<Image> ();
//		StartCoroutine (changeButtonColor (count));
		originalColor = jumpButtonImg.color;

//		tutorialText.gameObject.SetActive (true);
		switch (count) {
		case 1:
			StartCoroutine (changeButtonColor (2.0f));
			tutorialText.text = "STEP 1.  SINGLE JUMP";
			if (lang == SystemLanguage.Japanese) {
				tutorialTipsText.text = "Tips.  ジャンプボタンを 押す長さに応じて ジャンプ力が変わります。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  跳跃力将取决于按下跳跃键的长度改变";
			} else {
				tutorialTipsText.text = "Tips.  The jumping force will change according to the length you press the jump button.";
			}
//			tutorialImageSmall.gameObject.SetActive (true);
//			tutorialImageSmall.sprite = tutorialJumpImage;
			break;
		case 2:
			StartCoroutine (changeButtonColor (2.0f));
			tutorialText.text = "STEP 2.  DOUBLE JUMP";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  ジャンプボタンは 連打するのではなく、 1回1回地面を 確認しながら 飛びましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  跳转按钮是不是弹幕，让在检查地面一次一次的跳跃";
			} else {
				tutorialTipsText.text = "Tips.  Jump buttons should not be hit repeatedly, let's fly while checking the ground once.";
			}
//			tutorialImageSmall.gameObject.SetActive (true);
//			tutorialImageSmall.sprite = tutorialJumpImage;
			break;
		case 3:
			StartCoroutine (changeButtonColor (2.0f));
			tutorialText.text = "STEP 3.  TRIPPLE JUMP";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  ジャンプは空中で 最大3回まで有効です。 4回目は無効です。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  跳跃是有效的高达空气最多三次。四是无效";
			} else {
				tutorialTipsText.text = "Tips.  Jump is valid up to 3 times in the air. The fourth time is invalid.";
			}
//			tutorialImageSmall.gameObject.SetActive (true);
//			tutorialImageSmall.sprite = tutorialJumpImage;
			break;
		}
	}

	public void PlayerAttackButton (int fieldType) {
		switch (fieldType) {
		case 7:
			tutorialAttackCount = 1;
			tutorialButtonActionCount = 0;
			attackButtonImg = attackButton.GetComponent<Image> ();
			attackOriginalColor = attackButtonImg.color;
			StartCoroutine (changeAttackButtonColor (2.0f));
			tutorialText.text = "STEP 4.  NORMAL ATTACK";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  通常攻撃は 連続で打つことはできません。 ちょと待ってから 攻撃しましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  通常攻击不能连续被击中。让我们的进攻和等待卓";
			} else {
				tutorialTipsText.text = "Tips.  Normal attacks can not be hit continuously. Let's attack just after waiting.";
			}
//			tutorialImageSmall.gameObject.SetActive (true);
//			tutorialImageSmall.sprite = tutorialNormalAttackImage;
			break;
		case 8:
			tutorialText.text = "STEP 5.  FIRE ATTACK";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  ファイヤー攻撃も 連続で打つことはできません。  右上のファイヤー画像が 表示されてから 攻撃しましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  此外，它不能连续攻击火力击中。让我们从火图像的右上角攻击显示";
			} else {
				tutorialTipsText.text = "Tips.  Fire attacks can not be hit continuously. Let's attack from the fire image in the upper right after it is displayed.";
			}
			tutorialImage.gameObject.SetActive (true);
			tutorialImage.sprite = tutorialHadoukenImage;
			break;
		case 9:
			tutorialText.text = "STEP 6.  JUMP & FIRE ATTACK";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  ファイヤー攻撃は 十字キーを滑らせるようにして 入力しましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  火攻击我们的投入，使滑动十字键";
			} else {
				tutorialTipsText.text = "Tips.  Let's enter the fire attack so as to slide the cross key.";
			}
//			tutorialImageSmall.gameObject.SetActive (true);
//			tutorialImageSmall.sprite = tutorialJumpImage;
			tutorialImage.gameObject.SetActive (true);
			tutorialImage.sprite = tutorialHadoukenImage;
			break;
		case 10:
			tutorialText.text = "STEP 7.  DANGEROUS THUNDER ATTACK";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  サンダー攻撃も 連続で打つことはできません。  右上のサンダー画像が 表示されてから 攻撃しましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  此外，它不能在连续的雷霆一击击中。让我们从雷霆图像的右上角攻击显示";
			} else {
				tutorialTipsText.text = "Tips.  Thunder attack can not be hit continuously. Let's attack it after the thunder picture on the upper right is displayed.";
			}
			tutorialImage.gameObject.SetActive (true);
			tutorialImage.sprite = tutorialYogaFireImage;
			break;
		case 11:
			TutotrialSkipCanvas.gameObject.SetActive (true);
			tutorialText.text = "STEP 8.  UPPERCUT ATTACK";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  上から降ってくる的に 有効です。 アッパー中は無敵になるので 危ないと思ったら 使いましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  它是未来有效从上方落下。在上让我们使用如果你觉得危险，因为立于不败之地";
			} else {
				tutorialTipsText.text = "Tips.  It is effective for falling from above. Let's use it when it thinks it is dangerous because it becomes invincible during the upper.";
			}
			tutorialImage.gameObject.SetActive (true);
			tutorialImage.sprite = tutorialUpperImage;
			break;
		case 12:
			tutorialText.text = "STEP 9.  JUMP & KICK ATTACK";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  ジャンプを終えて 降りるときに 敵がいたら使いましょう。 ジャンプキック中は無敵状態になります。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  让我们用一旦敌人是当你下车，并完成了跳跃。在跳踢将是无敌状态";
			} else {
				tutorialTipsText.text = "Tips.  Let's use enemies when you get off after you finish the jump. During a jump kick you will be in an invincible state.";
			}
			tutorialImage.gameObject.SetActive (true);
			tutorialImage.sprite = tutorialKickImage;
			break;
		case 13:
			tutorialText.text = "STEP 10.  ROLLING ATTACK";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  敵がいないときは 左キーを押しておいて いつでも攻撃ができる状態に しておきましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  让我们可以在任何时间提前按左键攻击状态是当敌人不是";
			} else {
				tutorialTipsText.text = "Tips.  If there is no enemy, leave the state where you can attack at any time by pressing the left key.";
			}
			tutorialImage.gameObject.SetActive (true);
			tutorialImage.sprite = tutorialRollingImage;
			break;
		case 14:
			tutorialText.text = "STEP 11.  TORNADO DEFENSE";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  ボス戦などで 上から降ってくる 攻撃・汚物に対して有効です。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  它是有效的进攻，污物从上面像一个boss战下来";
			} else {
				tutorialTipsText.text = "Tips.  It is effective against attacks and filths coming down from above in boss fight etc.";
			}
			tutorialImage.gameObject.SetActive (true);
			tutorialImage.sprite = tutorialTornadoDefenseImage;
			break;
		case 15:
			tutorialText.text = "STEP 12.  BOSS BATTLE";
			if (lang == SystemLanguage.Japanese) {
			tutorialTipsText.text = "Tips.  ボス戦では上下左右 自由に動くことが できます。 攻撃から逃げてトドメを さしましょう。";
			} else if (lang == SystemLanguage.ChineseSimplified) {
				tutorialTipsText.text = "Tips.  是boss战，你可以向上，下，左，右自由。让我们在棺材从攻击逃避";
			} else {
				tutorialTipsText.text = "Tips.  In boss fight you can move freely up, down, right and left. Run away from the attack and let 's do it.";
			}
			break;
		}
	}
	IEnumerator changeButtonColor (float secound) {
		while (tutorialButtonActionCount < 3) {
			yield return new WaitForSeconds (secound);
			iTween.ValueTo (gameObject, iTween.Hash ("from", 1f, "to", 0f, "time", 0.3f, "onupdate", "SetColorValue"));
			flashCount = 0;
			tutorialButtonActionCount++;
		}
		_restartField ();
	}
	IEnumerator changeAttackButtonColor (float secound) {
		while (tutorialAttackButtonActionCount < 3) {
			yield return new WaitForSeconds (secound);
			iTween.ValueTo (gameObject, iTween.Hash ("from", 1f, "to", 0f, "time", 0.3f, "onupdate", "SetAttackColorValue"));
			attackFlashCount = 0;
			tutorialAttackButtonActionCount++;
		}
	}

	int flashCount = 0;
	void SetColorValue(float imgColor) {
		Color diff = Color.red - originalColor;
		if (imgColor == 0.0f) {
			if (flashCount == tutorialJumpCount - 1) {
				jumpButtonImg.color = originalColor;
//				flashCount = 0;
//				_restartField ();
				return;
			}
			imgColor = 255f;
			iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 0.3f, "onupdate", "SetColorValue"));
			flashCount++;
		}
		jumpButtonImg.color = new Color (255, originalColor.g + diff.g * imgColor, originalColor.b + diff.b * imgColor, originalColor.a);
	}

	int attackFlashCount = 0;
	void SetAttackColorValue(float imgColor) {
		Color diff = Color.red - attackOriginalColor;
		if (imgColor == 0.0f) {
			if (attackFlashCount == tutorialAttackCount - 1) {
				attackButtonImg.color = attackOriginalColor;
				//				flashCount = 0;
				//				_restartField ();
				return;
			}
			imgColor = 255f;
			iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 0.3f, "onupdate", "SetAttackColorValue"));
			attackFlashCount++;
		}
		attackButtonImg.color = new Color (255, attackOriginalColor.g + diff.g * imgColor, attackOriginalColor.b + diff.b * imgColor, attackOriginalColor.a);
	}

	public void JumpSuccess () {
		if (!isJumpMissed) {
			tutorialText.text = "JUMP  SUCCESS !!!";
			tutorialTipsText.text = "";
			tutorialImage.gameObject.SetActive (false);
			StartCoroutine (TutorialTextDelete ());
		}
		isJumpMissed = false;
	}

	public void AttackSuccess (int enemyNo) {
		switch (enemyNo) {
		case 1:
			tutorialText.text = "NORMAL ATTACK  SUCCESS !!!";
			tutorialTipsText.text = "";
			break;
		case 2:
			tutorialText.text = "FIRE ATTACK  SUCCESS !!!";
			tutorialTipsText.text = "";
			break;
		case 3:
			tutorialText.text = "JUMP & FIRE ATTACK  SUCCESS !!!";
			tutorialTipsText.text = "";
			break;
		case 4:
			tutorialText.text = "DANGEROUS THUNDER ATTACK  SUCCESS !!!";
			tutorialTipsText.text = "";
			break;
		case 5:
			tutorialText.text = "UPPERCUT  SUCCESS !!!";
			tutorialTipsText.text = "";
			break;
		case 6:
			tutorialText.text = "JUMP & KICK  SUCCESS !!!";
			tutorialTipsText.text = "";
			break;
		case 7:
			tutorialText.text = "ROLLING ATTACK  SUCCESS !!!";
			tutorialTipsText.text = "";
			break;
		case 8:
			tutorialText.text = "TORNADO DEFENSE  SUCCESS !!!";
			tutorialTipsText.text = "";
			break;
		}
		tutorialImage.gameObject.SetActive (false);
		tutorialImage.gameObject.SetActive (false);
		tutorialImageSmall.gameObject.SetActive (false);
		StartCoroutine (TutorialTextDelete ());
		StartCoroutine (StartField ());
	}

	IEnumerator StartField () {
		yield return new WaitForSeconds (1);
		_restartField ();
	}
	IEnumerator TutorialTextDelete () {
		yield return new WaitForSeconds (2);
		tutorialText.text = "";
		tutorialTipsText.text = "";
	}

	void _reStartJump () {
		tutorialFieldCount -= 4;
		tutorialText.text = "Jump failure ..... Let's Retry !";
		StartCoroutine (TutorialTextDelete ());
		StartCoroutine (StartField ());
	}

	public void HitTutorialEnemy (int enemyNo) {
		if (enemyNo == 5) { // upper enemy
			tutorialFieldCount -= 2;
			tutorialText.text = "Upper failure ..... Let's Retry !";
			StartCoroutine (TutorialTextDelete ());
			StartCoroutine (StartField ());
		} else if (enemyNo == 6) { // kick nurikabe
			tutorialFieldCount -= 2;
			tutorialText.text = "Kick failure ..... Let's Retry !";
			StartCoroutine (TutorialTextDelete ());
			StartCoroutine (StartField ());
		} else if (enemyNo == 7) { // rolling uchuujin
			tutorialFieldCount -= 2;
			tutorialText.text = "Rolling failure ..... Let's Retry !";
			StartCoroutine (TutorialTextDelete ());
			StartCoroutine (StartField ());
		} else if (enemyNo == 8) { // tornado defense
			tutorialFieldCount -= 2;
			tutorialText.text = "Tornado defense failure ..... Let's Retry !";
			StartCoroutine (TutorialTextDelete ());
			StartCoroutine (StartField ());
		}
	}

	public void SkipTutorial () {
		PlayerPrefs.SetInt ("playerStatus", 2);
		PlayerPrefs.Save ();

		SceneManager.LoadScene ("Difficulty");
	}

	public void CompleteTutorial () {
		tutorialText.text = "Complete tutorial.";
		tutorialTipsText.text = "";
		PlayerPrefs.SetInt ("playerStatus", 2);
		PlayerPrefs.Save ();

		StartCoroutine (GoBackDifficultyCanvas ());
	}

	public void AddBossCount () {
		bossAliveCount ++;
		Debug.Log ("bossAliveCount : " + bossAliveCount);
	}


	void ShowAd() {
		if (Advertisement.IsReady())
		{
			Advertisement.Show();
		}
	}

	public void GetBattlePoint () {
		battlePoint += 3000;
		battlePointText.text = battlePoint.ToString ();
	}
}
