﻿using UnityEngine;
using System.Collections;

public class MarathonEnemyTrigger : MonoBehaviour {
	GameObject gameController;
	MarathonPlayer targetPlayer;
	Color color;
	Color colorOrg;
	Rigidbody2D rdbg;
	/** enmeyNo definition
	 1:enmey
	 2:nurikabe
	 3:enemy Jump
	 4:enemy come 1
	 5:enemy falling
	 6:doro
	 7:muteki
	 8:beef
	 9:dokuro
	 10:enemy slide
	 11:mirror
	 12:muteki big
	 13:nurikabe mini
	 14:life
	 15:box
	 16:blackOutImage
	 17:ojama
	 18:skebor
	 19:kame
	 20:usagi
	 21:hane
	 22:bomb
	 23:bombAfter(bakuhatu)
	 24:rocket(hit or on the rocket)
	 25:fire FromUnderGround
	 26:mini ika
	 27:big ika
	 28:fireMan

	 29:bubble
	 30:fish
	 31:rain (jump power gain)
	 32:iruka (on the iruka)

	 33:sanbo (mario)
	 34:saboten
	 35:saboten jump and attack
	 36:saboten toge
	 37:taiyou
	 38:rakuda
	 39:rakuda fire
	 40:sand wind (jump power gain)
	 41:saboten up down

	 42:big bomb
	 43:tako
	 44:tako and sumi
	 45:child hero
	 46:air plane
	 47:typhoon (hit and hero up)
	 48:mad hand
	 49:dossun
	 50:ground and fall out
	 51:money
	 52:lazer (no ground)
	 53:fire
	 54:air plane fire
	 55:kurage
	 56:uchuujin
	 57:uchuujin2
	 58:inseki
	 59:enemy hot
	 60:moon
	 61:nurikabe(murasaki)
	 62:rocket(hit ,with hero)
	 63:bad beef
	 64:multi uchuujin
	 65:uchuujin with reverse wall
	 66:multi inseki
	 67:bad beef , good beef
	 68:medamano oyaji
	 69:medamano oyaji with eye beam
	 70:ru-ret(beef, bad beef, star etc.)
	 71:enemy(disappear -> appear)

	 TODO : 以下、未実装
	 71:parashu-to
	 71:parashu-to with fire


	**/
	public int enemyNo;
	public GameObject[] list;
	public Sprite beefSprite;
	public Sprite blackImage;
	public Sprite dokuro;
	public Sprite meney;
	public Sprite star;
	public Sprite nurikabe;

	int hitPoint;

	// local position
	float posX;
	float posY;

	float posX2;
	float posY2;

	float rotateAngle;
	float alpha;
//	float speed = 5;

	float irukaSpeedX = 2;
	float irukaSpeedY = 2;

	float ikaSpeedX = 2;
	float ikaSpeedY = 2;
	float ikaSuperSpeedX;
	float ikaSuperSpeedY;
	bool isSuperIka;

	bool sabotenAttack;
	bool isMoving;
	bool randomItemGet;

	enum Direction {
		up,
		down
	};
	Direction dire;
	bool isHit;

	int moveSpeed;

	GameObject fireManAttack;
	GameObject airPlaneFireAttack;

	// Use this for initialization
	void Start () {
		rdbg = GetComponent<Rigidbody2D> ();
		posX = transform.localPosition.x;
		posY = transform.localPosition.y;

		posX2 = transform.position.x;
		posY2 = transform.position.y;

		if (enemyNo != 52) {
			color = GetComponent<SpriteRenderer> ().color;
			colorOrg = GetComponent<SpriteRenderer> ().color;
		}

		dire = Direction.up;

		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");

		moveSpeed = AllGameDefinition.fieldSpeed;

		_setHitPoint ();

		switch (enemyNo) {
		case 25:
			rotateAngle = 90;
			if (UnityEngine.Random.Range (1, 3) == 1) {
				rotateAngle = -90;
			}
			break;
		case 26:
		case 27:
			targetPlayer = GameObject.FindGameObjectWithTag ("Player").GetComponent<MarathonPlayer> ();
			StartCoroutine (_setIkaSpeed ());
			StartCoroutine (_startSuperIka ());
			break;
		case 28:
			fireManAttack = ResourceCache.Load ("Ground/fireManAttack");
			StartCoroutine (_fireManStartAttack ());
			break;
		case 35:
			sabotenAttack = true;
			StartCoroutine (_JumpSabote ());
			StartCoroutine (_attackSabotenToge ());
			break;
		case 37:
			targetPlayer = GameObject.FindGameObjectWithTag ("Player").GetComponent<MarathonPlayer> ();
			StartCoroutine (_startTaiyo ());
			break;
		case 38:
			if (UnityEngine.Random.Range (1, 3) == 1) {
				StartCoroutine(_attackRakuda ());
			}
			break;
		case 42:
			StartCoroutine (_setBigBomb ());
			break;
		case 46:
			airPlaneFireAttack = ResourceCache.Load ("Ground/longFireAttack");
			StartCoroutine (_startAirPlaneAttack ());
			break;
		case 48:
			iTween.ValueTo(this.gameObject, iTween.Hash("from", 0f, "to", 1f, "time", 3f, "onupdate", "SetValue"));
			break;
		case 49:
			StartCoroutine (_startDossun ());
			break;
		case 51:
			GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 100);
			GetComponent<Rigidbody2D> ().AddForce (Vector2.left * 800);
			break;
		case 52:
			isMoving = true;
			break;
		case 54:
			break;
		case 55:
			targetPlayer = GameObject.FindGameObjectWithTag ("Player").GetComponent<MarathonPlayer> ();
			break;
		case 69:
			StartCoroutine (_medamaBeamStart ());
			break;
		case 70:
			GetComponent<Animator> ().SetBool ("itemShuffle", true);
			break;
		case 71:
			GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 0);
			StartCoroutine (appear ());
			break;
		case 72:
			GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 0);
			StartCoroutine (appear ());
			break;
		default:
			break;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
//	protected override void ChildUpdate () {
		float speed = _getSpeed ();

		if (transform.localPosition.x < -25 || transform.localPosition.x > 130 ||
			transform.localPosition.y < -15 || transform.localPosition.y > 30) {
//			Debug.Log ("destroy enemy1 : " + this.name);
//			Debug.Log ("destroy enemy2 : " + this.transform.localPosition.x);
//			Debug.Log ("destroy enemy3 : " + this.transform.position.x);
			Destroy (this.gameObject);
		}

		switch (enemyNo) {
		case 3:
		case 16:
		case 41:
		case 43:
			if (!isHit) {
				// jump
				if (transform.localPosition.y >= 4.0f) {
					dire = Direction.down;
				}
				if (transform.localPosition.y <= 0.2f) {
					dire = Direction.up;
				}
				if (dire == Direction.down) {
					posY -= Time.deltaTime * speed;
				} else {
					posY += Time.deltaTime * speed;
				}
				transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
			}
			break;
		case 4:
			if (!isHit) {
				posX -= Time.deltaTime * speed;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			}
			break;
		case 7:
			if (!isHit) {
				posX -= Time.deltaTime * speed;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			}
			break;
		case 10:
			if (!isHit) {
				posX -= Time.deltaTime * speed;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			}
			break;
		case 12:
		case 20:
		case 21:
			if (!isHit) {
				if (transform.localPosition.y >= 12.0f) {
					dire = Direction.down;
				}
				if (transform.localPosition.y <= 5.0f) {
					dire = Direction.up;
				}
				if (dire == Direction.down) {
					posY -= Time.deltaTime * speed;
				} else {
					posY += Time.deltaTime * speed;
				}
				posX -= Time.deltaTime * speed;
				transform.localPosition = new Vector3 (posX, posY, transform.localPosition.z);
			}
			break;
		case 14:
			if (!isHit) {
				if (transform.localPosition.y >= 12.0f) {
					dire = Direction.down;
				}
				if (transform.localPosition.y <= 8.0f) {
					dire = Direction.up;
				}
				if (dire == Direction.down) {
					posY -= Time.deltaTime * speed * 2;
				} else {
					posY += Time.deltaTime * speed * 2;
				}
				posX -= Time.deltaTime * speed * 3;
				transform.localPosition = new Vector3 (posX, posY, transform.localPosition.z);
			}
			break;
		case 5:
//		case 17:
			if (!isHit) {
				posY -= Time.deltaTime * speed / 2;
				transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
			}
			break;
		case 23:
			if (transform.position.y < -20) {
				Destroy (this.gameObject);
			}
			break;
		case 24:
			if (!isHit) {
				posX -= Time.deltaTime * speed / 2;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, transform.localPosition.z);
			}
			break;
		case 25:
			rotateAngle += Time.deltaTime * 100;
			transform.rotation = Quaternion.Euler (0, 0, rotateAngle);
			break;
		case 26:
		case 27:
			if (!isHit) {
				if (transform.localPosition.x < 0) {
					Destroy (this.gameObject);
				}
				if (!isSuperIka) {
					float positionX = transform.localPosition.x;
					float positionY = transform.localPosition.y;
					positionX -= Time.deltaTime * ikaSpeedX;
					positionY -= Time.deltaTime * ikaSpeedY;
					transform.localPosition = new Vector3 (positionX, positionY, transform.localPosition.z);
				} else {
					moveTargetDirection ();
				}
			}
			break;
		case 29:
			posX -= Time.deltaTime * moveSpeed;
			transform.position = new Vector3 (posX, 0, 0);
			break;
		case 30:
			if (!isHit) {
				posX -= Time.deltaTime * speed;
				posY -= Time.deltaTime * speed / 4.5f;
				transform.localPosition = new Vector3 (posX, posY, transform.localPosition.z);
			}
			break;
		case 31:
			if (transform.position.y < -20) {
				Destroy (this.gameObject);
			}
			posY -= Time.deltaTime * speed / 6;
			transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
			break;
		case 32:
			StartCoroutine (_setIrukaSpeed ());
			posX -= Time.deltaTime * irukaSpeedX;
			posY -= Time.deltaTime * irukaSpeedY;
			transform.localPosition = new Vector3 (posX, posY, transform.localPosition.z);
			break;
		case 36:
			if (!isHit) {
				//			moveTargetDirection ();
				if (transform.position.y < -10) {
					Destroy (this.gameObject);
				}
				posX -= Time.deltaTime * speed / 2;
				posY -= Time.deltaTime * speed / 2;
				transform.localPosition = new Vector3 (posX, posY, transform.localPosition.z);
			}
			break;
		case 37:
			if (!isHit && !isMoving) {
				moveTargetDirection ();
			}
			break;
		case 39:
			if (transform.position.x > 50 || transform.position.x < -50) {
				Destroy (this.gameObject);
			}
			if (!isHit) {
				posX -= Time.deltaTime * speed * 2;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			}
			break;
		case 40:
			if (transform.position.x < -50) {
				Destroy (this.gameObject);
			}
			posX -= Time.deltaTime * speed;
			transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			break;
		case 45:
			if (!isHit) {
				// jump
				if (transform.localPosition.y >= 8.0f) {
					dire = Direction.down;
				}
				if (transform.localPosition.y <= 3.5f) {
					dire = Direction.up;
				}
				if (dire == Direction.down) {
					posY -= Time.deltaTime * speed * 2;
				} else {
					posY += Time.deltaTime * speed * 2;
				}
				transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
			}
			break;
		case 46:
			if (transform.position.x < -50) {
				Destroy (this.gameObject);
			}
			if (!isHit) {
				posX -= Time.deltaTime * speed / 5;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			}
			break;
		case 47:
			if (transform.position.x < -50) {
				Destroy (this.gameObject);
			}
			posX -= Time.deltaTime * speed / 2;
			transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			break;
		case 48:
			if (transform.localPosition.y > 0) {
				return;
			}
			posY += Time.deltaTime * speed / 10;
			transform.localPosition = new Vector3 (transform.localPosition.x, posY, 0);
			break;
		case 49:
			if (!isHit) {
				if (!isMoving) {
					return;
				}
				if (transform.localPosition.y < 0) {
					return;
				}
				posY -= Time.deltaTime * speed * 2;
				transform.localPosition = new Vector3 (transform.localPosition.x, posY, 0);
			}
			break;
		case 52:
			if (isMoving) {
				posX -= Time.deltaTime * speed * 2;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			}
			break;
		case 53:
			if (transform.position.x < -15 || transform.position.x > 15) {
//			if (!GetComponent<Renderer>().isVisible) {
				Destroy (this.gameObject);
			}
			break;
		case 54:
			if (transform.position.x < -15) {
				Destroy (this.gameObject);
			}
			break;
		case 55:
			if (!isHit) {
				moveTargetDirection ();
			}
			break;
		case 57:
			if (!isHit) {
				if (transform.localPosition.y >= 4.0f) {
					dire = Direction.down;
				}
				if (transform.localPosition.y <= 0.2f) {
					dire = Direction.up;
				}
				if (dire == Direction.down) {
					posY -= Time.deltaTime * speed;
				} else {
					posY += Time.deltaTime * speed;
				}
				transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
			}
			break;
		case 58:
			if (!isHit) {
				posX -= Time.deltaTime * speed;

				posY -= Time.deltaTime * speed / 2;
				transform.localPosition = new Vector3 (posX, posY, 0);
			}
			break;
		case 59:
			if (!isHit) {
				posX -= Time.deltaTime * speed;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			}
			break;
		case 60:
			if (!isHit) {
				posX -= Time.deltaTime * speed;

				posY -= Time.deltaTime * speed / 2;
				transform.localPosition = new Vector3 (posX, posY, 0);
			}
			break;
		case 61:
			if (!isHit) {
				if (transform.localPosition.y >= 10.0f) {
					dire = Direction.down;
				}
				if (transform.localPosition.y <= 0.2f) {
					dire = Direction.up;
				}
				if (dire == Direction.down) {
					posY -= Time.deltaTime * speed;
				} else {
					posY += Time.deltaTime * speed;
				}
				transform.localPosition = new Vector3 (transform.localPosition.x, posY, transform.localPosition.z);
			}
			break;
		case 62:
			if (!isHit) {
				posX -= Time.deltaTime * speed * 2;
				transform.localPosition = new Vector3 (posX, transform.localPosition.y, 0);
			}
			break;
		case 63:
			break;
		case 70:
			if (randomItemGet) {
				posY += Time.deltaTime * speed / 4;
				transform.localPosition = new Vector3 (transform.localPosition.x, posY, 0);
			}
			break;
		}
	}
	IEnumerator appear () {
		yield return new WaitForSeconds (1.5f);
		iTween.ValueTo(this.gameObject, iTween.Hash("from", 0f, "to", 1f, "time", 2f, "onupdate", "SetValue"));
	}
	IEnumerator _medamaBeamStart () {
		yield return new WaitForSeconds (2.5f);
		if (transform.position.x > -10) {
			GameObject laserBeam = ResourceCache.Load ("Ground/laserBeam");
			Instantiate (laserBeam, new Vector3 (transform.position.x, transform.position.y + 1, 0), Quaternion.identity);
		}
//		laserBeam.SetActive (true);
	}
	IEnumerator _startDossun () {
		yield return new WaitForSeconds (1);
		isMoving = true;
	}
	void SetValue(float alpha) {
		this.gameObject.GetComponent<SpriteRenderer> ().color = new Color(0/255, 255/255, 0/255, alpha);
	}
	IEnumerator _startAirPlaneAttack () {
		while (!isHit) {
			yield return new WaitForSeconds (1f);
			GameObject airPlaneFire = Instantiate (airPlaneFireAttack, transform.position, Quaternion.identity) as GameObject;
			airPlaneFire.GetComponent<Rigidbody2D> ().AddForce (Vector2.left * 500);
			airPlaneFire.GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 50);
		}
	}
	IEnumerator _fireManStartAttack () {
		while (!isHit) {
			yield return new WaitForSeconds (1.5f);
			GameObject fireManFire = Instantiate (fireManAttack, transform.position, Quaternion.identity) as GameObject;
			fireManFire.GetComponent<Rigidbody2D> ().AddForce (Vector2.left * 1000);
			fireManFire.GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 100);
		}
	}
	IEnumerator _setBigBomb () {
		yield return new WaitForSeconds (0.7f);
		rdbg.gravityScale = 0.5f;
		GetComponent<SpriteRenderer> ().enabled = true;
	}
	IEnumerator _attackRakuda () {
		GameObject hadouken = ResourceCache.Load ("Ground/enemyAttackFire");
		while (!isHit) {
			yield return new WaitForSeconds (0.4f);
			Vector3 itemPos = transform.position;
			itemPos.x = itemPos.x - 1.0f;
			itemPos.y = itemPos.y + 1.0f;
			GameObject fire = Instantiate (hadouken, itemPos, Quaternion.identity) as GameObject;
		}
	}
	IEnumerator _startTaiyo () {
		yield return new WaitForSeconds (2);
		isMoving = true;
	}
	IEnumerator _JumpSabote () {
		yield return new WaitForSeconds (1);
		GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 500);
		StartCoroutine (_stopSabotenAttack ());
	}
	IEnumerator _attackSabotenToge () {
		GameObject toge = ResourceCache.Load ("Ground/sabotenToge");

		while (sabotenAttack) {
			yield return new WaitForSeconds (0.3f);
			Instantiate (toge, transform.position, Quaternion.identity);
		}
	}
	IEnumerator _setIkaSpeed () {
		while (!isHit) {
			yield return new WaitForSeconds (0.5f);
			ikaSpeedX = 3;
			ikaSpeedY = UnityEngine.Random.Range (-13, 13);
		}
	}
	IEnumerator _startSuperIka () {
		while (true) {
			yield return new WaitForSeconds (0.5f);
			if (UnityEngine.Random.Range (1, 3) == 1) {
				isSuperIka = true;
			} else {
				isSuperIka = false;
				GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
			}
		}
	}
	IEnumerator _stopSabotenAttack () {
		yield return new WaitForSeconds (1.5f);
		sabotenAttack = false;
	}
	void moveTargetDirection () {
		Vector2 targetPos = targetPlayer.transform.localPosition;
		float x = targetPos.x;
		float y = targetPos.y;

		Vector2 direction = new Vector2 (x - transform.localPosition.x, y - transform.localPosition.y).normalized;
		GetComponent<Rigidbody2D> ().velocity = (direction * 7);
		posX = transform.position.x;
		posY = transform.position.y;
	}

	IEnumerator _setIrukaSpeed () {
		while (!isHit) {
			yield return new WaitForSeconds (0.5f);
			irukaSpeedX = 6;
			irukaSpeedY = UnityEngine.Random.Range (-20, 20);
		}
	}

	void addBattlePoint () {
	}

	void _hitEnemy (Collider2D other, bool normal, bool kick, bool tornado, bool hadouken, bool yogaFire, bool hitPlayer) {
		if (other.tag == "PlayerNormalAttack") {
			hitPoint -= 1;
			_changeColor ();
			if (hitPoint < 1) {
				gameController.SendMessage ("AddPoint", basePoint);
				isHit = true;
				rdbg.AddForce (Vector2.right * 3000);
			}
		} else if (other.tag == "PlayerKick" || other.tag == "PlayerUpper") {
			if (kick) {
				gameController.SendMessage ("AddPoint", basePoint * kickCoefficient);
				isHit = true;
				_changeColor ();
				rdbg.AddForce (Vector2.right * 3000);
			}
		} else if (other.tag == "PlayerTornado") {
			hitPoint -= 3;
			_changeColor ();
			if (hitPoint < 1) {
				gameController.SendMessage ("AddPoint", basePoint * tornadoCoefficient);
				isHit = true;
				_changeColor ();
				rdbg.AddForce (Vector2.right * 3000);
			}
		} else if (other.tag == "MarathonHadouken") {
			hitPoint -= 2;
			_changeColor ();
			if (hitPoint < 1) {
				gameController.SendMessage ("AddPoint", basePoint * hadoukenCoefficient);
				isHit = true;
				_changeColor ();
				rdbg.AddForce (Vector2.right * 3000);
			}
		} else if (other.tag == "MarathonYogaFire") {
			hitPoint -= 5;
			_changeColor ();
			if (hitPoint < 1) {
				gameController.SendMessage ("AddPoint", basePoint * yogaFireCoefficient);
				isHit = true;
				_changeColor ();
				rdbg.AddForce (Vector2.right * 3000);
			}
		} else if (other.tag == "Player") {
			if (hitPlayer) {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
//				Debug.Log ("enemyNo : " + enemyNo + " result : " + result);
				if (!result) {
					rdbg.AddForce (Vector2.right * 3000);
				} else { // muteki
					Destroy (this.gameObject);
				}
			}
		}
	}

	void _changeColor () {
//		GetComponent<SpriteRenderer>().color = new Color (1, 0, 0);
		GetComponent<SpriteRenderer>().color = new Color (1, 0, 0);
		StartCoroutine (ChangeColorOrg ());
	}
	IEnumerator ChangeColorOrg () {
		yield return new WaitForSeconds (0.3f);
		GetComponent<SpriteRenderer>().color = colorOrg;
	}
	void _hitPlayerAndContinue (Collider2D other) {
		if (other.tag == "Player") {
			isHit = true;
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			bool result = player.HitEnemy (enemyNo);
			if (!result) {
				//					gameController.SendMessage ("AddPoint", basePoint);
				rdbg.AddForce (Vector2.up * 3000);
			}
		}
	}
	void _hitPlayerAndDestroy (Collider2D other) {
		if (other.tag == "Player") {
			isHit = true;
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			bool result = player.HitEnemy (enemyNo);
			Destroy (this.gameObject);
		}
	}
	int basePoint = 100;
	int kickCoefficient = 5;
	int tornadoCoefficient = 3;
	int hadoukenCoefficient = 4;
	int yogaFireCoefficient = 5;

	void OnTriggerEnter2D (Collider2D other) {
		if (isHit) {
			return;
		}
//		if (enemyNo != 52 && !GetComponent<Renderer> ().isVisible) {
//			return;
//		}
		if (other.tag == "PlayerLaserBeam" && rdbg != null) {
			isHit = true;
			gameController.SendMessage ("AddPoint", basePoint);
			rdbg.AddForce (Vector2.up * 3000);

			return;
		}
		switch (enemyNo) {
		case 1: // enemy small
			_hitEnemy (other, true, true, false, true, true, true);
			break;
		case 2: // Nurikabe
			_hitEnemy (other, false, true, false, false , true, true);
			break;
		case 3: // enemy small jump
			_hitEnemy (other, true, true, false, true, true, true);
			break;
		case 4: // enemy small come
			_hitEnemy (other, true, true, true, true, true, true);
			break;
		case 5: // enemy big fall
			_hitEnemy (other, false, true, false, false, true, true);
			break;
		case 6: // doro
			_hitPlayerAndContinue (other);
			break;
		case 7: // muteki
			_hitPlayerAndDestroy (other);
			break;
		case 8: // beef
			_hitPlayerAndDestroy (other);
			break;
		case 9: // dokuro
			_hitPlayerAndDestroy (other);
			break;
		case 10: // enemy big sliding
			_hitEnemy (other, false, true, false, false, true, true);
			break;
		case 11: // mirror
			if (other.tag == "PlayerNormalAttack") {
				MarathonPlayerNormalAttack normalAttack = other.GetComponent<MarathonPlayerNormalAttack> ();
				if (normalAttack != null) {
					normalAttack.Reverse ();
				}
			} else if (other.tag == "PlayerKick" || other.tag == "PlayerUpper") {
			} else if (other.tag == "PlayerTornado") {
			} else if (other.tag == "MarathonHadouken") {
				MarathonHadouken hadouken = other.GetComponent<MarathonHadouken> ();
				if (hadouken != null) {
					hadouken.Reverse ();
				}
			} else if (other.tag == "MarathonYogaFire") {
				MarathonYogaFire yoga = other.GetComponent<MarathonYogaFire> ();
				if (yoga != null) {
					yoga.Reverse ();
				}
			} else if (other.tag == "Player") {
			}
			break;
		case 12: // muteki big
			_hitPlayerAndDestroy (other);
			break;
		case 13: // nurikabe mini
			_hitEnemy (other, true, true, false, true, true, true);
			break;
		case 14: // life
			_hitPlayerAndDestroy (other);
			break;
		case 15: // box
			break;
		case 16: // blackOutImage
			if (other.tag == "Player") {
				_hitEnemy (other, false, false, false, false, false, true);
				Destroy (this.gameObject);
			}
			break;
		case 17: // ojama
			break;
		case 18: // skebor
			_hitPlayerAndDestroy (other);
			break;
		case 19: // kame
			_hitPlayerAndDestroy (other);
			break;
		case 20: // usagi
			_hitPlayerAndDestroy (other);
			break;
		case 21: // hane
			_hitPlayerAndDestroy (other);
			break;
		case 22: // bomb
			if (other.tag == "PlayerNormalAttack") {
			} else if (other.tag == "PlayerKick" || other.tag == "PlayerUpper") {
			} else if (other.tag == "PlayerTornado") {
			} else if (other.tag == "MarathonHadouken") {
				isHit = true;
				StartCoroutine (BombStart ());
				StartCoroutine (BombDead ());
			} else if (other.tag == "MarathonYogaFire") {
				isHit = true;
				StartCoroutine (BombStart ());
				StartCoroutine (BombDead ());
			} else if (other.tag == "Player") {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
				if (!result) {
//					gameController.SendMessage ("AddPoint", basePoint);
					rdbg.AddForce (Vector2.up * 3000);
				}
			}
			break;
		case 23: // bomb after(bakuhatu)
			if (other.tag == "Player") {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
			}
			break;
		case 24: // rocket(hit and on the rocket)
			_hitPlayerAndContinue (other);
			break;
		case 25: // fire from under ground
			if (other.tag == "Player") {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
			}
			break;
		case 26: // ika
			_hitEnemy (other, false, true, true, true, true, true);
			break;
		case 27: // big ika
			_hitEnemy (other, false, true, true, false, true, true);
			break;
		case 28: // fireman
			_hitEnemy (other, false, true, false, false, true, true);
			break;
		case 29: // bubble
			if (other.tag == "Player") {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
				if (!result) {
//					gameController.SendMessage ("AddPoint", basePoint);
					rdbg.AddForce (Vector2.up * 3000);
				}

				moveSpeed = 0;
				StartCoroutine (hitBubble ());
			}
			break;
		case 30: // fish
			_hitEnemy (other, true, true, true, true, true, true);
			break;
		case 31: // rain(jump power gain)
			_hitPlayerAndContinue (other);
			break;
		case 32: // iruka(on the iruka)
			_hitPlayerAndDestroy (other);
			break;
		case 33: // sambo
			_hitEnemy (other, false, true, false, false, true, true);
			break;
		case 34: // saboten
			_hitEnemy (other, false, true,false, true, true, true);
			break;
		case 35: // saboten and attack
			_hitEnemy (other, false, true,false, true, true, true);
			break;
		case 36: // saboten toge
			_hitEnemy (other, true, true, true, true, true, true);
			break;
		case 37: // taiyo
			_hitEnemy (other, false, true, true, true, true, true);
			break;
		case 38: // rakuda
			_hitEnemy (other, false, true, false, false, true, true);
			break;
		case 39: // rakuda fire
			_hitEnemy (other, false, true, false, false, true, true);
			break;
		case 40: // sand wind(jump power gain)
			_hitPlayerAndContinue (other);
			break;
		case 41: // saboten up down
			_hitEnemy (other, false, true, true, true, true, true);
			break;
		case 42: // big bomb
			_bombOnTheGround ();
			break;
		case 43: // tako
			_hitEnemy (other, false, true, true, true, true, true);
			break;
		case 44: // tako and sumi
			if (other.tag == "PlayerNormalAttack") {
				hitPoint -= 1;
				_changeColor ();
				if (hitPoint < 1) {
					gameController.SendMessage ("AddPoint", basePoint);
					isHit = true;
					rdbg.AddForce (Vector2.right * 3000);
				}
			} else if (other.tag == "PlayerKick" || other.tag == "PlayerUpper") {
				gameController.SendMessage ("AddPoint", basePoint * kickCoefficient);
				isHit = true;
				Vector3 pos = transform.position;
				rdbg.AddForce (Vector2.up * 3000);
				GameObject parent = transform.root.gameObject;
				_setTakoSumi (pos, parent);
			} else if (other.tag == "PlayerTornado") {
				gameController.SendMessage ("AddPoint", basePoint * tornadoCoefficient);
				isHit = true;
				Vector3 pos = transform.position;
				rdbg.AddForce (Vector2.up * 3000);
				GameObject parent = transform.root.gameObject;
				_setTakoSumi (pos, parent);
			} else if (other.tag == "MarathonHadouken") {
				gameController.SendMessage ("AddPoint", basePoint * hadoukenCoefficient);
				isHit = true;
				Vector3 pos = transform.position;
				rdbg.AddForce (Vector2.up * 3000);
				GameObject parent = transform.root.gameObject;
				_setTakoSumi (pos, parent);
			} else if (other.tag == "MarathonYogaFire") {
				gameController.SendMessage ("AddPoint", basePoint * yogaFireCoefficient);
				isHit = true;
				Vector3 pos = transform.position;
				rdbg.AddForce (Vector2.up * 3000);
				GameObject parent = transform.root.gameObject;
				_setTakoSumi (pos, parent);
			} else if (other.tag == "Player") {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
				if (!result) {
//					gameController.SendMessage ("AddPoint", basePoint);
					rdbg.AddForce (Vector2.up * 3000);
				}
			}
			break;
		case 45: // child hero
			_hitPlayerAndDestroy (other);
			break;
		case 46: // air plane(attack fire)
			_hitEnemy (other, false, true, true, true, true, true);
			break;
		case 47: // typhoon(hit and hero up)
			_hitPlayerAndDestroy (other);
			break;
		case 48: // mad hand
			_hitEnemy (other, false, true, false, true, true, true);
			break;
		case 49: // dossun
			_hitPlayerAndContinue (other);
			break;
		case 50: // ground fall out
			if (other.tag == "Player") {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
				if (!result) {
//					gameController.SendMessage ("AddPoint", basePoint);
					rdbg.AddForce (Vector2.up * 3000);
				}
				StartCoroutine (_breakBridge ());
			}
			break;
		case 51: // meney
			_hitPlayerAndDestroy (other);
			break;
		case 52: // lazer
			_hitPlayerAndContinue (other);
			break;
		case 53: // fire man attack
			_hitEnemy (other, true, true, true, true, true, true);
			break;
		case 54: // air plane fire
			_hitEnemy (other, false, true, true, true, true, true);
			break;
		case 55: // kurage
			_hitEnemy (other, false, true, true, false, true, true);
			break;
		case 56: // uchuujin
			_hitEnemy (other, false, true, false, true, true, true);
			break;
		case 57: // uchuujin2
			_hitEnemy (other, false, true, false, true, true, true);
			break;
		case 58: // inseki
			_hitEnemy (other, false, true, true, false, true, true);
			break;
		case 59: // enemy hot
			_hitEnemy (other, false, true, false, false, true, true);
			break;
		case 60: // moon
			_hitEnemy (other, false, true, true, false, true, true);
			break;
		case 61: // nurikabe damage
			_hitEnemy (other, false, true, false, false, true, true);
			break;
		case 62: // rocket
			if (other.tag == "Player") {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
				Destroy (this.gameObject);
			}
			break;
		case 63: // bad beef
			_hitPlayerAndDestroy (other);
			break;
		case 68: // medama no oyaji
			if (other.tag == "PlayerNormalAttack") {
				hitPoint -= 1;
				_changeColor ();
				if (hitPoint < 1) {
					gameController.SendMessage ("AddPoint", basePoint);
					isHit = true;
					rdbg.AddForce (Vector2.right * 3000);
				}
			} else if (other.tag == "PlayerKick" || other.tag == "PlayerUpper") {
				gameController.SendMessage ("AddPoint", basePoint * kickCoefficient);
				isHit = true;
				rdbg.AddForce (Vector2.right * 3000);
			} else if (other.tag == "PlayerTornado") {
			} else if (other.tag == "MarathonHadouken") {
				gameController.SendMessage ("AddPoint", basePoint * hadoukenCoefficient);
				isHit = true;
				rdbg.AddForce (Vector2.right * 3000);
			} else if (other.tag == "MarathonYogaFire") {
				gameController.SendMessage ("AddPoint", basePoint * yogaFireCoefficient);
				isHit = true;
				rdbg.AddForce (Vector2.right * 3000);
			} else if (other.tag == "Player") {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
				if (!result) {
					//					gameController.SendMessage ("AddPoint", basePoint);
					rdbg.AddForce (Vector2.up * 3000);
				}
				Destroy (this.gameObject);
			}
			break;
		case 69: // medama no oyaji(with eye beam)
			if (other.tag == "PlayerNormalAttack") {
				hitPoint -= 1;
				_changeColor ();
				if (hitPoint < 1) {
					gameController.SendMessage ("AddPoint", basePoint);
					isHit = true;
					rdbg.AddForce (Vector2.right * 3000);
				}
			} else if (other.tag == "PlayerKick" || other.tag == "PlayerUpper") {
				gameController.SendMessage ("AddPoint", basePoint * kickCoefficient);
				isHit = true;
				rdbg.AddForce (Vector2.right * 3000);
			} else if (other.tag == "PlayerTornado") {
			} else if (other.tag == "MarathonHadouken") {
				gameController.SendMessage ("AddPoint", basePoint * hadoukenCoefficient);
				isHit = true;
				rdbg.AddForce (Vector2.right * 3000);
			} else if (other.tag == "MarathonYogaFire") {
				gameController.SendMessage ("AddPoint", basePoint * yogaFireCoefficient);
				isHit = true;
				rdbg.AddForce (Vector2.right * 3000);
			} else if (other.tag == "Player") {
				isHit = true;
				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (enemyNo);
				if (!result) {
					//					gameController.SendMessage ("AddPoint", basePoint);
					rdbg.AddForce (Vector2.up * 3000);
				}
				Destroy (this.gameObject);
			}
			break;
		case 70:
			if (other.tag == "Player") {
				isHit = true;
				int itemType = _stopShuffle ();
				randomItemGet = true;
				iTween.ValueTo(this.gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 1f, "onupdate", "SetRandomItemValue"));

				MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
				bool result = player.HitEnemy (itemType);
			}
			break;
		case 71: // enemy appear
			_hitEnemy (other, true, true, false, true, true, true);
			break;
		case 72: // nurikabe appear
			_hitEnemy (other, false, true, false, false , true, true);
			break;
		default:
			break;
		}
	}
	void SetRandomItemValue(float alpha) {
		this.gameObject.GetComponent<SpriteRenderer> ().color = new Color(255/255, 255/255, 255/255, alpha);
	}

	void _bombOnTheGround () {
		Destroy (this.gameObject);

		GameObject bomb = ResourceCache.Load ("Ground/bombAfter");
		Instantiate (bomb, transform.position, Quaternion.identity);
	}

	int _stopShuffle () {
//		GetComponent<Animator> ().SetBool ("itemShuffle", false);
		GetComponent<Animator> ().enabled = false;
		SpriteRenderer sprite = GetComponent<SpriteRenderer> ();
		int itemType = UnityEngine.Random.Range (1, 7);
		switch (itemType) {
		case 1:
			sprite.sprite = beefSprite;
			return 8;
		case 2:
			sprite.sprite = blackImage;
			return 16;
		case 3:
			sprite.sprite = dokuro;
			return 9;
		case 4:
			sprite.sprite = meney;
			return 51;
		case 5:
			sprite.sprite = star;
			return 7;
		case 6:
			sprite.sprite = nurikabe;
			return 2;
		default:
			sprite.sprite = beefSprite;
			return 8;
		}
	}
	IEnumerator _breakBridge () {
		yield return new WaitForSeconds (0.2f);
		GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.None;
	}
	void _setTakoSumi (Vector3 pos, GameObject parent) {
		GameObject sumi = ResourceCache.Load ("Ground/doroImage");
		GameObject sumiObj = Instantiate (sumi, pos, Quaternion.identity) as GameObject;
		sumiObj.transform.parent = parent.transform;
	}
	IEnumerator BombStart () {
		yield return new WaitForSeconds (0.5f);
//		GetComponent<SpriteRenderer> ().color = new Color (1,0f, 0, 0);
		GetComponent<SpriteRenderer> ().color = Color.red;
		StartCoroutine (BobmEnd ());
	}
	IEnumerator BobmEnd () {
		yield return new WaitForSeconds (0.5f);
		GetComponent<SpriteRenderer> ().color = Color.black;
		StartCoroutine (BombStart ());
	}

	IEnumerator BombDead () {
		yield return new WaitForSeconds (1.5f);
		GameObject bomb = ResourceCache.Load ("Ground/bombAfter");
		Instantiate (bomb, transform.position, Quaternion.identity);
		Destroy (this.gameObject);
	}
	IEnumerator hitBubble () {
		yield return new WaitForSeconds (5);
		Destroy (this.gameObject);
	}

	float _getSpeed () {
		return UnityEngine.Random.Range (1, 20);
	}

	public void setMovingFlag (bool flag) {
		isMoving = flag;
	}
	void _setHitPoint () {
		switch (enemyNo) {
		case 1:
		case 3:
		case 13:
		case 30:
		case 36:
		case 44:
		case 56:
		case 57:
		case 65:
			hitPoint = 1;
			break;
		case 2:
		case 27:
		case 28:
		case 33:
		case 38:
		case 39:
		case 46:
		case 53:
		case 54:
		case 55:
		case 59:
		case 61:
			hitPoint = 5;
			break;
		case 4:
		case 5:
		case 10:
		case 26:
		case 34:
		case 35:
		case 37:
		case 41:
		case 43:
		case 48:
		case 58:
		case 60:
		case 68:
		case 69:
			hitPoint = 2;
			break;
		}
	}
}
