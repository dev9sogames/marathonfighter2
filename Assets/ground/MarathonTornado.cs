﻿using UnityEngine;
using System.Collections;

public class MarathonTornado : MonoBehaviour {
	float posX;
	float posY;

	float speedX;
	float speedY;

	bool isReverse;
	PolygonCollider2D cldr;

	// Use this for initialization
	void Start () {
		posX = transform.localPosition.x;
		posY = transform.localPosition.y;

		cldr = GetComponent<PolygonCollider2D> ();
		cldr.isTrigger = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x >= 15) {
			Destroy (this.gameObject);
		}
		speedX = UnityEngine.Random.Range (-5.0f, 10.0f);
		speedY = UnityEngine.Random.Range (-10.0f, 10.0f);

		posX += Time.deltaTime * speedX;
		posY += Time.deltaTime * speedY;
		transform.position = new Vector3 (posX, posY, 1);
	}

	public void Reverse () {
		isReverse = true;
		cldr.isTrigger = true;
		transform.localScale = new Vector3 (-1.0f, 1.0f, 0);
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "MarathonBoss") {
			Destroy (this.gameObject);
		}
		if (other.tag == "Player") {
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			player.HitEnemy (1);
		}
	}
	public void DestroyObj () {
		Destroy (this.gameObject);
	}
}
