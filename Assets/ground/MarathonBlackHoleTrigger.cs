﻿using UnityEngine;
using System.Collections;

public class MarathonBlackHoleTrigger : MonoBehaviour {
	public bool isBig;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player") {
//			GameObject exist = GameObject.FindGameObjectWithTag ("BlackHole");
			if (true) {
				GameObject blackHole;
				if (isBig) {
					blackHole = (GameObject)Resources.Load ("ground/blackHoleBig");
					other.GetComponent<MarathonPlayer> ().SetBlackHoleBig (true);
				} else {
					blackHole = (GameObject)Resources.Load ("ground/blackHole");
					other.GetComponent<MarathonPlayer> ().SetBlackHole (true);
				}
				Instantiate (blackHole, new Vector3 (3.38f, 2.66f, 0), Quaternion.identity);
			}
		}
	}
}
