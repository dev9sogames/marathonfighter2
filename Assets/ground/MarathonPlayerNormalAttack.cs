﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonPlayerNormalAttack : MonoBehaviour {
	float angle;
	float speed;
	float posX;

	bool isReverse;
	BoxCollider2D cldr;

	// Use this for initialization
//	public override void Init() {
	void Start() {
		isReverse = false;

		angle = 0.0f;
		speed = 400;
		cldr = GetComponent<BoxCollider2D> ();
		cldr.isTrigger = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
//		if (!GetComponent<Renderer>().isVisible) {
//			Destroy(this.gameObject);
//		}
		if (transform.position.x > 15 || transform.position.x < -15) {
			DestroyObj ();
		}
		angle -= Time.deltaTime * speed;
		transform.rotation = Quaternion.Euler (0, 0, angle);
		Vector2 position = transform.position;
		if (!isReverse) {
			position.x += Time.deltaTime * 5;
		} else {
			position.x -= Time.deltaTime * 15;
		}
		transform.position = position;
	}
	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "MarathonBoss") {
			DestroyObj ();
		}
		if (other.tag == "Player") {
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			player.HitEnemy (1);
		}
	}
	public void Reverse () {
		isReverse = true;
		cldr.isTrigger = true;
	}

	public void DestroyObj () {
//		ReturnToPool ();
		Destroy (this.gameObject);
	}
}
