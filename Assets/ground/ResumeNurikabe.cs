﻿using UnityEngine;
using System.Collections;

public class ResumeNurikabe : MonoBehaviour {
	float posX;
	Vector3 pos;
	bool isStart;
	// Use this for initialization
	void Start () {
		pos = transform.position;
		posX = transform.position.x;
		isStart = true;
//		StartCoroutine ("StartMove");
	}

	IEnumerator StartMove () {
		yield return new WaitForSeconds (1);
		isStart = true;
	}
	// Update is called once per frame
	void Update () {
		if (isStart) {
			posX -= Time.deltaTime * 5;
			//		Debug.Log (posX + " :INSTANCE_ID " + this.GetInstanceID());
			transform.position = new Vector3 (posX, pos.y, pos.z);
			if (posX <= (-15.0f)) {
				Destroy (this.gameObject);
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player") {
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			player.HitEnemy (99);
		}
	}
}
