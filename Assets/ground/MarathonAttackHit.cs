﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonAttackHit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (hitAndDestroy ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator hitAndDestroy () {
		yield return new WaitForSeconds (0.5f);
		Destroy (this.gameObject);
	}
}
