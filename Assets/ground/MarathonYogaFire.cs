﻿using UnityEngine;
using System.Collections;

public class MarathonYogaFire : MonoBehaviour {
	bool isReverse;
	BoxCollider2D cldr;

	bool isTarget;

	// Use this for initialization
//	public override void Init() {
	void Start () {
		isTarget = false;
		isReverse = false;

		transform.localScale = new Vector3 (1.0f, 1.0f, 0);

		cldr = GetComponent<BoxCollider2D> ();
		cldr.isTrigger = false;
	}

	public void setTargetFlag (bool flag) {
		Debug.Log ("YogaFire setFlag : " + flag);
		isTarget = flag;
	}

	// Update is called once per frame
	void Update () {
		if (isTarget) {
			moveTargetDirection ();
			return;
		}

		Vector2 position = transform.position;
		if (!isReverse) {
			position.x += Time.deltaTime * 5;
		} else {
			position.x -= Time.deltaTime * 15;
		}
		if (position.x >= 17 || position.x <= -15) {
//			Destroy (this.gameObject);
			DestroyObj ();
		}
		transform.position = position;
	}
	void moveTargetDirection () {
		//		Vector2 targetPos = targetObj.transform.position;
		//		float x = targetPos.x;
		//		float y = targetPos.y;
		float x = 3.38f;
		float y = 2.66f;

		Vector2 direction = new Vector2 (x - transform.position.x, y - transform.position.y).normalized;
		GetComponent<Rigidbody2D> ().velocity = (direction * 7);
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "MarathonBoss") {
//			Destroy (this.gameObject);
			DestroyObj ();
		}
		if (other.tag == "Player") {
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			player.HitEnemy (1);
		}
	}
	public void Reverse () {
		isReverse = true;
		cldr.isTrigger = true;
		transform.localScale = new Vector3 (-1.0f, 1.0f, 0);
	}
	public void DestroyObj () {
		Destroy (this.gameObject);
//		ReturnToPool ();
	}

}
