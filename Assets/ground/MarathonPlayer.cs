﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class MarathonPlayer : MonoBehaviour {
	public Button PowerUpButton;
	public Text PowerUpText;
	public AudioClip normalAttackAudio;
	public AudioClip hadoukenAudio;
	public AudioClip yogafireAudio;
	public AudioClip kickAudio;
	public AudioClip upperAudio;
	public AudioClip rollingAudio;
	public AudioClip tornadoDefenseAudio;
	public AudioClip falloutAudio;
	public AudioClip hitenemyAudio;
	public AudioClip gameOverAudio;
	public AudioSource source;

	public float jumpPower;
	public float jumpPowerDoro;
	public float gravityScaleGround;
	public float gravityScaleSea;
	public float gravityScalePlanet;
	public float gravityScaleHane;
	public float gravityScaleDoro;

	public camera camera;

	public bool isTraining;

	Rigidbody2D rgbd;
	GameObject gameController;
	Animator animator;
	ParticleSystem particle;
	FixedJoint2D joint;

	public GameObject cloudSprint;
	bool cloudEnabled;

	int _pointRemainCount;
	public Text PointRemainText;
//	public Slider PointSlider;

	public Sprite MutekiSprite;
	public Sprite NormalSprite;
	public Sprite DamagedSprite;

	public Sprite MiniNurikabe;
	public Sprite MiniNurikabeMuteki;
	public Sprite MiniNurikabeDamage;

	public Sprite Nurikabe;
	public Sprite NurikabeMuteki;
	public Sprite NurikabeDamage;

	public Sprite BlackHero;
	public Sprite BlackHeroMuteki;
	public Sprite BlackHeroDamage;

	public Canvas EventCanvas;

	SpriteRenderer MainSpriteRenderer;

	public Image life1;
	public Image life2;
	public Image life3;

	GameObject rocketWithHero;
	GameObject rocketInstance;

	public Text damageText;

	public Text JumpCountText;

	DateTime mutekiFinishTime;
	bool isMutekiToNormal;

	enum Status {
		Ready,
		Start,
		End
	}
	Status GameState;

	bool isJumping;
	bool isDoro;

	int jumpCount;
	int jumpLimit = 3;

//*************************************//
//	int lifePoint = 2;
	int lifePoint;
	public Slider hpSlider;
	public Text hpText;
//*************************************//

	enum ButtonPos {
		none,
		right,
		rightbottom,
		rightbottomLeft,
		left,
		bottomLeft,
		leftbottom,
		leftbottomRight,
		up,
		upBottom,
		bottom,
		bottomRight,

		upRight,
		upRightBottom,
		upRightBottomLeft,
		upRightBottomLeftUp,
		//		longRightToLeft,
		//		longBottomToRight,
		longRight,
		//		longLeftToRight,
		//		longBottomToLeft,
		longLeft,
		longLeftRight,
		longLeftBottom,
		longLeftBottomRight,
		longLeftBottomRightUp,
		longLeftBottomRightUpLeft,
		longBottom,
		longBottomUp,
		//		longBottomToUp,
	}
	ButtonPos buttonPos;

	enum ButtonDirection {
		left,
		none,
		right,
	}
	ButtonDirection buttonDirection;

	Coroutine rightEndCrt;
	Coroutine leftEndCrt;
	Coroutine UpEndCrt;
	Coroutine BottomEndCrt;

	Coroutine rightMoveCrt;
	Coroutine leftMoveCrt;

	bool isPushRight;
	bool isPushLeft;
	bool isPushBottom;
	bool isAttack;
	bool isNear;
	bool isKick;
	bool isUpper;
	bool isBlackOut;
	bool isBlackHole;
	bool isBlackHoleBig;
	bool isSkebor;
	bool isIce;
	bool isRolling;
	bool isKame;
	bool isUsagi;
	bool onTheIruka;
	bool onTheRocket;

	bool isNormalAttack;
	bool isHit;
	float posX;
	bool isMuteki;
	bool isSnow;
	bool isScrewDriver;
	bool isScrewDriverTrigger;

	bool isLeftMoving;
	bool isRightMoving;

	int pushCount;

	bool isPause;

	bool isUnder;

	bool isEffect;
	bool isBoss;

	public bool isTutorial;
	// 長押し用
	Coroutine longBottomCrt;
	Coroutine longRightCrt;
	Coroutine longLeftCrt;


	GameObject childKick;
	GameObject childUpper;
	GameObject childNormalAttack;
	GameObject screwDriverTrigger;
	GameObject childTornado;
	GameObject childRolling;
	GameObject childHeartProtect;
	GameObject childDefenseImage;
	GameObject childMiniHero;

	GameObject blackOutImage;
	GameObject skeborObj;
	GameObject UpperTornado;
	GameObject item1;
	GameObject item2;
//	public GenericPool item1;
//	public GenericPool item2;
//	public GenericPool UpperTornado;
	GameObject normalAttackOno;
//	public GenericPool normalAttackOno;

	GameObject boss;
//	GameObject blackHole;

	GameObject irukaWithHero;
	GameObject iruka;

	int yogaFireUseFlag;
	int hadoukenUserFlag;
	int kickUseFlag;
	int upperUseFlag;
	int screwUseFlag;

	IEnumerator coroutineMuteki;

	float gravityScale;

	public Image HadoukenUseImage;
	public Image YogafireUseImage;
	public Image HadoukenNoUseImage;
	public Image YogafireNoUseImage;


	float startTime;
	float endTime;

	float limitX1;
	float limitX2;

	GameObject eventRes;

	void Awake () {
		item1 = ResourceCache.Load ("Ground/MarathonHadouken");
		item2 = ResourceCache.Load ("Ground/MarathonYogaFire");
		eventRes = ResourceCache.Load ("Ground/EventRemain");

		rocketWithHero = (GameObject)Resources.Load ("ground/rocketWithHero");
	}

	// Use this for initialization
	void Start () {
		limitX1 = 12;
		limitX2 = -3;

		limitX1 = (limitX1 * (9f / (16f * Screen.height / Screen.width))) + 1;
		limitX2 = (limitX2 * (9f / (16f * Screen.height / Screen.width))) + 1;
//		Debug.Log (limitX1);
//		Debug.Log (limitX2);

//		if (isTutorial) {
//			rgbd = GetComponent<Rigidbody2D> ();
//			animator = GetComponent<Animator> ();
//			jumpCount = 0;
//			_pointRemainCount = 100;
//			return;
//		}
		GameState = Status.Ready;
		jumpCount = 0;
//		life1.enabled = true;
//		life2.enabled = true;

		// cloud spring
		cloudEnabled = true;
		if (AllGameDefinition.gameMode == 4 || isTutorial || AllGameDefinition.StoryStageCourt == 3 || AllGameDefinition.StoryStageType == 5) {
			cloudEnabled = false;
		}

		rgbd = GetComponent<Rigidbody2D> ();
		gravityScale = gravityScaleGround;
		// space or sea
		if (AllGameDefinition.StoryStageType == 2) {
			gravityScale = gravityScalePlanet;
			rgbd.gravityScale = gravityScalePlanet;
		} else if (AllGameDefinition.StoryStageType == 3) {
			gravityScale = gravityScaleSea;
			rgbd.gravityScale = gravityScaleSea;
		}
		rgbd.gravityScale = gravityScale;

		MainSpriteRenderer = GetComponent<SpriteRenderer> ();
		_setSprite ();
		if (isTutorial) {
			_pointRemainCount = 1000000;
		}
		//******************************************************************
		//******************************************************************
		// for debug
//		lifePoint = 1000000;
//		_pointRemainCount = 1000000;
		//******************************************************************
		//******************************************************************
		if (AllGameDefinition.gameMode == 4) { // marathon mode
			lifePoint = 1;
			HadoukenUseImage.gameObject.SetActive (false);
			YogafireUseImage.gameObject.SetActive (false);
			PowerUpButton.gameObject.SetActive (false);
			PowerUpText.gameObject.SetActive (false);
		}

		animator = GetComponent<Animator> ();
		_defaultAnimation ();
//		particle = GetComponent<ParticleSystem> ();
//		particle.Stop ();
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
		joint = GetComponent<FixedJoint2D> ();
//		StartCoroutine ("loop");
		posX = transform.position.x;

		childKick = transform.Find ("playerKick").gameObject;
		childKick.SetActive (false);

		childUpper = transform.Find ("playerUpper").gameObject;
		childUpper.SetActive (false);

		childNormalAttack = transform.Find ("playerNormalAttack").gameObject;
		childNormalAttack.SetActive (false);

		screwDriverTrigger = transform.Find ("ScrewDriverTrigger").gameObject;
		screwDriverTrigger.SetActive (false);

		childTornado = transform.Find ("playerTornado").gameObject;
		childTornado.SetActive (false);

		childRolling = transform.Find ("playerRollingAttack").gameObject;
		childRolling.SetActive (false);

		childHeartProtect = transform.Find ("HeartProtection").gameObject;
		childHeartProtect.SetActive (false);

		childDefenseImage = transform.Find ("defenseImage").gameObject;
		childDefenseImage.SetActive (false);

		childMiniHero = transform.Find ("miniHero").gameObject;
		childMiniHero.SetActive (false);

		normalAttackOno = ResourceCache.Load ("Ground/ono");
		/*
		yogaFireUseFlag = PlayerPrefs.GetInt (Constants.YOGA_FIRE_LOCK_FLAG, 0);
		kickUseFlag = PlayerPrefs.GetInt (Constants.KICK_LOCK_FLAG, 0);
		upperUseFlag = PlayerPrefs.GetInt (Constants.UPPER_LOCK_FLAG, 0);
		screwUseFlag = PlayerPrefs.GetInt (Constants.SCREW_LOCK_FLAG, 0);
		*/
		yogaFireUseFlag = 1;
		hadoukenUserFlag = 1;
		kickUseFlag = 1;
		upperUseFlag = 1;
		screwUseFlag = 1;

		if (AllGameDefinition.gameMode == 4 || isTutorial) {
			transform.Find ("StatusCanvas").gameObject.SetActive (false);
			PointRemainText.enabled = false;
		}

		isCloudEnable = true;
		if (!cloudEnabled) { // for last boss
			isCloudEnable = false;
			cloudSprint.SetActive (false);
		}

		// for performance
		int chuldCount = EventCanvas.transform.childCount;
		Vector3 pos = new Vector3(2500, 1000);
		GameObject eventObj = Instantiate(eventRes, pos, Quaternion.identity) as GameObject;
		eventObj.GetComponent<MarathonEventRemainTime>().StartCountDown(1, 10);
		eventObj.transform.parent = EventCanvas.transform;
		eventObj.transform.localScale = new Vector3 (1, 1, 1);
		eventObj.transform.position = pos;
	}

	void _setSprite () {
//		Debug.Log ("characterType : " + AllGameDefinition.CharacterType);
		switch (AllGameDefinition.CharacterType) {
		case 0:
		case 1:
			int level = AllGameDefinition.level;
			_pointRemainCount = 100;
			_pointRemainCount += (level * 5);
			lifePoint = 10000;
			int lifeBunus = (int)Math.Truncate ((double)level / 10);
			int lifeBunus2 = level % 10;
//			Debug.Log ("life bunus : " + lifeBunus);
//			Debug.Log ("life bunus2 : " + lifeBunus2);
			lifePoint += ((lifeBunus * 3000) + lifeBunus2 * 300);
//			lifePoint += (level * 100);
			hpText.text = lifePoint.ToString ();
			break;
		case 2:
			_pointRemainCount = 150;
			_pointRemainCount += (AllGameDefinition.level * 10);
			lifePoint = 20000;
			lifePoint += (AllGameDefinition.level * 100);
			hpText.text = lifePoint.ToString ();
			MainSpriteRenderer.sprite = MiniNurikabe;
			break;
		case 3:
			_pointRemainCount = 200;
			_pointRemainCount += (AllGameDefinition.level * 10);
			lifePoint = 30000;
			lifePoint += (AllGameDefinition.level * 100);
			hpText.text = lifePoint.ToString ();
			MainSpriteRenderer.sprite = Nurikabe;
			break;
		case 4:
			_pointRemainCount = 300;
			_pointRemainCount += (AllGameDefinition.level * 10);
			lifePoint = 50000;
			lifePoint += (AllGameDefinition.level * 100);
			hpText.text = lifePoint.ToString ();
			MainSpriteRenderer.sprite = BlackHero;
			break;
		default:
			_pointRemainCount = 300;
			_pointRemainCount += (AllGameDefinition.level * 10);
			lifePoint = 10000;
			lifePoint += (AllGameDefinition.level * 100);
			hpText.text = lifePoint.ToString ();
			MainSpriteRenderer.sprite = MiniNurikabe;
			break;
		}
	}
	void OnApplicationPause (bool pauseStatus)
	{
		if (pauseStatus) {
			isPause = true;
		} else {
			if (isPause) {
				isPause = false;
			}
		}
	}

	public void SetReadyState () {
		GameState = Status.Ready;
		_defaultAnimation ();
//		_animatorRun (false);
//		animator.SetBool ("run", false);
	}
	public void SetStartState () {
		GameState = Status.Start;
		_animatorRun (true);
//		animator.SetBool ("run", true);
	}

	// tutorial start
	public void TrainingStart () {
		GameState = Status.Start;
		animator.SetBool ("run", true);
	}
	public void TrainingFinish () {
		animator.SetBool ("run", false);
	}

	public void GameStart () {
		GameState = Status.Start;
		_animatorRun (true);
//		animator.SetBool ("run", true);
		StartCoroutine ("AddPoint");
	}
	IEnumerator AddPoint () {
		while (!isHit) {
			yield return new WaitForSeconds (5);
			if (!isPause && GameState == Status.Start) {
				_pointRemainCount++;
			}
		}
	}
	IEnumerator loop () {
		while (true) {
			yield return new WaitForSeconds (1);
			_sendPosition ();
		}
	}

	void _sendPosition () {
		gameController.SendMessage ("sendPosition", transform.position.x);
	}
	public void bossBattle () {
//		Debug.Log ("isBoss = true");
		isBoss = true;
	}

	void FixedUpdate () {
//		Debug.Log (buttonPos);
		if (isTraining) {
			if (rgbd.velocity.y == 0) {
				jumpCount = 0;
			}
			return;
		}
//		DebugText.text = buttonPos.ToString();
		if (isBoss) {
			if (!isScrewDriver) {
				rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
			}

			if (isRolling) {
				return;
			}

			if (!isIce) {
				Vector2 velocity = rgbd.velocity;
//				if (buttonPos == ButtonPos.right || buttonPos == ButtonPos.longRight) {
				if (buttonDirection == ButtonDirection.right && transform.position.x < limitX1) {
//				if (isRightMoving) {
					velocity.x = 7.0f;
//				} else if (buttonPos == ButtonPos.left || buttonPos == ButtonPos.longLeft) {
				} else if (buttonDirection == ButtonDirection.left && transform.position.x > limitX2) {
//				} else if (isLeftMoving) {
					velocity.x = -7.0f;
				} else {
					velocity.x = 0.0f;
				}
				rgbd.velocity = velocity;
			}

			if (transform.position.x < -4) {
				transform.position = new Vector3 (-3, transform.position.y, 0);
//				buttonDirection = ButtonDirection.none;
			}
			if (transform.position.x > 13) {
				transform.position = new Vector3 (12, transform.position.y, 0);
//				buttonDirection = ButtonDirection.none;
			}
		} else {
			if (!isRolling && !isScrewDriver) {
				if (transform.position.x < -4) {
					transform.position = new Vector3 (posX, transform.position.y, transform.position.z);
				}
				if (transform.position.x > 0) {
					transform.position = new Vector3 (posX, transform.position.y, transform.position.z);
				}
			}
		}

		if (!isUnder && transform.position.y < -7 && AllGameDefinition.gameMode != 4 && !isTutorial) {
			isUnder = true;

			GameObject bridge = ResourceCache.Load ("Ground/bridge");
			GameObject bridgeInstance = Instantiate (bridge, new Vector2 (0, 0), Quaternion.identity) as GameObject;
			StartCoroutine (RemoveBridge (bridgeInstance));
			StartCoroutine (UnderGroundRemove ());
		}
	}
	IEnumerator jumpKickFinish () {
		yield return new WaitForSeconds (0.5f);
		isMuteki = false;
		isKick = false;
		animator.SetBool("jumpKick", false);
		childKick.SetActive (false);
	}

	public void ResetCloud () {
		StartCoroutine (UnderGroundRemove ());
	}

	bool isCloudEnable;
	IEnumerator UnderGroundRemove () {
		yield return new WaitForSeconds (3);
		isCloudEnable = false;
		cloudSprint.SetActive (false);
		StartCoroutine (UnderGroundEnabeled ());
	}
	IEnumerator RemoveBridge (GameObject bridgeInstance) {
		yield return new WaitForSeconds (1);
		Destroy (bridgeInstance.gameObject);
		isUnder = false;
	}

	IEnumerator UnderGroundEnabeled () {
		yield return new WaitForSeconds (15);

		if (!cloudEnabled) {
			isCloudEnable = true;
			cloudSprint.SetActive (true);
		}
		isUnder = false;
	}

	// Update is called once per frame
	void Update () {
		float velocityY = rgbd.velocity.y;
		if (velocityY != 0) {
			isJumping = true;
			//		} else if (transform.position.y < -3.2f) {
		} else {
			if (jumpCount != 0 && !isKick) {
				//			Debug.Log ("jump count reset");
				isJumping = false;
				jumpCount = 0;
			}
		}

		if (isKick && velocityY == 0) {
			//			Debug.Log ("kick muteki finish");
			StartCoroutine ("jumpKickFinish");
		}
		if (isUpper && velocityY < 0) {
			//			Debug.Log ("upper muteki finish");
			isMuteki = false;
			isUpper = false;
			animator.SetBool ("upper", false);
			childUpper.SetActive (false);

			UpperTornado = ResourceCache.Load ("Ground/MarathonTornado");
			//Vector3 pos = new Vector3 (transform.position.x, transform.position.y + 1.0f, 1);
			Instantiate (UpperTornado, transform.position, Quaternion.identity);
			//			UpperTornado.Place(transform.position);
		}
		if (isScrewDriver && velocityY == 0) {
			Debug.Log ("Screw Finish");
			// finish screw driver
			isMuteki = false;
			isScrewDriver = false;
			joint.connectedBody = null;
			joint.enabled = false;

			boss.GetComponent<MarathonBossBlackController> ().HitScrewDriver ();
		}

		if (jumpCount <= 3) {
			JumpCountText.text = jumpCount.ToString ();
		} else {
			JumpCountText.text = "3";
		}
//		Debug.Log (buttonPos);

		//		Debug.Log (lifePoint);
		/*
		if (transform.position.y > -2.0f) {
			isJumping = true;
		} else {
			isJumping = false;
			jumpCount = 0;
		}
		*/

		if (isHit) {
			rgbd.constraints = RigidbodyConstraints2D.None;
		}
		if (!(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)) {
			if (Input.GetKeyDown ("space")) {
				JumpPlayer ();
			}
			if (Input.GetKeyDown ("a")) {
				hadouken ();
			}
			if (Input.GetKeyDown ("b")) {
				yogaFire ();
			}
			if (Input.GetKeyDown ("v")) {
				Kick ();
			}
			if (Input.GetKeyDown ("f")) {
				upper ();
			}
			if (Input.GetKeyDown ("r")) {
				RollingAttack ();
			}
			if (Input.GetKeyDown ("t")) {
				TornadoAppear ();
			}
			if (Input.GetKeyDown ("n")) {
				normalAttack ();
			}
			if (Input.GetKeyDown ("up")) {
				UpPushDown ();
			}
			if (Input.GetKeyUp ("up")) {
				UpPushUp ();
			}
			if (Input.GetKeyDown ("down")) {
				BottomPushDown ();
			}
			if (Input.GetKeyUp ("down")) {
				BottomPushUp ();
			}
			if (Input.GetKeyDown ("left")) {
				LeftPushDown ();
			}
			if (Input.GetKeyUp ("left")) {
				LeftPushUp ();
			}
			if (Input.GetKeyDown ("right")) {
				RightPushDown ();
			}
			if (Input.GetKeyUp ("right")) {
				RightPushUp ();
			}
		}
		if (isTraining || isTutorial) {
			return;
		}
		if (isMuteki && !isMutekiToNormal) {
			if ((mutekiFinishTime - DateTime.Now).Seconds > 0 && (mutekiFinishTime - DateTime.Now).Seconds < 3) {
//				Debug.Log ("animator change");
				isMutekiToNormal = true;
				animator.SetBool ("mutekiToNormal", true);
			}
		}
		if (GameState != Status.Ready) {
			PointRemainText.text = "Action : " + _pointRemainCount;
//			PointSlider.value = _pointRemainCount;
		}
	}

	public void JumpPlayer () {
//		Debug.Log ("status " + GameState);
//		Debug.Log ("jumpcount : " + jumpCount);
		if (GameState != Status.Start) {
			return;
		}
		if (isTraining && (jumpCount < jumpLimit)) {
			rgbd.AddForce (Vector2.up * jumpPower);
			jumpCount++;
			return;
		}
		if (isIce) {
			return;
		}
		if (isSnow) {
			return;
		}
		jumpCount++;
//		Debug.Log (jumpCount);
		if (jumpCount < jumpLimit || transform.position.y < -5) {
			if (isDoro) {
				rgbd.AddForce (Vector2.up * 200);
			} else {
				rgbd.AddForce (Vector2.up * 300);
			}
		}
	}

	public void JumpPlayerUp () {
//		if (GameState != Status.Start) {
//			return;
//		}
		if (isTraining && (jumpCount < jumpLimit)) {
			rgbd.AddForce (Vector2.up * jumpPower);
			jumpCount++;
			return;
		}
		if (isIce) {
			return;
		}
		if (isSnow) {
			return;
		}
//		jumpCount++;
//		Debug.Log ("up " + jumpCount);
		if (jumpCount < jumpLimit || transform.position.y < -5) {
//			Debug.Log (rgbd.velocity.y);
			float velocity = rgbd.velocity.y;
			if (velocity > 0) {
				rgbd.velocity = new Vector3 (rgbd.velocity.x, velocity * 0.6f, 0);
			}
		}
	}
	public int JumpDownVelocity;
	public void JumpPlayerDown () {
//		if (GameState != Status.Start) {
//			return;
//		}
		if (isTraining && (jumpCount < jumpLimit)) {
			rgbd.AddForce (Vector2.up * jumpPower);
			jumpCount++;
			return;
		}
		if (isIce) {
			return;
		}
		if (isSnow) {
			return;
		}
//		Debug.Log ("donw " + jumpCount);
		if (jumpCount < jumpLimit || transform.position.y < -5) {
			switch (jumpCount) {
			case 0:
				if (rgbd.velocity.y < 1) {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, 0.7f, 0);
				} else {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, JumpDownVelocity, 0);
				}
				if (isDoro) {
					rgbd.AddForce (Vector2.up * jumpPowerDoro);
				} else {
					rgbd.AddForce (Vector2.up * jumpPower);
				}
				break;
			case 1:
				if (rgbd.velocity.y < 1) {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, 0.7f, 0);
				} else {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, JumpDownVelocity, 0);
				}
				if (isDoro) {
					rgbd.AddForce (Vector2.up * jumpPowerDoro * 0.7f);
				} else {
					rgbd.AddForce (Vector2.up * jumpPower * 0.7f);
				}
				break;
			case 2:
				if (rgbd.velocity.y < 1) {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, 0.7f, 0);
				} else {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, JumpDownVelocity, 0);
				}
				if (isDoro) {
					rgbd.AddForce (Vector2.up * jumpPowerDoro * 0.7f);
				} else {
					rgbd.AddForce (Vector2.up * jumpPower * 0.7f);
				}
				break;
			}
			jumpCount++;
		}
	}

	IEnumerator FinishThunder () {
		yield return new WaitForSeconds (3);
		isIce = false;
		MainSpriteRenderer.sprite = NormalSprite;
		animator.SetBool ("damage", false);
	}
	IEnumerator GoNormalSprite () {
		yield return new WaitForSeconds (0.5f);
//		Debug.Log ("GO Normal Sprite");
		MainSpriteRenderer.sprite = NormalSprite;
		isMuteki = false;
		animator.SetBool ("damage", false);
	}

	public bool HitEnemy (int enemyNo) {
//		Debug.Log ("enemy no : " + enemyNo);
		switch (enemyNo) {
		case 1: // enmey
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (1000);
			break;
		case 2: // nurikabe
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (2000);
			break;
		case 3: // enemy Jump
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (1000);
			break;
		case 4: // enemy come 1
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (1000);
			break;
		case 5: // enemy falling
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (1000);
			break;
		case 6: // doro
			if (!isDoro) {
				isDoro = true;
				_setRemainTime (1, 5f);
				StartCoroutine (DropDoro ());
			}
			break;
		case 7: // muteki
			_muteki ();
			break;
		case 8: // beef
			_pointRemainCount = _pointRemainCount + 50;
			lifePoint += 2000;
			hpText.text = lifePoint.ToString ();
			break;
		case 9: // dokuro
			_pointRemainCount = _pointRemainCount - 30;
			if (_pointRemainCount < 0) {
				_pointRemainCount = 0;
			}
			break;
		case 10: // enemy slide
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (2000);
			break;
		case 11: // mirror
			break;
		case 12: // muteki big
			_mutekiBig ();
			break;
		case 13: // nurikabe mini
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (1000);
			break;
		case 14: // life
			_lifeUp ();
			break;
		case 16: // blackOutImage
			_blackOut ();
			break;
		case 18: // skebor
			if (!isSkebor) {
				isSkebor = true;
				_setRemainTime (8, 5f);
				_skebor ();
			}
			break;
		case 19: // kame
			if (!isKame) {
				isKame = true;
				_setRemainTime (6, 5f);
				gameController.SendMessage ("KameTimeStart");
				StartCoroutine (KameTimeStart ());
			}
			break;
		case 20: // usagi
			if (!isUsagi) {
				isUsagi = true;
				_setRemainTime (5, 5f);
				gameController.SendMessage ("UsagiTimeStart");
				StartCoroutine (UsagiTimeStart ());
			}
			break;
		case 21: // hane
			if (rgbd.gravityScale != gravityScaleHane) {
				_setRemainTime (2, 5f);
//				Debug.Log ("hane start" + DateTime.Now);
				rgbd.gravityScale = gravityScaleHane;
				StartCoroutine (NormalGravity ());
			}
			break;
		case 22: // bomb
			break;
		case 23: // bombAfter(bakuhatu)
			_hitEnemyAttack (2000);
			break;
		case 26: // ika
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (1000);
			break;
		case 27: // big ika
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (2000);
			break;
		case 29: // bubble
			if (!isBubble) {
				_setRemainTime (12, 5f);
				_hitBubble ();
				isBubble = true;
			}
			break;
		case 30: // fish
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (1500);
			break;
		case 31: // rain
			if (!isDoro) {
				isDoro = true;
				_setRemainTime (11, 5f);
				StartCoroutine (DropDoro ());
			}
			break;
		case 32: // iruka
			if (!onTheIruka) {
				onTheIruka = true;
				_setRemainTime (9, 5f);
				_hitIruka ();
			}
			break;
		case 33: // sanbo
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (3000);
			break;
		case 34: // saboten
		case 35: // saboten and attack
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (3000);
			break;
		case 36: //saboten toge
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (1000);
			break;
		case 37: //taiyo
			if (isMuteki) {
				return false;
			}
			_hitEnemyAttack (1500);
			break;
		case 45: // mini hero create
			_showMiniHero ();
			break;
		case 47: // typhoon
			_hitTyphoon ();
			break;
		case 50: // bridge broken
			break;
		case 51:
			gameController.SendMessage ("GetBattlePoint");
			break;
		case 62: // rockets(with hero)
			if (!onTheRocket) {
				onTheRocket = true;
				_setRemainTime (10, 5f);
				_goWithRocket ();
			}
			break;
		case 93: // boss sliding
			_hitEnemyAttack (2500);
			break;
		case 94: // boss jump kick
			_hitEnemyAttack (1500);
			break;
		case 95: // boss body attack
			_hitEnemyAttack (1500);
			break;
		case 96: // boss attack Dragon
			_hitEnemyAttack (3000);
			break;
		case 97: // boss attack thunder
			if (!isIce) {
				_setRemainTime (4, 3f);
				_thunder ();
				isIce = true;
			}
			break;
		case 98: // boss attack ice
			if (!isIce) {
				_setRemainTime (3, 3f);
				isIce = true;
				_ice ();
			}
			break;
		case 99: // boss attack homing,other boss attack
			_hitEnemyAttack (1500);
			break;
		case 40: // sandStorm
			if (!isDoro) {
				isDoro = true;
				_setRemainTime (7, 8f);
				StartCoroutine (DropDoro (8));
			}
			break;
		default:
			if (!isMuteki) {
				_hitEnemyAttack (1000);
			} else {
				return false;
			}
			break;
		}

		return true;

		/*
		if (enemyNo == 99) {
		}

		if (enemyNo == 6) {
		} else if (enemyNo == 7) { // muteki
		} else if (enemyNo == 12) { //muteki big
		} else if (enemyNo == 14) { // lifeUp
		} else if (enemyNo == 16) {
		} else if (enemyNo == 8) { // beef
		} else if (enemyNo == 9) { // dokuro
		} else if (enemyNo == 18) {
		} else if (enemyNo == 11) {
			// mirror
		} else if (enemyNo == 19) {
			// kame
		} else if (enemyNo == 20) {
			// usgai
		} else if (enemyNo == 21) {
			// hane
		} else if (enemyNo == 98) {
		} else if (enemyNo == 97) {
		} else {
			if (!isMuteki) {
				lifePoint -= 1000;
				hpText.text = lifePoint.ToString ();
				hpSlider.value = lifePoint;
				Debug.Log ("Damage!!!!!");
				MainSpriteRenderer.sprite = DamagedSprite;
				isMuteki = true;
				animator.SetBool ("damage", true);
				StartCoroutine (GoNormalSprite());
				_lifePointImage ();
				if (lifePoint < 1) {
					isHit = true;
					rgbd.AddForce (Vector2.left * 3000);
					gameController.SendMessage ("GameOver");
					GameState = Status.End;
				}
			} else {
				// muteki
				return false;
			}
		}
		return true;
		*/
	}

	IEnumerator KameTimeStart () {
		yield return new WaitForSeconds (5);
		isKame = false;
	}
	IEnumerator UsagiTimeStart () {
		yield return new WaitForSeconds (5);
		isUsagi = false;
	}
	void _goWithRocket () {
		if (!isMuteki) {
			isMuteki = true;
			MainSpriteRenderer.color = new Color(255/255, 255/255, 255/255, 0);
			rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
			rocketInstance = Instantiate (rocketWithHero, transform.position, Quaternion.identity) as GameObject;
			StartCoroutine (rocketEnd ());
		}
	}
	IEnumerator rocketEnd () {
		yield return new WaitForSeconds (5);
		onTheRocket = false;
		isMuteki = false;
		Destroy (rocketInstance.gameObject);
//		MainSpriteRenderer.sprite = NormalSprite;
		MainSpriteRenderer.color = new Color(255/255, 255/255, 255/255, 255/255);
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
//		rgbd.constraints = RigidbodyConstraints2D.None;
//		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX;
//		rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
	}

	void _setRemainTime (int type, float time) {

		int chuldCount = EventCanvas.transform.childCount;
//		float eventObjX = 960f / (Screen.width / 1920f);
//		float eventObjY = 540f / (Screen.height / 1080f);
		float posY = (790 - (chuldCount * 150)) * (Screen.height / 1080f);
		Vector3 pos = new Vector3(1460 * (Screen.width / 1920f), posY);
		GameObject eventObj = Instantiate(eventRes, pos, Quaternion.identity) as GameObject;
		eventObj.GetComponent<MarathonEventRemainTime>().StartCountDown(type, time);
		eventObj.transform.parent = EventCanvas.transform;
		eventObj.transform.localScale = new Vector3 (1, 1, 1);
		eventObj.transform.position = pos;
	}

	void _hitTyphoon () {
		rgbd.AddForce (Vector2.up * 500);
	}
	void _showMiniHero () {
		childMiniHero.SetActive (true);
		childMiniHero.GetComponent<MarathonMiniPlayer> ().SetAliveFlag (true);
		StartCoroutine (_disableMiniHero ());
	}
	IEnumerator _disableMiniHero () {
		yield return new WaitForSeconds (10);
		childMiniHero.GetComponent<MarathonMiniPlayer> ().SetAliveFlag (false);
		childMiniHero.SetActive (false);
	}
	void _muteki () {
		if (!isMuteki) {
			isMuteki = true;
			mutekiFinishTime = DateTime.Now.AddSeconds (10);
			MainSpriteRenderer.sprite = MutekiSprite;
			animator.SetBool ("muteki", true);
			coroutineMuteki = MutekiFinish (10);
//			Debug.Log ("muteki coroutine1 : " + coroutineMuteki);
			StartCoroutine (coroutineMuteki);
		}
	}
	void _mutekiBig () {
		if (!isMuteki) {
			isMuteki = true;
			mutekiFinishTime = DateTime.Now.AddSeconds (15);
			MainSpriteRenderer.sprite = MutekiSprite;
			transform.Find ("StatusCanvas").gameObject.SetActive (false);
			animator.SetBool ("muteki", true);
			transform.localScale = new Vector3 (-0.5f, 0.5f, 0);
			coroutineMuteki = MutekiFinish (15);
//			Debug.Log ("muteki coroutine2 : " + coroutineMuteki);
			StartCoroutine (coroutineMuteki);
		}
	}
	void _lifeUp () {
		if (lifePoint <= 3) {
			lifePoint += 3000;
			hpText.text = lifePoint.ToString ();
			hpSlider.value = lifePoint;
			_lifePointImage ();
		}
	}

	void _blackOut () {
		if (!isBlackOut) {
			isBlackOut = true;
			GameObject black = (GameObject)Resources.Load ("Ground/blackOutImage");
			blackOutImage = Instantiate (black, new Vector3 (1.6f, -1.8f, 0), Quaternion.identity) as GameObject;
			//				blackOutImage.GetComponent<MarathonBlackHoleController> ().enabled = true;
			StartCoroutine (blackImageDisabled());
		}
	}
	void _skebor () {
		GameObject skebor = (GameObject)Resources.Load ("Ground/skebor");
		skeborObj = Instantiate (skebor, new Vector3 (0, 2, 0), Quaternion.identity) as GameObject;
		transform.position = new Vector3 (0, 3, 0);
		StartCoroutine ("DestroySkebor");
	}
	void _ice () {
		animator.SetBool ("run", false);
		transform.rotation = Quaternion.Euler (0, 0, 90);
		StartCoroutine (FinishIcing ());
	}
	bool isBubble;
	void _hitBubble () {
		animator.SetBool ("run", false);
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
//		rgbd.gravityScale = 0;
//		rgbd.velocity = Vector3.zero;
		StartCoroutine (FinishBubble ());
	}
	void _thunder () {
		animator.SetBool ("damage", true);
		StartCoroutine (FinishThunder ());
	}

	void _hitIruka () {
		if (!isMuteki) {
			isMuteki = true;
//			rgbd.constraints = RigidbodyConstraints2D.FreezePositionY;
			rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
			irukaWithHero = ResourceCache.Load ("Ground/iruka");
			iruka = Instantiate (irukaWithHero, transform.position, Quaternion.identity);
			transform.position = new Vector3 (transform.position.x, transform.position.y + 0.5f, transform.position.z);
			StartCoroutine (irukaEnd ());
		}
	}

	IEnumerator FinishBubble () {
		yield return new WaitForSeconds (5);
		isBubble = false;
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		animator.SetBool ("run", true);
	}
	IEnumerator irukaEnd () {
		yield return new WaitForSeconds (5);
		onTheIruka = false;
		isMuteki = false;
		Destroy (iruka.gameObject);
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
//		rgbd.constraints = RigidbodyConstraints2D.None;
//		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX;
//		rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
	}

	IEnumerator FinishIcing () {
		yield return new WaitForSeconds (3);
		isIce = false;
		animator.SetBool ("run", true);
		rgbd.gravityScale = gravityScale;
		transform.rotation = Quaternion.Euler (0, 0, 0);
	}
	IEnumerator NormalGravity () {
		yield return new WaitForSeconds (5);
//		Debug.Log ("hane end" + DateTime.Now);
		rgbd.gravityScale = gravityScale;
	}
	IEnumerator DestroySkebor () {
		yield return new WaitForSeconds (5);
		Destroy (skeborObj.gameObject);
		isSkebor = false;
	}
	IEnumerator blackImageDisabled () {
		yield return new WaitForSeconds (5);
		Destroy (blackOutImage.gameObject);
		isBlackOut = false;
	}

	void _hitEnemyAttack (int damage = 1000) {
		damage = _getAttackDamage (damage);
		if (!isMuteki) {
			if (buttonPos == ButtonPos.left) {
				childDefenseImage.SetActive (true);
				StartCoroutine (FinishDefenseImage ());
				damage = damage / 2;
			}
			lifePoint -= damage;

			displayDamage (damage);

			hpText.text = lifePoint.ToString ();
			hpSlider.value = lifePoint;
//			Debug.Log ("Damage!!!!!");
			MainSpriteRenderer.sprite = DamagedSprite;
			isMuteki = true;
			animator.SetBool ("damage", true);
			StartCoroutine (GoNormalSprite ());
			_lifePointImage ();
			if (lifePoint < 1) {
				isHit = true;
				AudioPlay (gameOverAudio);
				rgbd.AddForce (Vector2.left * 3000);
				gameController.SendMessage ("GameOver");
				GameState = Status.End;
			}
		}
	}

	/**
	 * decrease damage by user level
	**/
	int _getAttackDamage (int damage) {
		AudioPlay (hitenemyAudio);
		int level = AllGameDefinition.level;
		double ratio = level / 100d;
		return (int)(damage * (1d - ratio));
	}
	IEnumerator FinishDefenseImage () {
		yield return new WaitForSeconds (0.3f);
		childDefenseImage.SetActive(false);
	}
	void displayDamage (int damage) {
		MarathonPlayerDamageText text = damageText.GetComponent<MarathonPlayerDamageText> ();
		text.StartText (damage);
	}

	void _lifePointImage () {
		switch (lifePoint) {
		case 1:
			life1.enabled = true;
			life2.enabled = false;
			life3.enabled = false;
			break;
		case 2:
			life1.enabled = true;
			life2.enabled = true;
			life3.enabled = false;
			break;
		case 3:
			life1.enabled = true;
			life2.enabled = true;
			life3.enabled = true;
			break;
		}
	}
	IEnumerator DropDoro (int time = 5) {
		yield return new WaitForSeconds (time);
		isDoro = false;
	}

	IEnumerator MutekiFinish (int num) {
		yield return new WaitForSeconds (num);
//		Debug.Log ("muteki finish");
		isMuteki = false;
		isMutekiToNormal = false;
		transform.Find ("StatusCanvas").gameObject.SetActive (true);
		MainSpriteRenderer.sprite = NormalSprite;
		animator.SetBool ("mutekiToNormal", false);
		animator.SetBool ("muteki", false);
		transform.localScale = new Vector3 (-0.1f, 0.1f, 0);
	}
	public void RightPushDown () {
		buttonDirection = ButtonDirection.right;
//		transform.localScale = new Vector3 (-0.3f, transform.localScale.y, 0);
		switch (buttonPos) {
		case ButtonPos.bottom:
			buttonPos = ButtonPos.bottomRight;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
/*		case ButtonPos.longBottom:
			buttonPos = ButtonPos.longBottomToRight;
			break;
			*/
		case ButtonPos.leftbottom:
			buttonPos = ButtonPos.leftbottomRight;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
		case ButtonPos.up:
			buttonPos = ButtonPos.upRight;
			if (UpEndCrt != null) {
				StopCoroutine (UpEndCrt);
			}
			break;
		case ButtonPos.longLeft:
			buttonPos = ButtonPos.longLeftRight;
			break;
		case ButtonPos.longLeftBottom:
			buttonPos = ButtonPos.longLeftBottomRight;
			break;
			/*
		case ButtonPos.longLeft:
			buttonPos = ButtonPos.longLeftToRight;
			break;
			*/
		default:
			buttonPos = ButtonPos.right;
			longRightCrt = StartCoroutine (LongRightPush ());
			rightMoveCrt = StartCoroutine (RightMoving ());
			break;
		}
//		StartCoroutine ("RightPushEnd");
	}
	public void RightPushUp () {
		buttonDirection = ButtonDirection.none;
		if (longRightCrt != null) {
			StopCoroutine (longRightCrt);
		}
		if (rightMoveCrt != null) {
			StopCoroutine (rightMoveCrt);
		}
		isRightMoving = false;
		rightEndCrt = StartCoroutine (RightPushEnd ());
		//		buttonPos = ButtonPos.none;
//		StopCoroutine (rightCrt);
	}
	public void LeftPushDown () {
		buttonDirection = ButtonDirection.left;
//		transform.localScale = new Vector3 (0.3f, transform.localScale.y, 0);
		switch (buttonPos) {
		case ButtonPos.bottom:
			buttonPos = ButtonPos.bottomLeft;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
			/*
		case ButtonPos.longBottom:
			buttonPos = ButtonPos.longBottomToLeft;
			break;
			*/
		case ButtonPos.rightbottom:
			buttonPos = ButtonPos.rightbottomLeft;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
		case ButtonPos.upRightBottom:
			buttonPos = ButtonPos.upRightBottomLeft;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
		case ButtonPos.longLeftBottomRightUp:
			buttonPos = ButtonPos.longLeftBottomRightUpLeft;
			break;
		default:
			buttonPos = ButtonPos.left;
			longLeftCrt = StartCoroutine (LongLeftPush ());
			leftMoveCrt = StartCoroutine (LeftMoving ());
			break;
		}
//		StartCoroutine ("LeftPushEnd");
	}
	public void LeftPushUp () {
		buttonDirection = ButtonDirection.none;
		if (longLeftCrt != null) {
			StopCoroutine (longLeftCrt);
		} else {
			buttonPos = ButtonPos.none;
		}
		if (leftMoveCrt != null) {
			StopCoroutine (leftMoveCrt);
		}
		isLeftMoving = false;
		leftEndCrt = StartCoroutine ("LeftPushEnd");
		//		buttonPos = ButtonPos.none;
	}
	public void UpPushDown () {
		switch (buttonPos) {
		case ButtonPos.bottom:
			buttonPos = ButtonPos.upBottom;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
		case ButtonPos.upRightBottomLeft:
			buttonPos = ButtonPos.upRightBottomLeftUp;
			break;
		case ButtonPos.longBottom:
			buttonPos = ButtonPos.longBottomUp;
			break;
		case ButtonPos.longLeftBottomRight:
			buttonPos = ButtonPos.longLeftBottomRightUp;
			break;
		default:
			buttonPos = ButtonPos.up;
			break;
		}
			
		/*
		if (transform.position.y >= 0.65) {
			return;
		}
		*/
//		animator.SetBool ("upper", true);
		/*
		if (buttonPos == ButtonPos.bottom) {
			buttonPos = ButtonPos.upBottom;
		} else {
			buttonPos = ButtonPos.up;
		}
		*/
	}
	public void UpPushUp () {
		UpEndCrt = StartCoroutine ("UpPushEnd");
	}
	public void BottomPushDown () {
		switch (buttonPos) {
		case ButtonPos.right:
			buttonPos = ButtonPos.rightbottom;
			if (rightEndCrt != null) {
				StopCoroutine (rightEndCrt);
			}
			break;
		case ButtonPos.left:
			buttonPos = ButtonPos.leftbottom;
			if (leftEndCrt != null) {
				StopCoroutine (leftEndCrt);
			}
			break;
		case ButtonPos.upRight:
			buttonPos = ButtonPos.upRightBottom;
			if (rightEndCrt != null) {
				StopCoroutine (rightEndCrt);
			}
			break;
		case ButtonPos.longLeft:
			buttonPos = ButtonPos.longLeftBottom;
			break;
		default:
			isPushBottom = false;
			buttonPos = ButtonPos.bottom;
			break;
		}
		//		StartCoroutine ("BottomPushEnd");
		longBottomCrt = StartCoroutine (LongBottomPush ());
	}
	public void BottomPushUp () {
		isPushBottom = true;
		BottomEndCrt = StartCoroutine ("BottomPushEnd");
		if (longBottomCrt != null) {
			StopCoroutine (longBottomCrt);
		}
	}

	IEnumerator UpPushEnd () {
		yield return new WaitForSeconds (0.2f);
		if (buttonPos != ButtonPos.upRight && buttonPos != ButtonPos.upRightBottom && buttonPos != ButtonPos.upRightBottomLeft
			&& buttonPos != ButtonPos.longLeftBottomRightUpLeft
		) {
//			Debug.Log ("UpPushEnd");
			buttonPos = ButtonPos.none;
		}
	}
	IEnumerator RightPushEnd () {
		yield return new WaitForSeconds (0.2f);
		if (buttonPos != ButtonPos.rightbottom && buttonPos != ButtonPos.rightbottomLeft
			&& buttonPos != ButtonPos.upRightBottom && buttonPos != ButtonPos.upRightBottomLeft
			&& buttonPos != ButtonPos.upRightBottomLeftUp && buttonPos != ButtonPos.longLeftBottomRightUp
			&& buttonPos != ButtonPos.longLeftBottomRightUpLeft
		) {
//			Debug.Log ("RightPushEnd");
			buttonPos = ButtonPos.none;
		}
	}
	IEnumerator LeftPushEnd () {
		yield return new WaitForSeconds (0.2f);
		if (buttonPos != ButtonPos.leftbottom && buttonPos != ButtonPos.leftbottomRight && buttonPos != ButtonPos.upRightBottomLeftUp
			&& buttonPos != ButtonPos.longLeftBottom && buttonPos != ButtonPos.longLeftBottomRight && buttonPos != ButtonPos.longLeftBottomRightUp
			&& buttonPos != ButtonPos.longLeftRight
		) {
//			Debug.Log ("LeftPushEnd");
			buttonPos = ButtonPos.none;
		}
	}
	IEnumerator BottomPushEnd () {
		yield return new WaitForSeconds (0.2f);
		if (buttonPos != ButtonPos.bottomLeft && buttonPos != ButtonPos.bottomRight && buttonPos != ButtonPos.upBottom
			&& buttonPos != ButtonPos.upRightBottomLeft && buttonPos != ButtonPos.upRightBottomLeftUp && 
			buttonPos != ButtonPos.longBottomUp && buttonPos != ButtonPos.longLeftBottomRight && buttonPos != ButtonPos.longLeftBottomRightUp
			&& buttonPos != ButtonPos.longLeftBottomRightUpLeft
			/*
			&& buttonPos != ButtonPos.longBottomToLeft && buttonPos != ButtonPos.longBottomToRight
			&& buttonPos != ButtonPos.longBottomToUp
			*/
		) {
//			Debug.Log ("BottomPushEnd");
			buttonPos = ButtonPos.none;
		}
	}

	IEnumerator LongRightPush () {
		yield return new WaitForSeconds (1);
		buttonPos = ButtonPos.longRight;
	}
	IEnumerator LongBottomPush () {
		yield return new WaitForSeconds (1);
		buttonPos = ButtonPos.longBottom;
	}
	IEnumerator LongLeftPush () {
		yield return new WaitForSeconds (1);
		buttonPos = ButtonPos.longLeft;
	}
	IEnumerator LeftMoving () {
		yield return new WaitForSeconds (0.2f);
		isLeftMoving = true;
	}
	IEnumerator RightMoving () {
		yield return new WaitForSeconds (0.2f);
		isRightMoving = true;
	}

	public void Attack () {
//		Debug.Log ("Attack" + buttonPos);
		if (GameState != Status.Start) {
			return;
		}
		if (isIce) {
			return;
		}
		if (_pointRemainCount <= 0) {
			return;
		}

		switch (buttonPos) {
		case ButtonPos.bottomRight:
			hadouken ();
			break;
		case ButtonPos.bottomLeft:
			if (_pointRemainCount < 3) {
				return;
			}
			Kick ();
			break;
		case ButtonPos.longBottomUp:
			if (_pointRemainCount < 10) {
				return;
			}
			TornadoAppear ();
			break;
		case ButtonPos.longLeftRight:
			if (_pointRemainCount < 10) {
				return;
			}
			RollingAttack ();
			break;
			/*
		case ButtonPos.rightbottomLeft:
			break;
*/
		case ButtonPos.leftbottomRight:
			if (_pointRemainCount < 5) {
				return;
			}
			yogaFire ();
			return;
		case ButtonPos.upBottom:
			if (_pointRemainCount < 3) {
				return;
			}
			upper ();
			break;
		case ButtonPos.upRightBottomLeftUp:
			if (_pointRemainCount < 50) {
				return;
			}
			screwDriver ();
			break;
		case ButtonPos.longLeftBottomRightUpLeft:
			if (_pointRemainCount < 100 || isMuteki) {
				return;
			}
			HeartProtection ();
			break;
		default:
			normalAttack ();
			break;
		}

		StartCoroutine ("StopAttack");
		isAttack = true;
	}
	IEnumerator StopAttack () {
		yield return new WaitForSeconds (1);
//		animator.SetBool ("harite", false);
		isAttack = false;
	}

	void normalAttack () {
		if (isNormalAttack) {
			return;
		}
		isNormalAttack = true;
		StartCoroutine (canAttackOno ());
		Vector2 itemPos = transform.position;
		itemPos.x = itemPos.x + 1.0f;
		AudioPlay (normalAttackAudio);
		Instantiate (normalAttackOno, itemPos, Quaternion.identity);
//		normalAttackOno.Place(itemPos);
//		childNormalAttack.SetActive (true);
//		animator.SetBool ("normalAttack", true);
	}
	IEnumerator canAttackOno () {
		yield return new WaitForSeconds (0.8f);
		isNormalAttack = false;
	}
	public void FinishNormalAttack () {
		isNormalAttack = false;
		childNormalAttack.SetActive (false);
		animator.SetBool ("normalAttack", false);
	}

	void hadouken () {
		if (hadoukenUserFlag != 1) {
			return;
		}
		_pointRemainCount = _pointRemainCount - 1;
		hadoukenUserFlag = 0;
		HadoukenNoUseImage.gameObject.SetActive (true);
		StartCoroutine (canHadouken ());
		Vector3 itemPos = transform.position;
		itemPos.x = itemPos.x + 1.0f;
//		GameObject hadouken = Lean.LeanPool.Spawn (item1, itemPos, Quaternion.identity, null);
//		GameObject hadouken = hadoukenPool.GetComponent<SimplePooling>().SpawnPrefab(itemPos) as GameObject;
		AudioPlay (hadoukenAudio);
		GameObject hadouken = Instantiate (item1, itemPos, Quaternion.identity) as GameObject;
//		PoolableObject hadouken = item1.Place(itemPos);
//		Debug.Log ("Hadouken");
		if (isBlackHole || isBlackHoleBig) {
			Debug.Log ("Hadouken & blackOut");
//			hadouken.GetComponent<MarathonHadouken> ().setTarget (blackHole);
			hadouken.GetComponent<MarathonHadouken> ().setTargetFlag (true);
		}
	}
	void AudioPlay (AudioClip audioClip) {
		source.clip = audioClip;
		source.Play ();
	}
	void yogaFire () {
		if (yogaFireUseFlag != 1) {
			return;
		}
		_pointRemainCount = _pointRemainCount - 5;
		yogaFireUseFlag = 0;
		YogafireNoUseImage.gameObject.SetActive (true);
		StartCoroutine (canYogaFire ());
//		animator.SetBool ("upper", true);
		Vector3 itemPos = transform.position;
		itemPos.x = itemPos.x + 1.0f;
//		GameObject item2 = (GameObject)Resources.Load ("Ground/MarathonYogaFire");
		AudioPlay (yogafireAudio);
		GameObject yogaFire = Instantiate (item2, itemPos, Quaternion.identity) as GameObject;
//		PoolableObject yogaFire = item2.Place(itemPos);
		if (isBlackHoleBig) {
			Debug.Log ("Hadouken & blackOut");
			yogaFire.GetComponent<MarathonYogaFire> ().setTargetFlag (true);
		}
	}
	IEnumerator canHadouken () {
		yield return new WaitForSeconds (0.8f);
		hadoukenUserFlag = 1;
		HadoukenNoUseImage.gameObject.SetActive (false);
	}
	IEnumerator canYogaFire () {
		yield return new WaitForSeconds (1.0f);
		yogaFireUseFlag = 1;
		YogafireNoUseImage.gameObject.SetActive (false);
	}
	void Kick () {
		if (kickUseFlag != 1) {
			return;
		}
//		Debug.Log ("kick " + rgbd.velocity.y);
		if (!isMuteki && transform.position.y > -3.2f) {
			isKick = true;
			rgbd.velocity = Vector3.zero;
			rgbd.AddForce (Vector2.down * 200);
			if (isBoss) {
				rgbd.AddForce (Vector2.right * 50);
			}
			AudioPlay (kickAudio);
			animator.SetBool ("jumpKick", true);
			isMuteki = true;
			childKick.SetActive (true);
			_pointRemainCount = _pointRemainCount - 3;
		}
	}
	void upper () {
		if (upperUseFlag != 1) {
			return;
		}
		if (!isMuteki && rgbd.velocity.y == 0) {
			isUpper = true;
			rgbd.AddForce (Vector2.up * 300);
			if (isBoss) {
				rgbd.AddForce (Vector2.right * 50);
			}
			AudioPlay (upperAudio);
			animator.SetBool ("upper", true);
			childUpper.SetActive (true);
			isMuteki = true;
			_pointRemainCount = _pointRemainCount - 3;
		}
	}

	void screwDriver () {
		if (screwUseFlag != 1) {
			return;
		}
		if (!isScrewDriverTrigger) {
			isScrewDriverTrigger = true;
			screwDriverTrigger.SetActive (true);
			rgbd.AddForce (Vector2.right * 500);
			_pointRemainCount = _pointRemainCount - 50;
			StartCoroutine ("FinishScrewDriver");
		}
	}

	void RollingAttack () {
		if (isMuteki) {
			return;
		}
		camera.SetRollingFlag (true);
		animator.SetBool ("rotation", true);
		isMuteki = true;
		isRolling = true;
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionY;
		childRolling.SetActive (true);
		AudioPlay (rollingAudio);
		rgbd.AddForce (Vector2.right * 400);

		_pointRemainCount = _pointRemainCount - 10;
		StartCoroutine (RollingAttackFinish ());
	}

	void TornadoAppear () {
		AudioPlay (tornadoDefenseAudio);
		childTornado.SetActive (true);
		_pointRemainCount = _pointRemainCount - 10;
		StartCoroutine (FinishTornado ());
	}

	void HeartProtection () {
		isMuteki = true;
		childHeartProtect.SetActive (true);
		_pointRemainCount = _pointRemainCount - 100;
		StartCoroutine (FinishHeartProtection ());
	}
	IEnumerator FinishHeartProtection () {
		yield return new WaitForSeconds (10);
		childHeartProtect.SetActive (false);
		isMuteki = false;
	}
	IEnumerator FinishTornado () {
		yield return new WaitForSeconds (4);
		childTornado.SetActive (false);
	}
	IEnumerator RollingAttackFinish () {
		yield return new WaitForSeconds (1);
		isMuteki = false;
		isRolling = false;
		childRolling.SetActive (false);
		animator.SetBool ("rotation", false);
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		transform.position = new Vector3 (0, -3.24f, 0);
		transform.rotation = Quaternion.identity;
		camera.SetRollingFlag (false);
	}

	IEnumerator FinishScrewDriver () {
		yield return new WaitForSeconds (1);
		isScrewDriverTrigger = false;
		screwDriverTrigger.SetActive (false);
	}
	public void addActionPoint (int num) {
		isBoss = false;
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		transform.position = new Vector3 (0, -3.24f, 0);
		transform.rotation = Quaternion.identity;

		_pointRemainCount = _pointRemainCount + num;
	}
	public int getActionPoint () {
		return _pointRemainCount;
	}
	public float getPositionX () {
		return transform.position.x;
	}
	public float getPositionY () {
		return transform.position.y;
	}
	public int getLifePoint () {
		return lifePoint;
	}
	public void lifeMinus () { // player fall out
		if (!isMuteki) {
			lifePoint -= 1000;
			hpText.text = lifePoint.ToString ();
			hpSlider.value = lifePoint;
			MainSpriteRenderer.sprite = DamagedSprite;
			isMuteki = true;
			animator.SetBool ("damage", true);
			StartCoroutine ("GoNormalSprite");
			_lifePointImage ();
		}
		if (isTutorial) {
			transform.position = new Vector3 (transform.position.x, -3.24f, 0);
			rgbd.constraints = RigidbodyConstraints2D.FreezePositionY;
			StartCoroutine (FallOutAndrevivalForTutorial ());
		} else {
			transform.position = new Vector3 (transform.position.x, 3, 0);
			rgbd.velocity = new Vector3 (rgbd.velocity.x, 0, 0);
			jumpCount = 0;
//			rgbd.gravityScale = 1.5f;
//			StartCoroutine (FallOutAndrevival ());
		}
	}
	IEnumerator FallOutAndrevival () {
		yield return new WaitForSeconds (1);
		rgbd.gravityScale = gravityScale;
	}
	IEnumerator FallOutAndrevivalForTutorial () {
		yield return new WaitForSeconds (3);
		rgbd.constraints = RigidbodyConstraints2D.None;
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX;
		rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
//		rgbd.gravityScale = gravityScale;
	}
	public void setEffect () {
//		particle.Play ();
	}

	public void GoPlanet () {
//		rgbd.isKinematic = true;
		gameController.SendMessage ("GoPlanet");
		AllGameDefinition.actionPoint = _pointRemainCount;
		StartCoroutine ("GotoPlanetLoading");
//		Destroy (this.gameObject);
	}

	IEnumerator GotoPlanetLoading () {
		yield return new WaitForSeconds (2);
		Debug.Log ("GoPlanet!!! Trigger2");
		SceneManager.LoadScene ("LoadingPlanetScene");
	}
	public void hitBoss () {
		if (!isScrewDriverTrigger) {
//			Debug.Log ("hitBoss######");
			rgbd.AddForce (Vector2.left * 1000);
		}
	}

	public void SlideField () {
		if (!isMuteki || !isSnow) {
			animator.SetBool ("run", false);
			isSnow = true;
			StartCoroutine ("SlideFieldEnd");
		}
	}
	IEnumerator SlideFieldEnd () {
		yield return new WaitForSeconds (1);
		animator.SetBool ("run", true);
		isSnow = false;
	}

	public void SetBlackHole (bool flag) {
//		blackHole = obj;
		isBlackHole = flag;
	}
	public void SetBlackHoleBig (bool flag) {
		//		blackHole = obj;
		isBlackHoleBig = flag;
	}

	public void StartFixedJoint (GameObject target) {
		joint.enabled = true;
		joint.connectedBody = target.GetComponent<Rigidbody2D>();

		boss = target;

		isMuteki = true;
		isScrewDriver = true;
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX;
		rgbd.AddForce (Vector2.up * 500);
		rgbd.AddForce (Vector2.right * 10);
	}

	public void pauseGame () {
		if (coroutineMuteki != null) {
			Debug.Log ("stop coroutine muteki");
			if (coroutineMuteki != null) {
				StopCoroutine (coroutineMuteki);
			}
		}
	}
	public void unPauseGame () {
		if (coroutineMuteki != null) {
			Debug.Log ("start coroutine muteki");
			StartCoroutine (coroutineMuteki);
		}
	}
	void _defaultAnimation () {
//		Debug.Log ("_defaultAnimation");
		switch (AllGameDefinition.CharacterType) {
		case 1:
			break;
		case 2:
			animator.SetBool ("isMiniNurikabe", true);
			break;
		case 3:
			animator.SetBool ("isNurikabe", true);
			break;
		case 4:
			break;
		default:
			animator.SetBool ("run", true);
			break;
		}
	}

	void _animatorRun (bool isRun) {
		switch (AllGameDefinition.CharacterType) {
		case 1:
			animator.SetBool ("run", isRun);
			break;
		case 2:
			animator.SetBool ("mini_run", isRun);
			break;
		case 3:
			animator.SetBool ("nurikabe_run", isRun);
			break;
		case 4:
			break;
		default:
			animator.SetBool ("run", isRun);
			break;
		}
	}

	public void ResetJumpCount () {
		jumpCount = 0;
	}
	GameObject tutorialEnemy;
	public void CreateTutorialEnemy (int enemyNo) {
		GameObject enemyObj = null;
		Vector3 pos = new Vector3();
		switch (enemyNo) {
		case 7:
			enemyObj = ResourceCache.Load ("Tutorial/Ground/NormalAttackEnemy");
			pos = new Vector3 (transform.position.x + 6, -3.1f, transform.position.z);
			break;
		case 8:
			enemyObj = ResourceCache.Load ("Tutorial/Ground/HadoukenEnemy");
			pos = new Vector3 (transform.position.x + 6, -3.1f, transform.position.z);
			break;
		case 9:
			enemyObj = ResourceCache.Load ("Tutorial/Ground/JumpHadoukenEnemy");
			pos = new Vector3 (transform.position.x + 11, 2.3f, transform.position.z);
			break;
		case 10:
			enemyObj = ResourceCache.Load ("Tutorial/Ground/YogafireEnemy");
			pos = new Vector3 (transform.position.x + 6, -3.1f, transform.position.z);
			break;
		case 11:
			enemyObj = ResourceCache.Load ("Tutorial/Ground/UpperEnemy");
			pos = new Vector3 (transform.position.x, transform.position.y + 8, transform.position.z);
			break;
		case 12:
			enemyObj = ResourceCache.Load ("Tutorial/Ground/KickEnemyNurikabe");
			pos = new Vector3 (transform.position.x + 10, -3.1f, transform.position.z);
			break;
		case 13:
			enemyObj = ResourceCache.Load ("Tutorial/Ground/RollingEnemy");
			pos = new Vector3 (transform.position.x + 6, -2.1f, transform.position.z);
			break;
		case 14:
			enemyObj = ResourceCache.Load ("Tutorial/Ground/TornadoDefenseEnemy");
			pos = new Vector3 (transform.position.x, transform.position.y + 8, transform.position.z);
			break;
		case 15:
			enemyObj = ResourceCache.Load ("Ground/BossFireMan");
			pos = new Vector3 (transform.position.x + 8, -3.1f, transform.position.z);
			break;
		}
		tutorialEnemy = Instantiate (enemyObj, pos, Quaternion.identity) as GameObject;
		if (enemyNo == 15) {
			tutorialEnemy.GetComponent<MarathonBossBlackController> ().setTutorialFlag (true);
		}
	}

	public void HitTutorialEnemy (int enemyNo) {
		_hitEnemyAttack (1000);
		gameController.SendMessage ("HitTutorialEnemy", enemyNo);
	}

	public void PlayerHissatuAttack () {
		GameObject laser = ResourceCache.Load ("Ground/PlayerLaserBeam");
		Instantiate (laser, transform.position, Quaternion.identity);
	}

	public void SetCameraStatus (bool flag) {
		camera.SetRollingFlag (flag);
	}

	public void fallout () {
		AudioPlay (falloutAudio);
	}

	public void ShowRewardedAd()
	{
		if (Advertisement.IsReady ("rewardedVideo")) {
			PowerUpButton.gameObject.SetActive (false);
			PowerUpText.gameObject.SetActive (false);
			var options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show ("rewardedVideo", options);
		} else {
			PowerUpText.text = "エラーが発生しました。ごめんなさい。";
		}
	}

	private void HandleShowResult(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log ("The ad was successfully shown.");
			_pointRemainCount = (int)(_pointRemainCount * 1.3);
			lifePoint = (int)(lifePoint * 1.3);
//			PointRemainText.text = _pointRemainCount.ToString ();
			hpText.text = lifePoint.ToString ();
			break;
		case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			PowerUpButton.gameObject.SetActive (true);
			PowerUpText.gameObject.SetActive (true);
			break;
		case ShowResult.Failed:
			Debug.LogError("The ad failed to be shown.");
			PowerUpButton.gameObject.SetActive (true);
			PowerUpText.gameObject.SetActive (true);
			break;
		}
	}
}
