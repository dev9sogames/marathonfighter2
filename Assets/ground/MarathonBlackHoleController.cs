﻿using UnityEngine;
using System.Collections;
using System;

public class MarathonBlackHoleController : MonoBehaviour {
	public bool isBig;

	GameObject gameController;

	// Use this for initialization
	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
		/*
		GameObject[] objList = GameObject.FindGameObjectsWithTag ("MarathonHadouken");
		foreach (GameObject obj in objList) {
			try {
			obj.GetComponent<MarathonHadouken> ().setTarget (this.gameObject);
			} catch (Exception e) {
			}
		}
		*/
		StartCoroutine ("DestroyObject");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "MarathonHadouken") {
			other.GetComponent<MarathonHadouken> ().DestroyObj ();
		} else if (isBig && other.tag == "MarathonYogaFire") {
			other.GetComponent<MarathonYogaFire> ().DestroyObj ();
		}
	}
	IEnumerator DestroyObject () {
		yield return new WaitForSeconds (6);
		GameObject[] objList = GameObject.FindGameObjectsWithTag ("MarathonHadouken");
		foreach (GameObject obj in objList) {
			try {
				obj.GetComponent<MarathonHadouken> ().setTargetFlag (false);
			} catch (Exception) {
			}
		}
		gameController.SendMessage ("SetBlackHoleFlag", false);
		if (isBig) {
			GameObject[] objList2 = GameObject.FindGameObjectsWithTag ("MarathonYogaFire");
			foreach (GameObject obj in objList2) {
				try {
					obj.GetComponent<MarathonYogaFire> ().setTargetFlag (false);
				} catch (Exception) {
				}
			}
			gameController.SendMessage ("SetBlackHoleBigFlag", false);
		}
			
		Destroy (this.gameObject);
	}
}
