﻿using UnityEngine;
using System.Collections;

public class MarathonBossProtection : MonoBehaviour {
	int bossHp;

	bool isHit;

	// Use this for initialization
	void Start () {
		bossHp = 10;
	}

	void OnTriggerEnter2D (Collider2D other) {
		/*
		if (isHit) {
			return;
		}
		*/
		if (other.tag == "PlayerNormalAttack") {
			isHit = true;
			StartCoroutine (hitEnd ());
			bossHp--;
			other.GetComponent<MarathonPlayerNormalAttack> ().DestroyObj ();
		} else if (other.tag == "MarathonHadouken") {
			Debug.Log ("protect hadouken" + bossHp);
			isHit = true;
			StartCoroutine (hitEnd ());
			bossHp -= 2;
			Debug.Log ("protect hadouken2" + bossHp);
			other.GetComponent<MarathonHadouken> ().DestroyObj ();
		} else if (other.tag == "MarathonYogaFire") {
			Debug.Log ("protect MarathonYogaFire" + bossHp);
			isHit = true;
			StartCoroutine (hitEnd ());
			bossHp -= 3;
			other.GetComponent<MarathonYogaFire> ().DestroyObj ();
		} else if (other.tag == "PlayerKick") {
			Debug.Log ("protect PlayerKick" + bossHp);
			isHit = true;
			StartCoroutine (hitEnd ());
			bossHp -= 5;
		} else if (other.tag == "PlayerUpper") {
			isHit = true;
			Debug.Log ("protect PlayerUpper" + bossHp);
			StartCoroutine (hitEnd ());
			bossHp -= 5;
		}
//		Debug.Log ("protect " + bossHp);
		if (bossHp <= 0) {
			transform.root.gameObject.GetComponent<MarathonBossBlackController> ().FinishProtect ();
			this.gameObject.SetActive (false);
			return;
		}

		float alpha = bossHp / 10.0f;
//		Debug.Log ("alpha : " + alpha);
		GetComponent<SpriteRenderer> ().color = new Color (1,1,1,alpha);
	}

	IEnumerator hitEnd () {
		yield return new WaitForSeconds (0.5f);
		isHit = false;
	}

	public void SetDefaultValue () {
		bossHp = 10;
		GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
	}
}
