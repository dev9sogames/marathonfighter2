﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MarathonLevelUpController : MonoBehaviour {
	public Text levelText;
	public Text expNextText;

	// Use this for initialization
	void Start () {
		levelText.text = "LEVEL : " + AllGameDefinition.level;
		expNextText.text = "NEXT TO : " + AllGameDefinition.expNextTo;

//		StartCoroutine ("BackDifficulty");
	}
	
	IEnumerator BackDifficulty () {
		yield return new WaitForSeconds (5);
		SceneManager.LoadScene ("Difficulty");
	}
}
