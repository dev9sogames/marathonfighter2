﻿using UnityEngine;
using System.Collections;

public class MarathonScrewDriverTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "MarathonBoss") {
			MarathonBossBlackController boss = other.GetComponent<MarathonBossBlackController> ();
			boss.SetScrewDriverFlag (true);

			GameObject parent = transform.root.gameObject;
			MarathonPlayer player = parent.GetComponent<MarathonPlayer> ();

			player.StartFixedJoint (other.gameObject);
		}
	}
}
