﻿using UnityEngine;
using System.Collections;

public class MoveFieldInit : MonoBehaviour {
	MarathonPlayer _player;

	Vector3 pos;
	float posX;
	float playerPosX;
	GameObject gameController;

	float speed;

	bool addNextField;
	bool isReady;
	//	bool isFirst;
	// Use this for initialization
	void Start () {
		addNextField = false;
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
		pos = transform.position;
		posX = transform.position.x;
	}

	// Update is called once per frame
	void Update () {
		if (isReady) {
			return;
		}
		posX -= Time.deltaTime * speed;
		transform.position = new Vector3 (posX, pos.y, pos.z);
		if (posX <= (-15.0f)) {
			Destroy (this.gameObject);
		}
	}

	public void Ready () {
		isReady = true;
	}
	public void GameStart () {
		isReady = false;
		speed = AllGameDefinition.fieldSpeed;
	}
}
