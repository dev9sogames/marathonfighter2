﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MarathonBossBlackController : MonoBehaviour {

	const int BOSS_MIDDLE_EASY_HP = 1000;
	const int BOSS_EASY_HP = 3000;

	const int BOSS_MIDDLE_NORMAL_HP = 3000;
	const int BOSS_NORMAL_HP = 10000;

	const int BOSS_MIDDLE_HARD_HP = 10000;
	const int BOSS_HARD_HP = 30000;

	const int BOSS_MIDDLE_VERY_HARD_HP = 50000;
	const int BOSS_VERY_HARD_HP = 70000;

	const int HADOUKEN_DAMAGE = 500;
	const int YOGA_FIRE_DAMAGE = 900;
	const int NORMAL_ATTACK_DAMAGE = 150;
	const int UPPER_DAMAGE = 800;
	const int KICK_DAMAGE = 800;
	const int HISSATU_DAMAGE = 3000;

	const float RIGHT_LIMIT = 12.0f;
	const float LEFT_LIMIT = 3.0f;
	const float UP_LIMIT = 3.5f;
	const float DOWN_LIMIT = -3.5f;

	GameObject gameController;
	GameObject childMirror;
	GameObject childKick;
	GameObject childProtection;
	GameObject childBodyAttack;
	GameObject childSliding;

	bool isAttack;

	bool isDead;
	bool isJumping;
	bool isJumpingKick;
	bool isFalling;
	bool isDisappear;
	bool isProtect;
	bool isBodyAttack;
	bool isKick;
	bool isSliding;
	bool isMutekiAttack;
//	bool isMirror;
	bool mirrorEnabled;

	public int bossType; // 1:fireman,2:mini,3:nurikabe,4:tako,5:uchuujin,6:sandman,7:blackHero

	SpriteRenderer render;
	public Sprite damage;
	public Sprite normal;
//	public Sprite hard;
//	public Sprite veryHard;
//	public Sprite VegaAttackImage;

	public Slider hpSlider;
	int damagePoint = 0;
	int bossHitPoint;

	public Text damageText;
	public Text hpText;

	public bool isBlackHero;
	public bool isSubBoss;

	bool isScrewDriver;

	int difficulty;

	float posX;
	float posY;

	GameObject fire;
	GameObject hammer;
	GameObject beef;
	GameObject beefDokuro;
	GameObject muteki;
	GameObject homing;
	GameObject UpAndHoming;
	GameObject Ice;
	GameObject Dragon;
	GameObject Thunder;
	GameObject Reppuuken;

	GameObject MiniNurikabe;

	GameObject MiniBoss;
	GameObject[] miniBossList = new GameObject[5];

	MarathonPlayer player;

	enum HorizontalDirection {
		none,
		left,
		right
	};
	HorizontalDirection hDire;

	enum VerticalDirection {
		none,
		up,
		down
	}
	VerticalDirection vDire;

	Rigidbody2D rgbd;
	Animator animator;

	bool isTutorial;

	float colorOrgR;
	float colorOrgG;
	float colorOrgB;

	void Start () {
		/****
		 * debug
		 ***/
//		bossType = 7;
//		isBlackHero = true;

		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
		rgbd = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		render = GetComponent<SpriteRenderer> ();
		colorOrgR = GetComponent<SpriteRenderer> ().color.r;
		colorOrgG = GetComponent<SpriteRenderer> ().color.g;
		colorOrgB = GetComponent<SpriteRenderer> ().color.b;

		childKick = transform.Find ("BossKick").gameObject;
		childKick.SetActive (false);
		childProtection = transform.Find ("Protection").gameObject;
		childProtection.SetActive (false);
		childBodyAttack = transform.Find ("BossBodyAttack").gameObject;
		childBodyAttack.SetActive (false);
		childSliding = transform.Find ("BossSliding").gameObject;
		childSliding.SetActive (false);
//			animator.SetBool ("run", true);

		setBossStatus ();

		posX = transform.position.x;
		posY = transform.position.y;

		fire = ResourceCache.Load ("Ground/EnemyHonooImage");
		hammer = ResourceCache.Load ("Ground/Enemyhammer");
		beef = ResourceCache.Load ("Ground/EnemyBeef");
		beefDokuro = ResourceCache.Load ("Ground/EnemyBeefBad");
		muteki = ResourceCache.Load ("Ground/EnemyMuteki");
		homing = ResourceCache.Load ("Ground/EnemyHomingImage");
		UpAndHoming = ResourceCache.Load ("Ground/EnemyUpAndHoming");
		Ice = ResourceCache.Load ("Ground/EnemyIceObj");
		MiniBoss = ResourceCache.Load ("Ground/BossBunshin");
		Dragon = ResourceCache.Load ("Ground/EnemyDragon");
		Thunder = ResourceCache.Load ("Ground/EnemyThunder");
		Reppuuken = ResourceCache.Load ("Ground/EnemyReppuuken");
		MiniNurikabe = ResourceCache.Load ("Ground/BossVerySmallNurikabe");

//		fire = GameObject.Find ("PoolObjEnemyHonoo").GetComponent<GenericPool> ();
//		hammer = GameObject.Find ("PoolObjEnemyHammer").GetComponent<GenericPool> ();
//		beef = GameObject.Find ("PoolObjEnemyBeef").GetComponent<GenericPool> ();
//		beefDokuro = GameObject.Find ("PoolObjEnemyBadBeef").GetComponent<GenericPool> ();
//		muteki = GameObject.Find ("PoolObjEnemyMuteki").GetComponent<GenericPool> ();
//		homing = GameObject.Find ("PoolObjEnemyHoming").GetComponent<GenericPool> ();
//		UpAndHoming = GameObject.Find ("PoolObjEnemyHonoo").GetComponent<GenericPool> ();
//		Ice = GameObject.Find ("PoolObjEnemyIce").GetComponent<GenericPool> ();
//		Dragon = GameObject.Find ("PoolObjEnemyDragon").GetComponent<GenericPool> ();
//		Thunder = GameObject.Find ("PoolObjEnemyThunder").GetComponent<GenericPool> ();
//		Reppuuken = GameObject.Find ("PoolObjEnemyReppuken").GetComponent<GenericPool> ();

		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<MarathonPlayer> ();
	}

	void setBossStatus () {
		int bossCount = AllGameDefinition.bossCount; // 1:middle, 2:main
		/**
		 * 1-1 fireman mini
		 * 1-2 fireman mini
		 * 1-3 mini nurikabe
		 * 2-1 fireman mini(mirror)
		 * 2-2 mini(mirror) uchuu
		 * 2-3 mini(mirror) nurikabe with uchuu
		 * 3-1 tako tako*2
		 * 3-2 tako tako*2(mirror)
		 * 3-3 tako*2 nurikabe with tako
		 * 4-1 sandman nurikabe
		 * 4-2 sandman(mirror) nurikabe*2(mirror)
		 * 4-3 nurikabe*2 blackHero(mirror)
		 * 5   princess
		 * 
		 **/
		switch (AllGameDefinition.StoryStageType) {
		case 1:
			switch (AllGameDefinition.StoryStageCourt) {
			case 1:
				if (bossCount == 1) {
					_setBossDetail (1000, false);
				} else if (bossCount == 2) {
					_setBossDetail (1500, false);
				}
				break;
			case 2:
				if (bossCount == 1) {
					_setBossDetail (1000, false);
				} else if (bossCount == 2) {
					_setBossDetail (3000, false);
				} else {
					if (AllGameDefinition.gameMode == 3) {
						switch (bossCount) {
						case 3:
							_setBossDetail (3000, false);
							break;
						case 4:
							_setBossDetail (4000, false);
							break;
						case 5:
							_setBossDetail (5000, true);
							break;
						case 6:
							_setBossDetail (6000, false);
							break;
						case 7:
							_setBossDetail (7000, true);
							break;
						case 8:
							_setBossDetail (8000, false);
							break;
						case 9:
							_setBossDetail (9000, true);
							break;
						case 10:
							_setBossDetail (10000, true);
							break;
						default:
							_setBossDetail (10000, false);
							break;
						}
					}
				}
				break;
			case 3:
				if (bossCount == 1) {
					_setBossDetail (3000, false);
				} else if (bossCount == 2) {
					_setBossDetail (5000, false);
				}
				break;
			}
			break;
		case 2:
			switch (AllGameDefinition.StoryStageCourt) {
			case 1:
				if (bossCount == 1) {
					_setBossDetail (2000, false);
				} else if (bossCount == 2) {
					_setBossDetail (3000, false);
				}
				break;
			case 2:
				if (bossCount == 1) {
					_setBossDetail (2000, false);
				} else if (bossCount == 2) {
					_setBossDetail (4000, false);
				}
				break;
			case 3:
				if (bossCount == 1) {
					_setBossDetail (3000, false);
				} else if (bossCount == 2) {
					_setBossDetail (7000, false);
				}
				break;
			}
			break;
		case 3:
			switch (AllGameDefinition.StoryStageCourt) {
			case 1:
				if (bossCount == 1) {
					_setBossDetail (3000, false);
				} else if (bossCount == 2) {
					_setBossDetail (4500, false);
				}
				break;
			case 2:
				if (bossCount == 1) {
					_setBossDetail (4000, false);
				} else if (bossCount == 2) {
					_setBossDetail (7000, false);
				}
				break;
			case 3:
				if (bossCount == 1) {
					_setBossDetail (5000, false);
				} else if (bossCount == 2) {
					_setBossDetail (10000, false);
				}
				break;
			}
			break;
		case 4:
			switch (AllGameDefinition.StoryStageCourt) {
			case 1:
				if (bossCount == 1) {
					_setBossDetail (4000, false);
				} else if (bossCount == 2) {
					_setBossDetail (7000, false);
				}
				break;
			case 2:
				if (bossCount == 1) {
					_setBossDetail (5000, false);
				} else if (bossCount == 2) {
					_setBossDetail (10000, false);
				}
				break;
			case 3:
				if (bossCount == 1) {
					_setBossDetail (10000, false);
				} else if (bossCount == 2) {
					_setBossDetail (15000, false);
				}
				break;
			}
			break;
		case 5:
			if (bossCount == 1) {
				_setBossDetail (15000, false);
			} else if (bossCount == 2) {
				_setBossDetail (30000, false);
			}
			break;
		}

		if (isTutorial) {
			_setBossDetail (1000, false);
		}
	}

	void _setBossDetail (int bossHp, bool isChildMirror) {
		if (AllGameDefinition.isHardMode) {
			bossHp = bossHp * 2;
			if (AllGameDefinition.StoryStageType == 4 || AllGameDefinition.StoryStageType == 5) {
				isChildMirror = true;
			}
		}
//		Debug.Log ("_setBossDetaiol : " + bossHp + " : " + isChildMirror);
		if (bossType == 0) {
			bossHitPoint = 5000;
		} else {
			bossHitPoint = bossHp;
		}
		hpSlider.maxValue = bossHp;
		hpText.text = bossHp.ToString ();
		StartCoroutine (bossAttack ());
		StartCoroutine (normalAction ());
		if (isChildMirror) {
			childMirror = transform.Find ("mirror").gameObject;
			StartCoroutine (MirrorEnable ());
		}
//		childMirror.SetActive (isChildMirror);
	}

	//Update is called once per frame
	void FixedUpdate () {
//		if (bossType == 6) {
//			Debug.Log ("isJumpingKick : " + isJumpingKick);
//			Debug.Log ("isFalling : " + isFalling);
//			Debug.Log ("isMutekiAttack : " + isMutekiAttack);
//		}
		if (isScrewDriver) {
			return;
		}

		if (hDire == HorizontalDirection.none || vDire == VerticalDirection.none) {
			return;
		}

		float speed = _getSpeed ();

		if (isDead) {
			posY -= Time.deltaTime * speed * 3;
			transform.position = new Vector3 (transform.position.x, posY, transform.position.z);

			return;
		}

		if (isJumpingKick) {
			if (transform.position.y <= 5.0f) {
				posY += Time.deltaTime * speed * 1.5f;
				transform.position = new Vector3 (transform.position.x, posY, transform.position.z);

				return;
			} else if (transform.position.y > 5.0f) {
				isFalling = true;
			}

		}
		if (isFalling) {
			if (transform.position.y >= -1.5f) {
				moveToPlayer ();
			} else if (transform.position.y < -1.5f) {
				if (isFalling && animator != null) {
					animator.SetBool ("kickDown", false);
				}
				childKick.SetActive (false);
				isFalling = false;
				GetComponent<Rigidbody2D> ().velocity = new Vector2(0,0);
				transform.position = new Vector3 (7.25f, -1.34f, 0);
			}

			return;
		}
		if (isMutekiAttack) {
			if (transform.position.x < -10.0f) {
				isMutekiAttack = false;
				if (animator != null) {
					animator.SetBool ("vega", false);
				}
				childBodyAttack.SetActive (false);
//				childBodyAttack.GetComponent<MarathonBossAttack>().DestroyObj();
			}
			if (transform.position.x < -3.0f) {
				return;
			}
			posX -= Time.deltaTime * speed * 1.5f;
			transform.position = new Vector3 (posX, transform.position.y, transform.position.z);

			return;
		}


		if (transform.position.y >= UP_LIMIT) {
			vDire = VerticalDirection.down;
//				dire = Direction.down;
		}
		if (transform.position.y <= DOWN_LIMIT) {
			vDire = VerticalDirection.up;
//				dire = Direction.up;
		}
		if (transform.position.x < LEFT_LIMIT) {
			hDire = HorizontalDirection.right;
		}
		if (transform.position.x > RIGHT_LIMIT) {
			hDire = HorizontalDirection.left;
		}

//		Debug.Log (vDire);
		posY = transform.position.y;
		if (vDire == VerticalDirection.up) {
			posY += Time.deltaTime * speed;
		} else if (vDire == VerticalDirection.down) {
			posY -= Time.deltaTime * speed;
		}

		posX = transform.position.x;
		if (hDire == HorizontalDirection.right) {
			posX += Time.deltaTime * speed;
		} else if (hDire == HorizontalDirection.left) {
			posX -= Time.deltaTime * speed;
		}
		transform.position = new Vector3 (posX, posY, transform.position.z);

	}
	void moveToPlayer () {
		isJumpingKick = false;
		isFalling = true;
		Vector2 targetPos = player.transform.position;
		float x = targetPos.x;
		float y = targetPos.y;

		Vector2 direction = new Vector2 (x - transform.position.x, y - transform.position.y).normalized;
		GetComponent<Rigidbody2D> ().velocity = (direction * 7);
	}
	IEnumerator normalAction () {
		while (!isDead) {
			int second = UnityEngine.Random.Range (1, 5);
			yield return new WaitForSeconds (second);
			_normalAttack ();
		}
	}
	IEnumerator bossAttack () {
//		Debug.Log ("boss easy attack");
		while (!isDead) {
//			yield return new WaitForSeconds (UnityEngine.Random.Range (1, 2));
			int start;
			int end;
			bool doubleAttack = false;
			switch (bossType) {
			case 1:
				start = 3;
				end = 7;
				break;
			case 2:
				start = 3;
				end = 7;
				break;
			case 3:
				start = 2;
				end = 7;
				break;
			case 4:
				start = 2;
				end = 6;
				break;
			case 5:
				start = 2;
				end = 5;
				break;
			case 6:
				start = 2;
				end = 4;
				break;
			case 7:
				start = 1;
				end = 3;
				if (UnityEngine.Random.Range (1, 4) == 1) {
					doubleAttack = true;
				} else {
					doubleAttack = false;
				}
				break;
			default:
				start = 3;
				end = 7;
				break;
			}
			yield return new WaitForSeconds (UnityEngine.Random.Range (start, end));
			_attack (bossType);
			if (doubleAttack) {
				StartCoroutine (multiAttack ());
			}
		}
	}
	IEnumerator multiAttack () {
		yield return new WaitForSeconds (0.5f);
		_attack (bossType);
	}
	IEnumerator bossNormalAttack () {
		Debug.Log ("boss normal attack");
		while (!isDead) {
			int num = UnityEngine.Random.Range (2, 7);
			yield return new WaitForSeconds (num);
			for (int i = 1; i <= num; i++) {
				_attack (2);
			}
		}
	}
	IEnumerator bossHardAttack () {
		Debug.Log ("boss hard attack");
		while (!isDead) {
			int num = UnityEngine.Random.Range (2, 7);
			yield return new WaitForSeconds (num);
			for (int i = 1; i <= num; i++) {
				_attack (3);
			}
		}
	}
	IEnumerator bossVeryHardAttack () {
		Debug.Log ("boss very hard attack");
		while (!isDead) {
			int num = UnityEngine.Random.Range (2, 5);
			yield return new WaitForSeconds (num);
			for (int i = 1; i <= num; i++) {
				_attack (4);
			}
		}
	}
	int count = 0;
	void _attack (int bossType) { // 1:fireman, 2:mini nurikabe, 3:nurikabe, 4:tako, 5:uchuujin, 6:moai, 7:lastBoss, 8:princess
		/*
		Instantiate (Ice, transform.position, Quaternion.identity);
		return;
		*/
		// TODO : 泥投げ、バナナ、地面から出てくる(蔵馬)、投げ技
		if (isDead || isScrewDriver) {
			return;
		}
		int rangeStart;
		int rangeEnd;
		switch (bossType) {
		case 1:
		case 2:
		case 3:
			rangeStart = 1;
			rangeEnd = 10;
			break;
		case 4:
		case 5:
		case 8:
			rangeStart = 1;
			rangeEnd = 20;
			break;
		case 6:
			rangeStart = 5;
			rangeEnd = 30;
			break;
		case 7:
			rangeStart = 1;
			rangeEnd = 39;
			break;
		default:
			rangeStart = 1;
			rangeEnd = 10;
			break;
		}
		/*
		 * Instantiate (fire, transform.position, Quaternion.identity);
		 * Instantiate (beef, transform.position, Quaternion.identity);
		 * Instantiate (beefDokuro, transform.position, Quaternion.identity);
		 * Instantiate (muteki, transform.position, Quaternion.identity);
		 * Instantiate (hammer, transform.position, Quaternion.identity);
		 * Instantiate (UpAndHoming, transform.position, Quaternion.identity);
		 * 
		 * JumpAndFire ();
		 * JumpAndHoming ();
		 * moveNurikabe ();
		 * kick ()
		 * speedFire ()
		 * MultipleFire (3)
		 * 
		 * JumpAndKick ();
		 * bodyAttack ();
		 * 
		 * Disappear ();
		 * Protect ();
		 * 
		 * Instantiate (UpAndHoming, transform.position, Quaternion.identity);
		 * Instantiate (Ice, transform.position, Quaternion.identity);
		 * Instantiate (MiniNurikabe, transform.position, Quaternion.identity);
		 * 
		 * MutekiAttack ()
		 * Sliding ()
		 * MultiPleThunder (3)
		 * Instantiate (Dragon, transform.position, Quaternion.identity);
		 * Instantiate (Thunder, new Vector3 (-1.2f, 14.6f, 0), Quaternion.identity);
		 * Instantiate (Reppuuken, new Vector3 (12.0f, -2.3f, 0), Quaternion.identity);
		 * 
		 */
//		Debug.Log ("start : " + rangeStart + " : end : " + rangeEnd);
		int bossNum = UnityEngine.Random.Range (rangeStart, rangeEnd);
		Debug.Log ("boss attack : " + bossNum);
		switch (bossNum) {
		case 1:
		case 2:
		case 3:
			Instantiate (fire, transform.position, Quaternion.identity);
//			fire.Place(transform.position);
			break;
		case 4:
			Instantiate (beef, transform.position, Quaternion.identity);
//			beef.Place(transform.position);
			break;
		case 5:
			Instantiate (beefDokuro, transform.position, Quaternion.identity);
//			beefDokuro.Place(transform.position);
			break;
		case 6:
			Instantiate (muteki, transform.position, Quaternion.identity);
//			muteki.Place(transform.position);
			break;
		case 7:
		case 8:
			Instantiate (hammer, transform.position, Quaternion.identity);
//			hammer.Place(transform.position);
			break;
		case 9:
		case 10:
			GameObject attack = Instantiate (UpAndHoming, transform.position, Quaternion.identity) as GameObject;
//			PoolableObject attack = UpAndHoming.Place(transform.position);
			attack.GetComponent<MarathonBossAttack> ().setBossObj (this.gameObject);
			break;
		case 11:
			Instantiate (Ice, transform.position, Quaternion.identity);
//			Ice.Place(transform.position);
			break;
		case 12:
		case 13:
			JumpAndFire ();
			isAttack = true;
			break;
		case 14:
		case 15:
			JumpAndHoming ();
			break;
		case 16:
		case 17:
			GameObject bossAttack = Instantiate (UpAndHoming, transform.position, Quaternion.identity) as GameObject;
//			PoolableObject bossAttack = UpAndHoming.Place(transform.position);
			bossAttack.GetComponent<MarathonBossAttack> ().setBossObj (this.gameObject);
//			Instantiate (UpAndHoming, transform.position, Quaternion.identity);
			break;
		case 18:
		case 19:
		case 20:
			speedFire ();
			break;
		case 21:
		case 22:
			moveNurikabe ();
			Instantiate (fire, transform.position, Quaternion.identity);
//			fire.Place(transform.position);
			break;
		case 23:
			speedFire ();
			break;
		case 24:
		case 25:
			MultipleFire (3);
			break;
		case 26:
			Disappear ();
			break;
		case 27:
			Protect ();
			break;
		case 28:
			MultipleFire (2);
			break;
		case 29:
			MultipleFire (4);
//			JumpAndKick ();
//			isAttack = true;
			break;
		case 30:
			moveToPlayer ();
//			bodyAttack ();
			break;
		case 31:
			Instantiate (MiniNurikabe, transform.position, Quaternion.identity);
			gameController.SendMessage ("AddBossCount");
			break;
		case 32:
			MutekiAttack ();
			break;
		case 33:
			MultipleFire (5);
			break;
		case 34:
			GameObject bossAttack2 = Instantiate (Dragon, transform.position, Quaternion.identity) as GameObject;
//			PoolableObject bossAttack2 = Dragon.Place(transform.position);
			bossAttack2.GetComponent<MarathonBossAttack> ().setBossObj (this.gameObject);
//			Instantiate (Dragon, transform.position, Quaternion.identity);
			break;
		case 35:
			Instantiate (Thunder, new Vector3 (-1.2f, 14.6f, 0), Quaternion.identity);
			break;
		case 36:
			Instantiate (Reppuuken, new Vector3 (12.0f, -2.3f, 0), Quaternion.identity);
			break;
		case 37:
			MultiPleThunder (3);
			break;
		case 38:
			Bunshin ();
			break;
		default:
			Instantiate (fire, transform.position, Quaternion.identity);
//			fire.Place(transform.position);
			break;
		}
		/*
		switch (UnityEngine.Random.Range (1, rangeLimit)) {
//		switch (count) {
		case 1:
			Instantiate (fire, transform.position, Quaternion.identity);
			break;
		case 2:
			Instantiate (UpAndHoming, transform.position, Quaternion.identity);
			break;
		case 3:
			Instantiate (beef, transform.position, Quaternion.identity);
			break;
		case 4:
			Instantiate (beefDokuro, transform.position, Quaternion.identity);
			break;
		case 5:
			Instantiate (Ice, transform.position, Quaternion.identity);
			break;
		case 6:
			Instantiate (muteki, transform.position, Quaternion.identity);
			break;
		case 7:
			Instantiate (hammer, transform.position, Quaternion.identity);
			break;
		case 8:
			JumpAndFire ();
			isAttack = true;
			break;
		case 9:
			JumpAndHoming ();
			isAttack = true;
			break;
		case 10:
			Disappear ();
			isAttack = true;
			break;
		case 11:
			moveNurikabe ();
			Instantiate (fire, transform.position, Quaternion.identity);
			break;
		case 12:
			JumpAndKick ();
			isAttack = true;
			break;
		case 13:
			Protect ();
			break;
		case 14:
			Instantiate (Dragon, transform.position, Quaternion.identity);
			break;
		case 15:
			Instantiate (Thunder, new Vector3 (-1.2f, 14.6f, 0), Quaternion.identity);
			break;
		case 16:
			Instantiate (Reppuuken, new Vector3 (12.0f, -2.3f, 0), Quaternion.identity);
			break;
		default:
			break;
		}
		*/
	}

	void MultiPleThunder (int num = 3) {
		float posX = -1.2f;
		for (int i = 0; i < num; i++) {
			Instantiate (Thunder, new Vector3 (posX + (i*3.5f), 14.6f, 0), Quaternion.identity);
		}
	}
	void MutekiAttack () {
		isMutekiAttack = true;
		if (animator != null) {
			animator.SetBool ("vega", true);
		}
		childBodyAttack.SetActive (true);
		StartCoroutine (FinishMutekiAttack ());
	}
	IEnumerator FinishMutekiAttack () {
		yield return new WaitForSeconds (3);
		if (isMutekiAttack) {
			isMutekiAttack = false;
			if (animator != null) {
				animator.SetBool ("vega", false);
			}
			childBodyAttack.SetActive (false);
			childBodyAttack = transform.Find ("BossBodyAttack").gameObject;
		}
	}
	void Sliding () {
		isSliding = true;
		childSliding.SetActive (true);
		if (animator != null) {
			animator.SetBool ("slide", true);
		}
		rgbd.AddForce (Vector2.left * 200);
		StartCoroutine (FinishSliding ());
	}
	IEnumerator FinishSliding () {
		yield return new WaitForSeconds (2);
		if (isSliding) {
			isSliding = false;
			childSliding.SetActive (false);
			if (animator != null) {
				animator.SetBool ("slide", false);
			}
		}
	}
	void MultipleFire (int num = 3) {
		StartCoroutine (MultipleFireStart (num));
	}
	IEnumerator MultipleFireStart (int num) {
		int count = 1;
		while (count <= num) {
			yield return new WaitForSeconds (0.2f);
			Instantiate (fire, transform.position, Quaternion.identity);
			count++;
		}
	}
	void speedFire () {
		GameObject speedFire = Instantiate (fire, transform.position, Quaternion.identity) as GameObject;
//		PoolableObject speedFire = fire.Place(transform.position);
		speedFire.GetComponent<MarathonBossAttack> ().attackNo = 15;
	}

	void moveNurikabe () {
		transform.position = new Vector3 (2.5f, transform.position.y, transform.position.z);
		StartCoroutine ("positionReverse");
	}

	void JumpAndFire () {
		isJumping = true;
		StartCoroutine (FinishJump ());
		StartCoroutine (FireAttack ());
	}
	void JumpAndHoming () {
		isJumping = true;
		StartCoroutine (FinishJump ());
		StartCoroutine (HomingAttack ());
	}

	void JumpAndKick () {
		isJumpingKick = true;
		childKick.SetActive (true);
		if (animator != null) {
			animator.SetBool ("kickDown", true);
		}

	}
	void Protect () {
		if (isProtect) {
			return;
		}
		isProtect = true;
		childProtection.SetActive (true);
		childProtection.GetComponent<MarathonBossProtection> ().SetDefaultValue ();
	}
	public void FinishProtect () {
		isProtect = false;
	}
	void Disappear () {
		if (isDisappear) {
			return;
		}
		isDisappear = true;
		iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 5f, "onupdate", "SetValue"));
		GetComponent<CircleCollider2D> ().enabled = false;
//		StartCoroutine (BossAppear ());
	}
	void Bunshin () {
		if (isDisappear) {
			return;
		}
		isDisappear = true;
		iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 3f, "onupdate", "SetValueBunshin"));
		GetComponent<CircleCollider2D> ().enabled = false;
	}
	void SetValue(float alpha) {
		if (!isDisappear) {
			return;
		}
//		Color32 color = new Color32 (64,43,43,alpha);
		if (alpha == 0.0f) {
			GetComponent<CircleCollider2D> ().enabled = true;
			gameObject.GetComponent<SpriteRenderer> ().color = new Color(colorOrgR, colorOrgG, colorOrgB);
			isDisappear = false;
			isAttack = false;

			return;
		}
		gameObject.GetComponent<SpriteRenderer> ().color = new Color(colorOrgR, colorOrgG, colorOrgB, alpha);
	}
	void SetValueBunshin(float alpha) {
		if (!isDisappear) {
			return;
		}
		if (alpha == 0.0f) {
			// 分身
			for (int i = 0; i <= 4; i++) {
				posY = transform.position.y - 5;
				Vector3 pos = new Vector3 (transform.position.x, posY + (i * 2), 0);
				miniBossList [i] = Instantiate (MiniBoss, pos, Quaternion.identity) as GameObject;
			}
			StartCoroutine (FinishBunshin ());
			return;
		}
		gameObject.GetComponent<SpriteRenderer> ().color = new Color(colorOrgR, colorOrgG, colorOrgB, alpha);
	}

	void bodyAttack () {
		isBodyAttack = true;
		childBodyAttack.SetActive (true);
		if (animator != null) {
			animator.SetBool ("run", false);
		}
		StartCoroutine (FinishBodyAttack ());
	}

	void kick () {
		rgbd.AddForce (Vector2.up * 200);
		rgbd.AddForce (Vector2.left * 300);
		StartCoroutine (kickDown ());
	}

	void _normalAttack () {
		if (isMutekiAttack) {
			return;
		}

		/*
		if (transform.position.x < 1.0f) {
			MoveRight ();
			return;
		} else if (transform.position.x > 10.0f) {
			MoveLeft ();
			return;
		}

		if (transform.position.y > 5.0f) {
			MoveDown ();
			return;
		} else if (transform.position.y < -5.0f) {
			MoveUp ();
			return;
		}
		*/
		int start;
		int end;
		switch (bossType) {
		case 1:
			start = 1;
			end = 5;
			break;
		case 2:
			start = 1;
			end = 5;
			break;
		case 3:
			start = 1;
			end = 4;
			break;
		case 4:
			start = 1;
			end = 4;
			break;
		case 5:
			start = 1;
			end = 3;
			break;
		case 6:
			start = 1;
			end = 5;
			break;
		case 7:
			start = 1;
			end = 3;
			break;
		default:
			start = 1;
			end = 5;
			break;
		}

		switch (UnityEngine.Random.Range (start, end)) {
		case 1:
			MoveLeft ();
			break;
		case 2:
			MoveStop ();
			break;
		case 3:
			MoveUp ();
			break;
		case 4:
			MoveRight ();
			break;
		case 5:
			MoveDown ();
			break;
		case 6:
			MoveDown ();
			break;
		case 7:
			Instantiate (fire, transform.position, Quaternion.identity);
			break;
		default:
			break;
		}
	}
	void MoveStop () {
		hDire = HorizontalDirection.none;
		vDire = VerticalDirection.none;
		StartCoroutine (StartMoving ());
	}
	IEnumerator StartMoving () {
		yield return new WaitForSeconds (1.0f);
		hDire = HorizontalDirection.right;
		vDire = VerticalDirection.up;
	}
	void MoveLeft () {
		hDire = HorizontalDirection.left;
//		Vector2 velocity = rgbd.velocity;
//		velocity.x = -5.0f;
//		rgbd.velocity = velocity;
	}
	void MoveRight () {
		hDire = HorizontalDirection.right;
//		Vector2 velocity = rgbd.velocity;
//		velocity.x = 5.0f;
//		rgbd.velocity = velocity;
	}
	void MoveUp () {
		vDire = VerticalDirection.up;
//		Vector2 velocity = rgbd.velocity;
//		velocity.y = 5.0f;
//		rgbd.velocity = velocity;
//		rgbd.AddForce (Vector2.up * 150);
	}
	void MoveDown () {
		vDire = VerticalDirection.down;
//		Vector2 velocity = rgbd.velocity;
//		velocity.y = -5.0f;
//		rgbd.velocity = velocity;
//		rgbd.AddForce (Vector2.down * 100);
	}
	IEnumerator kickDown () {
		yield return new WaitForSeconds (0.5f);
		if (transform.position.y > -3.2f) {
			isKick = true;
			GetComponent<Rigidbody2D>().velocity = Vector3.zero;
			GetComponent<Rigidbody2D>().AddForce (Vector2.down * 200);
		}
	}

	IEnumerator FinishBodyAttack () {
		yield return new WaitForSeconds (3);
		isBodyAttack = false;
		childBodyAttack.SetActive (false);
		if (animator != null) {
			animator.SetBool ("run", true);
		}
	}
	IEnumerator FinishBunshin () {
		yield return new WaitForSeconds (5);
		foreach (GameObject miniBoss in miniBossList) {
			Destroy (miniBoss.gameObject);
		}
		gameObject.GetComponent<SpriteRenderer> ().color = new Color(colorOrgR, colorOrgG, colorOrgB, 1);
		GetComponent<CircleCollider2D> ().enabled = true;
		isDisappear = false;
		isAttack = false;
	}
	/*
	IEnumerator BossAppear () {
		yield return new WaitForSeconds (3);
		GetComponent<CircleCollider2D> ().enabled = true;
		gameObject.GetComponent<SpriteRenderer> ().color = new Color(64/255, 43/255, 43/255, 1);
//		isDisappear = false;
	}
	*/
	IEnumerator FireAttack () {
		while (isJumping) {
			yield return new WaitForSeconds (1);
			Instantiate (fire, transform.position, Quaternion.identity);
		}
	}
	IEnumerator HomingAttack () {
		while (isJumping) {
			yield return new WaitForSeconds (1);
			GameObject attack = Instantiate (homing, transform.position, Quaternion.identity) as GameObject;
//			PoolableObject attack = homing.Place(transform.position);
			attack.GetComponent<MarathonBossAttack> ().setBossObj (this.gameObject);
		}
	}
	IEnumerator FinishJump () {
		yield return new WaitForSeconds (5);
		isJumping = false;
		isAttack = false;
	}
	IEnumerator positionReverse () {
		yield return new WaitForSeconds (1);
		transform.position = new Vector3 (posX, transform.position.y, transform.position.z);
		isAttack = false;
	}
	void OnTriggerEnter2D (Collider2D other) {
//		Debug.Log (GetComponent<BoxCollider2D>().tag);
//		Debug.Log ("Boss ontrigger enter : " + other);
		bool isNoDamage = false;
		if (other.tag == "Player") {
			MarathonPlayer player = other.GetComponent<MarathonPlayer> ();
			player.hitBoss ();
			isNoDamage = true;
		} else if (other.tag == "MarathonHadouken") {
			other.GetComponent<MarathonHadouken> ().DestroyObj ();
			displayDamage (HADOUKEN_DAMAGE);
			render.sprite = damage;
			StartCoroutine ("normalSprite");
			damagePoint = setDamagePoint (HADOUKEN_DAMAGE);
		} else if (other.tag == "MarathonYogaFire") {
			other.GetComponent<MarathonYogaFire> ().DestroyObj ();
			displayDamage (YOGA_FIRE_DAMAGE);
			render.sprite = damage;
			StartCoroutine ("normalSprite");
			damagePoint = setDamagePoint (YOGA_FIRE_DAMAGE);
		} else if (other.tag == "PlayerNormalAttack") {
			displayDamage (NORMAL_ATTACK_DAMAGE);
			render.sprite = damage;
			StartCoroutine ("normalSprite");
			damagePoint = setDamagePoint (NORMAL_ATTACK_DAMAGE);
		} else if (other.tag == "PlayerUpper") {
			displayDamage (UPPER_DAMAGE);
			render.sprite = damage;
			StartCoroutine ("normalSprite");
			damagePoint = setDamagePoint (UPPER_DAMAGE);
		} else if (other.tag == "PlayerKick") {
			displayDamage (KICK_DAMAGE);
			render.sprite = damage;
			StartCoroutine ("normalSprite");
			damagePoint = setDamagePoint (KICK_DAMAGE);
		} else if (other.tag == "PlayerLaserBeam") {
			displayDamage (HISSATU_DAMAGE);
			render.sprite = damage;
			StartCoroutine ("normalSprite");
			damagePoint = setDamagePoint (HISSATU_DAMAGE);
		}

		if (!isNoDamage) {
			if (isFalling) {
				isFalling = false;
				childKick.SetActive (false);
				if (animator != null) {
					animator.SetBool ("kickDown", false);
				}
			}

			if (isSliding) {
				isSliding = false;
				childSliding.SetActive (false);
				if (animator != null) {
					animator.SetBool ("slide", false);
				}
			}
		}

		if (damagePoint >= bossHitPoint && !isDead) {
			damagePoint = bossHitPoint;
			isDead = true;
			//			AllGameDefinition.groundDifficulty = 2;
//			Debug.Log("nextGame#########");
			if (isTutorial) {
				gameController.SendMessage ("CompleteTutorial");
			} else {
				StartCoroutine (nextGame ());
			}
			//			_gameComplete ();
		}
		hpSlider.value = damagePoint;
		hpText.text = (bossHitPoint - damagePoint).ToString ();
	}
	void displayDamage (int damage) {
		damage = _getDamagePoint (damage);
		damageText.GetComponent<MarathonBossDamageText> ().StartText (damage);
		/*
		Debug.Log ("displayDamage");
		GameObject damageText = ResourceCache.Load ("Ground/damageText");
		GameObject text = Instantiate (damageText, transform.position, Quaternion.identity) as GameObject;
		text.GetComponent<MarathonBossDamageText> ().SetText (damage);
		*/
	}
	IEnumerator MirrorDisable () {
//		int num = 2;
//		if (difficulty == 3) {
		int num = UnityEngine.Random.Range (1, 3);
//		}
		yield return new WaitForSeconds (num);
		childMirror.SetActive (false);
		mirrorEnabled = false;
		StartCoroutine (MirrorEnable ());
	}
	IEnumerator MirrorEnable () {
//		int num = 5;
//		if (difficulty == 3) {
		int num = UnityEngine.Random.Range (5, 10);
//		}
		yield return new WaitForSeconds (num);
		childMirror.SetActive (true);
		mirrorEnabled = true;
		StartCoroutine (MirrorDisable ());
		//		isMirror = false;
	}
	void _gameComplete () {
		//		Debug.Log ("_gameComplete");
		//		rdbg.AddForce (Vector2.up * 1000);
		AllGameDefinition.groundDifficulty = 2;
		StartCoroutine ("nextGame");

	}
	IEnumerator nextGame () {
		yield return new WaitForSeconds (0.1f);
		//		Debug.Log ("nextGame");
		int num = 0;
		gameController.SendMessage ("nextStage", num);
		StartCoroutine ("DestroyObj");
		//		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}
	IEnumerator DestroyObj () {
		yield return new WaitForSeconds (2);
		Destroy (this.gameObject);
	}
	IEnumerator normalSprite () {
		yield return new WaitForSeconds (0.5f);
		render.sprite = normal;
	}
	float _getSpeed () {
//		return UnityEngine.Random.Range (1, 20);
		return UnityEngine.Random.Range (2, 10);
	}

	public void HitPlayerAttack (int actionPattern) {
		render.sprite = damage;
		StartCoroutine (normalSprite ());
//		damagePoint = damagePoint + 5;
		damagePoint = setDamagePoint (5);
	}

	public void SetScrewDriverFlag (bool flag) {
		isScrewDriver = flag;
	}

	public void HitScrewDriver () {
		Debug.Log ("Boss hit screw");
		StartCoroutine ("nextGame");
	}

	int setDamagePoint (int addDamage) {
		addDamage = _getDamagePoint (addDamage);

		return damagePoint + addDamage;
	}

	public void setDifficulty (int num) {
		difficulty = num;
	}

	/**
	 * increase damage by user level
	**/
	int _getDamagePoint (int damage) {
		int level = AllGameDefinition.level;
		double ratio = level / 100d;

		return (int)(damage * (1d + ratio));
	}

	public void setTutorialFlag (bool flag) {
		isTutorial = flag;
	}

	public bool getBossIsDead () {
		return isDead;
	}
}
