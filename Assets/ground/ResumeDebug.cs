﻿using UnityEngine;
using System.Collections;

public class ResumeDebug : MonoBehaviour {
	GameObject gameController;
	// Use this for initialization
	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Resume () {
		gameController.SendMessage ("ResumeAction");
	}
}
