﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MarathonPlayerDamageText : MonoBehaviour {
	public Text damageText;

	float alpha;

	// Use this for initialization
	void Start () {
//		damageText = GetComponent<Text> ();
	}

	// Update is called once per frame
	void Update () {
		if (alpha < 0) {
			return;
		}
		alpha -= 1.0f * Time.deltaTime;
		damageText.color = new Color(1, 0, 0, alpha);
	}

	public void StartText (int damage) {
//		Debug.Log ("StartText");
		damageText.text = damage.ToString ();
		alpha = 1.0f;
	}

	public void SetText (int damage) {
		damageText.text = damage.ToString ();
	}
}
