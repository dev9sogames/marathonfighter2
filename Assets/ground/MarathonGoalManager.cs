﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonGoalManager : MonoBehaviour {
	public MarathonGoalController[] goalList;

	bool isGoal;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setMoveFlag (bool flag) {
		foreach (MarathonGoalController goal in goalList) {
			goal.setMoveFlag (flag);
		}
	}

	public void getGoal (int goalType) {
		if (isGoal) {
			return;
		}
		isGoal = true;
		foreach (MarathonGoalController goal in goalList) {
			goal.getGoal (goalType);
		}
	}
}
