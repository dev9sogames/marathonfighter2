﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MarathonDifficulty : MonoBehaviour {
//	public Canvas DifficultyCanvas;
	GameObject gameController;

	// Use this for initialization
	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetDifficulty (int num) {
		// 1: easy, 2: normal, 3: hard
		SceneManager.LoadScene("Loading");
		AllGameDefinition.gameMode = 1;
		AllGameDefinition.groundDifficulty = num;
//		gameController.SendMessage ("SetDifficulty", num);
//		DifficultyCanvas.enabled = false;
	}

	public void SetBossStart (int num) {
		// 2: normal, 3: hard
		SceneManager.LoadScene("Loading");
		if (num == 3) {
			AllGameDefinition.currentDifficulty = 6;
		} else if (num == 4) {
			AllGameDefinition.currentDifficulty = 11;
		}
		if (num == 4) {
			num = 3;
		}
		AllGameDefinition.groundDifficulty = num;
		AllGameDefinition.gameMode = 2;
//		gameController.SendMessage ("GameBossStart", num);
//		DifficultyCanvas.enabled = false;
	}

}
