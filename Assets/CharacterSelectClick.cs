﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelectClick : MonoBehaviour {
	public GameObject CharacterSelectController;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SelectCharacter (int type) {
		/*
	 * 1:normal
	 * 2:mini
	 * 3:nurikabe
	 * 4:black
	*/
		AllGameDefinition.CharacterType = type;

		AllGameDefinition.gameMode = 1;
		AllGameDefinition.groundDifficulty = 2;
		SceneManager.LoadScene("Loading");
		Destroy (this.gameObject);
	}

	public void ClickCharaSelectBackButon () {
		Destroy (CharacterSelectController.gameObject);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);
	}
}
