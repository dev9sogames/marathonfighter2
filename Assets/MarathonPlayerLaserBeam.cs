﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonPlayerLaserBeam : MonoBehaviour {
	Vector3 pos;

	// Use this for initialization
	void Start () {
		pos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		pos.x += Time.deltaTime * 12;
		if (transform.position.x >= 20 || transform.position.x <= -20) {
			Destroy (this.gameObject);
		}
		transform.position = pos;
	}
}
