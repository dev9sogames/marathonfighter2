﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattlePlayerProtection : MonoBehaviour {
	PhotonView photonView;

	bool singleMode;
	bool singleEnemy;

	int hitPoint = 7;

	void Start () {
		photonView = transform.parent.gameObject.GetComponent<PhotonView> ();
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (!singleMode && !photonView.isMine) {
			return;
		}

		if (other.tag == "Player" || other.tag == "Untagged") {
			return;
		}

		if (!singleMode) {
			if (PhotonNetwork.isMasterClient) {
				if (other.tag == "EnemyPlayerNormalAttack") {
					_hitAttack (1);
				} else if (other.tag == "EnemyHadouken") {
					_hitAttack (2);
				} else if (other.tag == "EnemyYogaFire") {
					_hitAttack (4);
				} else if (other.tag == "EnemyRepuuken") {
					_hitAttack (2);
				} else if (other.tag == "EnemyPlayerTornado") {
					_hitAttack (3);
				}
			} else {
				if (other.tag == "PlayerNormalAttack") {
					_hitAttack (1);
				} else if (other.tag == "MarathonHadouken") {
					_hitAttack (2);
				} else if (other.tag == "MarathonYogaFire") {
					_hitAttack (4);
				} else if (other.tag == "Repuuken") {
					_hitAttack (2);
				} else if (other.tag == "PlayerTornado") {
					_hitAttack (3);
				}
			}
		} else {
			if (singleEnemy) {
				if (other.tag == "PlayerNormalAttack") {
					_hitAttack (1);
				} else if (other.tag == "MarathonHadouken") {
					_hitAttack (2);
				} else if (other.tag == "MarathonYogaFire") {
					_hitAttack (4);
				} else if (other.tag == "Repuuken") {
					_hitAttack (2);
				} else if (other.tag == "PlayerTornado") {
					_hitAttack (3);
				}
			} else {
				if (other.tag == "EnemyPlayerNormalAttack") {
					_hitAttack (1);
				} else if (other.tag == "EnemyHadouken") {
					_hitAttack (2);
				} else if (other.tag == "EnemyYogaFire") {
					_hitAttack (4);
				} else if (other.tag == "EnemyRepuuken") {
					_hitAttack (2);
				} else if (other.tag == "EnemyPlayerTornado") {
					_hitAttack (3);
				}
			}
		}
	}

	void _hitAttack (int attackPoint) {
//		transform.parent.gameObject.GetComponent<OnlineBattlePlayerController> ().setProtectionFlag (true);
		hitPoint -= attackPoint;
		if (hitPoint < 1) {
			transform.parent.gameObject.GetComponent<OnlineBattlePlayerController> ().setProtectionFlag (false);
		}
	}

	public void SetSingleMode (bool flag) {
		singleMode = flag;
	}

	public void SetSingleEnemy (bool flag) {
		singleEnemy = flag;
	}
}
