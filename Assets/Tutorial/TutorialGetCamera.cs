﻿using UnityEngine;
using System.Collections;

public class TutorialGetCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject obj = GameObject.FindGameObjectWithTag("MainCamera");
		Camera camera = obj.GetComponent<Camera> ();

		Canvas canvas = GetComponent<Canvas> ();
		canvas.worldCamera = camera;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
