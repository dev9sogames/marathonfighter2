﻿using UnityEngine;
using System.Collections;

public class TutorialFieldMove : MonoBehaviour {
	Vector3 pos;
	float posX;
	GameObject gameController;

	float speed = 450;

	bool isReady = true;

	// Use this for initialization
	void Start () {
//		gameController = GameObject.FindGameObjectWithTag ("TutorialGameController");
		pos = transform.localPosition;
		posX = transform.localPosition.x;
	}

	void Update () {
		if (isReady) {
			return;
		}
		posX -= Time.deltaTime * speed;
		transform.localPosition = new Vector3 (posX, pos.y, pos.z);
	}

	public void Ready () {
		isReady = true;
	}
	public void GameStart () {
		isReady = false;
	}
}
