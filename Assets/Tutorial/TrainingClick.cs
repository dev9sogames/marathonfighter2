﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TrainingClick : MonoBehaviour {
	public Canvas mainCanvas;

//	GameObject canvasObj;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ClickTraining () {
		Destroy (mainCanvas.gameObject);
		GameObject prefab = ResourceCache.Load ("Training/TrainingJumpCanvas");
		GameObject canvasObj = Instantiate (prefab, new Vector3(326.5f, 183.5f, 0), Quaternion.identity) as GameObject;
		TutorialSingleton.Instance.currentObj = canvasObj;
	}

	public void NextCanvas () {
		Destroy (TutorialSingleton.Instance.currentObj.gameObject);

		GameObject prefab = ResourceCache.Load ("Training/TrainingHadoukenCanvas");
		GameObject canvasObj = Instantiate (prefab, new Vector3(326.5f, 183.5f, 0), Quaternion.identity) as GameObject;
	}
}
