﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialGameController : MonoBehaviour {
	public Button StartButton;
	public Button RetryButton;
	public Button NextButton;

	public MarathonPlayer player;

	public TutorialFieldMove[] fieldList;

	public Canvas TutorialCanvas;

	// Use this for initialization
	void Start () {
		RetryButton.gameObject.SetActive (false);
		NextButton.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartClick () {
		StartButton.gameObject.SetActive (false);
		player.TrainingStart ();

		foreach (TutorialFieldMove field in fieldList) {
			field.GameStart ();
		}
	}

	public void FallPlayer () {
		foreach (TutorialFieldMove field in fieldList) {
			field.Ready ();
		}
		RetryButton.gameObject.SetActive (true);
	}
	public void JumpSuccess () {
		foreach (TutorialFieldMove field in fieldList) {
			field.Ready ();
		}
		player.TrainingFinish ();
		RetryButton.gameObject.SetActive (true);
		NextButton.gameObject.SetActive (true);
	}

	public void ClickTutorialBackButton () {
		Destroy (TutorialCanvas.gameObject);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);
	}
}
