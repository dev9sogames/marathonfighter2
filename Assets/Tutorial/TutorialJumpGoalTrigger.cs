﻿using UnityEngine;
using System.Collections;

public class TutorialJumpGoalTrigger : MonoBehaviour {
	GameObject gameController;

	// Use this for initialization
	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("TutorialGameController");	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D (Collider2D other) {
		gameController.SendMessage ("JumpSuccess");
	}
}
