﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialImageDisplay : MonoBehaviour {
	public Canvas MainCanvas;
	public Canvas Tutorialcanvas;

	public Button firstPageButton;
	public Button secondPageButton;

	public Canvas firstCanvas;
	public Canvas secondCanvas;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PushFirst () {
		firstPageButton.interactable = false;
		secondPageButton.interactable = true;

		firstCanvas.gameObject.SetActive(true);
		secondCanvas.gameObject.SetActive(false);
	}
	public void PushSecond () {
		firstPageButton.interactable = true;
		secondPageButton.interactable = false;

		firstCanvas.gameObject.SetActive(false);
		secondCanvas.gameObject.SetActive(true);
	}

	public void CloseCanvas () {
		Tutorialcanvas.gameObject.SetActive (false);
		MainCanvas.gameObject.SetActive (true);
	}
}
