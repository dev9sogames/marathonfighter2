﻿using UnityEngine;
using System.Collections;

public class TitleFadeOut : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke("FadeOut", 3f);
	}

	void FadeOut () {
		iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 3f, "onupdate", "SetValue"));
	}
	void SetValue(float alpha) {
		if (alpha == 0.0f) {
			this.gameObject.SetActive (false);
		}
		gameObject.GetComponent<UnityEngine.UI.Image>().color = new Color(255,255,255, alpha);
	}
}
