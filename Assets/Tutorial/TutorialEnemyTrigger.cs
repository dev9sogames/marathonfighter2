﻿using UnityEngine;
using System.Collections;

public class TutorialEnemyTrigger : MonoBehaviour {
	public int WazaType; // 1:hadouken, 2:yogafire, 3:kick, 4:upper, 5:rolling, 6:tornado, 7:screw, 8:angel

	Rigidbody2D rgbd;
	// Use this for initialization
	void Start () {
		rgbd = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y > 500) {
			Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		switch (WazaType) {
		case 1:
			if (other.tag == "MarathonHadouken") {
				rgbd.AddForce (Vector2.up * 1000);
			}
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
		case 6:
			break;
		case 7:
			break;
		case 8:
			break;
		}
	}
}
