﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonSetAudioSource : MonoBehaviour {
//	public AudioClip groundAudio;
//	public AudioClip PlanetAudio;
//	public AudioClip SeaAudio;
//	public AudioClip DesertAudio;
//	public AudioClip BossAudio;

	public AudioSource source;

	public UnityEngine.Audio.AudioMixer mixer;

	void Start () {
		switch (AllGameDefinition.StoryStageType) {
		case 1:
			source.clip = Resources.Load("sound/Aggressive1", typeof(AudioClip)) as AudioClip;
			break;
		case 2:
			source.clip = Resources.Load("sound/Soft1", typeof(AudioClip)) as AudioClip;
			break;
		case 3:
			source.clip = Resources.Load("sound/Quiet2", typeof(AudioClip)) as AudioClip;
			break;
		case 4:
			source.clip = Resources.Load("sound/Soft1", typeof(AudioClip)) as AudioClip;
			break;
		case 5:
			source.clip = Resources.Load("sound/Quiet2", typeof(AudioClip)) as AudioClip;
			break;
		default:
			source.clip = Resources.Load("sound/Soft1", typeof(AudioClip)) as AudioClip;
			break;
		}

		float bgmVolume = PlayerPrefs.GetFloat ("bgmVolume", 0);
		float seVolume = PlayerPrefs.GetFloat ("seVolume", 0);

		mixer.SetFloat ("BgmVolume", Mathf.Lerp(-80, 10, bgmVolume));
		mixer.SetFloat ("SeVolume", Mathf.Lerp(-80, 10, seVolume));

		source.Play ();
	}
}
