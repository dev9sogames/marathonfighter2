﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpeningCharacter : MonoBehaviour {
	SystemLanguage lang;

	public int characterType; // 0:leftHero, 1:rightHero, 2:nurikabe, 3:princess
	public GameObject OpeningController;

	public Sprite threeCountSprite;

	bool isStart;
	bool isHeroRun;
	bool isRunAway;
	bool isHeroSuperRun;
	bool isTalkFinish;

	RectTransform rect;
	float posX;
	float posY;

	SpriteRenderer PrincessSpriteRenderer;
	public Sprite runAwayPrincess;

	Image image1;
	Image image2;
	Text text1;
	Text text2;

	Animator animator;

	int count;

	// Use this for initialization
	void Start () {
		count = 1;
		rect = GetComponent<RectTransform> ();
		posX = rect.transform.localPosition.x;
		posY = rect.transform.localPosition.y;

		lang = Application.systemLanguage;

		if (characterType == 1) {
//			this.rect.transform.SetSiblingIndex (30);
			animator = GetComponent<Animator> ();
			animator.enabled = false;
			image1 = transform.Find ("Image1").GetComponent<Image> ();
			image2 = transform.Find ("Image2").GetComponent<Image> ();
			text1 = transform.Find ("Text1").GetComponent<Text> ();
			text2 = transform.Find ("Text2").GetComponent<Text> ();
			image1.enabled = false;
			text1.enabled = false;
			image2.enabled = false;
			text2.enabled = false;
			StartCoroutine (HeroTalk ());
		}
		if (characterType == 3) {
//			this.rect.transform.SetSiblingIndex (29);
			image1 = transform.Find ("Image1").GetComponent<Image> ();
			text1 = transform.Find ("Text1").GetComponent<Text> ();
			image1.enabled = false;
			text1.enabled = false;
			StartCoroutine (PrincessTalk ());
			PrincessSpriteRenderer = GetComponent<SpriteRenderer> ();
//			StartCoroutine (PrincessForward ());
		}
	}
	IEnumerator PrincessForward () {
		yield return new WaitForSeconds (22);
		Debug.Log ("princess forward");
		this.transform.SetAsLastSibling();
	}
	IEnumerator HeroTalk () {
		yield return new WaitForSeconds (2);
		image1.enabled = true;
		text1.enabled = true;
		if (lang == SystemLanguage.Japanese) {
			text1.text = "私はマラソンランナーだ。 次の大会に向けて真剣に練習するのだ。";
		} else if (lang == SystemLanguage.ChineseSimplified) {
			text1.text = "我是一个马拉松运动员。我们认真践行下一个比赛";
		} else {
			text1.text = "I am a marathon runner. Practice seriously for the next competition.";
		}
		StartCoroutine (Talk2 ());
//		StartCoroutine (FinishTalk ());
		StartCoroutine (StartRunning ());
	}
	IEnumerator Talk2 () {
		yield return new WaitForSeconds (5);
		if (lang == SystemLanguage.Japanese) {
			text1.text = "本日ついに、秘策を見つけたのだ。 聞いて驚くなよ";
		} else if (lang == SystemLanguage.ChineseSimplified) {
			text1.text = "今天终于，我发现了一个秘密计划。不要惊讶地听到";
		} else {
			text1.text = "Finally, I found a secret idea today. Listen and be amazed";
		}
		StartCoroutine (Talk3 ());
	}
	IEnumerator Talk3 () {
		yield return new WaitForSeconds (3);
		if (lang == SystemLanguage.Japanese) {
			text1.text = "名付けて！！！！！";
		} else if (lang == SystemLanguage.ChineseSimplified) {
			text1.text = "得名！！！！！";
		} else {
			text1.text = "Name it!!!!!!!";
		}
		StartCoroutine (Talk4 ());
	}
	IEnumerator Talk4 () {
		yield return new WaitForSeconds (4);
		if (lang == SystemLanguage.Japanese) {
			text1.text = "3の倍数と3が付く数字のときだけアホになる！！！！！";
		} else if (lang == SystemLanguage.ChineseSimplified) {
			text1.text = "当数乘数的3和3连接它只会傻瓜！！！！！";
		} else {
			text1.text = "It will become stupid only when the multiples of 3 and 3 are attached!!!!!";
		}
		StartCoroutine (FinishTalk ());
	}
	IEnumerator FinishTalk () {
		yield return new WaitForSeconds (3);
		image1.enabled = false;
		text1.enabled = false;
	}
	IEnumerator StartRunning () {
		yield return new WaitForSeconds (17);
		rect.transform.localPosition = new Vector3 (-205.3f, -35f, 1);
		posX = rect.transform.localPosition.x;
		posY = rect.transform.localPosition.y;
		rect.transform.localScale = new Vector3 (-1, 1, 1);
		animator.enabled = true;
		isTalkFinish = true;
		StartCoroutine (TrainingStart ());
	}
	IEnumerator TrainingStart () {
		yield return new WaitForSeconds (0.5f);
		StartCoroutine (OneTwoThree ());
	}
	IEnumerator OneTwoThree () {
		while (true) {
			yield return new WaitForSeconds (1);
			if (count % 3 == 0 || count == 13 || count == 23) {
				animator.enabled = false;
				GetComponent<Image> ().sprite = threeCountSprite;
			}
			image2.enabled = true;
			text2.enabled = true;
			StartCoroutine (StopImage2 ());
			text2.text = count.ToString ();
			count++;
		}
	}
	IEnumerator StopImage2 () {
		yield return new WaitForSeconds (0.3f);
		animator.enabled = true;
		image2.enabled = false;
		text2.enabled = false;
	}
	IEnumerator PrincessTalk () {
		yield return new WaitForSeconds (4);
		image1.enabled = true;
		text1.enabled = true;
		if (lang == SystemLanguage.Japanese) {
			text1.text = "頑張ってね、マラソンマン。 それと、万が一私が襲われたら助けてくださいね。";
		} else if (lang == SystemLanguage.ChineseSimplified) {
			text1.text = "祝你好运，马拉松男子。同时，还请以任何机会，如果我攻击了帮助";
		} else {
			text1.text = "Good luck, marathon man. Also, please help me should I be attacked by any chance.";
		}
		StartCoroutine (FinishTalk ());
	}
	// Update is called once per frame
	void FixedUpdate () {
		if (isRunAway) {
			float moveX = 50f * Time.deltaTime;
			posX += moveX;
			rect.transform.localPosition = new Vector3 (posX, posY, 1);
//			transform.Translate (3.5f * Time.deltaTime, 0, 0);
		}

		if (!isStart) {
			return;
		}

		if (isHeroSuperRun) {
			float moveX = 275f * Time.deltaTime;
			posX += moveX;
			rect.transform.localPosition = new Vector3 (posX, posY, 1);
		}

		if (characterType == 0) {
			float moveX = 35f * Time.deltaTime;
			posX += moveX;
			rect.transform.localPosition = new Vector3 (posX, posY, 1);
//			transform.Translate (3.5f * Time.deltaTime, 0, 0);
			if (transform.localPosition.x > -421) {
				Destroy (this.gameObject);
				OpeningController.GetComponent<OpeningCanvasController> ().GoToLeft ();
			}
		} else if (characterType == 1) {
			if (isTalkFinish && !isHeroRun) {
				float moveX = 40f * Time.deltaTime;
				posX += moveX;
				rect.transform.localPosition = new Vector3 (posX, posY, 1);
				if (rect.transform.localPosition.x > 190) {
					rect.transform.localPosition = new Vector3 (190, 49, 0);
					posY = rect.transform.localPosition.y;
					rect.transform.localScale = new Vector3 (1, 1, 1);
					rect.sizeDelta = new Vector2 (35, 35);
					text2.transform.localScale = new Vector3 (0.5f, 0.5f, 1);
					isStart = false;

					StartCoroutine (HeroRunning ());
				}
			} else if (isTalkFinish) {
				if (rect.transform.localPosition.x < -220) {
					return;
				}

				float moveX = 55f * Time.deltaTime;
				posX -= moveX;
				rect.transform.localPosition = new Vector3 (posX, posY, 1);
//				transform.Translate (10.0f * Time.deltaTime, 0, 0);
			}
		} else if (characterType == 2) {
			float moveX = 55f * Time.deltaTime;
			posX -= moveX;
			float moveY = 30f * Time.deltaTime;
			posY -= moveY;
			rect.transform.localPosition = new Vector3 (posX, posY, 0);
//			transform.Translate (-3.5f * Time.deltaTime, -1.5f * Time.deltaTime, 0);
		}
	}

	public void startHero () { // 1:right to left, 2: left to right
		isStart = true;
	}

	IEnumerator HeroRunning () {
		yield return new WaitForSeconds (0.5f);
		isStart = true;
//		this.rect.transform.SetSiblingIndex (28);
		isHeroRun = true;
	}

	public void GoNurikabe () {
		isStart = true;
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "FieldObj") {
			return;
		}

		if (characterType == 0 || characterType == 1) {
			return;
		}

		if (characterType == 2 || characterType == 3) {
			isStart = false;
			posX = rect.transform.localPosition.x;
			posY = rect.transform.localPosition.y;
			StartCoroutine (RunAway ());
			StartCoroutine (HeroSuperRun ());
		}
		/*
		if (other.tag == "FieldObj") {
			return;
		}

		nurikabeStart = false;
		nurikabeReturn = true;
		Debug.Log ("Trigger" + other);

		StartCoroutine (RunAway ());
		*/
	}

	public void HeroSuperRunStart () {
		isHeroSuperRun = true;
	}

	IEnumerator HeroSuperRun () {
		yield return new WaitForSeconds (5.5f);
		OpeningController.GetComponent<OpeningCanvasController> ().HeroSuperRunStart ();
	}

	IEnumerator RunAway () {
		yield return new WaitForSeconds (1);
		if (characterType == 3) {
			GetComponent<Image> ().sprite = runAwayPrincess;
//			PrincessSpriteRenderer.sprite = runAwayPrincess;
//			transform.localScale = new Vector3 (0.7f, 0.7f, 1.0f);
			StartCoroutine (MoveStorySelectScreen ());
		}
		isRunAway = true;
	}

	IEnumerator MoveStorySelectScreen () {
		yield return new WaitForSeconds (7);
		OpeningController.GetComponent<OpeningCanvasController> ().MoveStorySelect ();
	}
}
