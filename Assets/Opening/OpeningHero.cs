﻿using UnityEngine;
using System.Collections;

public class OpeningHero : MonoBehaviour {
	public GameObject heroLeft;
	public GameObject heroRight;
	public GameObject nurikabe;
	public GameObject princess;

	public Canvas OpeningCanvas;
	public Canvas StageSelectCanvas;

	public int characterType; // 0:leftHero, 1:rightHero, 2:nurikabe, 3:princess
//	public bool isNurikabe;
//	public bool isPrincess;
	bool isRunAway;

	public Sprite runAwayPrincess;

	bool nurikabeStart;
	bool nurikabeReturn;

	public bool isRunning;
	bool HeroRun;

	int heroType; // 0:big, 1:small, 2:big, 3:small

	SpriteRenderer PrincessSpriteRenderer;

//	Vector3 posLeft;
//	Vector3 posRight;

	// Use this for initialization
	void Start () {
		Debug.Log ("Opening Hero Start!!!!!");
		heroType = 0;
//		posLeft = heroLeft.transform.position;
//		posRight = heroRight.transform.position;
		StartCoroutine(nurikabeStartCount());
		if (characterType == 3) {
			PrincessSpriteRenderer = GetComponent<SpriteRenderer> ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		switch (characterType) {
		case 0: // left hero
			moveHero ();
			break;
		case 1: // right hero
			moveHero ();
			break;
		case 2: // nurikabe
			if (nurikabeStart) {
				nurikabe.transform.Translate (-3.5f * Time.deltaTime, -1.5f * Time.deltaTime, 0);
			}
			if (isRunAway) {
				transform.Translate (3.5f * Time.deltaTime, 0, 0);
			}
			break;
		case 3: // princess
			if (isRunAway) {
				transform.Translate (3.5f * Time.deltaTime, 0, 0);
			}
			break;
		default:
			break;
		}
	}

	public void moveHero () {
		if (heroType == 0) {
			heroLeft.transform.Translate (2.5f * Time.deltaTime, 0, 0);
			if (heroLeft.transform.localPosition.x > -421) {
				Destroy (heroLeft.gameObject);
				heroType = 1;
			}
		} else if (heroType == 1) {
			heroRight.transform.Translate (-2.5f * Time.deltaTime, 0, 0);
			if (heroRight.transform.localPosition.x < -431) {
				//Destroy (heroRight.gameObject);
				heroType = 2;
				transform.localScale = new Vector3 (-0.1f, 0.1f, 0);
				StartCoroutine (HeroRunning ());
			}
			//		} else if (heroType == 2) {
			//			GameObject heroLeft2 = ResourceCache.Load ("Opening/hero_left");
			//			Instantiate (heroLeft2, , Quaternion.identity);
			//		} else if (heroType == 3) {
		} else if (heroType == 2) {
			if (HeroRun) {
				transform.Translate (10.0f * Time.deltaTime, 0, 0);
			}
		}
	}

	IEnumerator nurikabeStartCount () {
		yield return new WaitForSeconds (9);
		nurikabeStart = true;
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "FieldObj") {
			return;
		}

		if (characterType == 0 || characterType == 1) {
			return;
		}
		nurikabeStart = false;
		nurikabeReturn = true;
		Debug.Log ("Trigger" + other);

		StartCoroutine (RunAway ());
	}


	IEnumerator RunAway () {
		yield return new WaitForSeconds (1);
		if (characterType == 3) {
			PrincessSpriteRenderer.sprite = runAwayPrincess;
			transform.localScale = new Vector3 (0.7f, 0.7f, 1.0f);
			StartCoroutine (MoveStorySelectScreen ());
		}
		isRunAway = true;
		//StartCoroutine (HeroRunning ());
	}

	IEnumerator HeroRunning () {
		yield return new WaitForSeconds (4);
		HeroRun = true;
	}

	IEnumerator MoveStorySelectScreen () {
		yield return new WaitForSeconds (7);
		OpeningCanvas.gameObject.SetActive (false);
		StageSelectCanvas.gameObject.SetActive (true);
	}
}
