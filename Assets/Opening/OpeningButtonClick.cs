﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpeningButtonClick : MonoBehaviour {
	public Canvas OpeningCanvas;

	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt ("NotShowStoryMovie", 0);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ClickBackButton () {
		Destroy (OpeningCanvas.gameObject);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);
	}

	public void ClickSkipButton () {
		Destroy (OpeningCanvas.gameObject);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Select/StageMultiSelectCanvas");
		Instantiate (prefab, position, transform.rotation);
	}

	public void NotShowButton () {
		PlayerPrefs.SetInt ("NotShowStoryMovie", 1);
		PlayerPrefs.Save ();

		Destroy (OpeningCanvas.gameObject);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Select/StageMultiSelectCanvas");
		Instantiate (prefab, position, transform.rotation);
	}
}
