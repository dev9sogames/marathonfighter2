﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattleThunder : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		Vector2 position = transform.position;
		if (position.y <= -15) {
			this.gameObject.SetActive (false);
		}

		position.y -= Time.deltaTime * 5;
		transform.position = position;

	}
}
