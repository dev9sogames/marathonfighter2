﻿using UnityEngine;
using System.Collections;

public class camera : MonoBehaviour {
	Camera MainCamera;
	public GameObject player;
	public GameObject Spear;
	public float yValue;
	public float addX;

	bool isThrowing;

	bool isRollingHero;

	// Use this for initialization
	void Start () {
		MainCamera = GetComponent<Camera> ();
	}

	// Update is called once per frame
	void Update () {
		if (isRollingHero) {
			return;
		}
		if (player) {
			transform.position = new Vector3 (player.transform.position.x + addX, yValue, -10);
		}
	}

	public void throwing() {
		isThrowing = true;
		MainCamera.orthographicSize = 5;
	}

	public void SetRollingFlag (bool flag) {
		Debug.Log ("SetRollingFlag : " + flag);
		isRollingHero = flag;
	}
}