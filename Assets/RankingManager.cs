﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NCMB;

public class RankingManager : SingletonMonoBehaviour <RankingManager> {
	RankingManager instance = null;
	int rankDataStatus = 0; //0:not, 1:success, 2:error
	NCMBObject cloudObj = new NCMBObject("highScore");


	public Dictionary<string, int> getStoryScoreRanking () {
		Dictionary<string, int> storyRankingData = new Dictionary<string, int>();
		rankDataStatus = 0;
		NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject> ("highScore");
		query.Limit = 10;
		query.OrderByDescending ("score");
		query.FindAsync ((List<NCMBObject> objList ,NCMBException e) => {
			if (e == null) {
				for(int i = 0; i < objList.Count; i++) {
					storyRankingData.Add(System.Convert.ToString(objList[i]["name"]), System.Convert.ToInt32(objList[i]["score"]));
				}

				rankDataStatus = 1;
			} else { // error
				rankDataStatus = 2;
			}
		});

		return storyRankingData;
	}

	public Dictionary<string, int> getEndlessScoreRanking () {
		Dictionary<string, int> endlessRankingData = new Dictionary<string, int>();
		rankDataStatus = 0;
		NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject> ("highScore");
		query.Limit = 10;
		query.OrderByDescending ("endlessScore");
		query.FindAsync ((List<NCMBObject> objList ,NCMBException e) => {
			if (e == null) {
				for(int i = 0; i < objList.Count; i++) {
					endlessRankingData.Add(System.Convert.ToString(objList[i]["name"]), System.Convert.ToInt32(objList[i]["endlessScore"]));
				}

				rankDataStatus = 1;
			} else { // error
				rankDataStatus = 2;
			}
		});

		return endlessRankingData;
	}

	public Dictionary<string, int> getMarathonScoreRanking (int difficulty) {
		string column = "";
		if (difficulty == 1) {
			column = "marathonScore";
		} else if (difficulty == 2) {
			column = "marathonHardScore";
		}
		Dictionary<string, int> marathonRankingData = new Dictionary<string, int>();
		rankDataStatus = 0;
		NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject> ("highScore");
		query.Limit = 10;
		query.OrderByDescending (column);
		query.FindAsync ((List<NCMBObject> objList ,NCMBException e) => {
			if (e == null) {
				for(int i = 0; i < objList.Count; i++) {
					marathonRankingData.Add(System.Convert.ToString(objList[i]["name"]), System.Convert.ToInt32(objList[i][column]));
				}

				rankDataStatus = 1;
			} else { // error
				rankDataStatus = 2;
			}
		});

		return marathonRankingData;
	}

	public void setRankingData (string name, int point) {
		NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject> ("highScore");
		query.WhereEqualTo ("name", name);
		query.FindAsync ((List<NCMBObject> objList ,NCMBException e) => {
			if (e == null) {
				Debug.Log("no exception");
				if( objList.Count == 0 ) {
					Debug.Log ("name : " + name);
					cloudObj ["name"] = name;
					cloudObj ["score"] = point;
					cloudObj.SaveAsync();
				} else {
//					int cloudScore = System.Convert.ToInt32(objList[0]["score"]);
//					Debug.Log("2" + cloudScore);
					//					if(totalBestPoint > cloudScore) {
					objList[0]["score"] = point;
					objList[0].SaveAsync();
					//					}
				}
			}
		});
	}

	public void setMarathonRankingData (string name, int point, int difficulty) {
		Debug.Log ("name : " + name);
		Debug.Log ("point : " + point);
		NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject> ("highScore");
		query.WhereEqualTo ("name", name);
		query.FindAsync ((List<NCMBObject> objList ,NCMBException e) => {
			if (e == null) {
				Debug.Log("no exception" + objList);
				if( objList.Count == 0 ) {
//					Debug.Log ("name : " + name);
					cloudObj ["name"] = name;
					if (difficulty == 1) {
					cloudObj ["marathonScore"] = point;
					} else if (difficulty == 2) {
						cloudObj ["marathonHardScore"] = point;
					}
					cloudObj.SaveAsync();
				} else {
//					int cloudScore = System.Convert.ToInt32(objList[0]["marathonScore"]);
//					Debug.Log("setMarathonRankingData : " + cloudScore);
//					if(point > cloudScore) {
					if (difficulty == 1) {
						objList[0]["marathonScore"] = point;
					} else if (difficulty == 2) {
						objList[0]["marathonHardScore"] = point;
					}
					objList[0].SaveAsync();
//					}
				}
			}
		});
	}

	public void setEndlessRankingData (string name, int point) {
		NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject> ("highScore");
		query.WhereEqualTo ("name", name);
		query.FindAsync ((List<NCMBObject> objList ,NCMBException e) => {
			if (e == null) {
//				Debug.Log("no exception" + objList);
				if( objList.Count == 0 ) {
					Debug.Log ("name : " + name);
					cloudObj ["name"] = name;
					cloudObj ["endlessScore"] = point;
					cloudObj.SaveAsync();
				} else {
//					int cloudScore = System.Convert.ToInt32(objList[0]["endlessScore"]);
//					Debug.Log("setMarathonRankingData : " + cloudScore);
					//					if(point > cloudScore) {
					objList[0]["endlessScore"] = point;
					objList[0].SaveAsync();
					//					}
				}
			}
		});
	}

	public void setRankingName (string oldName, string newName) {
		rankDataStatus = 0;
		NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject> ("highScore");
		query.WhereEqualTo ("name", oldName);
		query.FindAsync ((List<NCMBObject> objList ,NCMBException e) => {
			if (e == null) {
				Debug.Log("no exception" + objList);
				if( objList.Count > 0) { // update
//					Debug.Log ("oldName : " + oldName);
					objList[0]["name"] = newName;
					objList[0].SaveAsync();
				} else { // new register
					cloudObj ["name"] = newName;
					cloudObj ["score"] = 0;
					cloudObj ["marathonScore"] = 0;
					cloudObj ["endlessScore"] = 0;
					cloudObj.SaveAsync();
				}
				rankDataStatus = 1;
			} else {
				rankDataStatus = 2;
			}
		});
	}

	public int HasRankData () {
		return rankDataStatus;
	}
}
