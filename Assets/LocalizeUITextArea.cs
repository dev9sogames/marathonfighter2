﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class LocalizeUITextArea : MonoBehaviour {

	[SerializeField] string strEn;
	[SerializeField] string strJp;
	[SerializeField] string strCh;

	void Awake() {
		Dictionary<SystemLanguage, string> dict = new Dictionary<SystemLanguage, string>() {
			{ SystemLanguage.English, strEn},
			{ SystemLanguage.Japanese, strJp},
			{ SystemLanguage.ChineseSimplified, strCh},
		};

		//Change
		SystemLanguage lang = Application.systemLanguage;
		if (lang != SystemLanguage.Japanese && lang != SystemLanguage.English && lang != SystemLanguage.ChineseSimplified) {
			lang = SystemLanguage.English;
		}

		Text mText = GetComponent<Text>();
		mText.text = dict.ContainsKey(lang) ? dict[lang] : dict[SystemLanguage.English];
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(LocalizeUITextArea))]
[CanEditMultipleObjects]
public class LocalizeEditorTextArea : Editor {

	SerializedProperty propEn;
	SerializedProperty propJp;
	SerializedProperty propCh;

	void OnEnable() {
		propEn = serializedObject.FindProperty("strEn");
		propJp = serializedObject.FindProperty("strJp");
		propCh = serializedObject.FindProperty("strCh");
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();

		CreateUI("English", propEn, true);
		CreateUI("Japanese", propJp, true);
		CreateUI("ChineseSimplified", propCh, true);

		serializedObject.ApplyModifiedProperties();
	}

	private void CreateUI(string label, SerializedProperty prop, bool addSpace) {
		EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
		prop.stringValue = EditorGUILayout.TextArea(prop.stringValue);
		if(addSpace) EditorGUILayout.Space();
	}
}
#endif