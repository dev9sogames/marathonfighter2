﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoryClick : MonoBehaviour {
	public Canvas MainCanvas;
	public Canvas StageSelectCanvas;
	public Canvas CharacterSelectCanvas;
	public Canvas OpeningCanvas;

	GameObject gameController;

	// Use this for initialization
	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
	}

	public void StoryStart () {
		MainCanvas.gameObject.SetActive (false);
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Opening/OpeningCanvas");
		Instantiate (prefab, position, transform.rotation);

//		OpeningCanvas.gameObject.SetActive (true);
//		StageSelectCanvas.gameObject.SetActive (true);
	}

	public void SelectStage (int type) {
		/*
		 * 1:mini
		 * 2:nurikabe
		 * 3:black
		 * 4:last boss
		*/
		AllGameDefinition.StoryStageType = type;

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Select/StageSelectCanvas");


//		StageSelectCanvas.gameObject.SetActive (false);
//		CharacterSelectCanvas.gameObject.SetActive (true);
	}
	public void StageSelectBack () {
		StageSelectCanvas.gameObject.SetActive (false);
		MainCanvas.gameObject.SetActive (true);
	}
	public void CharaSelectBack () {
		CharacterSelectCanvas.gameObject.SetActive (false);
		StageSelectCanvas.gameObject.SetActive (true);
	}

	public void SelectCharacter (int type) {
	/*
	 * 1:normal
	 * 2:mini
	 * 3:nurikabe
	 * 4:black
	*/
		AllGameDefinition.CharacterType = type;

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Select/CharacterSelectCanvas");
		Instantiate (prefab, position, transform.rotation);


//		CharacterSelectCanvas.gameObject.SetActive (false);
		AllGameDefinition.gameMode = 1;
		AllGameDefinition.groundDifficulty = 2;
//		SceneManager.LoadScene("Loading");
	}

	public void ShowStatus () {
//		MainCanvas.gameObject.SetActive (false);
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Status/StatusCanvas");
		Instantiate (prefab, position, transform.rotation);
	}
}
