﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageSelectClick : MonoBehaviour {
	public GameObject StageSelectController;
	public bool isHellMode;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SelectStageType (int stageType, int stageDifficulty) {
	}

	/*
	 * 1:mini
	 * 2:nurikabe
	 * 3:black
	 * 4:last boss
	*/
	public void SelectStage (int stageType) {
		int type = (int) stageType / 10;
		AllGameDefinition.StoryStageType = type;
		AllGameDefinition.StoryStageCourt = stageType - (10 * type);

		AllGameDefinition.CharacterType = 1; // hero

		AllGameDefinition.gameMode = 1;
		AllGameDefinition.isHardMode = isHellMode;
//		AllGameDefinition.groundDifficulty = 2;
		SceneManager.LoadScene("Loading");
		Destroy (this.gameObject);

//		Vector3 position = new Vector3 (960, 540, 0);
//		GameObject prefab = (GameObject)Resources.Load("Select/CharaSelectCanvas");
//		Instantiate (prefab, position, transform.rotation);

		Destroy (StageSelectController.gameObject);
	}

	public void ClickStageBackButton () {
		Destroy (StageSelectController.gameObject);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);
	}
}
