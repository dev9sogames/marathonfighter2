﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetLockStatus : MonoBehaviour {
	/*
	 * 1:fireBomb
	 * 2:thunderAttack
	 * 3:kick
	 * 4:upper
	 * 
	*/
	public int type;

	public Image yogaFireLocked;
	public Image kickLocked;
	public Image upperLocked;

	// Use this for initialization
	void Start () {
		switch (type) {
		case 2:
			if (AllGameDefinition.yogaFireLocked == 1) {
				yogaFireLocked.enabled = false;
			}
			break;
		case 3:
			if (AllGameDefinition.kickLocked == 1) {
				kickLocked.enabled = false;
			}
			break;
		case 4:
			if (AllGameDefinition.upperLocked == 1) {
				upperLocked.enabled = false;
			}
			break;
		default:
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
