﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateDialog : MonoBehaviour {
	public bool initialFlag;

	public Canvas DifficultyCanvas;
	public Canvas TutorialCanvas;
	public Canvas Tutorial1Canvas;
	public Canvas Tutorial2Canvas;
	public Canvas Tutorial3Canvas;

	public Canvas RankPageCanvas;
	public Canvas TotalRankCanvas;
	public Canvas EasyRankCanvas;
	public Canvas NormalRankCanvas;
	public Canvas HardRankCanvas;

	public Canvas CharacterSelectCanvas;
	public Canvas StageSelectCanvas;
	public Canvas OpeningCanvas;

	public Button firstButton;
	public Button secondButton;
	public Button thirdButton;

	// Use this for initialization
	void Start () {
		if (initialFlag) {
			TutorialCanvas.gameObject.SetActive (false);
			Tutorial1Canvas.gameObject.SetActive (false);
			Tutorial2Canvas.gameObject.SetActive (false);
			Tutorial3Canvas.gameObject.SetActive (false);

			RankPageCanvas.gameObject.SetActive (false);
			TotalRankCanvas.gameObject.SetActive (false);
			EasyRankCanvas.gameObject.SetActive (false);
			NormalRankCanvas.gameObject.SetActive (false);
			HardRankCanvas.gameObject.SetActive (false);

			Vector3 position = new Vector3 (960, 540, 0);
			GameObject prefab = (GameObject)Resources.Load("DifficultyCanvas");
			Instantiate (prefab, position, transform.rotation);

		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TutorialCanvasOpen () {
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Tutorial/TutorialCanvas");
		Instantiate (prefab, position, transform.rotation);

		Destroy (DifficultyCanvas.gameObject);
		/*
		TutorialCanvas.gameObject.SetActive (true);
		Tutorial1Canvas.gameObject.SetActive (true);

		firstButton.interactable = false;
		secondButton.interactable = true;
		thirdButton.interactable = true;
		DifficultyCanvas.gameObject.SetActive (false);
		*/
	}

	public void PushFirst () {
		firstButton.interactable = false;
		secondButton.interactable = true;
		thirdButton.interactable = true;

		Tutorial1Canvas.gameObject.SetActive(true);
		Tutorial2Canvas.gameObject.SetActive(false);
		Tutorial3Canvas.gameObject.SetActive(false);
	}

	public void PushSecond () {
		firstButton.interactable = true;
		secondButton.interactable = false;
		thirdButton.interactable = true;

		Tutorial1Canvas.gameObject.SetActive(false);
		Tutorial2Canvas.gameObject.SetActive(true);
		Tutorial3Canvas.gameObject.SetActive(false);
	}

	public void PushThird () {
		firstButton.interactable = true;
		secondButton.interactable = true;
		thirdButton.interactable = false;

		Tutorial1Canvas.gameObject.SetActive(false);
		Tutorial2Canvas.gameObject.SetActive(false);
		Tutorial3Canvas.gameObject.SetActive(true);
	}

	public void CloseCanvas () {
		TutorialCanvas.gameObject.SetActive (false);
		Tutorial1Canvas.gameObject.SetActive (false);
		Tutorial2Canvas.gameObject.SetActive (false);
		Tutorial3Canvas.gameObject.SetActive (false);
		DifficultyCanvas.gameObject.SetActive (true);
	}

	public void RankingCanvasOpen () {
		DifficultyCanvas.gameObject.SetActive (false);
		RankPageCanvas.gameObject.SetActive (true);

		TotalRankCanvas.gameObject.SetActive (true);
	}

	public void RankingCanvasClose () {
		RankPageCanvas.gameObject.SetActive (false);
		TotalRankCanvas.gameObject.SetActive (false);
		EasyRankCanvas.gameObject.SetActive (false);
		NormalRankCanvas.gameObject.SetActive (false);
		HardRankCanvas.gameObject.SetActive (false);

		DifficultyCanvas.gameObject.SetActive (true);
	}

	public void TotalRankButtonClick () {
//		RankPageChange.Instance.FadeCanvasTotalRank ();
	}
	public void EasyRankButtonClick () {
//		RankPageChange.Instance.FadeCanvasEasy ();
	}
	public void NormalRankButtonClick () {
//		RankPageChange.Instance.FadeCanvasNormal ();
	}
	public void HardRankButtonClick () {
//		RankPageChange.Instance.FadeCanvasHard ();
	}
}
