﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DifficultyMainController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AllGameDefinition.gameMode = 1; // initialize

		DOTween.Init (false, true, LogBehaviour.Verbose);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load ("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);

		string appVersion = PlayerPrefs.GetString ("appVersion", "1.0.6");
		if (PlayerPrefs.GetInt ("verUpDialog106", 0) == 0) {
			GameObject VerUpCanvas = ResourceCache.Load ("VerUpCanvas");
			Instantiate (VerUpCanvas, Vector3.zero, Quaternion.identity);
		}
	}
}
