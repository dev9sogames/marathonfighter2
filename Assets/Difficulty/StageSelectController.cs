﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StageSelectController : MonoBehaviour {

	public Image NurikabeLock;
	public Image BlackHeroLock;
	public Image LastBossLock;

	public GameObject StageSelectCanvas;
	public GameObject[] buttonList;

	public Text totalPointText;

	// Use this for initialization
	void Start () {
		_setStartImage ();
		_setButtonText ();
		totalPointText.text = PlayerPrefs.GetInt ("totalPoint", 0).ToString ();
	}
	
	public void GoToCharacterSelect () {
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Select/CharaSelectCanvas");
		Instantiate (prefab, position, transform.rotation);

		Destroy (this.gameObject);
	}

	void _setButtonText () {
		int stageCount = 1;
		int courtCount = 1;
		foreach (GameObject button in buttonList) {
			string stageName = stageCount.ToString () + " - " + courtCount.ToString ();

			string key = stageCount.ToString() + "-" + courtCount.ToString() + "-highestPoint";
			int highestPoint = PlayerPrefs.GetInt (key, 0);

			button.transform.Find ("text").gameObject.GetComponent<Text> ().text = stageName;
			button.transform.Find ("textPoint").gameObject.GetComponent<Text> ().text = "BEST : " + highestPoint;

			courtCount ++;
			if (courtCount > 3) {
				stageCount++;
				courtCount = 1;
			}
		}
	}
	/**
	 * create star point image
	 * 1-1:-281,71
	 * 2-2:0,-20 (-91)
	 * 3-3:281:-111
	 * 4-1:-281,-202
	 * 
	 **/
	void _setStartImage () {
		GameObject star1 = ResourceCache.Load ("Select/1StarImage");
		GameObject star2 = ResourceCache.Load ("Select/2StarImage");
		GameObject star3 = ResourceCache.Load ("Select/3StarImage");
		GameObject key = ResourceCache.Load ("Select/keyLock");

		int posX = 0;
		Vector3 pos;
		string keyNameBefore = "1-1-starCount";

		for (int i = 1; i <= 3; i++) {
			if (i == 1 || i == 4) {
				posX = -281;
			} else if (i == 2) {
				posX = 0;
			} else if (i == 3) {
				posX = 281;
			}
			/*
			switch (i) {
			case 1:
			case 4:
				posX = -281;
				break;
			case 2:
				posX = 0;
				break;
			case 3:
				posX = 281;
				break;
			default:
				posX = -281;
				break;
			}
			*/
			for (int m = 1; m <= 4; m++) { // vertical
				string keyName = m.ToString() + "-" + i.ToString() + "-starCount";
				if (i == 1) {
					keyNameBefore = (m - 1).ToString () + "-3-starCount";
				} else {
					keyNameBefore = m.ToString () + "-" + (i - 1).ToString () + "-starCount";
				}
				if (i == 1 && m == 1) { // horizontal
					keyNameBefore = "1-1-starCount";
				}
//				Debug.Log (keyName);
//				Debug.Log (keyNameBefore);
				int bonusPointBefore = PlayerPrefs.GetInt (keyNameBefore, 0);
				int bonusPoint = PlayerPrefs.GetInt (keyName, 0);

				pos = new Vector3 (posX, 71 - (91 * (m -1)), 0);

				Vector3 keyPos;
				keyPos = new Vector3 (posX, 71 - (91 * (m -1) -20), 0);
				GameObject image;
				GameObject keyImage;

				if (bonusPointBefore == 0) {
					keyImage = Instantiate (key, pos, Quaternion.identity);
					keyImage.transform.SetParent (StageSelectCanvas.transform, false);
					keyImage.transform.localPosition = pos;
					keyImage.transform.localScale = new Vector3 (1, 1, 1);
				}
				switch (bonusPointBefore) {
				case 1:
					image = Instantiate (star1, pos, Quaternion.identity);
					break;
				case 2:
					image = Instantiate (star2, pos, Quaternion.identity);
					break;
				case 3:
					image = Instantiate (star3, pos, Quaternion.identity);
					break;
				default:
					image = null;
					break;
				}
				if (image != null) {
					image.transform.SetParent (StageSelectCanvas.transform, false);
					image.transform.localPosition = pos;
					image.transform.localScale = new Vector3 (1, 1, 1);
				}
			}
		}
	}
}
