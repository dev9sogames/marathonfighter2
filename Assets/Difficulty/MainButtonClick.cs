﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class MainButtonClick : MonoBehaviour {
	public Canvas MainCanvas;

	public Canvas OnlyOptionCanvas;
	public Canvas OnlyTutorialCanvas;

	public Canvas MarathonModeSelectCanvas;

	// Use this for initialization
	void Start () {
		int playerStatus = PlayerPrefs.GetInt ("playerStatus", 0);
		switch (playerStatus) {
		case 0:
			OnlyOptionCanvas.gameObject.SetActive (true);
			break;
		case 1:
			OnlyTutorialCanvas.gameObject.SetActive (true);
			break;
		default:
			break;
		}
	}
	
	public void EndlessClick () {
		AllGameDefinition.gameMode = 3;
		AllGameDefinition.groundDifficulty = 2;

		AllGameDefinition.StoryStageType = 1;
		AllGameDefinition.StoryStageCourt = 2;
//		SceneManager.LoadScene ("ground");
		SceneManager.LoadScene ("Loading");

		Destroy (MainCanvas.gameObject);
	}

	public void StoryClick () {
		int notShowMovie = PlayerPrefs.GetInt ("NotShowStoryMovie", 0);

		if (notShowMovie == 1) {
			Vector3 position = new Vector3 (960, 540, 0);
			GameObject prefab = (GameObject)Resources.Load ("Select/StageMultiSelectCanvas");
			Instantiate (prefab, position, transform.rotation);
		} else {
			Vector3 position = new Vector3 (960, 540, 0);
			GameObject prefab = (GameObject)Resources.Load ("Opening/OpeningCanvas");
			Instantiate (prefab, position, transform.rotation);
		}

		Destroy (MainCanvas.gameObject);
	}

	public void MarathonClick () {
		GameObject prefab = (GameObject)ResourceCache.Load ("MarathonModeCanvas");
		Instantiate (prefab, Vector3.zero, Quaternion.identity);
	}

	public void MarathonEasyClick () {
		AllGameDefinition.gameMode = 4;
		AllGameDefinition.groundDifficulty = 1;

		AllGameDefinition.StoryStageType = 1;
		AllGameDefinition.StoryStageCourt = 2;
		SceneManager.LoadScene ("Loading");
	}

	public void MarathonHardClick () {
		AllGameDefinition.gameMode = 4;
		AllGameDefinition.groundDifficulty = 2;

		AllGameDefinition.StoryStageType = 2;
		AllGameDefinition.StoryStageCourt = 2;
		SceneManager.LoadScene ("Loading");
	}

	public void MarathonBackClick () {
		Destroy (MarathonModeSelectCanvas.gameObject);
	}

	public void StatusClick () {
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Status/StatusCanvas");
		Instantiate (prefab, position, transform.rotation);

//		Destroy (MainCanvas.gameObject);
	}

	public void TrainingClick () {
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Training/TrainingCanvas");
		Instantiate (prefab, position, transform.rotation);

		Destroy (MainCanvas.gameObject);
	}

	public void RankingClick () {
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Ranking/RankingCanvas");
		Instantiate (prefab, position, transform.rotation);

//		Destroy (MainCanvas.gameObject);
	}

	public void OptionClick () {
		int playerStatus = PlayerPrefs.GetInt ("playerStatus", 0);
		if (playerStatus == 0) {
			Vector3 position = new Vector3 (960, 540, 0);
			GameObject prefab = (GameObject)Resources.Load ("DialogCanvasForTitle");
			Instantiate (prefab, position, transform.rotation);
		} else {
			Vector3 position = new Vector3 (225, 126.5f, 0);
			GameObject prefab = (GameObject)Resources.Load ("DialogCanvas");
			GameObject dialogCanvas = Instantiate (prefab, position, transform.rotation);
		}
	}

	public void GachaClick () {
		GameObject Gacha = ResourceCache.Load ("gachaNurikabe");
		Instantiate (Gacha, new Vector3 (-0.5f, 0, 0), Quaternion.identity);

		Destroy (MainCanvas.gameObject);
	}

	public void TutorialClick () {
		SceneManager.LoadScene ("Tutorial");
	}

	public void TechniqueClick () {
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Tutorial/TutorialCanvas");
		Instantiate (prefab, position, transform.rotation);

//		Destroy (MainCanvas.gameObject);
	}

	public void ShowAd() {
		if (Advertisement.IsReady()) {
			Advertisement.Show();
		}
	}

	public void ShowMovie () {
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("Opening/OpeningCanvas");
		GameObject canvas = Instantiate (prefab, position, transform.rotation);
		canvas.GetComponent<OpeningCanvasController> ().SetDifficultyCanvasFlag (true);
	}

	public void OnlinetBattle () {
		SceneManager.LoadScene ("OnlineBattle");
	}

	public void AppReview () {
		string url;
		if (Application.platform == RuntimePlatform.Android) {
			url = "market://details?id=com.sorida.marathon";
		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
			SystemLanguage lang = Application.systemLanguage;
			if (lang == SystemLanguage.Japanese) {
				url = "https://itunes.apple.com/jp/app/marathon-fighter2/id1221505473?mt=8";
			} else {
				url = "https://itunes.apple.com/us/app/marathon-fighter2/id1221505473?mt=8";
			}
//			url = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/\rviewContentsUserReviews?type=Purple+Software&id=1221505473";
		} else {
			url = "https://itunes.apple.com/jp/app/marathon-fighter2/id1221505473?mt=8";
		}

		Application.OpenURL(url);
	}
}
