﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerUpInfo : MonoBehaviour {
	public Canvas VerUpCanvas;
	public Text VerUpInfoText;

	SystemLanguage lang;

	// Use this for initialization
	void Start () {
		lang = Application.systemLanguage;

		Debug.Log (Application.version);
		if (Application.platform == RuntimePlatform.Android) {
			if (Application.version == "1.0.6") {
				_setVerUpInfoText ();
			}
		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
			if (Application.version == "1.0.6") {
				_setVerUpInfoText ();
			}
		} else {
			_setVerUpInfoText ();
		}
	}

	void _setVerUpInfoText () {
		Debug.Log ("Verup info");
		if (lang == SystemLanguage.Japanese) {
//			VerUpInfoText.text = "1. オンライン対戦の練習用モードを追加\n2. オンライン対戦の必殺技を追加\n3. PUSH通知に対応\n4.十字キーなどのボタンサイズをピンチ操作で変更可能に";
			VerUpInfoText.text = "1. 大人気のマラソンモードに新ステージ追加！\n2. オンライン対戦のHPを全員同じにしました\n3. ジャンプボタン・アクションボタンをピンチ操作で\n拡大・縮小できるようにしました\n4.うさぎを取った時のポイント増加率をあげました\n5.ジャンプ回数を表示するようにしました";
		} else if (lang == SystemLanguage.ChineseSimplified) {
//			VerUpInfoText.text = "1.添加的网络对战练习模式\\n2.加入在线竞争的致命打击\\n3.对应于PUSH通知\\n4.能够改变按钮的大小，如交键捏操作";
			VerUpInfoText.text = "1. New stage added to popular marathon mode！\n2. We made all HP online match-ups same\n3. Jump button / The action button can be enlarged / reduced by pinching operation\n4.I raised the point increase rate when I took a rabbit\n5.I showed the number of jumps";
		} else {
//			VerUpInfoText.text = "1. Add practice mode for online competition\\n2. Addition of Special Mortal Technique of Online Battle\\n3. Corresponds to PUSH notification\\n4. The size of the cross key etc. can be changed by pinch operation";
			VerUpInfoText.text = "1. 新阶段加入到流行的马拉松模式！\n2. 惠普的在线竞争是在所有相同\n3. 跳跃按钮操作按钮现在可以放大或按捏动作减少\n4.我们获得了一些点增加的速度，当你把兔子\n5.现在显示的时间跳跃的次数";
		}
	}

	public void CloseVerupDialog () {
		PlayerPrefs.SetInt ("verUpDialog106", 1);
		PlayerPrefs.Save ();

		Destroy (VerUpCanvas.gameObject);
	}
}
