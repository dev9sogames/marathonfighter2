﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionDialog : MonoBehaviour {
	public Canvas DialogCanvas;
	public Canvas DialogCanvasForTitle;
	public Canvas MainDifficultyCanvas;
	public Canvas LoadingCanvas;
	public Canvas ErrorDialog;

	string playerName;
	public InputField inputField;
	public Text titleText;
//	public Text nameText;
//	public InputField field;
	public Text errorText;

	public Button nextButton;
	public bool isOptionDialog;

	bool isDifficultyCanvas;
	bool isChangeName;

	const string NAME_KEY = "nameKey";

	int rankDataStatus = 0; //0:not, 1:success, 2:error

	// Use this for initialization
	void Start () {
//		PlayerPrefs.DeleteKey (NAME_KEY);
		if (!isOptionDialog && nextButton != null) {
			nextButton.interactable = false;
		}
//		Debug.Log ("OptionDialogClass Start");
		string name = PlayerPrefs.GetString (NAME_KEY, "");
		inputField.text = name;

		if (!isOptionDialog) {
			if (name.Length > 1) {
				titleText.text = "Let's proceed to the next step.";
			} else {
				titleText.text = "Please register your name.";
			}
		} else {
			titleText.text = "";
		}
	}
	
	public void CloseDialog () {
		Destroy (DialogCanvas.gameObject);
	}

	public void SaveText () {
		playerName = inputField.text;
		if (playerName == null || playerName == "") {
			return;
		}

		isChangeName = false;

		LoadingCanvas.gameObject.SetActive (true);
		string oldName = PlayerPrefs.GetString (NAME_KEY, null);
		string pw = PlayerPrefs.GetString ("passwordKey", null);
		inputField.text = playerName;
		inputField.text = "";
		if (playerName == null) {
			playerName = "Guest";
		}
		StartCoroutine (WaitChangeName (oldName, playerName));
		UserAuth.Instance.changeName (playerName, pw);
	}

	public void SaveTextForTitle () {
		playerName = inputField.text;
		Debug.Log ("name : " + playerName);
		if (playerName == null || playerName == "") {
			return;
		}
		isChangeName = false;

		LoadingCanvas.gameObject.SetActive (true);
		string oldName = PlayerPrefs.GetString (NAME_KEY, null);
		string pw = PlayerPrefs.GetString ("passwordKey", null);
//		playerName = inputField.text;
		inputField.text = playerName;
//		inputField.text = "";
		if (playerName == null) {
			playerName = "Guest";
		}
		StartCoroutine (WaitChangeNameForTitle (oldName, playerName));
		UserAuth.Instance.changeNameForTitle (playerName, pw);

		nextButton.interactable = true;
	}

	IEnumerator WaitChangeNameForTitle (string oldName, string playerName) {
		while (!isChangeName) {
			yield return new WaitForSeconds (1);
			int value = UserAuth.Instance.getChangePwStatus ();
			Debug.Log (value);
			if (value == 1) {
				PlayerPrefs.SetString (NAME_KEY, playerName);
				int playerStatus = PlayerPrefs.GetInt ("playerStatus", 0);
				Debug.Log (playerStatus);
				if (playerStatus < 2) {
					PlayerPrefs.SetInt ("playerStatus", 1);
				}
				PlayerPrefs.Save ();

				titleText.text = "Let's proceed to the next step.";

				LoadingCanvas.gameObject.SetActive (false);
				isChangeName = true;
			} else if (value == 2) {
				LoadingCanvas.gameObject.SetActive (false);
				//duplicate error
				//				errorText.gameObject.SetActive (true);
				ErrorDialog.gameObject.SetActive (true);
				errorText.text = "Duplicate Error.";
				isChangeName = true;
				break;
			} else if (value == 3) {
				LoadingCanvas.gameObject.SetActive (false);
				//				errorText.gameObject.SetActive (true);
				ErrorDialog.gameObject.SetActive (true);
				errorText.text = "Server Error.";
				isChangeName = true;
				break;
			}
		}
	}
	IEnumerator WaitChangeName (string oldName, string playerName) {
		while (!isChangeName) {
			yield return new WaitForSeconds (1);
			int value = UserAuth.Instance.getChangePwStatus ();
			Debug.Log (value);
			if (value == 1) {
				if (oldName != null) {
					RankingManager.Instance.setRankingName (oldName, playerName);
					StartCoroutine (rankingNameUpdate (playerName));
				}

				isChangeName = true;
			} else if (value == 2) {
				LoadingCanvas.gameObject.SetActive (false);
				//duplicate error
//				errorText.gameObject.SetActive (true);
				ErrorDialog.gameObject.SetActive (true);
				errorText.text = "Duplicate Error.";
				isChangeName = true;
				break;
			} else if (value == 3) {
				LoadingCanvas.gameObject.SetActive (false);
//				errorText.gameObject.SetActive (true);
				ErrorDialog.gameObject.SetActive (true);
				errorText.text = "Server Error.";
				isChangeName = true;
				break;
			}
		}
	}

	IEnumerator rankingNameUpdate (string playerName) {
		rankDataStatus = 0;
		while (rankDataStatus == 0) {
			yield return new WaitForSeconds (1);
			if (RankingManager.Instance.HasRankData () == 1) {
				rankDataStatus = 1;
				PlayerPrefs.SetString (NAME_KEY, playerName);

				int playerStatus = PlayerPrefs.GetInt ("playerStatus", 0);
				Debug.Log (playerStatus);
				if (playerStatus < 2) {
					PlayerPrefs.SetInt ("playerStatus", 1);
				}
				PlayerPrefs.Save ();

				LoadingCanvas.gameObject.SetActive (false);
				break;
			} else if (RankingManager.Instance.HasRankData () == 2) {
				rankDataStatus = 2;
				LoadingCanvas.gameObject.SetActive (false);
				ErrorDialog.gameObject.SetActive (true);
				errorText.text = "Server Error.";
				break;
			}
		}
	}

	public void GoTitle () {
		SceneManager.LoadScene ("MarathonTitle");
	}

	public void OpenButtonPositionCanvas () {
		Vector3 position = new Vector3 (285.5f, 160.5f, 0);
		GameObject prefab = (GameObject)Resources.Load("ButtonPositionCanvas");
		Instantiate (prefab, position, transform.rotation);
	}

	public void OpenVolumeAdjustCanvasForTitle () {
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("VolumeAdjustForTitle");
		Instantiate (prefab, position, transform.rotation);
		Destroy (DialogCanvasForTitle.gameObject);
	}

	public void CloseErrorDialogCanvas () {
		ErrorDialog.gameObject.SetActive (false);
	}
}
