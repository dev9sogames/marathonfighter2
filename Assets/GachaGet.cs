﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GachaGet : MonoBehaviour {

	public Sprite[] imageList;

	bool isFinish;

	public void ShowResultImage () {
		Debug.Log ("gacha result2" + isFinish);
		if (!isFinish) {
			SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
			renderer.sprite = RandomUtils.Random (imageList);
		}
	}

	public void RemoveResultImage () {
		SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
		renderer.sprite = null;
	}
}
