﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonAudioSetVolume : MonoBehaviour {
	public 	UnityEngine.Audio.AudioMixer mixer;

	// Use this for initialization
	void Start () {
		float bgmVolume = PlayerPrefs.GetFloat ("bgmVolume", 0);
		float seVolume = PlayerPrefs.GetFloat ("seVolume", 0);

		mixer.SetFloat ("BgmVolume", Mathf.Lerp(-80, 10, bgmVolume));
		mixer.SetFloat ("SeVolume", Mathf.Lerp(-80, 10, seVolume));
	}
}
