﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GuiAnimation : MonoBehaviour {
	Vector3 defaultPos;
	public float pos;

	public int moveType; // 1:moveX, 2:moveY

	public void MoveStart () {
		defaultPos = this.gameObject.GetComponent<RectTransform> ().localPosition;

//		Debug.Log(moveType);
//		Debug.Log(pos);
		if (moveType == 1) {
			this.gameObject.GetComponent<RectTransform> ().DOLocalMoveX (pos, 2f);
		} else if (moveType == 2) {
			this.gameObject.GetComponent<RectTransform> ().DOLocalMoveY (pos, 2f);
		}
	}

	public void SetDefalutPosition () {
		Debug.Log ("SetDefalutPosition");
		this.gameObject.GetComponent<RectTransform> ().localPosition = defaultPos;
	}

}
