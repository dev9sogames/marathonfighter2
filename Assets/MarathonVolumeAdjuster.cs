﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MarathonVolumeAdjuster : MonoBehaviour {
	public Slider BgmSlider;
	public Slider SeSlider;
	[SerializeField]
	UnityEngine.Audio.AudioMixer mixer;

	public AudioSource source;

	public Canvas DialogCanvas;
	public Canvas VolumeAdjustCanvasForTitle;

	// Use this for initialization
	void Start () {
		float bgmVolume = PlayerPrefs.GetFloat ("bgmVolume", 0);
		float seVolume = PlayerPrefs.GetFloat ("seVolume", 0);

		BgmSlider.value = bgmVolume;
		SeSlider.value = seVolume;
	}

	public float BgmVolume {
		set{ mixer.SetFloat("BgmVolume", Mathf.Lerp(-80, 10, BgmSlider.value)); }
	}

	public float SeVolume {
		set{ mixer.SetFloat("SeVolume", Mathf.Lerp(-80, 10, SeSlider.value)); }
	}

	public void GoTitle () {
		_setVolume ();
		SceneManager.LoadScene ("MarathonTitle");
	}

	public void CloseDialog () {
		_setVolume ();
		Destroy (DialogCanvas.gameObject);
	}

	void _setVolume () {
		PlayerPrefs.SetFloat ("bgmVolume", BgmSlider.value);
		PlayerPrefs.SetFloat ("seVolume", SeSlider.value);
		PlayerPrefs.Save ();
	}

	public void SampleListen () {
		source.Play();
	}
	public void OpenButtonPositionCanvas () {
		_setVolume ();
		Vector3 position = new Vector3 (285.5f, 160.5f, 0);
		GameObject prefab = (GameObject)Resources.Load("ButtonPositionCanvasForTitle");
		Instantiate (prefab, position, transform.rotation);
		Destroy (VolumeAdjustCanvasForTitle.gameObject);
	}
}
