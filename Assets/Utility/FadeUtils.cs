﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeUtils : MonoBehaviour {
	private GameObject preSceneObj;
	private GameObject nextSceneObj;

	public void SceneNext( GameObject preObj, GameObject nextObj ){

		preSceneObj = preObj;
		nextSceneObj = nextObj;

		nextObj.SetActive (true);
		nextObj.GetComponent<CanvasGroup>().alpha = 0.0f;

		iTween.ValueTo(gameObject,
			iTween.Hash(
				"from",1.0f,
				"to",0.0f,
				"time",0.5f,
				"easetype","easeOutCubic",
				"onUpdate","FadeUpdate",
				"oncomplete", "FadeComplete"
			)
		);

	}

	private void FadeUpdate(float fade){
		preSceneObj.GetComponent<CanvasGroup>().alpha = fade;
		nextSceneObj.GetComponent<CanvasGroup>().alpha = 1.0f - fade;
	}

	private void FadeComplete(){
		preSceneObj.SetActive (false);
		preSceneObj.GetComponent<CanvasGroup>().alpha = 1.0f;
	}
}