﻿using UnityEngine;
using System.Collections;

public class SettingDataManager : SingletonMonoBehaviour<SettingDataManager>{
	const string NAME_KEY = "nameKey";

	public void Awake (){
		if (this != Instance) {
			Destroy (this);
			return;
		}
		DontDestroyOnLoad (this.gameObject);
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public string LoadUserName () {
		return PlayerPrefs.GetString("NAME_KEY", "guest");
	}

	public void SaveUserName (string userName) {
		PlayerPrefs.SetString ("NAME_KEY", userName);
	}
}
