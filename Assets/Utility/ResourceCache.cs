﻿using UnityEngine;
using System.Collections.Generic;

public static class ResourceCache
{
	private static readonly Dictionary<string, GameObject> mList = new Dictionary<string, GameObject>();

	public static GameObject Load( string path )
	{
		if ( !mList.ContainsKey( path ) )
		{
			mList[ path ] = Resources.Load<GameObject>( path );
		}
		return mList[ path ];
	}

	public static void Clear()
	{
		mList.Clear();
	}
}