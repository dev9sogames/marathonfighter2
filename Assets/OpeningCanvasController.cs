﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpeningCanvasController : MonoBehaviour {
	public GameObject heroLeft;
	public GameObject heroRight;
	public GameObject nurikabe;
	public GameObject princess;

	public Canvas StageSelectCanvas;

	public Button SkipButton;
	public Button NotShowButton;

	bool isRunAway;
	bool isDifficultyCanvas;

	// Use this for initialization
	void Start () {
		if (isDifficultyCanvas) {
			SkipButton.gameObject.SetActive (false);
			NotShowButton.gameObject.SetActive (false);
		}
		StartCoroutine (princessForward ());
		StartCoroutine(nurikabeStartCount());
		heroLeft.GetComponent<OpeningCharacter> ().startHero ();
	}

	public void HeroSuperRunStart () {
		heroRight.GetComponent<OpeningCharacter> ().HeroSuperRunStart ();
	}
	public void GoToLeft () {
		heroRight.GetComponent<OpeningCharacter> ().startHero ();
	}

	public void MoveStorySelect () {
//		StageSelectCanvas.gameObject.SetActive (true);
		if (!isDifficultyCanvas) {
			Vector3 position = new Vector3 (960, 540, 0);
			GameObject prefab = (GameObject)Resources.Load ("Select/StageMultiSelectCanvas");
			Instantiate (prefab, position, transform.rotation);
		}

		Destroy (this.gameObject);
	}
	IEnumerator princessForward () {
		yield return new WaitForSeconds (30);
		princess.transform.SetAsLastSibling ();
	}
	IEnumerator nurikabeStartCount () {
		yield return new WaitForSeconds (34);
		nurikabe.GetComponent<OpeningCharacter> ().GoNurikabe ();
	}

	public void SetDifficultyCanvasFlag (bool flag) {
		isDifficultyCanvas = flag;
	}
}
