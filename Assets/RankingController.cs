﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingController : MonoBehaviour {
	public Image rightSetButton;
	public Image leftSetButton;

	public Text rankingText;

	public GameObject storyRankingCanvas;
	public GameObject endlessRankingCanvas;
	public GameObject marathonRankingCanvas;
	public GameObject marathonHardRankingCanvas;

	public bool isButton;

	// Use this for initialization
	void Start () {
		if (isButton) {
			return;
		}

		storyRankingCanvas.SetActive (true);
		endlessRankingCanvas.SetActive (false);
		marathonRankingCanvas.SetActive (false);
		marathonHardRankingCanvas.SetActive (false);
		rankingText.text = "STORY MODE";

		rightSetButton.GetComponent<Button> ().interactable = true;
		leftSetButton.GetComponent<Button> ().interactable = false;
	}

//	IEnumerator DefaultCanvasEnable () {
//		yield return new WaitForSeconds (0.5f);
//		storyRankingCanvas.SetActive (true);
//	}
	public void CloseRankingCanvas () {
//		Vector3 position = new Vector3 (960, 540, 0);
//		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
//		Instantiate (prefab, position, transform.rotation);
//
		Destroy (this.gameObject);
	}

	public void GoRightCanvas () {
		leftSetButton.GetComponent<Button> ().interactable = true;
		if (storyRankingCanvas.activeSelf) {
			storyRankingCanvas.SetActive (false);
			endlessRankingCanvas.SetActive (true);
			marathonRankingCanvas.SetActive (false);
			marathonHardRankingCanvas.SetActive (false);
			rankingText.text = "ENDLESS MODE";
		} else if (endlessRankingCanvas.activeSelf) {
			storyRankingCanvas.SetActive (false);
			endlessRankingCanvas.SetActive (false);
			marathonRankingCanvas.SetActive (true);
			marathonHardRankingCanvas.SetActive (false);
			rankingText.text = "MARATHON EASY MODE";
		} else if (marathonRankingCanvas.activeSelf) {
			storyRankingCanvas.SetActive (false);
			endlessRankingCanvas.SetActive (false);
			marathonRankingCanvas.SetActive (false);
			marathonHardRankingCanvas.SetActive (true);
			rankingText.text = "MARATHON HARD MODE";
			rightSetButton.GetComponent<Button> ().interactable = false;
		}
	}

	public void GoLeftCanvas () {
		rightSetButton.GetComponent<Button> ().interactable = true;
		if (marathonHardRankingCanvas.activeSelf) {
			storyRankingCanvas.SetActive (false);
			endlessRankingCanvas.SetActive (false);
			marathonRankingCanvas.SetActive (true);
			marathonHardRankingCanvas.SetActive (false);
			rankingText.text = "MARATHON EASY MODE";
		} else if (marathonRankingCanvas.activeSelf) {
			storyRankingCanvas.SetActive (false);
			endlessRankingCanvas.SetActive (true);
			marathonRankingCanvas.SetActive (false);
			marathonHardRankingCanvas.SetActive (false);
			rankingText.text = "ENDLESS MODE";
		} else if (endlessRankingCanvas.activeSelf) {
			storyRankingCanvas.SetActive (true);
			endlessRankingCanvas.SetActive (false);
			marathonRankingCanvas.SetActive (false);
			marathonHardRankingCanvas.SetActive (false);
			rankingText.text = "STORY MODE";
			leftSetButton.GetComponent<Button> ().interactable = false;
		}
	}
}
