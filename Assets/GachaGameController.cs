﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GachaGameController : MonoBehaviour {
	public GameObject gachaResult;
	public GameObject gachaTrigger;

	Coroutine showResultCrt;

	void Start () {
		string name = PlayerPrefs.GetString ("nameKey", null);
		if (name.Length < 1) {
			Vector3 position = new Vector3 (960, 540, 0);
			GameObject prefab = (GameObject)Resources.Load("DialogCanvasForTitle");
			Instantiate (prefab, position, transform.rotation);

			return;
		}
	}

	public void InitializeGacha () {
		gachaResult.GetComponent<GachaGet> ().RemoveResultImage ();
		gachaTrigger.GetComponent<GachaTrigger> ().InitializeGachaFlag ();

		StopCoroutine (showResultCrt);

	}

	void OnTriggerEnter2D (Collider2D other) {
		try {
		other.GetComponent<GachaEgg> ().Explosion ();
		showResultCrt = StartCoroutine (ShowResult ());
		} catch (System.Exception e) {
		}
	}

	IEnumerator ShowResult () {
		yield return new WaitForSeconds (2);
		GameObject result = GameObject.FindGameObjectWithTag ("GachaResult");
		result.GetComponent<GachaGet> ().ShowResultImage ();
//		gachaResult.GetComponent<GachaGet> ().ShowResultImage ();
	}
}
