﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterSelectController : MonoBehaviour {
	public Image MiniNurikabeLockImage;
	public Image NurikabeLockImage;
	public Image BlackHeroLockImage;

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetInt (Constants.MINI_NURIKABE_LOCK_FLAG, 0) == 1) {
			MiniNurikabeLockImage.gameObject.SetActive (false);
		} else {
			MiniNurikabeLockImage.gameObject.SetActive (true);
		}

		if (PlayerPrefs.GetInt (Constants.NURIKABE_LOCK_FLAG, 0) == 1) {
			NurikabeLockImage.gameObject.SetActive (false);
		} else {
			NurikabeLockImage.gameObject.SetActive (true);
		}

		if (PlayerPrefs.GetInt (Constants.BLACK_HERO_LOCK_FLAG, 0) == 1) {
			BlackHeroLockImage.gameObject.SetActive (false);
		} else {
			BlackHeroLockImage.gameObject.SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
