public static class Constants {
	public const string TOTAL_DISTANCE = "totalDistance";

	public const string PLAYER_LEVEL = "playerLevel";
	public const string EXP_NEXT_TO = "ExpNextTo";

	public const string YOGA_FIRE_LOCK_FLAG = "yogaFireLocked";
	public const string KICK_LOCK_FLAG = "kickLocked";
	public const string UPPER_LOCK_FLAG = "upperLocked";
	public const string SCREW_LOCK_FLAG = "screwLocked";

	public const string MINI_NURIKABE_LOCK_FLAG = "miniNurikabeLocked";
	public const string NURIKABE_LOCK_FLAG = "nurikabeLocked";
	public const string BLACK_HERO_LOCK_FLAG = "blackHeroLocked";

	public const int fieldSpeed = 8;

	public const int basePoint = 70;

	public const int moveSpeed = 6;
}