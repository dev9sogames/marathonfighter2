﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPlayerJumpCount : MonoBehaviour {
	MarathonPlayer targetPlayer;

	// Use this for initialization
	void Start () {
		targetPlayer = GameObject.FindGameObjectWithTag ("Player").GetComponent<MarathonPlayer> ();
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player") {
//			Debug.Log ("reset jump count");
			targetPlayer.ResetJumpCount ();
		}
	}
}
