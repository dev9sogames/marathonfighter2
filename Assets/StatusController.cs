﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusController : MonoBehaviour {
	public Text LevelText;
	public Text NextText;
	public Text ExpText;
	public Text HpText;
	public Text ActionPointText;
	public Text AttackText;
	public Text DefenseText;

	public bool isButton;

	public GameObject StatusCanvas;

	// Use this for initialization
	void Start () {
		if (isButton) {
			return;
		}

		LevelText.text = PlayerPrefs.GetInt (Constants.PLAYER_LEVEL, 1).ToString ();
		NextText.text = PlayerPrefs.GetInt (Constants.EXP_NEXT_TO, 0).ToString ();
		ExpText.text = LoadHighScore (Constants.TOTAL_DISTANCE).ToString ();
		int level = PlayerPrefs.GetInt (Constants.PLAYER_LEVEL, 1);
		int lifePoint = 10000;

		int lifeBunus = (int)System.Math.Truncate ((double)level / 10);
		int lifeBunus2 = level % 10;
		lifePoint += ((lifeBunus * 3000) + lifeBunus2 * 300);

//		lifePoint += (level * 100);
		HpText.text = lifePoint.ToString ();
		ActionPointText.text = "300";
		AttackText.text = (3333 + (level * 3)).ToString();
		DefenseText.text = (2589 + (level * 5)).ToString();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	int LoadHighScore (string key) {
		// default 0
		return PlayerPrefs.GetInt (key, 0);
	}

	public void BackFromStatusCanvas () {
		Destroy (StatusCanvas.gameObject);

//		Vector3 position = new Vector3 (960, 540, 0);
//		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
//		Instantiate (prefab, position, transform.rotation);
	}
}
