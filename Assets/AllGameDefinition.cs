﻿public class AllGameDefinition {

	//1: easy, 2:normal
	public static int groundDifficulty = 1;

	//1:easy ~ 10:hard
	public static int currentDifficulty = 1;
	public static int fieldSpeed;

	//1: normalMode, 2: bossMode, 3:endlessMode, 4:marathonMode
	public static int gameMode;

	public static int currentPoint;
	public static int actionPoint;
	public static int totalDistance;

	public static int bossCount;

	public static int level;
	public static int expNextTo;

	public static int yogaFireLocked; //level 5~10
	public static int kickLocked; //level 11~20
	public static int upperLocked; //level 21~30
	public static int screwLocked; //level 40~50

	public static bool isHardMode;

	public static int StoryStageType;
	/*
	 * 1:mini
	 * 2:nurikabe
	 * 3:black
	 * 4:last boss
	*/

	public static int StoryStageCourt;
	/**
	 * 1-1,1-2 etc
	 * 
	 **/

	public static int CharacterType;
	/*
	 * 1:normal
	 * 2:mini
	 * 3:nurikabe
	 * 4:black
	*/

}