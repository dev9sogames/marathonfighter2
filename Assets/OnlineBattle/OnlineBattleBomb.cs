﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattleBomb : Photon.MonoBehaviour {
	public bool isAfter;

	bool canHeading;

//	Rigidbody2D rgbd;

	bool isWithPlayer;
	GameObject Player;

	// Use this for initialization
	void Start () {
		if (!isAfter) {
//			rgbd = GetComponent<Rigidbody2D> ();
			StartCoroutine (BombFireStart ());
			StartCoroutine (BombCanHeading ());
		}
	}

	void Update () {
		if (transform.position.x < -20 || transform.position.x > 20) {
			this.gameObject.SetActive (false);
			return;
		}

		if (transform.position.y < -10) {
			this.gameObject.SetActive (false);
			return;
		}

		if (!isWithPlayer) {
			return;
		}

		// bomb with player
//		if (!isAfter) {
//			transform.position = Player.transform.position;
//		}
	}

	IEnumerator BombCanHeading () {
		yield return new WaitForSeconds (0.5f);
		canHeading = true;
	}

	IEnumerator BombFireStart () {
		// for debug
		yield return new WaitForSeconds (UnityEngine.Random.Range (2, 10));
		_setBombFire ();
		isWithPlayer = false;
		isAfter = true;
		this.gameObject.SetActive (false);
	}

	void _setBombFire () {
		PhotonNetwork.Instantiate ("OnlineBattle/OnlineBombAfter", transform.position, Quaternion.identity, 0);
	}

	public void ThrowToEnemy (int playerPos) {
		Debug.Log ("bomb throw to enemy");
//		isWithPlayer = false;
		if (GetComponent<Rigidbody2D> () != null) {
			Rigidbody2D rgbd = GetComponent<Rigidbody2D> ();
			rgbd.isKinematic = false;
			transform.parent = null;
			if (playerPos == 1) { // player left. throw right
				GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 500);
				GetComponent<Rigidbody2D> ().AddForce (Vector2.right * 300);
			} else {
				GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 500);
				GetComponent<Rigidbody2D> ().AddForce (Vector2.left * 300);
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (!isWithPlayer && other.tag == "BombThrow") {
			isWithPlayer = true;
			if (GetComponent<Rigidbody2D> () != null) {
				other.transform.root.gameObject.GetComponent<OnlineBattlePlayerController> ().AddBombList (this.gameObject);
				GetComponent<Rigidbody2D> ().isKinematic = true;
				transform.parent = other.transform.root.transform;
			}
//			Player = other.transform.gameObject;
//			other.GetComponent<OnlineBattleBomb> ().GoWithPlayer (transform.root.gameObject);
		} else if (canHeading && (other.tag == "PlayerKick" || other.tag == "EnemyPlayerKick")) {
			if (isAfter) {
				return;
			}
//			Debug.Log ("other.tag : " + other.tag);
			if (other.transform.root.GetComponent<OnlineBattlePlayerPosition> ().GetPlayerPosition () == 1) {
				GetComponent<Rigidbody2D> ().AddForce (Vector2.right * 500);
				GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 500);
			} else {
				GetComponent<Rigidbody2D> ().AddForce (Vector2.left * 500);
				GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 500);
			}
		} else if (canHeading && other.tag == "BombHeading") {
			if (isAfter) {
				return;
			}
			GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 500);
		}
	}
}
