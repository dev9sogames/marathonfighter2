﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattleSingleEnemyPlayer : MonoBehaviour {
	OnlineBattlePlayerController controller;
	OnlineBattlePlayerCommand command;
	OnlineBattlePlayerPosition position;

	bool battleStart;
	bool isDead;

	bool isHorizontalButtonPush;
	enum HorizontalDirection {
		none,
		left,
		right
	};
	HorizontalDirection hDire;

	enum VerticalDirection {
		none,
		up,
		down
	}
	VerticalDirection vDire;

	int difficulty;
	int attackPattern; // 1:normal,2:hard,3:hell

	// Use this for initialization
	void Start () {
		controller = GetComponent<OnlineBattlePlayerController> ();
		command = GetComponent<OnlineBattlePlayerCommand> ();
		position = GetComponent<OnlineBattlePlayerPosition> ();
	}
	
	public void setBattleStart (bool flag, int singleModeDifficulty) {
		battleStart = flag;
		difficulty = singleModeDifficulty;
		attackPattern = singleModeDifficulty;

//		transform.Find ("singleEnemyHitTrigger").GetComponent<OnlineBattleSingleEnemyHitTrigger> ().SetDifficulty (singleModeDifficulty);

		if (battleStart) {
			StartCoroutine (EnemyMove ());
			StartCoroutine (EnemyJump ());
			StartCoroutine (EnemyAttack ());
			StartCoroutine (EnemyAttackPattern ());
		}
	}

	IEnumerator EnemyMove () {
		float start = 0.1f;
		float end = 0.9f;

		while (!isDead) {
			switch (difficulty) {
			case 0:
				start = 0.1f;
				end = 0.9f;
				break;
			case 1:
				start = 0.1f;
				end = 0.6f;
				break;
			case 2:
				start = 0.1f;
				end = 0.5f;
				break;
			default:
				start = 0.1f;
				end = 0.9f;
				break;
			}

			while (!isDead) {
				yield return new WaitForSeconds (UnityEngine.Random.Range (start, end));
				_move ();
			}
		}
	}

	IEnumerator EnemyJump () {
		float start = 1f;
		float end = 3f;
		while (!isDead) {
			switch (difficulty) {
			case 0:
				start = 1f;
				end = 3f;
				break;
			case 1:
				start = 1f;
				end = 2f;
				break;
			case 2:
				start = 0.1f;
				end = 1.5f;
				break;
			default:
				start = 1f;
				end = 3f;
				break;
			}
			while (!isDead) {
				yield return new WaitForSeconds (UnityEngine.Random.Range (start, end));
				controller.JumpPlayerDown ();
				StartCoroutine (JumpUp ());
			}
		}
	}

	IEnumerator JumpUp () {
		yield return new WaitForSeconds (UnityEngine.Random.Range(0.2f, 1f));
		controller.JumpPlayerUp ();
	}

	IEnumerator EnemyAttack () {
		float start = 1.0f;
		float end = UnityEngine.Random.Range(1.1f, 2.5f);
		switch (difficulty) {
		case 0:
			start = 1.0f;
			end = UnityEngine.Random.Range(1.1f, 2.5f);
			break;
		case 1:
			start = 0.5f;
			end = UnityEngine.Random.Range(0.6f, 1.5f);
			break;
		case 2:
			start = 0.3f;
			end = UnityEngine.Random.Range(0.6f, 1.1f);
			break;
		default:
			start = 1.0f;
			end = UnityEngine.Random.Range(1.1f, 2.5f);
			break;
		}
		while (!isDead) {
			yield return new WaitForSeconds (UnityEngine.Random.Range (start, end));
			_attack ();
		}
	}

	IEnumerator EnemyAttackPattern () {
		float start = 1.0f;
		float end = UnityEngine.Random.Range(3f, 5f);
		while (!isDead) {
			yield return new WaitForSeconds (UnityEngine.Random.Range (start, end));
			int num = UnityEngine.Random.Range (1, 10);
			switch (difficulty) {
			case 0:
				switch (num) {
				case 1:
				case 2:
				case 3:
				case 4:
					attackPattern = 1;
					break;
				case 5:
				case 6:
				case 7:
					attackPattern = 2;
					break;
				case 8:
				case 9:
					attackPattern = 3;
					break;
				}
				break;
			case 1:
				switch (num) {
				case 1:
				case 2:
					attackPattern = 1;
					break;
				case 3:
				case 4:
				case 5:
					attackPattern = 2;
					break;
				case 6:
				case 7:
				case 8:
				case 9:
					attackPattern = 3;
					break;
				}
				break;
			case 2:
				switch (num) {
				case 1:
				case 2:
				case 3:
					attackPattern = 2;
					break;
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
					attackPattern = 3;
					break;
				}
				break;
			default:
				break;
			}
		}
	}

	void _attack () {
		if (isDead) {
			return;
		}

		if (attackPattern == 1) {
			_normalAttack ();
		} else if (attackPattern == 2) {
			_hardAttack ();
		} else {
			_hellAttack ();
		}
	}
	void _enemyNormalAttack () {
		command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.none;
	}
	void _enemyHadouken () {
		if (position.GetPlayerPosition() == 1) {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.bottomRight;
		} else {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.bottomLeft;
		}
	}
	void _enemyYogaFire () {
		if (position.GetPlayerPosition() == 1) {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.leftbottomRight;
		} else {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.rightbottomLeft;
		}
	}
	void _enemyReppuken () {
		if (position.GetPlayerPosition() == 1) {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.bottomLeft;
		} else {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.bottomRight;
		}
	}
	void _enemyBomb () {
		if (position.GetPlayerPosition() == 1) {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.leftRight;
		} else {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.rightLeft;
		}
	}
	void _enemyRolling () {
		if (position.GetPlayerPosition() == 1) {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.longLeftRight;
		} else {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.longRightLeft;
		}
	}
	void _enemyUpper () {
		command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.upBottom;
	}
	void _enemyTrippleFire () {
		if (position.GetPlayerPosition() == 1) {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.rightbottomRight;
		} else {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.leftbottomLeft;
		}
	}
	void _enemyThunder () {
		if (position.GetPlayerPosition() == 1) {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.leftBottomLeftBottom;
		} else {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.rightBottomRightBottom;
		}
	}
	void _enemyMultiBomb () {
		for (int i = 0; i < UnityEngine.Random.Range (1, 6); i++) {
			if (position.GetPlayerPosition() == 1) {
				command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.leftRight;
			} else {
				command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.rightLeft;
			}
			controller.Attack ();
		}
	}
	void _enemyHeartProtection () {
		if (position.GetPlayerPosition() == 1) {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.longLeftBottomRightUpLeft;
		} else {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.longRightBottomLeftUpRight;
		}
	}
	void _enemyScrewDriver () {
		if (position.GetPlayerPosition() == 1) {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.upRightBottomLeftUp;
		} else {
			command.buttonPos = OnlineBattlePlayerCommand.ButtonPos.upLeftBottomRightUp;
		}
	}
	void _normalAttack () {
//		Debug.Log ("normalAttack");
		switch (UnityEngine.Random.Range (1, 20)) {
		case 1: // normal attack
			_enemyNormalAttack ();
			break;
		case 2: // hadouken
			_enemyHadouken ();
			break;
		case 3: // yogafire
			_enemyYogaFire ();
			break;
		case 4: // reppuken
			_enemyReppuken ();
			break;
		case 5: // bomb
			_enemyBomb ();
			break;
		case 6: // rolling
			_enemyRolling ();
			break;
		case 7: // upper
			_enemyUpper ();
			break;
		case 8: // tripple fire
			_enemyTrippleFire ();
			break;
		case 9: // thunder
			_enemyThunder ();
			break;
		case 10:
			_enemyMultiBomb ();
			break;
		case 11: // heart protection
			_enemyHeartProtection ();
			break;
		case 12:
			_enemyNormalAttack ();
			break;
		case 13:
			_enemyHadouken ();
			break;
		case 14:
			_enemyYogaFire ();
			break;
		case 15:
			_enemyTrippleFire ();
			break;
		case 16:
			_enemyReppuken ();
			break;
		case 17:
			_enemyThunder ();
			break;
		case 18:
			_enemyHadouken ();
			break;
		case 19:
			_enemyUpper ();
			break;
		default:
			break;
		}

		controller.Attack ();
	}

	void _hardAttack () {
//		Debug.Log ("_hardAttack");
		switch (UnityEngine.Random.Range (1, 20)) {
		case 1: // normal attack
			_enemyTrippleFire ();
			break;
		case 2: // hadouken
			_enemyHadouken ();
			break;
		case 3: // yogafire
			_enemyYogaFire ();
			break;
		case 4: // reppuken
			_enemyReppuken ();
			break;
		case 5: // bomb
			_enemyBomb ();
			break;
		case 6: // rolling
			_enemyRolling ();
			break;
		case 7: // upper
			_enemyUpper ();
			break;
		case 8: // tripple fire
			_enemyTrippleFire ();
			break;
		case 9: // thunder
			_enemyThunder ();
			break;
		case 10:
			_enemyMultiBomb ();
			break;
		case 11: // heart protection
			_enemyHeartProtection ();
			break;
		case 12:
			_enemyScrewDriver ();
			break;
		case 13:
			_enemyThunder ();
			break;
		case 14:
			_enemyYogaFire ();
			break;
		case 15:
			_enemyReppuken ();
			break;
		case 16:
			_enemyReppuken ();
			break;
		case 17:
			_enemyHeartProtection ();
			break;
		case 18:
			_enemyYogaFire ();
			break;
		case 19:
			_enemyYogaFire ();
			break;
		default:
			break;
		}

		controller.Attack ();
	}

	void _hellAttack () {
//		Debug.Log ("_hellAttack");
		switch (UnityEngine.Random.Range (1, 20)) {
		case 1: // normal attack
			_enemyTrippleFire ();
			break;
		case 2: // hadouken
			_enemyTrippleFire ();
			break;
		case 3: // yogafire
			_enemyYogaFire ();
			break;
		case 4: // reppuken
			_enemyReppuken ();
			break;
		case 5: // bomb
			_enemyBomb ();
			break;
		case 6: // rolling
			_enemyRolling ();
			break;
		case 7: // upper
			_enemyUpper ();
			break;
		case 8: // tripple fire
			_enemyTrippleFire ();
			break;
		case 9: // thunder
			_enemyThunder ();
			break;
		case 10:
			_enemyMultiBomb ();
			break;
		case 11: // heart protection
			_enemyHeartProtection ();
			break;
		case 12:
			_enemyScrewDriver ();
			break;
		case 13:
			_enemyThunder ();
			break;
		case 14:
			_enemyYogaFire ();
			break;
		case 15:
			_enemyReppuken ();
			break;
		case 16:
			_enemyHeartProtection ();
			break;
		case 17:
			_enemyHeartProtection ();
			break;
		case 18:
			_enemyYogaFire ();
			break;
		case 19:
			_enemyYogaFire ();
			break;
		default:
			break;
		}

		controller.Attack ();
	}

	void _move () {
		switch (UnityEngine.Random.Range (1, 3)) {
		case 1:
			command.SetSingleModePosition (OnlineBattlePlayerCommand.ButtonDirection.left);
//			command.RightPushDown ();
			StartCoroutine (stopEnemy ());
//			isHorizontalButtonPush = true;
			break;
		case 2:
			command.SetSingleModePosition (OnlineBattlePlayerCommand.ButtonDirection.right);
//			command.LeftPushDown ();
			StartCoroutine (stopEnemy ());
//			isHorizontalButtonPush = true;
			break;
		default:
			break;
		}
	}

	IEnumerator stopEnemy () {
		yield return new WaitForSeconds (UnityEngine.Random.Range(0.1f, 3.0f));
		command.SetSingleModePosition (OnlineBattlePlayerCommand.ButtonDirection.none);
	}

	public void SetDeadFlag (bool flag) {
		isDead = flag;

		if (!flag) {
			StartCoroutine (EnemyMove ());
			StartCoroutine (EnemyJump ());
			StartCoroutine (EnemyAttack ());
		}
	}
}
