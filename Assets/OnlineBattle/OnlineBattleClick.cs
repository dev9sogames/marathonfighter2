﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class OnlineBattleClick : Photon.MonoBehaviour {
	public PhotonManager manager;

	int singleMode; // 1:single、2:online
	int singleModeDifficulty; // 0:hard,1:hell,2:impossible

	public Canvas LevelUpCanvas;
	public Canvas GameFinishCanvas;
	public Text gameResultText;
	public Text getExpText;
	public Text currentExpText;
	public Text NextToExpText;

	int[] expTable = {0,11000,23100,36410,51051,67156,84872,104359,125795,149374,175312,203843,235227,269750,307725,349497,395447,445992,501591,562750,630025,704027,785430,874973,973471,1081818,1200999,1332099,1476309,1634940,1809434,2001378,2212515,2444767,2700244,2981268,3290395,3630434,4004478,4415926,4868518,5366370,5914007,6516408,7179048,7907953,8709749,9591723,10561896,11629085,12802994};

	int HostRemainHp;
	int ClientRemainHp;

	public Slider HostSlider;
	public Slider ClientSlider;
	public Text HostNameText;
	public Text ClientNameText;
	public Text HostHpText;
	public Text ClientHpText;

	public Text BattleStatusText;

	public Text MasterWinCountText;
	public Text MasterLoseCountText;
	public Text ClientWinCountText;
	public Text ClientLoseCountText;

	int HostHp;
	int ClientHp;

	OnlineBattlePlayerCommand player;
	OnlineBattlePlayerController playerController;

	GameObject MasterPlayer;
	GameObject ClientPlayer;

	/*
	 * Lethal Weapon
	 */
	public Image LethalWeaponImage;
	public Image LethalWeaponTextImage;
	public Slider MaasterLethalWeaponSlider;
	public Slider ClientLethalWeaponSlider;

	public GameObject LethalWeaponParticle;
	GameObject LethalWeaponMiniParticle;
	bool particleDisplay;

	//	public Button LethalWeaponButton;

	/*
	 * タップ関連
	 * 
	 * 
	 */
	int tapCount;
	int nextTapCount;
	DateTime beforeClickTime;
	DateTime clickTime;

	int hariteTapCount;
	int hariteContinueCount;

	Coroutine longActionCrt;
	bool isLongActionTap;

	// 20count per second
	TimeSpan doubleTap = new TimeSpan(0, 0, 00, 00, 50);

	TimeSpan hariteTap = new TimeSpan(0, 0, 00, 00, 200);

	void Start () {
		LethalWeaponMiniParticle = ResourceCache.Load ("OnlineBattle/LethalWeaponMini");
		singleMode = 2;
	}

	public void MarathonJumpUp () {
		playerController.JumpPlayerUp ();
	}
	public void MarathonJumpDown () {
		playerController.JumpPlayerDown ();
	}

	public void ActionButtonClick () {
		tapCount++;
		longActionCrt = StartCoroutine (LongActionPush ());

		if (tapCount == nextTapCount) {
			TimeSpan diff = DateTime.Now - beforeClickTime;
			if (diff < doubleTap) {
				hariteTapCount = 0;
				playerController.DoubleAttack ();
			} else if (diff >= doubleTap && diff < hariteTap) {
				hariteTapCount++;
				if (hariteTapCount == 5) {
					playerController.HariteAttack (true);
					StartCoroutine (HariteStart ());
				}
			} else {
				playerController.HariteAttack (false);
				hariteTapCount = 0;
			}
		}
		beforeClickTime = DateTime.Now;
		nextTapCount++;
		playerController.Attack ();
	}

	public void ActionButtonClickUp () {
		if (longActionCrt != null) {
			StopCoroutine (longActionCrt);
		}
		isLongActionTap = false;
		playerController.ThrowBomb ();
	}

	IEnumerator LongActionPush () {
		yield return new WaitForSeconds (1);
		isLongActionTap = true;
		playerController.CatchBomb ();
	}

	IEnumerator HariteStart () {
		while (true) {
			yield return new WaitForSeconds (0.5f);
			if (hariteContinueCount == hariteTapCount) {
				playerController.HariteAttack (false);
				hariteTapCount = 0;
				hariteContinueCount = hariteTapCount;
				break;
			}
			hariteContinueCount = hariteTapCount;
		}
	}

	public void RightDown() {
		player.RightPushDown ();
	}
	public void RightUp() {
		player.RightPushUp ();
	}
	public void LeftDown() {
		player.LeftPushDown();
	}
	public void LeftUp() {
		player.LeftPushUp ();
	}
	public void TopDown() {
		player.UpPushDown ();
	}
	public void TopUp() {
		player.UpPushUp();
	}
	public void BottomDown() {
		player.BottomPushDown ();
	}
	public void BottomUp() {
		player.BottomPushUp ();
	}

	public void SetPlayer (GameObject playerObj) {
		playerController = playerObj.GetComponent<OnlineBattlePlayerController> ();
		player = playerObj.GetComponent<OnlineBattlePlayerCommand> ();
		BattleStatusText.gameObject.SetActive (true);
//		LethalWeaponButton.interactable = false;
		MaasterLethalWeaponSlider.value = 0;
		ClientLethalWeaponSlider.value = 0;

		BattleStatusText.text = "waiting ...";

		tapCount = 0;
		nextTapCount = 1;

		hariteTapCount = 0;
		hariteContinueCount = 0;

	}

	public void SetHostName (string name) {
		HostNameText.text = name;
	}

	public void SetClientName (string name) {
		ClientNameText.text = name;
	}

	public void SetSingleHostHp (int hp) {
		HostHp = hp;
		HostHpText.text = hp.ToString ();
		HostSlider.maxValue = hp;
		HostSlider.value = hp;

		if (HostHp > 0 && ClientHp > 0) {
			BattleStatusText.text = "Battle Start !";
			playerController.BattleStart ();
			StartCoroutine (BattleStartTextDelete ());
			_setMaxHp ();
		}
	}

	public void SetSingleClientHp (int hp) {
		ClientHp = hp;
		ClientHpText.text = hp.ToString ();
		ClientSlider.maxValue = hp;
		ClientSlider.value = hp;

		if (HostHp > 0 && ClientHp > 0) {
			BattleStatusText.text = "Battle Start !";
			playerController.BattleStart ();
			StartCoroutine (BattleStartTextDelete ());
			_setMaxHp ();
		}
	}

	public void SetHostHp (int hp) {
//		HostSlider.maxValue = hpList[0];
		hp = 10000;
		HostHp = hp;
		HostHpText.text = hp.ToString ();
		HostSlider.maxValue = hp;
		HostSlider.value = hp;

		if (HostHp > 0 && ClientHp > 0) {
			BattleStatusText.text = "Battle Start !";
			playerController.BattleStart ();
			StartCoroutine (BattleStartTextDelete ());
			_setMaxHp ();
		}
	}

	public void SetClientHp (int hp) {
		hp = 10000;
		ClientHp = hp;
		ClientHpText.text = hp.ToString ();
		ClientSlider.maxValue = hp;
		ClientSlider.value = hp;

		if (HostHp > 0 && ClientHp > 0) {
			BattleStatusText.text = "Battle Start !";
			playerController.BattleStart ();
			StartCoroutine (BattleStartTextDelete ());
			_setMaxHp ();
		}
	}

	IEnumerator BattleStartTextDelete () {
		yield return new WaitForSeconds (1);
		BattleStatusText.text = "";
		BattleStatusText.gameObject.SetActive (false);
	}

	void _setMaxHp () {
		int maxHp = HostHp;
		float ratio = 1f;
		float width = 450f;
		if (ClientHp > HostHp) {
			maxHp = ClientHp;
			ratio = (float) HostHp / (float) ClientHp;
			width = (float)width * ratio;
			HostSlider.GetComponent<RectTransform> ().sizeDelta = new Vector2 (width, 87f);
		} else {
			ratio = (float) ClientHp / (float) HostHp;
			width = (float)width * ratio;
			ClientSlider.GetComponent<RectTransform> ().sizeDelta = new Vector2 (width, 87f);
		}
	}

	public void SendMasterDamage (int[] pointList) {
		HostHpText.text = pointList[0].ToString ();
		HostSlider.value = pointList[0];

		ClientLethalWeaponSlider.value += pointList [1];

		if (ClientLethalWeaponSlider.value > 8000) {
//			LethalWeaponButton.interactable = true;
			playerController.SetCanLethalWeapon (true);
		}
	}

	public void SendClientDamage (int[] pointList) {
		ClientHpText.text = pointList[0].ToString ();
		ClientSlider.value = pointList[0];

		MaasterLethalWeaponSlider.value += pointList [1];

		if (MaasterLethalWeaponSlider.value > 8000) {
//			LethalWeaponButton.interactable = true;
			playerController.SetCanLethalWeapon (true);
		}
	}

	public void SendMasterLethalDamage (int[] pointList) {
		HostHpText.text = pointList[0].ToString ();
		HostSlider.value = pointList[0];
	}
	public void SendClientLethalDamage (int[] pointList) {
		ClientHpText.text = pointList[0].ToString ();
		ClientSlider.value = pointList[0];
	}
	public void SetBattleStatusText (string text) {
		BattleStatusText.gameObject.SetActive (true);
		BattleStatusText.text = "GAME FINISH";

		StartCoroutine (SetResult (text));
//		BattleStatusText.gameObject.SetActive (true);
//		BattleStatusText.text = text;
	}

	IEnumerator SetResult (string text) {
		yield return new WaitForSeconds (2);
//		GameFinishCanvas.gameObject.SetActive (true);
		gameResultText.text = text;

		HostRemainHp = HostSlider.value >= 0 ? (int)HostSlider.value : 0;
		ClientRemainHp = ClientSlider.value >= 0 ? (int)ClientSlider.value : 0;

		int getExp;
		int totalExp;
		int nextToExp;
		int playerLevel;

		Debug.Log("host max " + (int)HostSlider.maxValue);
		Debug.Log("clien max " + (int)ClientSlider.maxValue);
		Debug.Log("ClientRemainHp " + ClientRemainHp);
		Debug.Log ("HostRemainHp : " + HostRemainHp);

		float bonusRatio = 0.7f;
		if (singleMode == 1) {
			switch (singleModeDifficulty) {
			case 0:
				bonusRatio = 0.3f;
				break;
			case 1:
				bonusRatio = 0.4f;
				break;
			case 2:
				bonusRatio = 0.7f;
				break;
			default:
				break;
			}
		}

		Debug.Log ("bonusRatio : " + bonusRatio);
		if (PhotonNetwork.isMasterClient) {
			if (HostRemainHp > ClientRemainHp) { //win
				getExp = (int)(((int)ClientSlider.maxValue) * bonusRatio);
			} else { //lose
				getExp = ((int)ClientSlider.maxValue - (int)ClientRemainHp) / 3;
			}
		} else {
			if (ClientRemainHp > HostRemainHp) { //win
				getExp = (int)(((int)HostSlider.maxValue) * bonusRatio);
			} else { //lose
				getExp = ((int)HostSlider.maxValue - (int)HostRemainHp) / 3;
			}
		}
		getExpText.text = getExp.ToString ();

		totalExp = PlayerPrefs.GetInt (Constants.TOTAL_DISTANCE, 0);
		totalExp += getExp;
		PlayerPrefs.SetInt (Constants.TOTAL_DISTANCE, totalExp);
		currentExpText.text = totalExp.ToString ();

		nextToExp = PlayerPrefs.GetInt (Constants.EXP_NEXT_TO, 0);
		Debug.Log ("current next to : " + nextToExp);
		nextToExp -= getExp;

		Debug.Log ("get1 : " + getExp);
		Debug.Log ("total1 : " + totalExp);
		Debug.Log ("next to1 : " + nextToExp);

		if (nextToExp < 0) { // level up
			GameFinishCanvas.gameObject.SetActive (false);
			playerLevel = PlayerPrefs.GetInt (Constants.PLAYER_LEVEL, 1);
			playerLevel++;
			PlayerPrefs.SetInt (Constants.PLAYER_LEVEL, playerLevel);
			nextToExp = expTable [playerLevel];
			Debug.Log ("Level up next to" + nextToExp);
			PlayerPrefs.SetInt (Constants.EXP_NEXT_TO, (nextToExp - totalExp));
			NextToExpText.text = (nextToExp - totalExp).ToString ();

			AllGameDefinition.expNextTo = nextToExp;
			AllGameDefinition.level = playerLevel;

			PlayerPrefs.Save ();
			LevelUpCanvasOpen ();
		} else {
			GameFinishCanvas.gameObject.SetActive (true);

			PlayerPrefs.SetInt (Constants.EXP_NEXT_TO, nextToExp);
			NextToExpText.text = nextToExp.ToString ();

			PlayerPrefs.Save ();

			Debug.Log ("get2 : " + getExp);
			Debug.Log ("total2 : " + totalExp);
			Debug.Log ("next to2 : " + nextToExp);

			StartCoroutine (FinishOnlineGame ());
		}
	}

	void LevelUpCanvasOpen () {
		GameFinishCanvas.gameObject.SetActive (false);
		LevelUpCanvas.gameObject.SetActive (true);
		LevelUpCanvas.GetComponent<GuiAnimationManager> ().MoveTextList ();
		LevelUpCanvas.GetComponent<GuiAnimationManager> ().MoveImageList ();

		StartCoroutine (CloseLevelUpCanvas ());
	}

	IEnumerator CloseLevelUpCanvas () {
		yield return new WaitForSeconds (5);
		LevelUpCanvas.gameObject.SetActive (false);
		GameFinishCanvas.gameObject.SetActive (true);

		StartCoroutine (FinishOnlineGame ());
	}

	public void UnSetBattleStatusText () {
		BattleStatusText.text = "";
		BattleStatusText.gameObject.SetActive (false);
	}
	public void SetBattleCountUp (int type) { // 1:lose, 2:win
		manager.SetBattleStatus(false);
		if (type == 1) {
			int loseCount = PlayerPrefs.GetInt ("OnlineBattleLoseCount", 0);
			PlayerPrefs.SetInt ("OnlineBattleLoseCount", loseCount+1);
		} else if (type == 2) {
			int winCount = PlayerPrefs.GetInt ("OnlineBattleWinCount", 0);
			PlayerPrefs.SetInt ("OnlineBattleWinCount", winCount+1);
		}
		PlayerPrefs.Save ();
	}
	IEnumerator FinishOnlineGame () {
		yield return new WaitForSeconds (5);
		manager.ExitRoom ();
	}

	public void SetMasterPlayerBattleCount (int[] count) {
		MasterWinCountText.text = "Win : " + count [0];
		MasterLoseCountText.text = "Lose : " + count [1];
	}
	public void SetClientPlayerBattleCount (int[] count) {
		ClientWinCountText.text = "Win : " + count [0];
		ClientLoseCountText.text = "Lose : " + count [1];
	}

	public void LethalWeaponImageStart () {
		LethalWeaponImage.gameObject.SetActive (true);
		LethalWeaponParticle.SetActive (true);
		particleDisplay = true;
		StartCoroutine(LethalWeaponMiniStart ());
	}

	IEnumerator LethalWeaponMiniStart () {
		while (true) {
			yield return new WaitForSeconds (0.2f);
			if (!particleDisplay) {
				break;
			}
			Vector3 pos = new Vector3 (UnityEngine.Random.Range(-5f, 5f), UnityEngine.Random.Range(-3f, 3f));
			Instantiate (LethalWeaponMiniParticle, pos, Quaternion.identity);
		}
	}

	public void LethalWeaponImageEnd () {
		particleDisplay = false;
		LethalWeaponParticle.SetActive (false);

		StartCoroutine (ShowLethalWeaponText ());
	}

	IEnumerator ShowLethalWeaponText () {
		yield return new WaitForSeconds (0.6f);

		LethalWeaponImage.gameObject.SetActive (true);
		LethalWeaponTextImage.gameObject.SetActive (true);

		StartCoroutine (RemoveLethalWeaponText ());
	}

	IEnumerator RemoveLethalWeaponText () {
		yield return new WaitForSeconds (1.5f);
		LethalWeaponImage.gameObject.SetActive (false);
		LethalWeaponTextImage.gameObject.SetActive (false);

		playerController.EndLethalWeaponAttack ();
	}

	public void LethalWeaponSingleImageEnd () {
		particleDisplay = false;
		LethalWeaponParticle.SetActive (false);

		StartCoroutine (ShowSingleLethalWeaponText ());
	}

	IEnumerator ShowSingleLethalWeaponText () {
		yield return new WaitForSeconds (0.6f);

		LethalWeaponImage.gameObject.SetActive (true);
		LethalWeaponTextImage.gameObject.SetActive (true);

		StartCoroutine (RemoveSingleLethalWeaponText ());
	}
	IEnumerator RemoveSingleLethalWeaponText () {
		yield return new WaitForSeconds (1.5f);
		LethalWeaponImage.gameObject.SetActive (false);
		LethalWeaponTextImage.gameObject.SetActive (false);

		playerController.EndSingleLethalWeaponAttack ();
	}

	public void UseMasterLethalWeapon () {
		MaasterLethalWeaponSlider.value = 0;
	}
	public void UseClientLethalWeapon () {
		ClientLethalWeaponSlider.value = 0;
	}

	public void SetSingleMode (int type) { // 2:online, 1:single
		singleMode = type;
	}

	public void SetSingleModeDifficulty (int mode) { // 0:hard,1:hell,2:impossible
		singleModeDifficulty = mode;
	}
}
