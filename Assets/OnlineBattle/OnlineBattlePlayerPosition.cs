﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnlineBattlePlayerPosition : Photon.MonoBehaviour {
	public bool singleMode;

	GameObject enemy;

	PhotonView photonView;

	public Text nameText;
	public Text hpText;
	public Text damageText;
	public Slider slider;

//	float posX;

	Vector3 enemyPos;

	enum PlayerPosition {
		none,
		right,
		left
	}
	PlayerPosition pos;

	void Start () {
		if (!singleMode) {
			enemy = null;
			photonView = GetComponent<PhotonView> ();

			if (!PhotonNetwork.isMasterClient) {
				photonView.RPC ("SetEnemyPlayer", PhotonTargets.AllBuffered);
			}
		}
	}

	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info) {
		if (stream.isWriting) {
			//データの送信
			stream.SendNext(transform.position);
		} else {
			//データの受信
			enemyPos = (Vector3)stream.ReceiveNext();
		}
	}

	void Update () {
		if (!singleMode) {
			if (!photonView.isMine) {
				transform.position = Vector3.Lerp (transform.position, this.enemyPos, Time.deltaTime * 5);
			}
		}

		if (!singleMode) {
			if (photonView.isMine && enemy != null) {
				//			Debug.Log ("own : " + transform.position.x);
				//			Debug.Log ("enemy ; " + enemy.transform.position.x);
				if (enemy.transform.position.x > transform.position.x) {
					if (pos != PlayerPosition.left) {
						//					Debug.Log ("player left" + " own : " + transform.position.x + " : enemy : " + enemy.transform.position.x );
						transform.localScale = new Vector3 (-0.1f, 0.1f, 0);
						photonView.RPC ("SetPosition", PhotonTargets.Others, 1);
//						nameText.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
//						hpText.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
//						damageText.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
//						slider.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
						pos = PlayerPosition.left;
					}
				} else {
					if (pos != PlayerPosition.right) {
						//					Debug.Log ("player right" + " own : " + transform.position.x + " : enemy : " + enemy.transform.position.x );
						transform.localScale = new Vector3 (0.1f, 0.1f, 0);
						photonView.RPC ("SetPosition", PhotonTargets.Others, 2);
//						nameText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
//						hpText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
//						damageText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
//						slider.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
						pos = PlayerPosition.right;
					}
				}
			}
		} else {
			if (enemy != null) {
				if (enemy.transform.position.x > transform.position.x) {
					if (pos != PlayerPosition.left) {
						//					Debug.Log ("player left" + " own : " + transform.position.x + " : enemy : " + enemy.transform.position.x );
						transform.localScale = new Vector3 (-0.1f, 0.1f, 0);
						nameText.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
						hpText.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
						damageText.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
						slider.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
						pos = PlayerPosition.left;
					}
				} else {
					if (pos != PlayerPosition.right) {
						//					Debug.Log ("player right" + " own : " + transform.position.x + " : enemy : " + enemy.transform.position.x );
						transform.localScale = new Vector3 (0.1f, 0.1f, 0);
						nameText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
						hpText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
						damageText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
						slider.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
						pos = PlayerPosition.right;
					}
				}
			}
		}
	}

	[PunRPC]
	void SetPosition (int type) {
		if (type == 1) {
			nameText.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
			hpText.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
			damageText.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
			slider.transform.localScale = new Vector3 (-0.15f, 0.15f, 0);
		} else {
			nameText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
			hpText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
			damageText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
			slider.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
		}
	}

	public int GetPlayerPosition () {
		if (pos == PlayerPosition.left) {
			return 1;
		} else if (pos == PlayerPosition.right) {
			return 2;
		}

		return 0;
	}

	public void SetLeftPosition () {
		transform.localScale = new Vector3 (0.1f, 0.1f, 0);
		nameText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
		hpText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
		damageText.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
		slider.transform.localScale = new Vector3 (0.15f, 0.15f, 0);
		pos = PlayerPosition.right;
	}

	[PunRPC]
	void SetEnemyPlayer () {
		if (PhotonNetwork.isMasterClient) {
			Debug.Log ("isMasterClient SetEnemyPlayer");
			enemy = GameObject.FindGameObjectWithTag ("EnemyPlayer");
			GetComponent<OnlineBattlePlayerController> ().SetEnemyPlayer (enemy);
		} else {
			Debug.Log ("is not MasterClient SetEnemyPlayer");
			enemy = GameObject.FindGameObjectWithTag ("Player");
			GetComponent<OnlineBattlePlayerController> ().SetEnemyPlayer (enemy);
		}
	}

	public void SetEachEnemyPlayer () {
		photonView.RPC ("SetEnemyPlayer", PhotonTargets.AllBuffered);
	}

	public void SetSingleEnemyPlayer (GameObject enemyObj) {
		enemy = enemyObj;
	}
}
