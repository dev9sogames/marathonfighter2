﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattlePlayerCommand : MonoBehaviour {

	Rigidbody2D rgbd;
	PhotonView photonView;
	PhotonTransformView photonTransformView;

	/*
	 * ボタン操作系
	 * 
	 * 
	 */
	public enum ButtonDirection {
		none,
		left,
		right,
	}
	ButtonDirection buttonDirection;

	bool singleMode;

	public enum ButtonPos {
		none,
		right,
		rightbottom,
		rightbottomLeft,
		rightbottomRight,
		rightLeft,
		rightBottomRightBottom,

		rightLeftBottom,
		rightLeftBottomright,

		left,
		leftbottom,
		leftbottomRight,
		leftbottomLeft,
		leftRight,
		leftBottomLeftBottom,

		leftRightBottom,
		leftRightBottomLeft,

		up,
		upBottom,
		upRight,
		upRightBottom,
		upRightBottomLeft,
		upRightBottomLeftUp,

		upLeft,
		upLeftBottom,
		upLeftBottomRight,
		upLeftBottomRightUp,

		bottom,
		bottomRight,
		bottomLeft,

		longRight,
		longRightLeft,
		//		longBottomToRight,
		//		longLeftToRight,
		//		longBottomToLeft,
		longLeft,
		longLeftRight,
		longLeftBottom,
		longLeftBottomRight,
		longLeftBottomRightUp,
		longLeftBottomRightUpLeft,
		longBottom,
		longBottomUp,

		longRightBottom,
		longRightBottomLeft,
		longRightBottomLeftUp,
		longRightBottomLeftUpRight,
		//		longBottomToUp,
	}
	public ButtonPos buttonPos;

	Coroutine rightEndCrt;
	Coroutine leftEndCrt;
	Coroutine UpEndCrt;
	Coroutine BottomEndCrt;

	Coroutine rightMoveCrt;
	Coroutine leftMoveCrt;

	// 長押し用
	Coroutine longBottomCrt;
	Coroutine longRightCrt;
	Coroutine longLeftCrt;

	bool isLeftMoving;
	bool isRightMoving;
	bool isPushBottom;

	float limitX1;
	float limitX2;

	bool canMove; // public -> for debug

	void Start () {
		rgbd = GetComponent<Rigidbody2D> ();
		photonView = GetComponent<PhotonView> ();
		photonTransformView = GetComponent<PhotonTransformView> ();

		buttonDirection = ButtonDirection.none;

		// can move limit
		limitX1 = 8;
		limitX2 = -8;

		limitX1 = (limitX1 * (9f / (16f * Screen.height / Screen.width))) - 1;
		limitX2 = (limitX2 * (9f / (16f * Screen.height / Screen.width))) + 1;
	}

	void FixedUpdate () {
		if (!canMove) {
			return;
		}

		Vector2 velocity = rgbd.velocity;

		if (transform.position.x > (limitX1 + 1)) {
			transform.position = new Vector3 (limitX1 - 0.5f, transform.position.y, 0);
			return;
		}
		if (transform.position.x < (limitX2 - 1)) {
			transform.position = new Vector3 (limitX2 + 0.5f, transform.position.y, 0);
			return;
		}

		if (buttonDirection == ButtonDirection.right && transform.position.x < limitX1) {
//		if (isRightMoving && transform.position.x < limitX1) {
			velocity.x = 7.0f;
		} else if (buttonDirection == ButtonDirection.left && transform.position.x > limitX2) {
//		} else if (isLeftMoving && transform.position.x > limitX2) {
			velocity.x = -7.0f;
		} else {
			velocity.x = 0.0f;
		}
		rgbd.velocity = velocity;

		if (!singleMode && photonView.isMine) {
			//移動速度を指定
			photonTransformView.SetSynchronizedValues (rgbd.velocity, 0);
		}
	}

	public void SetCanMoveFlag (bool flag) {
		canMove = flag;
	}

	public void RightPushDown () {
		buttonDirection = ButtonDirection.right;
		//		transform.localScale = new Vector3 (-0.3f, transform.localScale.y, 0);
		switch (buttonPos) {
		case ButtonPos.bottom:
			buttonPos = ButtonPos.bottomRight;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
			/*		case ButtonPos.longBottom:
			buttonPos = ButtonPos.longBottomToRight;
			break;
			*/
		case ButtonPos.leftbottom:
			buttonPos = ButtonPos.leftbottomRight;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
		case ButtonPos.up:
			buttonPos = ButtonPos.upRight;
			if (UpEndCrt != null) {
				StopCoroutine (UpEndCrt);
			}
			break;
		case ButtonPos.longLeft:
			buttonPos = ButtonPos.longLeftRight;
			break;
		case ButtonPos.longLeftBottom:
			buttonPos = ButtonPos.longLeftBottomRight;
			break;
		case ButtonPos.rightbottom:
			buttonPos = ButtonPos.rightbottomRight;
			break;
		case ButtonPos.left:
			buttonPos = ButtonPos.leftRight;
			break;
		case ButtonPos.upLeftBottom:
			buttonPos = ButtonPos.upLeftBottomRight;
			break;
		case ButtonPos.longRightBottomLeftUp:
			buttonPos = ButtonPos.longRightBottomLeftUpRight;
			break;
		case ButtonPos.rightLeftBottom:
			buttonPos = ButtonPos.rightLeftBottomright;
			break;
			/*
		case ButtonPos.longLeft:
			buttonPos = ButtonPos.longLeftToRight;
			break;
			*/
		default:
			buttonPos = ButtonPos.right;
			longRightCrt = StartCoroutine (LongRightPush ());
			rightMoveCrt = StartCoroutine (RightMoving ());
			break;
		}
		//		StartCoroutine ("RightPushEnd");
	}
	public void RightPushUp () {
		if (buttonDirection == ButtonDirection.right) {
			buttonDirection = ButtonDirection.none;
		}
		if (longRightCrt != null) {
			StopCoroutine (longRightCrt);
		}
		if (rightMoveCrt != null) {
			StopCoroutine (rightMoveCrt);
		}
//		isRightMoving = false;
		rightEndCrt = StartCoroutine (RightPushEnd ());
		//		buttonPos = ButtonPos.none;
		//		StopCoroutine (rightCrt);
	}
	public void LeftPushDown () {
		buttonDirection = ButtonDirection.left;
		//		transform.localScale = new Vector3 (0.3f, transform.localScale.y, 0);
		switch (buttonPos) {
		case ButtonPos.bottom:
			buttonPos = ButtonPos.bottomLeft;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
			/*
		case ButtonPos.longBottom:
			buttonPos = ButtonPos.longBottomToLeft;
			break;
			*/
		case ButtonPos.rightbottom:
			buttonPos = ButtonPos.rightbottomLeft;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
		case ButtonPos.longRight:
			buttonPos = ButtonPos.longRightLeft;
			break;
		case ButtonPos.upRightBottom:
			buttonPos = ButtonPos.upRightBottomLeft;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
		case ButtonPos.longLeftBottomRightUp:
			buttonPos = ButtonPos.longLeftBottomRightUpLeft;
			break;
		case ButtonPos.right:
			buttonPos = ButtonPos.rightLeft;
			break;
		case ButtonPos.up:
			buttonPos = ButtonPos.upLeft;
			break;
		case ButtonPos.longRightBottom:
			buttonPos = ButtonPos.longRightBottomLeft;
			break;
		case ButtonPos.leftbottom:
			buttonPos = ButtonPos.leftbottomLeft;
			break;
		case ButtonPos.leftRightBottom:
			buttonPos = ButtonPos.leftRightBottomLeft;
			break;
		default:
			buttonPos = ButtonPos.left;
			longLeftCrt = StartCoroutine (LongLeftPush ());
			leftMoveCrt = StartCoroutine (LeftMoving ());
			break;
		}
		//		StartCoroutine ("LeftPushEnd");
	}
	public void LeftPushUp () {
		if (buttonDirection == ButtonDirection.left) {
			buttonDirection = ButtonDirection.none;
		}
		if (longLeftCrt != null) {
			StopCoroutine (longLeftCrt);
		} else {
			buttonPos = ButtonPos.none;
		}
		if (leftMoveCrt != null) {
			StopCoroutine (leftMoveCrt);
		}
//		isLeftMoving = false;
		leftEndCrt = StartCoroutine (LeftPushEnd ());
		//		buttonPos = ButtonPos.none;
	}
	public void UpPushDown () {
		switch (buttonPos) {
		case ButtonPos.bottom:
			buttonPos = ButtonPos.upBottom;
			if (BottomEndCrt != null) {
				StopCoroutine (BottomEndCrt);
			}
			break;
		case ButtonPos.upRightBottomLeft:
			buttonPos = ButtonPos.upRightBottomLeftUp;
			break;
		case ButtonPos.longBottom:
			buttonPos = ButtonPos.longBottomUp;
			break;
		case ButtonPos.longLeftBottomRight:
			buttonPos = ButtonPos.longLeftBottomRightUp;
			break;
		case ButtonPos.upLeftBottomRight:
			buttonPos = ButtonPos.upLeftBottomRightUp;
			break;
		case ButtonPos.longRightBottomLeft:
			buttonPos = ButtonPos.longRightBottomLeftUp;
			break;
		default:
			buttonPos = ButtonPos.up;
			break;
		}

		/*
		if (transform.position.y >= 0.65) {
			return;
		}
		*/
		//		animator.SetBool ("upper", true);
		/*
		if (buttonPos == ButtonPos.bottom) {
			buttonPos = ButtonPos.upBottom;
		} else {
			buttonPos = ButtonPos.up;
		}
		*/
	}
	public void UpPushUp () {
		UpEndCrt = StartCoroutine ("UpPushEnd");
	}
	public void BottomPushDown () {
		switch (buttonPos) {
		case ButtonPos.right:
			buttonPos = ButtonPos.rightbottom;
			if (rightEndCrt != null) {
				StopCoroutine (rightEndCrt);
			}
			break;
		case ButtonPos.left:
			buttonPos = ButtonPos.leftbottom;
			if (leftEndCrt != null) {
				StopCoroutine (leftEndCrt);
			}
			break;
		case ButtonPos.upRight:
			buttonPos = ButtonPos.upRightBottom;
			if (rightEndCrt != null) {
				StopCoroutine (rightEndCrt);
			}
			break;
		case ButtonPos.longLeft:
			buttonPos = ButtonPos.longLeftBottom;
			break;
		case ButtonPos.upLeft:
			buttonPos = ButtonPos.upLeftBottom;
			break;
		case ButtonPos.longRight:
			buttonPos = ButtonPos.longRightBottom;
			break;
		case ButtonPos.rightbottomRight:
			buttonPos = ButtonPos.rightBottomRightBottom;
			break;
		case ButtonPos.leftbottomLeft:
			buttonPos = ButtonPos.leftBottomLeftBottom;
			break;
		case ButtonPos.rightLeft:
			buttonPos = ButtonPos.rightLeftBottom;
			break;
		case ButtonPos.leftRight:
			buttonPos = ButtonPos.leftRightBottom;
			break;
		default:
			isPushBottom = false;
			buttonPos = ButtonPos.bottom;
			break;
		}
		//		StartCoroutine ("BottomPushEnd");
		longBottomCrt = StartCoroutine (LongBottomPush ());
	}
	public void BottomPushUp () {
		isPushBottom = true;
		BottomEndCrt = StartCoroutine ("BottomPushEnd");
		if (longBottomCrt != null) {
			StopCoroutine (longBottomCrt);
		}
	}

	IEnumerator UpPushEnd () {
		yield return new WaitForSeconds (0.2f);
		if (buttonPos != ButtonPos.upRight && buttonPos != ButtonPos.upRightBottom && buttonPos != ButtonPos.upRightBottomLeft
			&& buttonPos != ButtonPos.longLeftBottomRightUpLeft
			&& buttonPos != ButtonPos.upLeft && buttonPos != ButtonPos.upLeftBottom && buttonPos != ButtonPos.upLeftBottomRight
			&& buttonPos != ButtonPos.upLeftBottomRightUp && buttonPos != ButtonPos.longRightBottomLeftUpRight
		) {
			buttonPos = ButtonPos.none;
		}
	}
	IEnumerator RightPushEnd () {
		yield return new WaitForSeconds (0.2f);
		if (buttonPos != ButtonPos.rightbottom && buttonPos != ButtonPos.rightbottomLeft
			&& buttonPos != ButtonPos.upRightBottom && buttonPos != ButtonPos.upRightBottomLeft
			&& buttonPos != ButtonPos.upRightBottomLeftUp && buttonPos != ButtonPos.longLeftBottomRightUp
			&& buttonPos != ButtonPos.longLeftBottomRightUpLeft && buttonPos != ButtonPos.rightbottomRight
			&& buttonPos != ButtonPos.rightLeft && buttonPos != ButtonPos.upLeftBottomRightUp
			&& buttonPos != ButtonPos.longRightBottom && buttonPos != ButtonPos.longRightBottomLeft
			&& buttonPos != ButtonPos.longRightBottomLeftUp && buttonPos != ButtonPos.longRightBottomLeftUpRight
			&& buttonPos != ButtonPos.rightBottomRightBottom && buttonPos != ButtonPos.rightLeftBottom
			&& buttonPos != ButtonPos.rightLeftBottomright && buttonPos != ButtonPos.leftRightBottom
			&& buttonPos != ButtonPos.leftRightBottomLeft
		) {
			buttonPos = ButtonPos.none;
			isRightMoving = false;
		}
	}
	IEnumerator LeftPushEnd () {
		yield return new WaitForSeconds (0.2f);
		if (buttonPos != ButtonPos.leftbottom && buttonPos != ButtonPos.leftbottomRight && buttonPos != ButtonPos.upRightBottomLeftUp
			&& buttonPos != ButtonPos.longLeftBottom && buttonPos != ButtonPos.longLeftBottomRight && buttonPos != ButtonPos.longLeftBottomRightUp
			&& buttonPos != ButtonPos.longLeftRight && buttonPos != ButtonPos.leftbottomLeft
			&& buttonPos != ButtonPos.leftRight && buttonPos != ButtonPos.upLeftBottom && buttonPos != ButtonPos.upLeftBottomRight
			&& buttonPos != ButtonPos.upLeftBottomRightUp && buttonPos != ButtonPos.longRightBottomLeftUp
			&& buttonPos != ButtonPos.longRightBottomLeftUpRight && buttonPos != ButtonPos.leftBottomLeftBottom
			&& buttonPos != ButtonPos.rightLeftBottom && buttonPos != ButtonPos.rightLeftBottomright
			&& buttonPos != ButtonPos.leftRightBottom && buttonPos != ButtonPos.leftRightBottomLeft
		) {
			buttonPos = ButtonPos.none;
			isLeftMoving = false;
		}
	}
	IEnumerator BottomPushEnd () {
		yield return new WaitForSeconds (0.2f);
		if (buttonPos != ButtonPos.bottomLeft && buttonPos != ButtonPos.bottomRight && buttonPos != ButtonPos.upBottom
			&& buttonPos != ButtonPos.upRightBottomLeft && buttonPos != ButtonPos.upRightBottomLeftUp && 
			buttonPos != ButtonPos.longBottomUp && buttonPos != ButtonPos.longLeftBottomRight && buttonPos != ButtonPos.longLeftBottomRightUp
			&& buttonPos != ButtonPos.longLeftBottomRightUpLeft && 
			buttonPos != ButtonPos.rightbottomLeft && buttonPos != ButtonPos.rightbottomRight &&
			buttonPos != ButtonPos.leftbottomRight && buttonPos != ButtonPos.leftbottomLeft
			&& buttonPos != ButtonPos.upLeftBottomRight && buttonPos != ButtonPos.upLeftBottomRightUp
			&& buttonPos != ButtonPos.longRightBottomLeft && buttonPos != ButtonPos.longRightBottomLeftUp
			&& buttonPos != ButtonPos.longRightBottomLeftUpRight && buttonPos != ButtonPos.rightBottomRightBottom
			&& buttonPos != ButtonPos.leftBottomLeftBottom && buttonPos != ButtonPos.rightLeftBottomright
			&& buttonPos != ButtonPos.leftRightBottomLeft
			/*
			&& buttonPos != ButtonPos.longBottomToLeft && buttonPos != ButtonPos.longBottomToRight
			&& buttonPos != ButtonPos.longBottomToUp
			*/
		) {
			buttonPos = ButtonPos.none;
		}
	}

	IEnumerator LongRightPush () {
		yield return new WaitForSeconds (1);
		buttonPos = ButtonPos.longRight;
	}
	IEnumerator LongBottomPush () {
		yield return new WaitForSeconds (1);
		buttonPos = ButtonPos.longBottom;
	}
	IEnumerator LongLeftPush () {
		yield return new WaitForSeconds (1);
		buttonPos = ButtonPos.longLeft;
	}
	IEnumerator LeftMoving () {
		yield return new WaitForSeconds (0.2f);
		isLeftMoving = true;
	}
	IEnumerator RightMoving () {
		yield return new WaitForSeconds (0.2f);
		isRightMoving = true;
	}

	public void SetSingleMode (bool flag) {
		singleMode = flag;
	}

	public void SetSingleModePosition (ButtonDirection pos) {
		buttonDirection = pos;
	}
}
