﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattleSingleEnemyHitTrigger : MonoBehaviour {
	GameObject enemy;
	OnlineBattlePlayerController enemyPlayer;

	int difficulty;

	bool gameStart;

//	void Start () {
//		enemyPlayer = transform.root.GetComponent<OnlineBattlePlayerController> ();
//	}

	void Update () {
		if (gameStart) {
			transform.position = enemy.transform.position;
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "PlayerNormalAttack" || other.tag == "MarathonHadouken" ||
		    other.tag == "MarathonYogaFire" || other.tag == "Repuuken") {

			switch (difficulty) {
			case 0:
				if (UnityEngine.Random.Range (1, 5) == 1) {
					enemyPlayer.JumpPlayerDown ();
					StartCoroutine (JumpUp ());
				}
				break;
			case 1:
				if (UnityEngine.Random.Range (1, 3) == 1) {
					enemyPlayer.JumpPlayerDown ();
					StartCoroutine (JumpUp ());
				}
				break;
			case 2:
				enemyPlayer.JumpPlayerDown ();
				StartCoroutine (JumpUp ());
				break;
			default:
				if (UnityEngine.Random.Range (1, 5) == 1) {
					enemyPlayer.JumpPlayerDown ();
					StartCoroutine (JumpUp ());
				}
				break;
			}
		} else if (other.tag == "Player") {
			int num;
			switch (difficulty) {
			case 0:
				return;
				break;
			case 1:
				num = UnityEngine.Random.Range (1, 7);
				if (num == 1) {
					enemyPlayer.screwDriver ();
				} else if (num == 2) {
					enemyPlayer.LethalWeapon ();
				}
				break;
			case 2:
				num = UnityEngine.Random.Range (1, 5);
				if (num == 1) {
					enemyPlayer.screwDriver ();
				} else if (num == 2) {
					enemyPlayer.LethalWeapon ();
				}
				break;
			default:
				break;
			}
		}
	}

	IEnumerator JumpUp () {
		yield return new WaitForSeconds (UnityEngine.Random.Range(0.2f, 1f));
		enemyPlayer.JumpPlayerUp ();
	}

	public void SetDifficulty (int difficulty) {
		this.difficulty = difficulty;
		gameStart = true;
	}

	public void SetEnemyObj (GameObject obj) {
		enemy = obj;
		enemyPlayer = obj.GetComponent<OnlineBattlePlayerController> ();
	}
}
