﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattleAttackBaseController : Photon.MonoBehaviour {
	public bool isNotAttackObj;
	public bool isBombThrowTrigger;

	int playerId;
	PhotonView photonView;

	bool singleMode;
	bool singleEnemy;

	public int hitPoint; // ono:1, hadouken:2, tornado:2, reppuken:2, yogafire:4
//	public int attackPoint; // ono:1, hadouken:2, tornado:2, reppuken:2, yogafire:4

	/*
	 * For bomb throw with player
	 */

	void Start () {
		photonView = GetComponent<PhotonView> ();
	}

	public int GetPlayerPhotonViewId () {
		return playerId;
	}

	public void SetPlayerPhotonViewId (int photonViewId) {
		playerId = photonViewId;
	}

	public void DestroyObject () {
		if (this.name == "playerHarite") {
			return;
		}

		if (!singleMode) {
			try {
			GetComponent<PhotonView> ().RPC ("DestroyOpponentObject", PhotonTargets.Others);
			} catch (System.Exception e) {
				Debug.Log (e);
			}
		}

		this.gameObject.SetActive (false);

//		PhotonNetwork.Destroy (this.gameObject);
	}

	[PunRPC]
	void DestroyOpponentObject () {
		this.gameObject.SetActive (false);
	}

	void OnTriggerEnter2D (Collider2D other) {
//		Debug.Log ("attack OnTriggerEnter2D");
//		Debug.Log (other.tag);
		if (photonView != null && !photonView.isMine) {
			return;
		}

		if (other.tag == "Player" || other.tag == "Untagged") {
			return;
		}

		if (!singleMode) {
			if (PhotonNetwork.isMasterClient) {
//				Debug.Log (other.tag);
				if (other.tag == "EnemyPlayerNormalAttack") {
					_hitAttack (1);
				} else if (other.tag == "EnemyHadouken") {
					_hitAttack (2);
				} else if (other.tag == "EnemyYogaFire") {
					_hitAttack (4);
				} else if (other.tag == "EnemyRepuuken") {
					_hitAttack (2);
				} else if (other.tag == "EnemyPlayerTornado") {
					_hitAttack (3);
				}
			} else {
//				Debug.Log (other.tag);
				if (other.tag == "PlayerNormalAttack") {
					_hitAttack (1);
				} else if (other.tag == "MarathonHadouken") {
					_hitAttack (2);
				} else if (other.tag == "MarathonYogaFire") {
					_hitAttack (4);
				} else if (other.tag == "Repuuken") {
					_hitAttack (2);
				} else if (other.tag == "PlayerTornado") {
					_hitAttack (3);
				}
			}
		} else {
			if (!singleEnemy) {
				if (other.tag == "EnemyPlayerNormalAttack") {
					_hitAttack (1);
				} else if (other.tag == "EnemyHadouken") {
					_hitAttack (2);
				} else if (other.tag == "EnemyYogaFire") {
					_hitAttack (4);
				} else if (other.tag == "EnemyRepuuken") {
					_hitAttack (2);
				} else if (other.tag == "EnemyPlayerTornado") {
					_hitAttack (3);
				}
			} else {
				if (other.tag == "PlayerNormalAttack") {
					_hitAttack (1);
				} else if (other.tag == "MarathonHadouken") {
					_hitAttack (2);
				} else if (other.tag == "MarathonYogaFire") {
					_hitAttack (4);
				} else if (other.tag == "Repuuken") {
					_hitAttack (2);
				} else if (other.tag == "PlayerTornado") {
					_hitAttack (3);
				}
			}
		}
	}

	void _hitAttack (int attackPoint) {
		if (isNotAttackObj) {
			return;
		}

		hitPoint -= attackPoint;
		if (hitPoint < 1) {
			DestroyObject ();
		}
	}

	public void SetSingleMode (bool flag) {
		singleMode = flag;
	}

	public void SetSingleEnemy (bool flag) {
		singleEnemy = flag;
	}
}
