using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using NCMB;
using System;

public class PhotonManager : Photon.MonoBehaviour {
	public GameObject gameController;

	public Image loadingImage;

	public Canvas LobyCanvas;
	public Canvas RoomWaitingCanvas;
	public Canvas EnemyExitRoomCanvas;
	public Canvas BattleExitCanvas;
	public Canvas ChallengerComesCanvas;

	public Text BattleExitText;

	public Text MasterNameText;
	public Text ClientNameText;

	public Text MasterWinCountText;
	public Text MasterLoseCountText;
	public Text ClientWinCountText;
	public Text ClientLoseCountText;

	public Button ExitButton;
	public Button BattleButton;
	public Button SingleGameStartButton;

	public Text ErrorText;

	GameObject Player;

	string masterName;
	string clientName;

	bool inRoom;
	bool isBattleStart;
	int emptyRoomNo;

	SystemLanguage lang;

	RoomOptions roomOptions;

	bool singleMode;
	GameObject singlePlayer;
	GameObject singleEnemyPlayer;
	GameObject singleEnemyTrigger;
	int singleModeDifficulty;

	bool isPushSend;
	public Text PushDescription;
	public Button PushSendButton;
	public Dropdown PushDropDown;
	public Image PushSendingImage;
	public Text PushSendingText;

	void Start () {
		if (!PhotonNetwork.connected) {
			PhotonNetwork.ConnectUsingSettings ("v1.0");

			string userName = PlayerPrefs.GetString ("nameKey", null);
			PhotonNetwork.playerName = userName;

			LobyCanvas.gameObject.SetActive (true);
			RoomWaitingCanvas.gameObject.SetActive (false);
			loadingImage.gameObject.SetActive (true);
			ChallengerComesCanvas.gameObject.SetActive (false);

			ExitButton.gameObject.SetActive (true);
			BattleButton.gameObject.SetActive (false);
		} else {
			loadingImage.gameObject.SetActive (true);
			LobyCanvas.gameObject.SetActive (true);
			RoomWaitingCanvas.gameObject.SetActive (false);
			ChallengerComesCanvas.gameObject.SetActive (false);
			SingleGameStartButton.gameObject.SetActive (false);
		}

		DateTime now = DateTime.Now;
		DateTime today = now.Date;

		if (PlayerPrefs.GetString ("PushSendDate", null) == today.ToString ()) {
			PushSendButton.interactable = false;
			PushDropDown.interactable = false;
			PushDescription.text = "You can send it tomorrow.";
		} else {
			PushSendButton.interactable = true;
			PushDropDown.interactable = true;
			PushDescription.text = "You can call partners once a day by PUSH notice.";
		}
		isPushSend = false;
	}

	void OnJoinedLobby () {
	}

	public void CreateRoom() {
		if (inRoom) {
			return;
		}
		inRoom = true;
		string userName = PlayerPrefs.GetString ("nameKey", null);
//		int userLevel = PlayerPrefs.GetInt (Constants.PLAYER_LEVEL, 1);
//		string userLevel = level;
		PhotonNetwork.autoCleanUpPlayerObjects = false;
		//カスタムプロパティ
		ExitGames.Client.Photon.Hashtable customProp = new ExitGames.Client.Photon.Hashtable();
		customProp.Add ("userName", userName); //ユーザ名
		customProp.Add ("masterName", userName);
//		customProp.Add ("userLevel", userLevel); //ユーザID
		PhotonNetwork.SetPlayerCustomProperties(customProp);

		roomOptions = new RoomOptions ();
		roomOptions.customRoomProperties = customProp;
		//ロビーで見えるルーム情報としてカスタムプロパティのuserName,userIdを使いますよという宣言
		roomOptions.customRoomPropertiesForLobby = new string[]{ "userName", "masterName", "clientName"};
		roomOptions.maxPlayers = 2; //部屋の最大人数
		roomOptions.isOpen = true; //入室許可する
		roomOptions.isVisible = true; //ロビーから見えるようにする

		masterName = userName;
		PhotonNetwork.CreateRoom ("MarathonRoom" + emptyRoomNo, roomOptions, null);
	}

	//ルーム一覧が取れると
	void OnReceivedRoomListUpdate() {
		loadingImage.gameObject.SetActive (false);

		//ルーム一覧を取る
		RoomInfo[] rooms = PhotonNetwork.GetRoomList();

		Text text;
		Button button;
		emptyRoomNo = 1;
		if (rooms.Length == 0) {
			for (int i = 0; i < 10; i++) {
				string roomNo = (i + 1).ToString ();
				text = LobyCanvas.transform.Find ("roomName" + roomNo).gameObject.GetComponent<Text> ();
				text.text = "No member";
				button = LobyCanvas.transform.Find ("roomJoinButton" + roomNo).gameObject.GetComponent<Button> ();
				button.interactable = false;
			}
		} else {
			//ルームが1件以上ある時ループでRoomInfo情報をログ出力
			for (int i = 0; i < 10; i++) {
				if ((i + 1) <= rooms.Length) {
					if (rooms [i].name.Substring (12, 1) == (i + 1).ToString()) {
						string roomNo = (i + 1).ToString ();
						text = LobyCanvas.transform.Find ("roomName" + roomNo).gameObject.GetComponent<Text> ();
						if (rooms [i].customProperties ["userName"] != null) {
							text.text = rooms [i].customProperties ["userName"].ToString ();
						}
						button = LobyCanvas.transform.Find ("roomJoinButton" + roomNo).gameObject.GetComponent<Button> ();

						if (rooms [i].PlayerCount < 2) {
							button.interactable = true;
						} else {
							button.interactable = false;
						}
					} else {
						if (emptyRoomNo == 1) {
							emptyRoomNo = i + 1;
						}
						string roomNo = (i + 1).ToString ();
						text = LobyCanvas.transform.Find ("roomName" + roomNo).gameObject.GetComponent<Text> ();
						text.text = "No member";
						button = LobyCanvas.transform.Find ("roomJoinButton" + roomNo).gameObject.GetComponent<Button> ();
						button.interactable = false;
					}
				} else {
					if (emptyRoomNo == 1) {
						emptyRoomNo = i + 1;
					}
					string roomNo = (i + 1).ToString ();
					text = LobyCanvas.transform.Find ("roomName" + roomNo).gameObject.GetComponent<Text> ();
					text.text = "No member";
					button = LobyCanvas.transform.Find ("roomJoinButton" + roomNo).gameObject.GetComponent<Button> ();
					button.interactable = false;
				}
			}
		}
	}

	public void JoinRoom (Text roomNameText) {
		string roomName = "MarathonRoom" + roomNameText.gameObject.name.Substring (8, 1);

		try {
		PhotonNetwork.JoinRoom(roomName);
		} catch (System.Exception e) {
			Debug.Log (e);
		}

		LobyCanvas.gameObject.SetActive (false);
		RoomWaitingCanvas.gameObject.SetActive (true);

		string userName = PlayerPrefs.GetString ("nameKey", null);

		masterName = roomNameText.text;
		clientName = userName;
	}

	//ルーム入室した時に呼ばれるコールバックメソッド
	void OnJoinedRoom () {
		LobyCanvas.gameObject.SetActive (false);
		BattleButton.gameObject.SetActive (false);
		RoomWaitingCanvas.gameObject.SetActive (true);

		MasterNameText.text = masterName;

		if (clientName != null) {
			ClientNameText.text = clientName;
			BattleButton.gameObject.SetActive (true);
		} else {
			ClientNameText.text = "waiting...";
		}

		if (PhotonNetwork.isMasterClient) {
			Player = PhotonNetwork.Instantiate ("OnlineBattle/OnlinePlayer", new Vector3 (-6.9f, -1.1f, 0), Quaternion.identity, 0) as GameObject;
			GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetPlayer", Player);
//			GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetMasterPlayer", Player);
		} else {
			Player = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyPlayer", new Vector3 (6.9f, -1.1f, 0), Quaternion.identity, 0) as GameObject;
//			Player.transform.localScale = new Vector3 (0.1f, 0.1f, 0);
			Player.GetComponent<OnlineBattlePlayerPosition>().SetLeftPosition();
			GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetPlayer", Player);
			_removeSingleGameObject ();
//			Player.GetComponent<OnlineBattlePlayerPosition> ().FindEnemyPlayer ();
//			GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetClientPlayer", Player);
		}
	}

	IEnumerator ChallengerComes () {
		yield return new WaitForSeconds (2);

		ChallengerComesCanvas.gameObject.SetActive (false);
	}

	/// <summary>
	/// 入室に失敗した際に呼ばれます。
	/// </summary>
	/// <param name='codeAndMsg'>
	/// エラー情報
	/// </param>
	void OnPhotonJoinRoomFailed(object[] codeAndMsg) {
		LobyCanvas.gameObject.SetActive (true);
		RoomWaitingCanvas.gameObject.SetActive (false);

		if (codeAndMsg [0].ToString() == "32765") {
			ErrorText.text = "That room is fighting.";
			StartCoroutine (ErrorTextRemove ());
		} else {
			ErrorText.text = "I'm sorry. An error occurred.";
		}
		//codeAndMsg[0]は エラーコード です。(int)
		//codeAndMsg[1]は デバッグメッセージ です。(string)
	}

	IEnumerator ErrorTextRemove () {
		yield return new WaitForSeconds (2);
		ErrorText.text = "";
	}

	/// <summary>
	/// 他のプレイヤーが入室した際に呼ばれます。
	/// </summary>
	/// <param name='player'>
	/// プレイヤー情報
	/// </param>
	void OnPhotonPlayerConnected (PhotonPlayer player) {
		if (singleMode) {
//			Time.timeScale = 0;
			Player.GetComponent<OnlineBattlePlayerController>().SetMutekiMode (true);
			singleEnemyPlayer.GetComponent<OnlineBattlePlayerController> ().SetMutekiMode (true);

			singleMode = false;
			ChallengerComesCanvas.gameObject.SetActive (true);

			Player.gameObject.SetActive (true);
			GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetPlayer", Player);
			Player.GetComponent<OnlineBattlePlayerPosition> ().SetEachEnemyPlayer ();

			singlePlayer.gameObject.SetActive (false);
			singleEnemyPlayer.gameObject.SetActive (false);

			_removeSingleGameObject ();

			StartCoroutine (ChallengerComes ());
		}

		LobyCanvas.gameObject.SetActive (false);
		RoomWaitingCanvas.gameObject.SetActive (true);

		ClientNameText.text = player.name;

		BattleButton.gameObject.SetActive (true);
	}

	void _removeSingleGameObject () {
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("PlayerNormalAttack")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("MarathonHadouken")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("MarathonYogaFire")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Repuuken")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("PlayerTornado")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("thunder")) {
			obj.gameObject.SetActive (false);
		}
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Explosion")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("BombAfter")) {
			obj.gameObject.SetActive (false);
		}

		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("EnemyPlayerNormalAttack")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("EnemyHadouken")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("EnemyYogaFire")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("EnemyRepuuken")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("EnemyPlayerTornado")) {
			obj.gameObject.SetActive (false);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("EnemyThunder")) {
			obj.gameObject.SetActive (false);
		}
	}

	public void BattleExitRoom () {
		if (singleMode) {
			singleMode = false;
			PhotonNetwork.DestroyAll ();
			RoomWaitingCanvas.gameObject.SetActive (false);
			LobyCanvas.gameObject.SetActive (true);
			// ルーム退室
			PhotonNetwork.LeaveRoom ();
			SceneManager.LoadScene ("OnlineBattle"); // masterのobjectが残るためシーン再読み込み
		}

		BattleExitCanvas.gameObject.SetActive (true);
		lang = Application.systemLanguage;

		if (lang == SystemLanguage.Japanese) {
			BattleExitText.text = "退室しますか？ \nあなた負けますよ";
		} else if (lang == SystemLanguage.ChineseSimplified) {
			BattleExitText.text = "Would you like to leave the room? \nYou will lose.";
		} else {
			BattleExitText.text = "你确定要退出？\n你会失去";
		}
	}

	public void BattleExit () {
		int loseCount = PlayerPrefs.GetInt ("OnlineBattleLoseCount", 0);
		PlayerPrefs.SetInt ("OnlineBattleLoseCount", loseCount++);
		PlayerPrefs.Save ();
		ExitRoom ();
	}

	public void BattleExitCancel () {
		BattleExitCanvas.gameObject.SetActive (false);
	}

	// ExitButton click
	public void ExitRoom (bool isReload = true) {
		inRoom = false;
		if (Player != null) {
			if (PhotonNetwork.isMasterClient) {
				PhotonNetwork.DestroyAll ();
				RoomWaitingCanvas.gameObject.SetActive (false);
				LobyCanvas.gameObject.SetActive (true);
				// ルーム退室
				PhotonNetwork.LeaveRoom ();
				if (isReload) {
					SceneManager.LoadScene ("OnlineBattle"); // masterのobjectが残るためシーン再読み込み
				}

				return;
			} else {
				// ルーム退室
				PhotonNetwork.Destroy (Player.gameObject);
				PhotonNetwork.LeaveRoom ();
				RoomWaitingCanvas.gameObject.SetActive (false);
				LobyCanvas.gameObject.SetActive (true);
				if (isReload) {
					SceneManager.LoadScene ("OnlineBattle"); // masterのobjectが残るためシーン再読み込み
				}

				return;
			}
		}

		RoomWaitingCanvas.gameObject.SetActive (false);
		LobyCanvas.gameObject.SetActive (true);

		PhotonNetwork.LeaveRoom ();
		masterName = null;
		clientName = null;

		MasterNameText.text = "";
		ClientNameText.text = "";

	}

	public void AllExitRoom () {
		inRoom = false;
		PhotonNetwork.DestroyAll ();
	}

	/// <summary>
	/// プレイヤーの退室時に呼ばれます。
	/// </summary>
	/// <param name='otherPlayer'>
	/// プレイヤー情報
	/// </param>
	void OnPhotonPlayerDisconnected (PhotonPlayer player) {
		BattleButton.gameObject.SetActive (false);
		string userName = PlayerPrefs.GetString ("nameKey", null);
		int loseCount = PlayerPrefs.GetInt ("OnlineBattleLoseCount", 0);
		int winCount = PlayerPrefs.GetInt ("OnlineBattleWinCount", 0);

		MasterNameText.text = userName;
		MasterWinCountText.text = "Win : " + winCount;
		MasterLoseCountText.text = "Lose : " + loseCount;

		ClientNameText.text = "waiting...";
		ClientWinCountText.text = "";
		ClientLoseCountText.text = "";

		var properties  = new ExitGames.Client.Photon.Hashtable();
		properties.Add("userName", userName);

//		PhotonNetwork.room.name = userName;
		PhotonNetwork.room.Name = userName;
		PhotonNetwork.room.SetCustomProperties(properties);

		if (isBattleStart) {
//			ExitRoom (false);
			EnemyExitRoomCanvas.gameObject.SetActive (true);
		}
	}

	public void EnemyExitRoomOKClick () {
		// ルーム退室
		EnemyExitRoomCanvas.gameObject.SetActive (false);
		RoomWaitingCanvas.gameObject.SetActive (true);
		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.DestroyAll ();
			ExitRoom ();
		}
//		PhotonNetwork.LeaveRoom ();
//		SceneManager.LoadScene ("OnlineBattle");
	}

	public void BattleStart () {
		_removeSingleGameObject ();

		SingleGameStartButton.gameObject.SetActive (false);
		RoomWaitingCanvas.gameObject.SetActive (false);
		isBattleStart = true;
		GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetSingleMode", 2);

		Player.GetComponent<OnlineBattlePlayerController> ().setPlayerStatus ();
	}

	public void BackScene () {
		loadingImage.gameObject.SetActive (true);

		PhotonNetwork.Disconnect ();
//		PhotonNetwork.LeaveLobby ();
		SceneManager.LoadScene ("Difficulty");
	}

	public void PushNotification (Dropdown dropDown) {
		if (isPushSend) {
			return;
		}

		isPushSend = true;
		PushSendingImage.gameObject.SetActive (true);
		PushSendingText.gameObject.SetActive (true);
		PushSendButton.interactable = false;
		PushDropDown.interactable = false;

		DateTime now = DateTime.Now;
		DateTime today = now.Date;
		PlayerPrefs.SetString ("PushSendDate", today.ToString());
		PlayerPrefs.Save ();

		NCMBPush push = new NCMBPush ();
		string userName = PlayerPrefs.GetString ("nameKey", null);
		string message = "Come on ! Let's fight !";

		switch (dropDown.value) {
		case 0:
			message = "Come on ! Let's fight !";
			break;
		case 1:
			message = "Please practice partner.";
			break;
		case 2:
			message = "I am the strongest. Take me.";
			break;
		default:
			message = "Come on ! Let's fight !";
			break;
		}

		push.Message = "PLAYER : " + userName + " :  Online battle room joined !  " + message;
		push.SendPush ();

		StartCoroutine (PushSending ());
	}

	IEnumerator PushSending () {
		yield return new WaitForSeconds (1);
		PushSendingImage.gameObject.SetActive (false);
		PushSendingText.gameObject.SetActive (false);
	}

	public void StartSingleBattle (Dropdown dropDown) {
		if (singleMode) {
			return;
		}

		singleModeDifficulty = dropDown.value;

		singleMode = true;
		Player.gameObject.SetActive (false);

		GameObject player1 = ResourceCache.Load ("OnlineBattle/OnlineSinglePlayer");
		singlePlayer = Instantiate (player1, new Vector3 (-6.9f, -1.1f, 0), Quaternion.identity) as GameObject;
		singlePlayer.GetComponent<OnlineBattlePlayerController> ().setSingleMode (true);
		GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetPlayer", singlePlayer);

		GameObject player2 = ResourceCache.Load ("OnlineBattle/OnlineSingleEnemyPlayer");
		singleEnemyPlayer = Instantiate (player2, new Vector3 (6.9f, -1.1f, 0), Quaternion.identity) as GameObject;
		singleEnemyPlayer.GetComponent<OnlineBattlePlayerController> ().setSingleMode (true);

		GameObject enemyTrigger = ResourceCache.Load ("OnlineBattle/singleEnemyHitTrigger");
		singleEnemyTrigger = Instantiate (enemyTrigger, new Vector3 (6.9f, -1.1f, 0), Quaternion.identity) as GameObject;

		RoomWaitingCanvas.gameObject.SetActive (false);
		SingleGameStartButton.gameObject.SetActive (true);
		isBattleStart = true;
	}

	public void SingleGameStart () {
		SingleGameStartButton.gameObject.SetActive (false);
		int level = PlayerPrefs.GetInt (Constants.PLAYER_LEVEL, 1);
		int playerHp = _getPlayerHp (level);

		string userName = PlayerPrefs.GetString ("nameKey", null);

		singlePlayer.GetComponent<OnlineBattlePlayerPosition> ().SetSingleEnemyPlayer (singleEnemyPlayer);
		singlePlayer.GetComponent<OnlineBattlePlayerCommand> ().SetSingleMode (true);
		singlePlayer.GetComponent<OnlineBattlePlayerController> ().setSingleMode (true);
		singlePlayer.GetComponent<OnlineBattlePlayerController> ().SetEnemyObj (singleEnemyPlayer);
		singlePlayer.GetComponent<OnlineBattlePlayerController> ().SetHitPoint (playerHp);
		singlePlayer.GetComponent<OnlineBattlePlayerController> ().SetPlayerName (userName);
		singlePlayer.GetComponent<OnlineBattlePlayerController> ().BattleStart ();
//		singlePlayer.GetComponent<OnlineBattlePlayerCommand> ().SetSingleMode (true);
		singlePlayer.GetComponent<OnlineBattlePlayerCommand> ().SetCanMoveFlag (true);
		singlePlayer.transform.Find ("HeartProtection").GetComponent<OnlineBattlePlayerProtection> ().SetSingleMode (true);

		GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetSingleMode", 1);
		GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetSingleHostHp", playerHp);
		GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetSingleModeDifficulty", singleModeDifficulty);

		int enemyHp = 0;
		switch (singleModeDifficulty) {
		case 0:
			enemyHp = _getPlayerHp (20);
			break;
		case 1:
			enemyHp = _getPlayerHp (30);
			break;
		case 2:
			enemyHp = _getPlayerHp (50);
			break;
		default:
			enemyHp = _getPlayerHp (20);
			break;
		}

		singleEnemyPlayer.GetComponent<OnlineBattlePlayerPosition> ().SetSingleEnemyPlayer (singlePlayer);
		singleEnemyPlayer.GetComponent<OnlineBattlePlayerCommand> ().SetSingleMode (true);
		singleEnemyPlayer.GetComponent<OnlineBattlePlayerController> ().setSingleMode (true);
		singleEnemyPlayer.GetComponent<OnlineBattlePlayerController> ().SetEnemyObj (singlePlayer);
		singleEnemyPlayer.GetComponent<OnlineBattlePlayerController> ().SetHitPoint (enemyHp);
		singleEnemyPlayer.GetComponent<OnlineBattlePlayerController> ().SetPlayerName ("Computer");
		singleEnemyPlayer.GetComponent<OnlineBattlePlayerController> ().BattleStart ();
		singleEnemyPlayer.GetComponent<OnlineBattlePlayerController> ().SetEnemyOwnPlayer (singleEnemyPlayer); // for enemy
		singleEnemyPlayer.GetComponent<OnlineBattleSingleEnemyPlayer> ().setBattleStart (true, singleModeDifficulty);
		singleEnemyPlayer.transform.Find ("HeartProtection").GetComponent<OnlineBattlePlayerProtection> ().SetSingleMode (true);
		singleEnemyPlayer.transform.Find ("HeartProtection").GetComponent<OnlineBattlePlayerProtection> ().SetSingleEnemy (true);

		GameObject.FindGameObjectWithTag ("OnlineBattleGameController").SendMessage ("SetSingleClientHp", enemyHp);

		singleEnemyTrigger.GetComponent<OnlineBattleSingleEnemyHitTrigger> ().SetEnemyObj (singleEnemyPlayer);
		singleEnemyTrigger.GetComponent<OnlineBattleSingleEnemyHitTrigger> ().SetDifficulty (singleModeDifficulty);
	}

	int _getPlayerHp (int level) {
		int lifePoint = 10000;
		int lifeBunus = (int)System.Math.Truncate ((double)level / 10);
		int lifeBunus2 = level % 10;
		lifePoint += ((lifeBunus * 3000) + lifeBunus2 * 300);

		return lifePoint;
	}

	public void SetBattleStatus (bool flag) {
		isBattleStart = flag;
	}
}
