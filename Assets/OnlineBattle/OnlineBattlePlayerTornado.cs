﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattlePlayerTornado : Photon.MonoBehaviour {
//	float posX;
//	float posY;

	float speedX;
	float speedY;

	int playerPos; // 1:left, 2:right

	// Update is called once per frame
	void Update () {
		if (transform.position.x >= 15 || transform.position.x < -15) {
			this.gameObject.SetActive (false);
			this.GetComponent<OnlineBattleAttackBaseController> ().DestroyObject ();
//			GetComponent<PhotonView> ().RPC ("DestroyOpponentObject", PhotonTargets.Others);
//			PhotonNetwork.Destroy (this.gameObject);
		}

		Vector2 position = transform.position;

		speedX = UnityEngine.Random.Range (-5.0f, 10.0f);
		speedY = UnityEngine.Random.Range (-10.0f, 10.0f);

		if (playerPos == 1) {
			position.x += Time.deltaTime * 2;
		} else if (playerPos == 2) {
			position.x -= Time.deltaTime * 2;
		}

		position.y += Time.deltaTime * speedY;
		transform.position = position;
	}

//	[PunRPC]
//	void DestroyOpponentObject () {
//		this.gameObject.SetActive (false);
//	}

	public void SetPosition (int pos) {
		playerPos = pos;
	}

}
