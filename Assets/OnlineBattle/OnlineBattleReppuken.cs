﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattleReppuken : MonoBehaviour {
	int playerPos; // 1:left, 2:right

	// Update is called once per frame
	void Update () {
		Vector2 position = transform.position;
		if (position.x >= 17 || position.x <= -15) {
			this.gameObject.SetActive (false);
			this.GetComponent<OnlineBattleAttackBaseController> ().DestroyObject ();
		}

		if (playerPos == 1) {
//			position.x += Time.deltaTime * 2;
			position.x += Time.deltaTime * 8;
		} else if (playerPos == 2) {
//			position.x -= Time.deltaTime * 2;
			position.x -= Time.deltaTime * 8;
		}
		transform.position = position;
	}

	public void SetPosition (int pos) {
		playerPos = pos;
	}
}
