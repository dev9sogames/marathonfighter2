﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnlineBattlePlayerController : Photon.MonoBehaviour {
	bool singleMode;
	public bool singleEnemy;

	Rigidbody2D rgbd;
	PhotonView photonView;
	Animator animator;

	GameObject enemyPlayer;
	GameObject enemyOwnPlayer;
	OnlineBattlePlayerPosition positionController;
	OnlineBattlePlayerCommand command;

	GameObject gameController;

	public Text damageText;

	bool isMuteki;
	bool isHarite;

	/*
	 * プレイヤーステータス関連
	 * 
	 * 
	 */
	int lifePoint;
	public Text nameText;
	public Slider hpSlider;
	public Text hpText;

	int HostHp;
	int ClientHp;
	int MaxHp;

	bool isJumping;
	bool isKick;
	bool isUpper;
	int jumpCount;
	int jumpLimit;
	public int JumpDownVelocity;
	public int jumpPower;

	enum BattleStatus {
		none,
		battle,
		win,
		lose
	}
	BattleStatus battleStatus;

	/*
	 * 攻撃系
	 * 
	 * 
	 */
	int _pointRemainCount;
	bool isAttack;
	bool isNormalAttack;
	int hadoukenUserFlag;
	int yogaFireUseFlag;
	int kickUseFlag;
	int upperUseFlag;
	int jumpFireUseFlag;
	int reppukenUseFlag;

	bool isRolling;
	int homingAttackType; // 1:hadouken, 2:yogafire

	Vector3 rollingBeforePos;

	bool isScrewDriver;
	bool hitScrewDriver;
	bool getScrewDriver;
	bool screwDriverFalling;

	bool isLethalWeapon;
	bool hitLethalWeapon;
	bool canLethalWeapon;

	bool isHeartProtection;
	bool isThunder;

	/*
	 * For Bomb throw
	 */
	Dictionary<int, GameObject> bombList;

	bool canHeading;

	/*
	 * GameObject各種
	 * 
	 * 
	 */
	GameObject hadoukenObj;
	GameObject YogafireObj;
	GameObject reppukenObj;
	GameObject OnoObj;
	GameObject TornadoObj;
	GameObject ThunderObj;

	GameObject childKick;
	GameObject childUpper;
	GameObject childDefenseImage;
	GameObject childHarite;
	GameObject childBombHeading;

	// Use this for initialization
	void Start () {
		isMuteki = true;
		gameController = GameObject.FindGameObjectWithTag ("OnlineBattleGameController");
//		gameController.SendMessage ("UnSetBattleStatusText");

		// get component
		rgbd = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		positionController = GetComponent<OnlineBattlePlayerPosition> ();
		photonView = GetComponent<PhotonView> ();
		command = GetComponent<OnlineBattlePlayerCommand> ();

		// 子要素
		childKick = transform.Find ("playerKick").gameObject;
//		childKick.GetComponent<OnlineBattleAttackBaseController> ().SetPlayerPhotonViewId (photonView.ownerId);
//		childKick.SetActive (false);

		childUpper = transform.Find ("playerUpper").gameObject;
//		childUpper.GetComponent<OnlineBattleAttackBaseController> ().SetPlayerPhotonViewId (photonView.ownerId);
//		childUpper.SetActive (false);

		childHarite = transform.Find ("playerHarite").gameObject;
		childHarite.GetComponent<SpriteRenderer> ().enabled = false;

		childDefenseImage = transform.Find ("defenseImage").gameObject;
		childDefenseImage.SetActive (false);

		childBombHeading = transform.Find ("playerBombHeading").gameObject;
		childBombHeading.SetActive (false);
		bombList = new Dictionary<int, GameObject> ();

		// set player status
		battleStatus = BattleStatus.none;
		_setPlayerName ();

		_pointRemainCount = 100000;
		jumpLimit = 3;
		jumpCount = 0;

		// action
		yogaFireUseFlag = 1;
		hadoukenUserFlag = 1;
		kickUseFlag = 1;
		upperUseFlag = 1;
		jumpFireUseFlag = 1;
		reppukenUseFlag = 1;

		canLethalWeapon = false;

		command.SetCanMoveFlag (false);

//		if (!PhotonNetwork.isMasterClient) {
//			gameController.SendMessage ("SetEnemy");
//		}
	}

	public void setPlayerStatus () {
		isMuteki = false;

		string userName = PlayerPrefs.GetString ("nameKey", null);

		int level = PlayerPrefs.GetInt (Constants.PLAYER_LEVEL, 1);
		AllGameDefinition.level = level;

		lifePoint = 10000;
		int lifeBunus = (int)System.Math.Truncate ((double)level / 10);
		int lifeBunus2 = level % 10;
		lifePoint += ((lifeBunus * 3000) + lifeBunus2 * 300);

		if (!singleMode) {
			if (PhotonNetwork.isMasterClient) {
				photonView.RPC ("SetMasterStatus", PhotonTargets.Others, lifePoint);
			} else {
				photonView.RPC ("SetClientStatus", PhotonTargets.Others, lifePoint);
			}
		}
	}

	[PunRPC]
	void SetMasterStatus (int masterHp) {
		int level = PlayerPrefs.GetInt (Constants.PLAYER_LEVEL, 1);
		AllGameDefinition.level = level;

		lifePoint = 10000;
		int lifeBunus = (int)System.Math.Truncate ((double)level / 10);
		int lifeBunus2 = level % 10;
		lifePoint += ((lifeBunus * 3000) + lifeBunus2 * 300);

		gameController = GameObject.FindGameObjectWithTag ("OnlineBattleGameController");
		gameController.SendMessage ("SetHostHp", masterHp);
		gameController.SendMessage ("SetClientHp", lifePoint);
	}

	[PunRPC]
	void SetClientStatus (int clientHp) {
		int level = PlayerPrefs.GetInt (Constants.PLAYER_LEVEL, 1);
		AllGameDefinition.level = level;

		lifePoint = 10000;
		int lifeBunus = (int)System.Math.Truncate ((double)level / 10);
		int lifeBunus2 = level % 10;
		lifePoint += ((lifeBunus * 3000) + lifeBunus2 * 300);

		gameController = GameObject.FindGameObjectWithTag ("OnlineBattleGameController");
		gameController.SendMessage ("SetHostHp", lifePoint);
		gameController.SendMessage ("SetClientHp", clientHp);
	}

	void _setPlayerName () {
		if (singleMode) {
			return;
		}

		string userName = PlayerPrefs.GetString ("nameKey", null);
		if (photonView.isMine) {
			photonView.RPC ("SetEnemyName", PhotonTargets.AllBuffered, userName);
		}

		int winCount = PlayerPrefs.GetInt ("OnlineBattleWinCount", 0);
		int loseCount = PlayerPrefs.GetInt ("OnlineBattleLoseCount", 0);

		if (PhotonNetwork.isMasterClient) {
			photonView.RPC ("SetMasterPlayerBattleCount",PhotonTargets.AllBuffered, winCount, loseCount);
		} else {
			photonView.RPC ("SetClientPlayerBattleCount",PhotonTargets.AllBuffered, winCount, loseCount);
		}
	}

	[PunRPC]
	void SetMasterPlayerBattleCount (int winCount, int loseCount) {
		int[] count = {winCount, loseCount};

		if (gameController == null) {
			gameController = GameObject.FindGameObjectWithTag ("OnlineBattleGameController");
		}
		gameController.SendMessage ("SetMasterPlayerBattleCount", count);
	}

	[PunRPC]
	void SetClientPlayerBattleCount (int winCount, int loseCount) {
		int[] count = {winCount, loseCount};
		if (gameController == null) {
			gameController = GameObject.FindGameObjectWithTag ("OnlineBattleGameController");
		}
		gameController.SendMessage ("SetClientPlayerBattleCount", count);
	}

	[PunRPC]
	void SetEnemyName (string userName) {
		nameText.text = userName;
	}

	void Update () {
		if (singleEnemy) {
			return;
		}

		if (!(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)) {
			if (Input.GetKeyDown ("space")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					JumpPlayerUp ();
				}
			}
			if (Input.GetKeyDown ("a")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					hadouken ();
				}
			}
			if (Input.GetKeyDown ("b")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					yogaFire ();
				}
			}
			if (Input.GetKeyDown ("v")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					Kick ();
				}
			}
			if (Input.GetKeyDown ("f")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					upper ();
				}
			}
			if (Input.GetKeyDown ("r")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					RollingAttack ();
				}
			}
			if (Input.GetKeyDown ("t")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					TornadoAppear ();
				}
			}
			if (Input.GetKeyDown ("n")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					normalAttack ();
				}
			}
			if (Input.GetKeyDown ("d")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					hadouken ();
					DoubleAttack ();
				}
			}
			if (Input.GetKeyDown ("l")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					SetBomb ();
				}
			}
			if (Input.GetKeyDown ("s")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					screwDriver ();
				}
			}
			if (Input.GetKeyDown ("h")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					heartProtection ();
				}
			}
			if (Input.GetKeyDown ("j")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					JumpFire ();
				}
			}
			if (Input.GetKeyDown ("u")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					thunder ();
				}
			}
			if (Input.GetKeyDown ("p")) {
				if (singleMode || GetComponent<PhotonView> ().isMine) {
					LethalWeapon ();
				}
			}
			if (Input.GetKeyDown ("r")) {
				Attack ();
			}

			if (Input.GetKeyDown ("g")) {
				reppuken ();
			}
			if (Input.GetKeyDown ("up")) {
				command.UpPushDown ();
			}
			if (Input.GetKeyUp ("up")) {
				command.UpPushUp ();
			}
			if (Input.GetKeyDown ("down")) {
				command.BottomPushDown ();
			}
			if (Input.GetKeyUp ("down")) {
				command.BottomPushUp ();
			}
			if (Input.GetKeyDown ("left")) {
				command.LeftPushDown ();
			}
			if (Input.GetKeyUp ("left")) {
				command.LeftPushUp ();
			}
			if (Input.GetKeyDown ("right")) {
				command.RightPushDown ();
			}
			if (Input.GetKeyUp ("right")) {
				command.RightPushUp ();
			}
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (battleStatus != BattleStatus.battle) {
			return;
		}

		if (isRolling) {
			return;
		}

		if (screwDriverFalling) {
			transform.Rotate(new Vector3(0, 30, 00));
		}

		if (!screwDriverFalling && (hitScrewDriver || getScrewDriver) && transform.position.y > 10) {
//			hitScrewDriver = false;
//			getScrewDriver = false;
			screwDriverFalling = true;
		}

		if (screwDriverFalling && transform.position.y < -1) {
			screwDriverFalling = false;
			if (!singleMode) {
				photonView.RPC ("FinishScrewDriverJump", PhotonTargets.All);
			} else {
				FinishSingleScrewDriverJump ();
				enemyPlayer.GetComponent<OnlineBattlePlayerController> ().FinishSingleScrewDriverJump ();
			}
		}

		if (isLethalWeapon) {
			Vector2 position = transform.position;

			if (positionController.GetPlayerPosition () == 1) {
				position.x += Time.deltaTime * 10;
			} else {
				position.x -= Time.deltaTime * 10;
			}
			transform.position = position;
		}

		float velocityY = rgbd.velocity.y;
		if (velocityY != 0) {
			isJumping = true;
			//		} else if (transform.position.y < -3.2f) {
		} else {
			if (jumpCount != 0 && !isKick) {
				isJumping = false;
				jumpCount = 0;
				if (canHeading) {
					canHeading = false;
					_bombHeadingEnabled (false);
//					Debug.Log ("1");
				}
			}
		}
		if (isKick && velocityY == 0) {
			StartCoroutine (jumpKickFinish ());
		}
		if (isUpper && velocityY < 0) {
			isMuteki = false;
			isUpper = false;
			if (!singleMode) {
				photonView.RPC ("UnsetColliderTrigger", PhotonTargets.Others, 2);
			} else {
				UnsetSingleColliderTrigger (2);
			}
			command.SetCanMoveFlag (true);
			animator.SetBool ("upper", false);
//			childUpper.SetActive (false);
//			GameObject tornado;

			Vector3 itemPos = transform.position;
			if (positionController.GetPlayerPosition () == 1) { // left
				itemPos.x = itemPos.x + 1.0f;
			} else {
				itemPos.x = itemPos.x - 1.0f;
			}
			if (singleEnemy) {
				TornadoObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyTornado", itemPos, Quaternion.identity, 0) as GameObject;
			} else {
				if (PhotonNetwork.isMasterClient) {
					TornadoObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineTornado", itemPos, Quaternion.identity, 0) as GameObject;
				} else {
					TornadoObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyTornado", itemPos, Quaternion.identity, 0) as GameObject;
				}
			}
			TornadoObj.GetComponent<OnlineBattlePlayerTornado> ().SetPosition (positionController.GetPlayerPosition ());
			if (!singleMode) {
				TornadoObj.GetComponent<OnlineBattleAttackBaseController> ().SetPlayerPhotonViewId (photonView.ownerId);
			} else {
				TornadoObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleMode (true);
				if (singleEnemy) {
					TornadoObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleEnemy (true);
				}
			}
		}

	}

	[PunRPC]
	void UnsetColliderTrigger (int attackType) {
		switch (attackType) {
		case 1:
			transform.Find ("playerKick").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 2:
			transform.Find ("playerUpper").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 3:
			transform.Find ("playerRollingAttack").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 4:
			transform.Find ("playerScrewDriver").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 5:
			transform.Find ("HeartProtection").gameObject.GetComponent<CircleCollider2D> ().enabled = false;
			transform.Find ("HeartProtection").gameObject.GetComponent<SpriteRenderer> ().enabled = false;
			break;
		case 6:
			transform.Find ("playerScrewDriverGet").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 7:
			transform.Find ("playerHarite").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 8:
			transform.Find ("playerBombThrow").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 9:
			transform.Find ("playerLethalWeapon").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		default:
			break;
		}
	}

	void UnsetSingleColliderTrigger (int attackType) {
		switch (attackType) {
		case 1:
			transform.Find ("playerKick").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 2:
			transform.Find ("playerUpper").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 3:
			transform.Find ("playerRollingAttack").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 4:
			transform.Find ("playerScrewDriver").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 5:
			transform.Find ("HeartProtection").gameObject.GetComponent<CircleCollider2D> ().enabled = false;
			transform.Find ("HeartProtection").gameObject.GetComponent<SpriteRenderer> ().enabled = false;
			break;
		case 6:
			transform.Find ("playerScrewDriverGet").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 7:
			transform.Find ("playerHarite").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 8:
			transform.Find ("playerBombThrow").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		case 9:
			transform.Find ("playerLethalWeapon").gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			break;
		default:
			break;
		}
	}

	IEnumerator jumpKickFinish () {
		yield return new WaitForSeconds (0.5f);
		isMuteki = false;
		isKick = false;
		if (!singleMode) {
			photonView.RPC ("UnsetColliderTrigger", PhotonTargets.Others, 1);
		} else {
			UnsetSingleColliderTrigger (1);
		}
		command.SetCanMoveFlag (true);
		animator.SetBool("kick", false);
//		childKick.SetActive (false);
	}

	public void JumpPlayerUp () {
		if (jumpCount < jumpLimit || transform.position.y < -5) {
			float velocity = rgbd.velocity.y;
			if (velocity > 0) {
				rgbd.velocity = new Vector3 (rgbd.velocity.x, velocity * 0.6f, 0);
				if (!canHeading) {
					_bombHeadingEnabled (true);
					canHeading = true;
//					Debug.Log ("0");
				}
			}
		}
	}

	void _bombHeadingEnabled (bool flag) {
		childBombHeading.SetActive (flag);
	}

	public void JumpPlayerDown () {
		if (jumpCount < jumpLimit || transform.position.y < -5) {
			switch (jumpCount) {
			case 0:
				if (rgbd.velocity.y < 1) {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, 0.7f, 0);
				} else {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, JumpDownVelocity, 0);
				}
				rgbd.AddForce (Vector2.up * jumpPower);
				break;
			case 1:
				if (rgbd.velocity.y < 1) {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, 0.7f, 0);
				} else {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, JumpDownVelocity, 0);
				}
				rgbd.AddForce (Vector2.up * jumpPower * 0.7f);
				break;
			case 2:
				if (rgbd.velocity.y < 1) {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, 0.7f, 0);
				} else {
					rgbd.velocity = new Vector3 (rgbd.velocity.x, JumpDownVelocity, 0);
				}
				rgbd.AddForce (Vector2.up * jumpPower * 0.7f);
				break;
			}
			jumpCount++;
		}
	}

	public void Attack () {
		if (battleStatus != BattleStatus.battle) {
			return;
		}

		if (isHarite) {
			return;
		}

		switch (command.buttonPos) {
		case OnlineBattlePlayerCommand.ButtonPos.bottomRight:
			if (positionController.GetPlayerPosition() == 1) { // left hadouken
				_pointRemainCount = _pointRemainCount - 1;
				hadouken ();
			} else if (positionController.GetPlayerPosition() == 2) { // right kick
				if (_pointRemainCount < 3) {
					return;
				}
				Kick ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.bottomLeft:
			if (positionController.GetPlayerPosition() == 2) { // right hadouken
				_pointRemainCount = _pointRemainCount - 1;
				hadouken ();
			} else if (positionController.GetPlayerPosition() == 1) { // left kick
				if (_pointRemainCount < 3) {
					return;
				}
				Kick ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.longBottomUp:
			if (_pointRemainCount < 10) {
				return;
			}
			TornadoAppear ();
			break;
		case OnlineBattlePlayerCommand.ButtonPos.longLeftRight:
			if (positionController.GetPlayerPosition() == 1) { // left rolling
				if (_pointRemainCount < 10) {
					return;
				}
				RollingAttack ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.longRightLeft:
			if (positionController.GetPlayerPosition() == 2) { // right rolling
				if (_pointRemainCount < 10) {
					return;
				}
				RollingAttack ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.leftbottomRight:
			if (positionController.GetPlayerPosition() == 1) { // left yogafire
				if (_pointRemainCount < 5) {
					return;
				}
				_pointRemainCount = _pointRemainCount - 5;
				yogaFire ();
			}
			return;
		case OnlineBattlePlayerCommand.ButtonPos.rightbottomLeft:
			if (positionController.GetPlayerPosition() == 2) { // right yogafire
				if (_pointRemainCount < 5) {
					return;
				}
				_pointRemainCount = _pointRemainCount - 5;
				yogaFire ();
			}
			return;
		case OnlineBattlePlayerCommand.ButtonPos.upBottom:
			if (_pointRemainCount < 3) {
				return;
			}
			upper ();
			break;
		case OnlineBattlePlayerCommand.ButtonPos.rightbottomRight:
			if (positionController.GetPlayerPosition () == 1) { // left jump fire
				if (_pointRemainCount < 5) {
					return;
				}
				JumpFire ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.leftbottomLeft:
			if (positionController.GetPlayerPosition () == 2) { // right jump fire
				if (_pointRemainCount < 5) {
					return;
				}
				JumpFire ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.leftRight:
			if (positionController.GetPlayerPosition() == 1) { // left bomb
				SetBomb ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.rightLeft:
				if (positionController.GetPlayerPosition() == 2) { // right bomb
				SetBomb ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.upRightBottomLeftUp:
			if (positionController.GetPlayerPosition () == 1) { // left screw driver
				screwDriver ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.upLeftBottomRightUp:
			if (positionController.GetPlayerPosition () == 2) { // right screw driver
				screwDriver ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.longLeftBottomRightUpLeft:
			if (positionController.GetPlayerPosition () == 1) { // left heart protection
				heartProtection ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.longRightBottomLeftUpRight:
			if (positionController.GetPlayerPosition () == 2) { // right heart protection
				heartProtection ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.rightBottomRightBottom:
			if (positionController.GetPlayerPosition () == 2) { // right thunder
				thunder ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.leftBottomLeftBottom:
			if (positionController.GetPlayerPosition () == 1) { // right thunder
				thunder ();
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.rightLeftBottomright:
			if (!canLethalWeapon) {
				return;
			}

			if (positionController.GetPlayerPosition () == 1) { // right lethal weapon
				LethalWeapon ();
				if (PhotonNetwork.isMasterClient) {
					gameController.SendMessage ("UseMasterLethalWeapon");
				} else {
					gameController.SendMessage ("UseClientLethalWeapon");
				}
				canLethalWeapon = false;
			}
			break;
		case OnlineBattlePlayerCommand.ButtonPos.leftRightBottomLeft:
			if (!canLethalWeapon) {
				return;
			}

			if (positionController.GetPlayerPosition () == 2) { // left lethal weapon
				LethalWeapon ();
				if (PhotonNetwork.isMasterClient) {
					gameController.SendMessage ("UseMasterLethalWeapon");
				} else {
					gameController.SendMessage ("UseClientLethalWeapon");
				}
				canLethalWeapon = false;
			}
			break;
//		case ButtonPos.longLeftBottomRightUpLeft:
//			if (_pointRemainCount < 100 || isMuteki) {
//				return;
//			}
//			HeartProtection ();
//			break;
		default:
			normalAttack ();
			break;
		}

		StartCoroutine ("StopAttack");
		isAttack = true;
	}

	void thunder () {
		if (isThunder) {
			return;
		}
		isThunder = true;
		Vector3 itemPos = new Vector3 (enemyPlayer.transform.position.x, enemyPlayer.transform.position.y + 5, 0);

		if (singleEnemy) {
			ThunderObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyThunder", itemPos, Quaternion.identity, 0) as GameObject;
		} else {
			if (PhotonNetwork.isMasterClient) {
				ThunderObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineThunder", itemPos, Quaternion.identity, 0) as GameObject;
			} else {
				ThunderObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyThunder", itemPos, Quaternion.identity, 0) as GameObject;
			}
		}
		if (singleMode) {
			ThunderObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleMode (true);
			if (singleEnemy) {
				ThunderObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleEnemy (true);
			}
		}
		_setAttackAnimation ();
		StartCoroutine (FinishThunder ());
	}
	IEnumerator FinishThunder () {
		yield return new WaitForSeconds (3);
		isThunder = false;
	}

	void heartProtection () {
		isHeartProtection = true;
		if (!singleMode) {
			photonView.RPC ("SetCollierTrigger", PhotonTargets.All, 5);
		} else {
			SetSingleCollierTrigger (5);
		}
	}

	public void screwDriver () {
		if (isMuteki) {
			return;
		}

		if (isScrewDriver) {
			return;
		}
		isScrewDriver = true;
		command.SetCanMoveFlag (false);
		if (!singleMode) {
			photonView.RPC ("SetCollierTrigger", PhotonTargets.Others, 4);
		} else {
			SetSingleCollierTrigger (4);
		}
		StartCoroutine (FinishScrewDriver ());
		if (positionController.GetPlayerPosition () == 1) { // left
			rgbd.AddForce (Vector2.right * 200);
		} else { // right
			rgbd.AddForce (Vector2.left * 200);
		}
	}

	IEnumerator FinishScrewDriver () {
		yield return new WaitForSeconds (0.2f);

		isScrewDriver = false;
		command.SetCanMoveFlag (true);
		if (!singleMode) {
			photonView.RPC ("UnsetColliderTrigger", PhotonTargets.Others, 4);
			photonView.RPC ("UnsetColliderTrigger", PhotonTargets.Others, 6);
		} else {
			UnsetSingleColliderTrigger (4);
			UnsetSingleColliderTrigger (6);
		}
	}

	void SetBomb () {
		GameObject bomb;
		Rigidbody2D bombRgbd;
		Vector3 bombPos = transform.position;
		if (positionController.GetPlayerPosition () == 1) { // left
			bombPos.x = transform.position.x + 1;
		} else {
			bombPos.x = transform.position.x - 1;
		}

		bomb = PhotonNetwork.Instantiate ("OnlineBattle/OnlineBomb", bombPos, Quaternion.identity, 0) as GameObject;
		bombRgbd = bomb.GetComponent<Rigidbody2D> ();
		bombRgbd.AddForce (Vector2.up * 250);
		if (positionController.GetPlayerPosition () == 1) { // left
			bombRgbd.AddForce (Vector2.right * 200);
		} else { // right
			bombRgbd.AddForce (Vector2.left * 200);
		}
	}

	public void DoubleAttack () {
		if (homingAttackType == 1) {
			if (hadoukenObj != null) {
				hadoukenObj.GetComponent<OnlineBattlePlayerHadouken> ().SetHomingObj (enemyPlayer);
			} else { // search hadoukenObj
//				StartCoroutine (SerchHadouken ());
			}
		} else if (homingAttackType == 2) {
			if (YogafireObj != null) {
				YogafireObj.GetComponent<OnlineBattlePlayerYogafire>().SetHomingObj (enemyPlayer);
			} else { // search YogafireObj
			}
		}
	}
//	IEnumerator SerchHadouken () {
//		while (hadoukenUserFlag == 0) {
//			yield return new WaitForSeconds (0.1f);
//			if (hadoukenObj != null) {
//				hadoukenObj.GetComponent<OnlineBattlePlayerHadouken> ().SetHomingObj (enemyPlayer);
//				yield break;
//			}
//		}
//	}
	IEnumerator StopAttack () {
		yield return new WaitForSeconds (1);
		isAttack = false;
	}

	void normalAttack () {
		if (isNormalAttack) {
			return;
		}
		isNormalAttack = true;
		StartCoroutine (canAttackOno ());
		Vector3 itemPos = transform.position;

		if (positionController.GetPlayerPosition () == 1) { // left
			itemPos.x = itemPos.x + 1.0f;
		} else { // right
			itemPos.x = itemPos.x - 1.0f;
		}

//		GameObject ono;
		if (singleEnemy) {
			OnoObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyOno", itemPos, Quaternion.identity, 0) as GameObject;
		} else {
			if (PhotonNetwork.isMasterClient) {
				OnoObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineOno", itemPos, Quaternion.identity, 0) as GameObject;
			} else {
				OnoObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyOno", itemPos, Quaternion.identity, 0) as GameObject;
			}
		}
		OnoObj.GetComponent<OnlineBattlePlayerNormalAttack> ().SetPosition (positionController.GetPlayerPosition ());
		if (!singleMode) {
			OnoObj.GetComponent<OnlineBattleAttackBaseController> ().SetPlayerPhotonViewId (photonView.ownerId);
		} else {
			OnoObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleMode (true);
			if (singleEnemy) {
				OnoObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleEnemy (true);
			}
		}
		_setAttackAnimation ();
	}

	void _setAttackAnimation () {
		animator.SetBool ("attack", true);
		StartCoroutine (FinishAttackAnimation ());
	}

	IEnumerator FinishAttackAnimation () {
		yield return new WaitForSeconds (0.5f);
		animator.SetBool ("attack", false);
	}
	IEnumerator canAttackOno () {
		yield return new WaitForSeconds (0.8f);
		isNormalAttack = false;
	}

	void hadouken () {
		if (jumpFireUseFlag == 1 && hadoukenUserFlag != 1) {
			return;
		}
		hadoukenUserFlag = 0;
		homingAttackType = 1;
		StartCoroutine (canHadouken ());
		Vector3 itemPos = transform.position;
		Vector3 itemScale;
		if (positionController.GetPlayerPosition () == 1) { // left
			itemPos.x = itemPos.x + 1.0f;
			itemScale = new Vector3 (1,1,0);
		} else {
			itemPos.x = itemPos.x - 1.0f;
			itemScale = new Vector3 (-1,1,0);
		}

		if (singleEnemy) {
			hadoukenObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyHadouken", itemPos, Quaternion.identity, 0) as GameObject;
			hadoukenObj.transform.localScale = itemScale;
		} else {
			if (PhotonNetwork.isMasterClient) {
				hadoukenObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineHadouken", itemPos, Quaternion.identity, 0) as GameObject;
				hadoukenObj.transform.localScale = itemScale;
			} else {
				hadoukenObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyHadouken", itemPos, Quaternion.identity, 0) as GameObject;
				hadoukenObj.transform.localScale = itemScale;
			}
		}
		hadoukenObj.GetComponent<OnlineBattlePlayerHadouken> ().SetPosition (positionController.GetPlayerPosition ());
		if (!singleMode) {
			hadoukenObj.GetComponent<OnlineBattleAttackBaseController> ().SetPlayerPhotonViewId (photonView.ownerId);
		} else {
			hadoukenObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleMode (true);
			if (singleEnemy) {
				hadoukenObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleEnemy (true);
			}
		}
		_setAttackAnimation ();
	}

	void yogaFire () {
		if (yogaFireUseFlag != 1) {
			return;
		}
		yogaFireUseFlag = 0;
		homingAttackType = 2;
		StartCoroutine (canYogaFire ());
		Vector3 itemPos = transform.position;
		Vector3 itemScale;
		if (positionController.GetPlayerPosition () == 1) { // left
			itemPos.x = itemPos.x + 1.0f;
			itemScale = new Vector3 (1,1,0);
		} else {
			itemPos.x = itemPos.x - 1.0f;
			itemScale = new Vector3 (-1,1,0);
		}

		if (singleEnemy) {
			YogafireObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyYogafire", itemPos, Quaternion.identity, 0) as GameObject;
			YogafireObj.transform.localScale = itemScale;
		} else {
			if (PhotonNetwork.isMasterClient) {
				YogafireObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineYogafire", itemPos, Quaternion.identity, 0) as GameObject;
				YogafireObj.transform.localScale = itemScale;
			} else {
				YogafireObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyYogafire", itemPos, Quaternion.identity, 0) as GameObject;
				YogafireObj.transform.localScale = itemScale;
			}
		}
		YogafireObj.GetComponent<OnlineBattlePlayerYogafire> ().SetPosition (positionController.GetPlayerPosition ());
		if (!singleMode) {
			YogafireObj.GetComponent<OnlineBattleAttackBaseController> ().SetPlayerPhotonViewId (photonView.ownerId);
		} else {
			YogafireObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleMode (true);
			if (singleEnemy) {
				YogafireObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleEnemy (true);
			}
		}
		_setAttackAnimation ();
	}

	void reppuken () {
		if (reppukenUseFlag != 1) {
			return;
		}
		reppukenUseFlag = 0;
		StartCoroutine (canReppuken ());
		Vector3 itemPos = transform.position;
		Vector3 itemScale;
		if (positionController.GetPlayerPosition () == 1) { // left
			itemPos.x = itemPos.x + 1.0f;
			itemScale = new Vector3 (1.5f,1,0);
		} else {
			itemPos.x = itemPos.x - 1.0f;
			itemScale = new Vector3 (-1.5f,1,0);
		}

		if (singleEnemy) {
			reppukenObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyReppuken", itemPos, Quaternion.identity, 0) as GameObject;
			reppukenObj.transform.localScale = itemScale;
		} else {
			if (PhotonNetwork.isMasterClient) {
				reppukenObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineReppuken", itemPos, Quaternion.identity, 0) as GameObject;
				reppukenObj.transform.localScale = itemScale;
			} else {
				reppukenObj = PhotonNetwork.Instantiate ("OnlineBattle/OnlineEnemyReppuken", itemPos, Quaternion.identity, 0) as GameObject;
				reppukenObj.transform.localScale = itemScale;
			}
		}
		reppukenObj.GetComponent<OnlineBattleReppuken> ().SetPosition (positionController.GetPlayerPosition ());
		if (!singleMode) {
			reppukenObj.GetComponent<OnlineBattleAttackBaseController> ().SetPlayerPhotonViewId (photonView.ownerId);
		} else {
			reppukenObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleMode (true);
			if (singleEnemy) {
				reppukenObj.GetComponent<OnlineBattleAttackBaseController> ().SetSingleEnemy (true);
			}
		}
		_setAttackAnimation ();
	}
	IEnumerator canHadouken () {
		yield return new WaitForSeconds (1.0f);
		hadoukenUserFlag = 1;
	}
	IEnumerator canYogaFire () {
		yield return new WaitForSeconds (1.5f);
		yogaFireUseFlag = 1;
	}
	IEnumerator canReppuken () {
		yield return new WaitForSeconds (1.5f);
		reppukenUseFlag = 1;
	}

	void Kick () {
		if (kickUseFlag != 1) {
			return;
		}

		if (!isMuteki && transform.position.y > 0) {
			isKick = true;
			command.SetCanMoveFlag (false);
			if (!singleMode) {
				photonView.RPC ("SetCollierTrigger", PhotonTargets.Others, 1);
			} else {
				SetSingleCollierTrigger (1);
			}
			if (positionController.GetPlayerPosition () == 1) { // left
				rgbd.AddForce (Vector2.right * 100);
			} else { // right
				rgbd.AddForce (Vector2.left * 100);
			}
			rgbd.velocity = Vector3.zero;
			rgbd.AddForce (Vector2.down * 200);
			animator.SetBool ("kick", true);
			isMuteki = true;
//			childKick.SetActive (true);
			_pointRemainCount = _pointRemainCount - 3;
		} else {
			if (_pointRemainCount < 3) {
				return;
			}
			_pointRemainCount = _pointRemainCount - 3;
			reppuken ();
		}
	}

	public void HariteAttack (bool flag) {
		isHarite = flag;
		if (flag) {
			if (!singleMode) {
				photonView.RPC ("SetCollierTrigger", PhotonTargets.Others, 7);
				photonView.RPC ("SetHariteSpriteRendere", PhotonTargets.Others);
			} else {
				SetSingleCollierTrigger (7);
			}
			childHarite.GetComponent<SpriteRenderer> ().enabled = true;
		} else {
			if (!singleMode) {
				photonView.RPC ("UnsetColliderTrigger", PhotonTargets.Others, 7);
				photonView.RPC ("UnSetHariteSpriteRendere", PhotonTargets.Others);
			} else {
				UnsetSingleColliderTrigger (7);
			}
			childHarite.GetComponent<SpriteRenderer> ().enabled = false;
		}
	}

	[PunRPC]
	void SetHariteSpriteRendere () {
		transform.Find ("playerHarite").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
	}

	[PunRPC]
	void UnSetHariteSpriteRendere () {
		transform.Find ("playerHarite").gameObject.GetComponent<SpriteRenderer> ().enabled = false;
	}

	[PunRPC]
	void SetCollierTrigger (int attackType) { // 1:kick, 2:upper, 3:rolling, 4:screwDriverHit, 5:heartProtection, 6:screwDriverGet
		switch (attackType) {
		case 1:
			transform.Find ("playerKick").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 2:
			transform.Find ("playerUpper").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 3:
			transform.Find ("playerRollingAttack").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 4:
			transform.Find ("playerScrewDriver").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 5:
			transform.Find ("HeartProtection").gameObject.GetComponent<CircleCollider2D> ().enabled = true;
			transform.Find ("HeartProtection").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			break;
		case 6:
			transform.Find ("playerScrewDriverGet").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 7:
			transform.Find ("playerHarite").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 8: // not use
			transform.Find ("playerBombThrow").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 9:
			transform.Find ("playerLethalWeapon").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		default:
			break;
		}
	}

	void SetSingleCollierTrigger (int attackType) { // 1:kick, 2:upper, 3:rolling, 4:screwDriverHit, 5:heartProtection, 6:screwDriverGet
		switch (attackType) {
		case 1:
			transform.Find ("playerKick").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 2:
			transform.Find ("playerUpper").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 3:
			transform.Find ("playerRollingAttack").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 4:
			transform.Find ("playerScrewDriver").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 5:
			transform.Find ("HeartProtection").gameObject.GetComponent<CircleCollider2D> ().enabled = true;
			transform.Find ("HeartProtection").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			break;
		case 6:
			transform.Find ("playerScrewDriverGet").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 7:
			transform.Find ("playerHarite").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 8:
			transform.Find ("playerBombThrow").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		case 9:
			transform.Find ("playerLethalWeapon").gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			break;
		default:
			break;
		}
	}

	void upper () {
		if (upperUseFlag != 1) {
			return;
		}
		if (!isMuteki && rgbd.velocity.y == 0) {
			isUpper = true;

			command.SetCanMoveFlag (false);
			if (!singleMode) {
				photonView.RPC ("SetCollierTrigger", PhotonTargets.Others, 2);
			} else {
				SetSingleCollierTrigger (2);
			}
			rgbd.AddForce (Vector2.up * 300);
			if (positionController.GetPlayerPosition () == 1) { // left
				rgbd.AddForce (Vector2.right * 100);
			} else { // right
				rgbd.AddForce (Vector2.left * 100);
			}
			animator.SetBool ("upper", true);
//			childUpper.SetActive (true);
			isMuteki = true;
			_pointRemainCount = _pointRemainCount - 3;
		}
	}

	void TornadoAppear () {
	}

	void RollingAttack () {
		if (isMuteki) {
			return;
		}

		rollingBeforePos = transform.position;
		isMuteki = true;
		isRolling = true;
		command.SetCanMoveFlag (false);
		if (!singleMode) {
			photonView.RPC ("SetCollierTrigger", PhotonTargets.Others, 3);
		} else {
			SetSingleCollierTrigger (3);
		}
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionY;
		if (positionController.GetPlayerPosition () == 1) { // left
			rgbd.AddForce (Vector2.right * 400);
		} else {
			rgbd.AddForce (Vector2.left * 400);
		}

		_pointRemainCount = _pointRemainCount - 10;
		StartCoroutine (RollingAttackFinish ());
	}

	IEnumerator RollingAttackFinish () {
		yield return new WaitForSeconds (1);
		isMuteki = false;
		isRolling = false;
		command.SetCanMoveFlag (true);
		if (!singleMode) {
			photonView.RPC ("UnsetColliderTrigger", PhotonTargets.Others, 3);
		} else {
			UnsetSingleColliderTrigger (3);
		}
//		animator.SetBool ("rotation", false);
		rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
		transform.position = rollingBeforePos;
		transform.rotation = Quaternion.identity;
	}

	public void LethalWeapon () {
		if (isMuteki || !canLethalWeapon) {
			return;
		}

		if (isScrewDriver) {
			return;
		}

		isLethalWeapon = true;
		command.SetCanMoveFlag (false);
		animator.SetBool ("lethalWeapon", true);
		if (!singleMode) {
			photonView.RPC ("SetCollierTrigger", PhotonTargets.Others, 9);
		} else {
			SetSingleCollierTrigger (9);
		}
		StartCoroutine (FinishLethalWeapon ());
	}

	IEnumerator FinishLethalWeapon () {
		yield return new WaitForSeconds (0.6f);

		isLethalWeapon = false;
		command.SetCanMoveFlag (true);
		animator.SetBool ("lethalWeapon", false);
		if (!singleMode) {
			photonView.RPC ("UnsetColliderTrigger", PhotonTargets.Others, 9);
		} else {
			UnsetSingleColliderTrigger (9);
		}
	}

	public void SetCanLethalWeapon (bool flag) {
		canLethalWeapon = flag;
	}

	void JumpFire () {
		if (jumpFireUseFlag != 1) {
			return;
		}
		if (!isMuteki && rgbd.velocity.y == 0) {
			jumpFireUseFlag = 0;
			rgbd.AddForce (Vector2.up * 300);
			StartCoroutine (MultipleHadouken ());
//			for (int i = 1; i < 4; i++) {
//				hadouken();
//			}
			StartCoroutine (canJumpFire ());
			_pointRemainCount = _pointRemainCount - 5;
		}
	}
	IEnumerator MultipleHadouken () {
		int count = 0;
		while (count < 4) {
			yield return new WaitForSeconds (0.1f);
			hadouken ();
			count++;
		}
	}
	IEnumerator canJumpFire () {
		yield return new WaitForSeconds (2f);
		jumpFireUseFlag = 1;
	}

	public void SetEnemyPlayer (GameObject enemy) {
		enemyPlayer = enemy;
//		photonView.RPC ("SetEnemyPlayerForNotIsMine", PhotonTargets.Others);
//		positionController.SetEnemyPlayer (enemy);
	}

	[PunRPC]
	void SetEnemyPlayerForNotIsMine () {
		if (PhotonNetwork.isMasterClient) {
			enemyPlayer = GameObject.FindGameObjectWithTag ("Player");
		} else {
			enemyPlayer = GameObject.FindGameObjectWithTag ("EnemyPlayer");
		}
	}

	GameObject getEnemyPlayer () {
		if (enemyPlayer != null) {
			return enemyPlayer;
		}
		if (PhotonNetwork.isMasterClient) {
			return GameObject.FindGameObjectWithTag ("Player");
		} else {
			return GameObject.FindGameObjectWithTag ("EnemyPlayer");
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (!singleMode && !photonView.isMine) {
			return;
		}

		if (other.tag == "Player") {
			return;
		}

		if (isHeartProtection) {
			return;
		}

		if (other.tag == "Explosion") {
			jumpCount = 0;
		}

		if (singleMode) {
//			Debug.Log ("other " + other.tag);
			if (!singleEnemy) {
				if (other.tag == "EnemyPlayerNormalAttack") {
					_hitAttack (other, 200);
				} else if (other.tag == "EnemyHadouken") {
					_hitAttack (other, 500);
				} else if (other.tag == "EnemyYogaFire") {
					_hitAttack (other, 900);
				} else if (other.tag == "EnemyRepuuken") {
					_hitAttack (other, 500);
				} else if (other.tag == "EnemyPlayerKick") {
					_hitPlayerAttack (other, 900);
				} else if (other.tag == "EnemyPlayerUpper") {
					_hitPlayerAttack (other, 900);
				} else if (other.tag == "EnemyPlayerTornado") {
					_hitAttack (other, 500);
				} else if (other.tag == "EnemyRollingAttack") {
					_hitPlayerAttack (other, 1000);
				} else if (other.tag == "BombAfter") {
					_hitAttack (other, 500);
				} else if (other.tag == "EnemyScrewDriver") {
					HitScrewDriver (other);
				} else if (other.tag == "EnemyThunder") {
					_hitAttack (other, 900, false);
				} else if (other.tag == "EnemyScrewDriverGet") {
					SetHingeJoint ();
				} else if (other.tag == "EnemyHarite") {
					_hitPlayerAttack (other, 600);
				} else if (other.tag == "EnemyLethalWeapon") {
					_hitLethalWeapon (other);
				}
			} else {
				if (other.tag == "PlayerNormalAttack") {
					_hitAttack (other, 200);
				} else if (other.tag == "MarathonHadouken") {
					_hitAttack (other, 500);
				} else if (other.tag == "MarathonYogaFire") {
					_hitAttack (other, 900);
				} else if (other.tag == "Repuuken") {
					_hitAttack (other, 500);
				} else if (other.tag == "PlayerKick") {
					_hitPlayerAttack (other, 900);
				} else if (other.tag == "PlayerUpper") {
					_hitPlayerAttack (other, 900);
				} else if (other.tag == "PlayerTornado") {
					_hitAttack (other, 500);
				} else if (other.tag == "RollingAttack") {
					_hitPlayerAttack (other, 1000);
				} else if (other.tag == "BombAfter") {
					_hitAttack (other, 500);
				} else if (other.tag == "ScrewDriver") {
					HitScrewDriver (other);
				} else if (other.tag == "thunder") {
					_hitAttack (other, 900, false);
				} else if (other.tag == "screwDriverGet") {
					SetHingeJoint ();
				} else if (other.tag == "PlayerHarite") {
					_hitPlayerAttack (other, 600);
				} else if (other.tag == "PlayerLethalWeapon") {
					_hitLethalWeapon (other);
				}
			}

			return;
		}

		if (PhotonNetwork.isMasterClient) {
			if (other.tag == "EnemyPlayerNormalAttack") {
				_hitAttack (other, 200);
			} else if (other.tag == "EnemyHadouken") {
				_hitAttack (other, 500);
			} else if (other.tag == "EnemyYogaFire") {
				_hitAttack (other, 900);
			} else if (other.tag == "EnemyRepuuken") {
				_hitAttack (other, 500);
			} else if (other.tag == "EnemyPlayerKick") {
				_hitPlayerAttack (other, 900);
			} else if (other.tag == "EnemyPlayerUpper") {
				_hitPlayerAttack (other, 900);
			} else if (other.tag == "EnemyPlayerTornado") {
				_hitAttack (other, 500);
			} else if (other.tag == "EnemyRollingAttack") {
				_hitPlayerAttack (other, 1000);
			} else if (other.tag == "BombAfter") {
				_hitAttack (other, 500);
			} else if (other.tag == "EnemyScrewDriver") {
				HitScrewDriver (other);
			} else if (other.tag == "EnemyThunder") {
				_hitAttack (other, 900, false);
			} else if (other.tag == "EnemyScrewDriverGet") {
				SetHingeJoint ();
			} else if (other.tag == "EnemyHarite") {
				_hitPlayerAttack (other, 600);
			} else if (other.tag == "EnemyLethalWeapon") {
				_hitLethalWeapon (other);
			}
		} else {
			if (other.tag == "PlayerNormalAttack") {
				_hitAttack (other, 200);
			} else if (other.tag == "MarathonHadouken") {
				_hitAttack (other, 500);
			} else if (other.tag == "MarathonYogaFire") {
				_hitAttack (other, 900);
			} else if (other.tag == "Repuuken") {
				_hitAttack (other, 500);
			} else if (other.tag == "PlayerKick") {
				_hitPlayerAttack (other, 900);
			} else if (other.tag == "PlayerUpper") {
				_hitPlayerAttack (other, 900);
			} else if (other.tag == "PlayerTornado") {
				_hitAttack (other, 500);
			} else if (other.tag == "RollingAttack") {
				_hitPlayerAttack (other, 1000);
			} else if (other.tag == "BombAfter") {
				_hitAttack (other, 500);
			} else if (other.tag == "ScrewDriver") {
				HitScrewDriver (other);
			} else if (other.tag == "thunder") {
				_hitAttack (other, 900, false);
			} else if (other.tag == "screwDriverGet") {
				SetHingeJoint ();
			} else if (other.tag == "PlayerHarite") {
				_hitPlayerAttack (other, 600);
			} else if (other.tag == "PlayerLethalWeapon") {
				_hitLethalWeapon (other);
			}
		}
	}

	void _hitLethalWeapon (Collider2D other) {
		if (hitLethalWeapon) {
			return;
		}
		hitLethalWeapon = true;
		command.SetCanMoveFlag (false);
//		rgbd.constraints = RigidbodyConstraints2D.None;
		rgbd.isKinematic = false;
		if (!singleMode) {
			photonView.RPC ("StartLethalWeapon", PhotonTargets.All, photonView.viewID);
		} else {
			if (singleEnemy) {
				enemyOwnPlayer.GetComponent<OnlineBattleSingleEnemyPlayer> ().SetDeadFlag (true);
				enemyPlayer.GetComponent<OnlineBattlePlayerController> ().StartLethalWeapon ();
			} else {
				enemyPlayer.GetComponent<OnlineBattleSingleEnemyPlayer> ().SetDeadFlag (true);
				enemyPlayer.GetComponent<OnlineBattlePlayerController> ().StartLethalWeapon ();
			}
		}
		_hitAndUnsetLethalWeapon ();
	}

	[PunRPC]
	void StartLethalWeapon (int photonViewId = 0) {
		command.SetCanMoveFlag (false);
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
		rgbd.isKinematic = false;
		gameController.SendMessage ("LethalWeaponImageStart");
		StartCoroutine (FinishLethalWeaponAttack ());
	}

	void StartSingleModeLethalWeapon () {
		gameController.SendMessage ("LethalWeaponImageStart");
		StartCoroutine (FinishLethalWeaponAttack ());

		command.SetCanMoveFlag (false);
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
		rgbd.isKinematic = false;
		GetLethalWeapon ();
	}

	public void GetLethalWeapon () {
		if (!singleMode) {
			photonView.RPC ("GetLethalWeaponStart", PhotonTargets.All);
		} else {
			gameController.SendMessage ("LethalWeaponImageStart");
			StartCoroutine (FinishLethalWeaponAttack ());
		}
	}

	[PunRPC]
	void GetLethalWeaponStart () {
		command.SetCanMoveFlag (false);
//		rgbd.constraints = RigidbodyConstraints2D.None;
		rgbd.isKinematic = false;
		gameController.SendMessage ("LethalWeaponImageStart");
		StartCoroutine (FinishLethalWeaponAttack ());
	}

	IEnumerator FinishLethalWeaponAttack () {
		yield return new WaitForSeconds (3.5f);
		gameController.SendMessage ("LethalWeaponSingleImageEnd");
	}

	void GetSingleModeLethalWeaponStart () {
		command.SetCanMoveFlag (false);
		//		rgbd.constraints = RigidbodyConstraints2D.None;
		rgbd.isKinematic = false;
//		gameController.SendMessage ("LethalWeaponImageStart");
		StartCoroutine (FinishLethalWeaponAttack ());
	}

	public void EndLethalWeaponAttack () {
		if (hitLethalWeapon) {
			animator.SetBool ("death", true);
			_hitLethalWeapon (8000);
			StartCoroutine (hitLethalWeaponAlive ());
		} else {
			Vector3 pos = transform.position;
			if (positionController.GetPlayerPosition () == 1) {
				transform.position = new Vector3 (pos.x - 1.0f, pos.y, 0);
				transform.localScale = new Vector3 (0.1f, 0.1f, 0);
			} else {
				transform.position = new Vector3 (pos.x + 1.0f, pos.y, 0);
				transform.localScale = new Vector3 (-0.1f, 0.1f, 0);
			}
			animator.SetBool ("FinishLethalWeapon", true);
			command.SetCanMoveFlag (false);
			StartCoroutine (gitLethalWeaponAlive ());
		}
	}

	public void EndSingleLethalWeaponAttack () {
		EndLethalWeaponAttack ();
		enemyPlayer.GetComponent<OnlineBattlePlayerController> ().EndLethalWeaponAttack ();
	}

	IEnumerator hitLethalWeaponAlive () {
		yield return new WaitForSeconds (2);
		if (lifePoint > 0) {
			animator.SetBool("death", false);
			rgbd.isKinematic = false;
			rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
			transform.rotation = Quaternion.identity;
			command.SetCanMoveFlag (true);
			hitLethalWeapon = false;
		}
	}

	IEnumerator gitLethalWeaponAlive () {
		yield return new WaitForSeconds (2);
		animator.SetBool ("FinishLethalWeapon", false);
		if (positionController.GetPlayerPosition () == 1) {
			transform.localScale = new Vector3 (-0.1f, 0.1f, 0);
		} else {
			transform.localScale = new Vector3 (0.1f, 0.1f, 0);
		}

		rgbd.isKinematic = false;
		rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
		transform.rotation = Quaternion.identity;
		command.SetCanMoveFlag (true);
		hitLethalWeapon = false;
	}

	void HitScrewDriver (Collider2D other) {
		if (hitScrewDriver) {
			return;
		}
		hitScrewDriver = true;
		command.SetCanMoveFlag (false);
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		if (!singleMode) {
			photonView.RPC ("SetHingeJoint", PhotonTargets.All);
		} else {
			enemyPlayer.GetComponent<OnlineBattlePlayerController> ().SetSingleModeHingeJoins ();
		}
	}

	[PunRPC]
	void SetHingeJoint () {
		command.SetCanMoveFlag (false);
		GetComponent<HingeJoint2D> ().enabled = true;
		GetComponent<HingeJoint2D> ().connectedBody = enemyPlayer.GetComponent<Rigidbody2D> ();
		enemyPlayer.GetComponent<OnlineBattlePlayerController> ().GetScrewDriver ();
	}

	public void SetSingleModeHingeJoins () {
		command.SetCanMoveFlag (false);
		GetComponent<HingeJoint2D> ().enabled = true;
		GetComponent<HingeJoint2D> ().connectedBody = enemyPlayer.GetComponent<Rigidbody2D> ();
		GetScrewDriver ();
	}

	public void GetScrewDriver () {
		if (!singleMode) {
			photonView.RPC ("GetScrewDriverStart", PhotonTargets.All);
		} else {
			GetSingleScrewDriverStart ();
			enemyPlayer.GetComponent<OnlineBattlePlayerController> ().GetSingleScrewDriverStart ();
		}
	}

	[PunRPC]
	void GetScrewDriverStart () {
		getScrewDriver = true;
		command.SetCanMoveFlag (false);
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		rgbd.AddForce (Vector2.up * 1000);
	}

	public void GetSingleScrewDriverStart () {
		getScrewDriver = true;
		command.SetCanMoveFlag (false);
		rgbd.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		rgbd.AddForce (Vector2.up * 1000);
	}

	[PunRPC]
	void FinishScrewDriverJump () {
		if (hitScrewDriver) {
			_setDamage (5000, false);
		}
		hitScrewDriver = false;
		getScrewDriver = false;
		command.SetCanMoveFlag (true);
		GetComponent<HingeJoint2D> ().enabled = false;
		GetComponent<HingeJoint2D> ().connectedBody = null;
//		transform.Rotate(new Vector3(0, 0, 0));

		rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
		transform.rotation = Quaternion.identity;
	}

	void FinishSingleScrewDriverJump () {
		if (hitScrewDriver) {
			_setDamage (5000, false);
		}
		hitScrewDriver = false;
		getScrewDriver = false;
		command.SetCanMoveFlag (true);
		GetComponent<HingeJoint2D> ().enabled = false;
		GetComponent<HingeJoint2D> ().connectedBody = null;
		//		transform.Rotate(new Vector3(0, 0, 0));

		rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
		transform.rotation = Quaternion.identity;
	}

	// 相手のObjectにヒット(kick, upper, rolling)
	void _hitPlayerAttack (Collider2D other, int damage) {
		if (_hitAndUnsetLethalWeapon ()) {
			return;
		}

		if (isMuteki) {
			return;
		}

		_setDamage (damage);
	}

	// 相手の攻撃物にヒット(ono, hadouken, yogafire)
	void _hitAttack (Collider2D other, int damage, bool isDestroy = true) {
		if (isMuteki) {
			return;
		}

		_setDamage (damage);
		if (isDestroy) {
			other.GetComponent<OnlineBattleAttackBaseController> ().DestroyObject ();
		}
	}

	bool _hitAndUnsetLethalWeapon () {
		if (isLethalWeapon) {
			isLethalWeapon = false;
			command.SetCanMoveFlag (true);
			animator.SetBool ("lethalWeapon", false);
			if (!singleMode) {
				photonView.RPC ("UnsetColliderTrigger", PhotonTargets.Others, 9);
			} else {
				UnsetSingleColliderTrigger (9);
			}

			return true;
		}
		return false;
	}

	void _hitLethalWeapon (int damage) {
		lifePoint -= damage;
		displayDamage (damage);

		if (lifePoint < 1) {
			if (!singleMode) {
				StartCoroutine (RevivalFromLethalWeaponCount ());
				if (photonView.isMine) {
					if (PhotonNetwork.isMasterClient) {
						int[] pointList = { lifePoint, damage };
						photonView.RPC ("SetMasterDamage", PhotonTargets.Others, pointList);
						gameController.SendMessage ("SendMasterLethalDamage", pointList);
					} else {
						int[] pointList = { lifePoint, damage };
						photonView.RPC ("SetClientDamage", PhotonTargets.Others, pointList);
						gameController.SendMessage ("SendClientLethalDamage", pointList);
					}
					photonView.RPC ("SetEnemyDamege", PhotonTargets.Others, lifePoint);
					hpText.text = lifePoint.ToString ();
					hpSlider.value = lifePoint;
				}
			} else {
				int[] pointList = { lifePoint, damage };
				if (singleEnemy) {
					gameController.SendMessage ("SendClientLethalDamage", pointList);
				} else {
					gameController.SendMessage ("SendMasterLethalDamage", pointList);
				}
				hpText.text = lifePoint.ToString ();
				hpSlider.value = lifePoint;
			}
			battleStatus = BattleStatus.lose;
			gameController.SendMessage ("SetBattleStatusText", "You lose.");
			gameController.SendMessage ("SetBattleCountUp", 1);
			enemyPlayer = getEnemyPlayer ();
			enemyPlayer.gameObject.GetComponent<OnlineBattlePlayerController> ().WinPlayer ();
		} else {
			if (!singleMode) {
				StartCoroutine (RevivalFromLethalWeaponCount ());
				if (photonView.isMine) {
					if (PhotonNetwork.isMasterClient) {
						int[] pointList = {lifePoint, damage};
						photonView.RPC ("SetMasterDamage", PhotonTargets.Others, pointList);
						gameController.SendMessage ("SendMasterLethalDamage", pointList);
					} else {
						int[] pointList = {lifePoint, damage};
						photonView.RPC ("SetClientDamage", PhotonTargets.Others, pointList);
						gameController.SendMessage ("SendClientLethalDamage", pointList);
					}
					photonView.RPC ("SetEnemyDamege", PhotonTargets.Others, lifePoint);
					hpText.text = lifePoint.ToString ();
					hpSlider.value = lifePoint;
				}
			} else {
				int[] pointList = {lifePoint, damage};
				if (singleEnemy) {
					gameController.SendMessage ("SendClientLethalDamage", pointList);
				} else {
					gameController.SendMessage ("SendMasterLethalDamage", pointList);
				}
				hpText.text = lifePoint.ToString ();
				hpSlider.value = lifePoint;

				RevivalFromLethalWeapon ();
				enemyPlayer.GetComponent<OnlineBattlePlayerController> ().RevivalFromLethalWeapon ();
			}
//			battleStatus = BattleStatus.lose;
//			StartCoroutine (GameoverByLethalWeapon ());
		}
	}

	IEnumerator RevivalFromLethalWeaponCount () {
		yield return new WaitForSeconds (1.5f);
		photonView.RPC ("RevivalFromLethalWeaponOnline", PhotonTargets.All);
	}

	[PunRPC]
	void RevivalFromLethalWeaponOnline () {
		command.SetCanMoveFlag (true);
		rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
		rgbd.isKinematic = true;
		animator.SetBool ("FinishLethalWeapon", false);
		animator.SetBool ("death", false);
	}

	void RevivalFromLethalWeapon () {
		if (singleEnemy) {
			enemyOwnPlayer.GetComponent<OnlineBattleSingleEnemyPlayer> ().SetDeadFlag (false);
		}
		CanMoveFromLethalWeapomn ();
		enemyPlayer.GetComponent<OnlineBattlePlayerController> ().CanMoveFromLethalWeapomn ();
	}

	public void CanMoveFromLethalWeapomn () {
		command.SetCanMoveFlag (true);
		rgbd.constraints = RigidbodyConstraints2D.FreezeRotation;
		rgbd.isKinematic = true;
	}

	IEnumerator GameoverByLethalWeapon () {
		yield return new WaitForSeconds (5);
		gameController.SendMessage ("SetBattleStatusText", "You lose.");
		gameController.SendMessage ("SetBattleCountUp", 1);
		enemyPlayer = getEnemyPlayer ();
		enemyPlayer.gameObject.GetComponent<OnlineBattlePlayerController> ().WinPlayer ();
	}

	void _setDamage (int damage, bool GoAway = false) {
		if (battleStatus != BattleStatus.battle) {
			return;
		}

		damage = _getAttackDamage (damage);
		if (!isMuteki) {
			if (positionController.GetPlayerPosition() == 1 && command.buttonPos == OnlineBattlePlayerCommand.ButtonPos.left) { // left
				childDefenseImage.SetActive (true);
				photonView.RPC ("SetDefenseImage", PhotonTargets.Others);
				StartCoroutine (FinishDefenseImage ());
				damage = damage / 2;
			} else if (positionController.GetPlayerPosition() == 2 && command.buttonPos == OnlineBattlePlayerCommand.ButtonPos.right) {
				childDefenseImage.SetActive (true);
				photonView.RPC ("SetDefenseImage", PhotonTargets.Others);
				StartCoroutine (FinishDefenseImage ());
				damage = damage / 2;
			} else {
				animator.SetBool ("damage", true);
				StartCoroutine (DamageAnimStop ());
			}

			lifePoint -= damage;
			displayDamage (damage);

			if (!singleMode) {
				if (photonView.isMine) {
					if (PhotonNetwork.isMasterClient) {
						int[] pointList = {lifePoint, damage};
						photonView.RPC ("SetMasterDamage", PhotonTargets.Others, pointList);
						gameController.SendMessage ("SendMasterDamage", pointList);
					} else {
						int[] pointList = {lifePoint, damage};
						photonView.RPC ("SetClientDamage", PhotonTargets.Others, pointList);
						gameController.SendMessage ("SendClientDamage", pointList);
					}
					photonView.RPC ("SetEnemyDamege", PhotonTargets.Others, lifePoint);
					hpText.text = lifePoint.ToString ();
					hpSlider.value = lifePoint;
				}
			} else {
				int[] pointList = {lifePoint, damage};
				if (singleEnemy) {
					gameController.SendMessage ("SendClientDamage", pointList);
				} else {
					gameController.SendMessage ("SendMasterDamage", pointList);
				}
				hpText.text = lifePoint.ToString ();
				hpSlider.value = lifePoint;
			}
			if (lifePoint < 1) {
				battleStatus = BattleStatus.lose;
				gameController.SendMessage ("SetBattleStatusText", "You lose.");
				gameController.SendMessage ("SetBattleCountUp", 1);
				enemyPlayer = getEnemyPlayer ();
				enemyPlayer.gameObject.GetComponent<OnlineBattlePlayerController> ().WinPlayer ();
			}
			isMuteki = true;

			if (GoAway) {
				if (positionController.GetPlayerPosition () == 1) { // left
					rgbd.AddForce (Vector2.left * damage);
				} else if (positionController.GetPlayerPosition () == 2) { // right
					rgbd.AddForce (Vector2.right * damage);
				}
			}
				
//			animator.SetBool ("damage", true);
			StartCoroutine (DamageMutekiFinish ());
//			if (lifePoint < 1) {
//				rgbd.AddForce (Vector2.left * 3000);
//			}
		}

	}

	[PunRPC]
	void SetDefenseImage () {
		transform.Find ("defenseImage").gameObject.SetActive (true);
	}

	[PunRPC]
	void UnSetDefenseImage () {
		transform.Find ("defenseImage").gameObject.SetActive (false);
	}

	public void WinPlayer () {
		if (!singleMode) {
			photonView.RPC ("SetWinPlayer", PhotonTargets.Others);
		} else {
			if (!singleEnemy) {
				if (lifePoint > 0) {
					gameController.SendMessage ("SetBattleStatusText", "You win !");
				} else {
					gameController.SendMessage ("SetBattleStatusText", "You lose.");
				}
			}
		}
	}

	[PunRPC]
	void SetWinPlayer () {
		battleStatus = BattleStatus.win;
		gameController.SendMessage ("SetBattleStatusText", "You win !");
		gameController.SendMessage ("SetBattleCountUp", 2);
	}

	[PunRPC]
	void SetMasterDamage (int[] pointList) {
		gameController.SendMessage ("SendMasterDamage", pointList);
	}

	[PunRPC]
	void SetClientDamage (int[] pointList) {
		gameController.SendMessage ("SendClientDamage", pointList);
	}

	[PunRPC]
	void SetEnemyDamege (int enemyPoint) {
		hpText.text = enemyPoint.ToString ();
		hpSlider.value = enemyPoint;
	}

	IEnumerator DamageMutekiFinish () {
		yield return new WaitForSeconds (0.5f);
		isMuteki = false;
	}

	void displayDamage (int damage) {
		MarathonPlayerDamageText text = damageText.GetComponent<MarathonPlayerDamageText> ();
		text.StartText (damage);
	}

	int _getAttackDamage (int damage) {
		int level = AllGameDefinition.level;
		double ratio = level / 100d;
		return (int)(damage * (1d - ratio));
	}

	IEnumerator FinishDefenseImage () {
		yield return new WaitForSeconds (0.3f);
		childDefenseImage.SetActive(false);
		photonView.RPC ("UnSetDefenseImage", PhotonTargets.Others);
	}

	IEnumerator DamageAnimStop () {
		yield return new WaitForSeconds (0.5f);
		animator.SetBool ("damage", false);
	}

	public void DestroyPlayer () {
		Destroy (this.gameObject);
	}

	public void setProtectionFlag (bool flag) {
		if (!flag) {
			StartCoroutine (FinishHeartProtection ());
		}
	}

	IEnumerator FinishHeartProtection () {
		yield return new WaitForSeconds (1);
		isHeartProtection = false;
		if (!singleMode) {
			photonView.RPC ("UnsetColliderTrigger", PhotonTargets.All, 5);
		} else {
			UnsetSingleColliderTrigger (5);
		}
	}

	public void BattleStart () {
		battleStatus = BattleStatus.battle;
		command.SetCanMoveFlag (true);
	}

	public void SetSinglePlayObjct (OnlineBattlePlayerPosition positionObj, OnlineBattlePlayerCommand commandObj) {
		positionController = positionObj;
		command = commandObj;
	}

	public void SetEnemyObj (GameObject enemy) {
		enemyPlayer = enemy;
	}

	public void setSingleMode (bool flag) {
		singleMode = flag;
		isMuteki = false;
	}

	public void SetHitPoint (int hp) {
		lifePoint = hp;
	}

	public int GetHitPoint () {
		return lifePoint;
	}

	public void SetPlayerName (string name) {
		nameText.text = name;
	}

	public void CatchBomb () {
		SetSingleCollierTrigger (8);
	}

	public void ThrowBomb () {
		UnsetSingleColliderTrigger (8);
		foreach (KeyValuePair<int, GameObject> pair in bombList) {
			if (pair.Value.GetComponent<OnlineBattleBomb>() != null) {
				pair.Value.GetComponent<OnlineBattleBomb>().ThrowToEnemy (positionController.GetPlayerPosition());
			}
		}
		bombList = new Dictionary<int, GameObject> ();
	}

	public void AddBombList (GameObject bomb) {
		bombList.Add (bombList.Count + 1, bomb);
	}

	public void SetMutekiMode (bool flag) {
		isMuteki = flag;
	}

	public void SetEnemyOwnPlayer (GameObject enemy) {
		enemyOwnPlayer = enemy;
	}
}
