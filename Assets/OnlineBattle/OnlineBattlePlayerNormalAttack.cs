﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattlePlayerNormalAttack : Photon.MonoBehaviour {
	float angle;
	float speed;
	float posX;

//	BoxCollider2D cldr;

	int playerPos; // 1:left, 2:right

	// Use this for initialization
	void Start () {
		angle = 0.0f;
		speed = 400;
//		cldr = GetComponent<BoxCollider2D> ();
//		cldr.isTrigger = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (transform.position.x > 15 || transform.position.x < -15) {
			this.GetComponent<OnlineBattleAttackBaseController> ().DestroyObject ();
//			this.gameObject.SetActive (false);
//			GetComponent<PhotonView> ().RPC ("DestroyOpponentObject", PhotonTargets.Others);

//			PhotonNetwork.Destroy (this.gameObject);
		}
		angle -= Time.deltaTime * speed;
		transform.rotation = Quaternion.Euler (0, 0, angle);
		Vector2 position = transform.position;

		if (playerPos == 1) {
//			position.x += Time.deltaTime * 2;
			position.x += Time.deltaTime * 5;
		} else if (playerPos == 2) {
//			position.x -= Time.deltaTime * 2;
			position.x -= Time.deltaTime * 5;
		}
		transform.position = position;
	}

//	[PunRPC]
//	void DestroyOpponentObject () {
//		this.gameObject.SetActive (false);
//	}

	public void SetPosition (int pos) {
		playerPos = pos;
	}
}
