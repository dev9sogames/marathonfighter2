﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattlePlayerYogafire : Photon.MonoBehaviour {
	int playerPos; // 1:left, 2:right

	GameObject targetObj = null;
	bool isHoming;

	// Update is called once per frame
	void Update () {
		if (isHoming && targetObj != null) {
			HomingAttack ();
			return;
		}

		Vector2 position = transform.position;
		if (position.x >= 17 || position.x <= -15) {
			this.gameObject.SetActive (false);
			this.GetComponent<OnlineBattleAttackBaseController> ().DestroyObject ();
//			GetComponent<PhotonView> ().RPC ("DestroyOpponentObject", PhotonTargets.Others);
//			PhotonNetwork.Destroy (this.gameObject);
		}

		if (playerPos == 1) {
			position.x += Time.deltaTime * 5;
		} else if (playerPos == 2) {
			position.x -= Time.deltaTime * 5;
		}
		transform.position = position;
	}

//	[PunRPC]
//	void DestroyOpponentObject () {
//		this.gameObject.SetActive (false);
//	}

	void HomingAttack () {
		Vector2 targetPos = targetObj.transform.position;
		float x = targetPos.x;
		float y = targetPos.y;

		Vector2 direction = new Vector2 (x - transform.position.x, y - transform.position.y).normalized;
		GetComponent<Rigidbody2D> ().velocity = (direction * 7);
	}

	public void SetHomingObj (GameObject homingEnemy) {
		isHoming = true;
		targetObj = homingEnemy;
	}

	public void SetPosition (int pos) {
		playerPos = pos;
	}

}
