﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattleLethalWeaponMiniParticle : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (DeleteObject ());
	}

	IEnumerator DeleteObject () {
		yield return new WaitForSeconds (0.7f);
		this.gameObject.SetActive (false);
	}
}
