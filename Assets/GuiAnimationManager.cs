﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiAnimationManager : MonoBehaviour {
	public Button[] buttonList;
	public Text[] textList;
	public Image[] imageList;

	public void MoveButtonList () {
		foreach (Button button in buttonList) {
			button.GetComponent<GuiAnimation> ().MoveStart ();
		}
	}

	public void MoveTextList () {
		foreach (Text text in textList) {
			text.GetComponent<GuiAnimation> ().MoveStart ();
		}
	}

	public void MoveImageList () {
		foreach (Image image in imageList) {
			image.GetComponent<GuiAnimation> ().MoveStart ();
		}
	}

	public void SetDefaultPosForButton () {
		foreach (Button button in buttonList) {
			button.GetComponent<GuiAnimation> ().SetDefalutPosition ();
		}
	}

	public void SetDefaultPosForText () {
		foreach (Text text in textList) {
			text.GetComponent<GuiAnimation> ().SetDefalutPosition ();
		}
	}

	public void SetDefaultPosForImage () {
		foreach (Image image in imageList) {
			image.GetComponent<GuiAnimation> ().SetDefalutPosition ();
		}
	}
}
