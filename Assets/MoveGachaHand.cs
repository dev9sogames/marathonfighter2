﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//public class MoveGachaHand : MonoBehaviour {
public class MoveGachaHand : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	float y;
	float dragY;
	float diff;

	Rigidbody2D rgbd;
	bool isHit;
	bool isStart;

	void Start () {
		rgbd = GetComponent<Rigidbody2D> ();
//		y = transform.position.y;
		Debug.Log(transform.position.y);
		y = Camera.main.WorldToScreenPoint(this.transform.position).y;
		Debug.Log (y);
	}

	void Update () {
		if (isStart) {
//			rgbd.AddForce (Vector2.up * 3);
		}
	}

	void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
		Debug.Log ("OnBeginDrag");
		rgbd.gravityScale = 1;
//		startPoint = eventData.pressPosition;
	}

	public void OnDrag(PointerEventData eventData) {
		if (!isHit) {
			isStart = true;
			dragY = eventData.position.y;
			Debug.Log ("OnDrag : " + dragY);

			diff = y - dragY;

			if (diff < 0) {
				return;
			}

			rgbd.mass = diff;
		}
	}

	public void OnEndDrag(PointerEventData eventData) {
		Debug.Log ("OnEndDrag");
		if (!isHit) {
			rgbd.mass = 1;
			rgbd.gravityScale = -3;
//			rgbd.AddForce (Vector2.up * 5);
		}
	}

	public void HitTrigger () {
		isHit = true;
	}

	/*
	void OnMouseDrag() {

		dragY = Input.mousePosition.y;
		diff = y - dragY;

		if (diff < 0) {
			return;
		}

		rgbd.mass = diff / 5.0f;
	}
	*/
}
