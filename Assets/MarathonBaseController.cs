﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonBaseController : MonoBehaviour {
	bool pauseFlag;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (pauseFlag) {
			return;
		}
//		Debug.Log ("base Update" + GetInstanceID());
		ChildUpdate ();
	}

	void FixedUpdate () {
		if (pauseFlag) {
			return;
		}

//		Debug.Log ("base FixedUpdate");
		ChildFixedUpdate ();
	}

	protected virtual void ChildUpdate () { // not call
	}

	protected virtual void ChildFixedUpdate () { // not call
	}

	void OnApplicationPause (bool pauseStatus) {
		if (pauseStatus) {
		} else {
			if (pauseFlag) {
				pauseFlag = false;
			}
		}
	}

	public void SetPauseStatus (bool flag) {
		Debug.Log ("SetPauseStatus : " + flag);
		pauseFlag = flag;
	}
}
