﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonPlayerHissatuWaza : MonoBehaviour {
	public MarathonPlayer player;
	GameObject gameController;

	// Use this for initialization
	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("MarathonGameController");
	}

	public void HissatuWaza () {
//		Debug.Log ("hissatuwaza");
		gameController.SendMessage ("FinishHissatuWaza");
		player.PlayerHissatuAttack ();
	}
}
