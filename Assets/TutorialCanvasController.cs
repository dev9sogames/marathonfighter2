﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialCanvasController : MonoBehaviour {
	public Canvas TutorialCanvas; // main canvas

	public Canvas Tutorial1Canvas;
	public Canvas Tutorial2Canvas;
	public Canvas Tutorial3Canvas;
	public Canvas Tutorial4Canvas;
	public Canvas Tutorial5Canvas;
	public Canvas Tutorial6Canvas;
	public Canvas Tutorial7Canvas;

	public Button rightButton;
	public Button leftButton;
	//public Button thirdButton;

	// Use this for initialization
	void Start () {
		Tutorial1Canvas.gameObject.SetActive (true);

		rightButton.interactable = true;
		leftButton.interactable = false;
	}

	public void PushRight () {
		if (Tutorial1Canvas.gameObject.activeSelf) {
			Tutorial1Canvas.gameObject.SetActive (false);
			Tutorial2Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		} else if (Tutorial2Canvas.gameObject.activeSelf) {
			Tutorial2Canvas.gameObject.SetActive (false);
			Tutorial3Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		} else if (Tutorial3Canvas.gameObject.activeSelf) {
			Tutorial3Canvas.gameObject.SetActive (false);
			Tutorial4Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		} else if (Tutorial4Canvas.gameObject.activeSelf) {
			Tutorial4Canvas.gameObject.SetActive (false);
			Tutorial5Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		} else if (Tutorial5Canvas.gameObject.activeSelf) {
			Tutorial5Canvas.gameObject.SetActive (false);
			Tutorial6Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		} else if (Tutorial6Canvas.gameObject.activeSelf) {
			Tutorial6Canvas.gameObject.SetActive (false);
			Tutorial7Canvas.gameObject.SetActive (true);

			rightButton.interactable = false;
			leftButton.interactable = true;
		}
	}

	public void PushLeft () {
		if (Tutorial1Canvas.gameObject.activeSelf) {
		} else if (Tutorial2Canvas.gameObject.activeSelf) {
			Tutorial2Canvas.gameObject.SetActive (false);
			Tutorial1Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = false;
		} else if (Tutorial3Canvas.gameObject.activeSelf) {
			Tutorial3Canvas.gameObject.SetActive (false);
			Tutorial2Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		} else if (Tutorial4Canvas.gameObject.activeSelf) {
			Tutorial4Canvas.gameObject.SetActive (false);
			Tutorial3Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		} else if (Tutorial5Canvas.gameObject.activeSelf) {
			Tutorial5Canvas.gameObject.SetActive (false);
			Tutorial4Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		} else if (Tutorial6Canvas.gameObject.activeSelf) {
			Tutorial6Canvas.gameObject.SetActive (false);
			Tutorial5Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		} else if (Tutorial7Canvas.gameObject.activeSelf) {
			Tutorial7Canvas.gameObject.SetActive (false);
			Tutorial6Canvas.gameObject.SetActive (true);

			rightButton.interactable = true;
			leftButton.interactable = true;
		}
	}

	public void ClickTtechniqueCanvasClose () {
		Destroy (TutorialCanvas.gameObject);
//
//		Vector3 position = new Vector3 (960, 540, 0);
//		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
//		Instantiate (prefab, position, transform.rotation);
	}

}
