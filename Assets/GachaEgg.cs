﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GachaEgg : MonoBehaviour {
	public Sprite gachaResult;

	Coroutine getItemCrt = null;
	Coroutine returnCrt = null;
	GameObject explosion = null;

	public void Explosion () {
		GameObject exlosionPrefab = ResourceCache.Load ("Explosion");
		explosion = Instantiate (exlosionPrefab, transform.position, Quaternion.identity);

		getItemCrt = StartCoroutine (GetItem ());
		returnCrt = StartCoroutine (Return ());
	}

	IEnumerator GetItem () {
		yield return new WaitForSeconds (2);
		this.gameObject.SetActive (false);
	}

	IEnumerator Return () {
		yield return new WaitForSeconds (5);
		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load ("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);
	}

	public void StopEvent () {
		if (getItemCrt != null) {
			StopCoroutine (getItemCrt);
		}

		if (returnCrt != null) {
			StopCoroutine (returnCrt);
		}

		if (explosion != null) {
			Destroy (explosion.gameObject);
		}

		Destroy (this.gameObject);
	}
}
