﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ButtonPositionJump : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	[SerializeField] 
	Vector2 startPoint;

	[SerializeField]
	Vector2 endPoint;

	[SerializeField] 
	bool drag;

	Vector3 position;
	float x;
	float y;
	float sizeX;
	float sizeY;

	void Start () {
		sizeX = PlayerPrefs.GetFloat ("JumpButtonSizeX", 152f);
		sizeY = PlayerPrefs.GetFloat ("JumpButtonSizeY", 149f);

		GetComponent<RectTransform> ().sizeDelta = new Vector2 (sizeX, sizeY);

		x = PlayerPrefs.GetFloat ("JumpButtonX", 349);
		y = PlayerPrefs.GetFloat ("JumpButtonY", -153);
//		Debug.Log ("Jump " + x + " : " + y);
		transform.localPosition = new Vector3 (x, y, 0);
	}

	void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
	{
		startPoint = eventData.pressPosition;
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (Input.touchCount >= 2) {
			return;
		}

		drag = true;
		endPoint = eventData.position;
		transform.position = endPoint;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (Input.touchCount >= 2) {
			return;
		}

		drag = false;
		transform.position = endPoint;

	}

}