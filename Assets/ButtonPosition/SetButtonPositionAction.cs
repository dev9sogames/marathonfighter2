﻿using UnityEngine;
using System.Collections;

public class SetButtonPositionAction : MonoBehaviour {
	float x;
	float y;

	float sizeX;
	float sizeY;

	// Use this for initialization
	void Start () {
		if (AllGameDefinition.gameMode == 4) { // marathon mode
			this.gameObject.SetActive (false);

			return;
		}

		x = PlayerPrefs.GetFloat ("ActionButtonX", 327);
		y = PlayerPrefs.GetFloat ("ActionButtonY", -4);

		sizeX = PlayerPrefs.GetFloat ("ActionButtonSizeX", 177f);
		sizeY = PlayerPrefs.GetFloat ("ActionButtonSizeY", 149f);

//		Debug.Log ("action " + x + " : " + y);
		GetComponent<RectTransform> ().sizeDelta = new Vector2 (sizeX, sizeY);
		transform.localPosition = new Vector3 (x, y, 0);
	}
}
