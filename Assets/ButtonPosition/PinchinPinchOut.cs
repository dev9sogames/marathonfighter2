﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PinchinPinchOut : MonoBehaviour {
	public int buttonType; // 1:action, 2:jump, 3:juuji

	private float scale;
	private float backDist = 0.0f;

	float min;
	float max;

	bool onTap;

	// デフォルトサイズ
	private float defaultScale;

	void Start () {
		Debug.Log ("start : " + buttonType);
		onTap = false;

		// 初期値取得
		RectTransform rt = this.GetComponent(typeof (RectTransform)) as RectTransform;
		defaultScale = rt.sizeDelta.x;

		min = 100f;
		max = 500f;
		if (buttonType == 3) {
			min = 200f;
		}
	}

	void Update () {
		if (!onTap) {
			return;
		}

		// マルチタッチ判定
		if (Input.touchCount >= 2) {
			Debug.Log ("update : " + buttonType);

			// タッチ開始時に初期値を取得
			RectTransform rt = this.GetComponent(typeof (RectTransform)) as RectTransform;
			defaultScale = rt.sizeDelta.x;

			// タッチしている２点を取得
			Touch touch1 = Input.GetTouch (0);
			Touch touch2 = Input.GetTouch (1);

			// 2点タッチ開始時の距離を記憶
			if (touch2.phase == TouchPhase.Began) {
				backDist = Vector2.Distance (touch1.position, touch2.position);
			} else if (touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved) {
				// タッチ位置の移動後、長さを再測し、前回の距離からの相対値を取る。
				float newDist = Vector2.Distance (touch1.position, touch2.position);
				scale = defaultScale + (newDist - backDist) / 30.0f;

				Debug.Log ("scale " + scale);
				// 相対値が変更した場合、オブジェクトに相対値を反映させる

				if (scale < min || scale > max) {
					return;
				}

				if(scale != 0) {
					UpdateScaling(scale);  
				}
			}
		}
	}

	/// 設定された拡大率に基づいてオブジェクトの大きさを更新する
	private void UpdateScaling(float argScale) {
		if (onTap) {
//			Debug.Log ("UpdateScaling");
			RectTransform rt = this.GetComponent (typeof(RectTransform)) as RectTransform;
			if (buttonType == 1) {
				rt.sizeDelta = new Vector2 (argScale, argScale * 149f / 177f);
			} else if (buttonType == 2) {
				rt.sizeDelta = new Vector2 (argScale, argScale * 149f / 152f);
			} else {
				rt.sizeDelta = new Vector2 (argScale, argScale);
			}
		}
	}

	public void OnTapping (bool flag) {
		onTap = flag;
		Debug.Log (flag);
	}
}
