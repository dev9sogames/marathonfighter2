﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ButtonPositionAction : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
//	[SerializeField] 
	public Vector2 startPoint;

//	[SerializeField]
	public Vector2 endPoint;

//	[SerializeField] 
	public bool drag;

	Vector3 position;
	float x;
	float y;
	float sizeX;
	float sizeY;

	void Start () {
		sizeX = PlayerPrefs.GetFloat ("ActionButtonSizeX", 177f);
		sizeY = PlayerPrefs.GetFloat ("ActionButtonSizeY", 149f);

		GetComponent<RectTransform> ().sizeDelta = new Vector2 (sizeX, sizeY);

		x = PlayerPrefs.GetFloat ("ActionButtonX", 209);
		y = PlayerPrefs.GetFloat ("ActionButtonY", -153);

		transform.localPosition = new Vector3 (x, y, 0);
	}

	void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
		startPoint = eventData.pressPosition;
	}

	public void OnDrag(PointerEventData eventData) {
		if (Input.touchCount >= 2) {
			return;
		}

		drag = true;
		endPoint = eventData.position;
		transform.position = endPoint;
	}

	public void OnEndDrag(PointerEventData eventData) {
		if (Input.touchCount >= 2) {
			return;
		}

		drag = false;
		transform.position = endPoint;
	}

}