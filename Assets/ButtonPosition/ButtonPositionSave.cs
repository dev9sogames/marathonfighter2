﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonPositionSave : MonoBehaviour {
	public Button ActionButton;
	public Button JumpButton;
	public Button JuujiButton;
	public Canvas ButtonPositionCanvas;
	public Canvas OptionCanvas;

	public Toggle NormalSizeToggle;
	public Toggle BigSizeToggle;

	public bool isTitle;

	// Use this for initialization
	void Start () {
		/*
		PlayerPrefs.DeleteKey ("ActionButtonX");
		PlayerPrefs.DeleteKey ("ActionButtonY");

		PlayerPrefs.DeleteKey ("JumpButtonX");
		PlayerPrefs.DeleteKey ("JumpButtonY");

		PlayerPrefs.DeleteKey ("JuujiButtonX");
		PlayerPrefs.DeleteKey ("JuujiButtonY");
		*/
		}

	public void SaveButtonPosition () {
		PlayerPrefs.SetFloat ("ActionButtonX", ActionButton.transform.localPosition.x);
		PlayerPrefs.SetFloat ("ActionButtonY", ActionButton.transform.localPosition.y);

		PlayerPrefs.SetFloat ("JumpButtonX", JumpButton.transform.localPosition.x);
		PlayerPrefs.SetFloat ("JumpButtonY", JumpButton.transform.localPosition.y);

		PlayerPrefs.SetFloat ("JuujiButtonX", JuujiButton.transform.localPosition.x);
		PlayerPrefs.SetFloat ("JuujiButtonY", JuujiButton.transform.localPosition.y);

//		if (BigSizeToggle.isOn) {
//			PlayerPrefs.SetInt ("ButtonScale", 2);
//		} else {
//			PlayerPrefs.SetInt ("ButtonScale", 1);
//		}

		PlayerPrefs.SetFloat ("ActionButtonSizeX", ActionButton.GetComponent<RectTransform>().sizeDelta.x);
		PlayerPrefs.SetFloat ("ActionButtonSizeY", ActionButton.GetComponent<RectTransform>().sizeDelta.y);

		PlayerPrefs.SetFloat ("JumpButtonSizeX", JumpButton.GetComponent<RectTransform>().sizeDelta.x);
		PlayerPrefs.SetFloat ("JumpButtonSizeY", JumpButton.GetComponent<RectTransform>().sizeDelta.y);

		PlayerPrefs.SetFloat ("JuujiButtonSizeX", JuujiButton.GetComponent<RectTransform>().sizeDelta.x);
		PlayerPrefs.SetFloat ("JuujiButtonSizeY", JuujiButton.GetComponent<RectTransform>().sizeDelta.y);

		PlayerPrefs.Save ();

		if (isTitle) {
			SceneManager.LoadScene ("Difficulty");
		} else {
			ButtonPositionCanvas.enabled = false;
		}
	}

	public void CloseCanvas () {
		Destroy (ButtonPositionCanvas.gameObject);
	}
}
