﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonPositionJuuji : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
//	public Toggle NormalSizeToggle;
//	public Toggle BigSizeToggle;

	[SerializeField] 
	Vector2 startPoint;

	[SerializeField]
	Vector2 endPoint;

	[SerializeField] 
	bool drag;

	Vector3 position;
	float x;
	float y;
	float sizeX;
	float sizeY;

	void Start () {
//		int buttonSize = PlayerPrefs.GetInt ("ButtonScale", 1);
//		if (buttonSize == 1) {
//			NormalSizeToggle.isOn = true;
//			BigSizeToggle.isOn = false;
//		} else {
//			NormalSizeToggle.isOn = false;
//			BigSizeToggle.isOn = true;
//		}
		sizeX = PlayerPrefs.GetFloat ("JuujiButtonSizeX", 280f);
		sizeY = PlayerPrefs.GetFloat ("JuujiButtonSizeY", 280f);

		GetComponent<RectTransform> ().sizeDelta = new Vector2 (sizeY, sizeY);

		x = PlayerPrefs.GetFloat ("JuujiButtonX", -329);
		y = PlayerPrefs.GetFloat ("JuujiButtonY", -138);
//		Debug.Log ("run " + x + " : " + y);
		transform.localPosition = new Vector3 (x, y, 0);
	}

	void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
	{
		startPoint = eventData.pressPosition;
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (Input.touchCount >= 2) {
			return;
		}

		drag = true;
		endPoint = eventData.position;
		transform.position = endPoint;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (Input.touchCount >= 2) {
			return;
		}

		drag = false;
		transform.position = endPoint;
	}

//	public void SetButtonSize (int size = 1) { // 1:normal, 2:big
//		if (size == 1) {
//			GetComponent<RectTransform> ().sizeDelta = new Vector2 (280f, 280f);
//		} else {
//			GetComponent<RectTransform> ().sizeDelta = new Vector2 (364f, 364f);
//		}
//	}

}