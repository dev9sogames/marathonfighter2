﻿using UnityEngine;
using System.Collections;

public class SetButtonPositionJuuji : MonoBehaviour {
	public int type; // 1:top, 2:bottom, 3:right, 4:left

	float x;
	float y;

	float sizeX;
	float sizeY;

	// Use this for initialization
	void Start () {
		if (AllGameDefinition.gameMode == 4) { // marathon mode
			this.gameObject.SetActive (false);

			return;
		}

		/*
		PlayerPrefs.DeleteKey ("JuujiButtonX");
		PlayerPrefs.DeleteKey ("JuujiButtonY");
		*/

		int buttonScale = PlayerPrefs.GetInt ("ButtonScale", 1); // 1:normal, 2:big

		// base button size : 130, 50

		sizeX = PlayerPrefs.GetFloat ("JuujiButtonSizeX", 280f);
		sizeY = PlayerPrefs.GetFloat ("JuujiButtonSizeY", 280f);

		switch (type) {
		case 1: // top
			x = PlayerPrefs.GetFloat ("JuujiButtonX", -276);
			y = PlayerPrefs.GetFloat ("JuujiButtonY", -17);

			GetComponent<RectTransform> ().sizeDelta = new Vector2 (50f * (sizeX / 280f), (130f * (sizeY / 280f)));
			transform.localPosition = new Vector3 (x - 3, y + (98 * (sizeY / 280f) / 1.13f));
//			Debug.Log ("action " + x + " : " + y);
//			transform.localPosition = new Vector3 (x * 0.938f, y * 0.16f, 0);
//			if (buttonScale == 1) {
//				GetComponent<RectTransform>().sizeDelta = new Vector2(55f, 110f);
//				transform.localPosition = new Vector3 (x - 3, y + 83, 0);
//			} else {
//				GetComponent<RectTransform>().sizeDelta = new Vector2(71.5f, 143f);
//				transform.localPosition = new Vector3 (x - 3, y + 113, 0);
//			}
			break;
		case 2: // bottom
			x = PlayerPrefs.GetFloat ("JuujiButtonX", -276);
			y = PlayerPrefs.GetFloat ("JuujiButtonY", -170);

			GetComponent<RectTransform> ().sizeDelta = new Vector2 (50f * (sizeX / 280f), (130f * (sizeY / 280f)));
			transform.localPosition = new Vector3 (x - 3, y - (112 * (sizeY / 280f) / 1.13f));

//			Debug.Log ("action " + x + " : " + y);
//			transform.localPosition = new Vector3 (x * 0.938f, y * 1.63f, 0);
//			if (buttonScale == 1) {
//				GetComponent<RectTransform>().sizeDelta = new Vector2(55f, 110f);
//				transform.localPosition = new Vector3 (x - 3, y - 97, 0);
//			} else {
//				GetComponent<RectTransform>().sizeDelta = new Vector2(71.5f, 143f);
//				transform.localPosition = new Vector3 (x - 3, y - 127, 0);
//			}
			break;
		case 3: // right
			x = PlayerPrefs.GetFloat ("JuujiButtonX", -199);
			y = PlayerPrefs.GetFloat ("JuujiButtonY", -93);

			GetComponent<RectTransform> ().sizeDelta = new Vector2 (130f * (sizeX / 280f), (50 * (sizeY / 280f)));
			transform.localPosition = new Vector3 (x + (102 * (sizeX / 280f) / 1.13f), y - 7);

//			Debug.Log ("action " + x + " : " + y);
//			transform.localPosition = new Vector3 (x * 0.677f, y * 0.89f, 0);
//			if (buttonScale == 1) {
//				GetComponent<RectTransform>().sizeDelta = new Vector2(110f, 55f);
//				transform.localPosition = new Vector3 (x + 87, y - 7, 0);
//			} else {
//				GetComponent<RectTransform>().sizeDelta = new Vector2(143f, 71.5f);
//				transform.localPosition = new Vector3 (x + 117, y - 7, 0);
//			}
			break;
		case 4: // left
			x = PlayerPrefs.GetFloat ("JuujiButtonX", -352);
			y = PlayerPrefs.GetFloat ("JuujiButtonY", -93);

			GetComponent<RectTransform> ().sizeDelta = new Vector2 (130f * (sizeX / 280f), (50 * (sizeY / 280f)));
			transform.localPosition = new Vector3 (x - (108 * (sizeX / 280f) / 1.13f), y - 7);

//			Debug.Log ("action " + x + " : " + y);
//			transform.localPosition = new Vector3 (x * 1.197f, y * 0.89f, 0);
//			if (buttonScale == 1) {
//				GetComponent<RectTransform>().sizeDelta = new Vector2(110f, 55f);
//				transform.localPosition = new Vector3 (x - 93, y - 7, 0);
//			} else {
//				GetComponent<RectTransform>().sizeDelta = new Vector2(143f, 71.5f);
//				transform.localPosition = new Vector3 (x - 123, y - 7, 0);
//			}
			break;
		default:
			break;
		}
	}
}
