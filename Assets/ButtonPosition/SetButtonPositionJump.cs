﻿using UnityEngine;
using System.Collections;

public class SetButtonPositionJump : MonoBehaviour {
	float x;
	float y;

	float sizeX;
	float sizeY;

	// Use this for initialization
	void Start () {
		x = PlayerPrefs.GetFloat ("JumpButtonX", 345);
		y = PlayerPrefs.GetFloat ("JumpButtonY", -144);
//		Debug.Log ("run " + x + " : " + y);

		sizeX = PlayerPrefs.GetFloat ("JumpButtonSizeX", 152f);
		sizeY = PlayerPrefs.GetFloat ("JumpButtonSizeY", 149f);
		Debug.Log (sizeX);
		Debug.Log (sizeY);
		GetComponent<RectTransform> ().sizeDelta = new Vector2 (sizeX, sizeY);
		transform.localPosition = new Vector3 (x, y, 0);
	}
}
