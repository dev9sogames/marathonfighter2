﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonTitleVolumeAdujst : MonoBehaviour {
	public UnityEngine.Audio.AudioMixer mixer;

	float bgmValue;
	float seValue;

	// Use this for initialization
	void Start () {
		bgmValue = PlayerPrefs.GetFloat ("bgmVolume", 0);
		seValue = PlayerPrefs.GetFloat ("seVolume", 0);
//		Debug.Log (bgmValue);
//		Debug.Log (seValue);
		mixer.SetFloat("BgmVolume", Mathf.Lerp(-80, 10, bgmValue));
		mixer.SetFloat("SeVolume", Mathf.Lerp(-80, 10, seValue));

		GetComponent<AudioSource> ().Play ();
	}
}
