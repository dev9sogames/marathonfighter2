﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TrainingController : MonoBehaviour {
	public Canvas TrainingCanvas;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ClickBossBattlt (int difficulty) {
		// 1:easy, 2:normal, 3:hard, 4:hell

		Destroy (TrainingCanvas.gameObject);

		AllGameDefinition.gameMode = 2;
		AllGameDefinition.groundDifficulty = 2;
		SceneManager.LoadScene("Loading");
	}

	public void ClickTrainingBackButton () {
		Destroy (TrainingCanvas.gameObject);

		Vector3 position = new Vector3 (960, 540, 0);
		GameObject prefab = (GameObject)Resources.Load("DifficultyMainCanvas");
		Instantiate (prefab, position, transform.rotation);
	}
}
