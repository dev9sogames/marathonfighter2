﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingTitle : MonoBehaviour {
	public Text loadingText;
	private AsyncOperation async;

	string loadingstr;

	// Use this for initialization
	void Start () {
		loadingstr = "Now loading .";
		StartCoroutine ("LoadScene");
	}

	IEnumerator LoadScene () {
		//loading
//		Debug.Log ("AsyncLoad Start");
		// 非同期で読み込む
		async = SceneManager.LoadSceneAsync("Difficulty");
		async.allowSceneActivation= false;

		// なぜか90パーで止まるのでとりあえず・・・
		while(async.progress < 0.9f) {
			yield return new WaitForSeconds(1);
//			Debug.Log ("async.progress : " + async.progress);
			loadingstr += ".";
			loadingText.text = loadingstr;
		}
		//		loadingText.text = "100 %";
		//		loadingSlider.value = 1;

		yield return new WaitForSeconds(0.0f);
		async.allowSceneActivation = true;
	}
}
