﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class debugMenu : MonoBehaviour {
	public Canvas MainCanvas;
	public Canvas Tutorialcanvas;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void DeletePrefs () {
		PlayerPrefs.DeleteAll();
	}

	public void GetItem () {
		PlayerPrefs.SetInt ("ItemCount", 1);
		PlayerPrefs.Save ();
	}

	public void DisplayRanking () {
		Vector3 position = new Vector3 (203, 114, 0);
		GameObject prefab = (GameObject)Resources.Load("RankingCanvas");
		Instantiate (prefab, position, transform.rotation);

	}
	public void DisplayOptionDialog () {
		Vector3 position = new Vector3 (225, 126.5f, 0);
		GameObject prefab = (GameObject)Resources.Load("DialogCanvas");
		Instantiate (prefab, position, transform.rotation);

	}

	public void LoadHammer () {
		SceneManager.LoadScene ("onlineBattleSumo");
	}

	public void RetryGame () {
		AllGameDefinition.groundDifficulty = 1;
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void GoTitle () {
		SceneManager.LoadScene ("Title");
	}

	public void GoDifficulty () {
		SceneManager.LoadScene ("Difficulty");
	}

	public void TechniqueCanvas () {
		MainCanvas.gameObject.SetActive(false);
		Tutorialcanvas.gameObject.SetActive (true);
	}
}
