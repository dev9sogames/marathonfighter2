﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GachaTrigger : MonoBehaviour {
	bool isGacha;

	void OnTriggerEnter2D (Collider2D other) {
		if (!isGacha) {
			other.GetComponent<MoveGachaHand> ().HitTrigger ();
			
			GameObject eggPrefab = ResourceCache.Load ("egg");
			GameObject egg = Instantiate (eggPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			float red = UnityEngine.Random.Range (0f, 255f);
			float green = UnityEngine.Random.Range (0f, 255f);
			float blue = UnityEngine.Random.Range (0f, 255f);
			egg.GetComponent<SpriteRenderer> ().color = new Color (red/255, green/255, blue/255);

			float total = red + green + blue;

			isGacha = true;
		}
	}

	public void InitializeGachaFlag () {
		isGacha = false;
	}
}
