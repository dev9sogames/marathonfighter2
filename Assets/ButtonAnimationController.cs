﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonAnimationController : MonoBehaviour {
//	int height;
//
//	int MovebyYpos = 900;
//	int moveYpos;
//	public float goalX;
	Vector3 defaultPos;
	public float pos;

	public int moveType; // 1:moveX, 2:moveY

	// Use this for initialization
	void Start () {
		Debug.Log ("DoTween start");
//		DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
	}

	public void MoveStart () {
		defaultPos = this.gameObject.GetComponent<RectTransform> ().localPosition;

		Debug.Log(moveType);
		Debug.Log(pos);
		if (moveType == 1) {
			this.gameObject.GetComponent<RectTransform> ().DOLocalMoveX (pos, 2f);
		} else if (moveType == 2) {
			this.gameObject.GetComponent<RectTransform> ().DOLocalMoveY (pos, 2f);
		}
	}

	public void SetDefalutPosition () {
		Debug.Log ("SetDefalutPosition");
		this.gameObject.GetComponent<RectTransform> ().localPosition = defaultPos;
	}
}
